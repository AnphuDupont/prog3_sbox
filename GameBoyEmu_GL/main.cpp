#include "stdafx.h"
#include <fstream>
#include <iostream>
#include <vector>

// GB CPU
#include "GameBoyZ80.h"
//#include "SoundManager.h"
#include "GBSound.h"  
#include "Input.h"

#ifndef __APPLE__
// GLAD & GLFW
#include <glad/glad.h>
#include <GLFW/glfw3.h>
// Function prototypes
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode);
void OnDragAndDrop(GLFWwindow* w, int i, const char** f);
void ParseNvInput(NvGamepad::State &);

#else
// GLFW Legacy
#include <GL/glfw.h>
// Function prototypes
void key_callback(int key, int action);
void OnDragAndDrop(int i, const char** f);
#endif

// Shader sources 
const GLchar* vertexSource =
"#version 110\n"
"attribute vec2 position;"
"attribute vec2 uv;"
"varying vec2 tc;"
"void main() {"
"	tc = uv;"
"   gl_Position = vec4(position, 0.0, 1.0);"
"}";

const GLchar* fragmentSource =
"#version 110\n"
"varying vec2 tc;"
"uniform sampler2D tex;"
"void main() {"
"   gl_FragColor = texture2D(tex, tc);"
"}";

void UpdateKeys();
GameBoyZ80 * gbcpu;

bool CheckGLFWShader(GLuint item, DWORD type)
{
	GLint itemOk = GL_FALSE;
	glGetShaderiv(item, GL_COMPILE_STATUS, &itemOk);
	if (itemOk != GL_TRUE)
	{
		printf("Error on item %d!\n", item);
		glfwTerminate();
		return false;
	}
	return true;
}
#define GLSLShaderCheck(i, t) if(!CheckGLFWShader(i, t)) return -1;

bool _pressedKeys[8];
namespace Key
{
	enum Key
	{
		Up = 0, Down, Left, Right, A, B, Start, Select
	};
}

// The MAIN function, from here we start the application and run the game loop
int main()
{
	GamePad gPad = GamePad();
	gbcpu = GameBoyZ80::GetGameBoyZ80();

	//const char * startRom = "roms\\Legend of Zelda, The - Link's Awakening (USA, Europe).gb";
	//const char * startRom = "roms\\Neon Genesis Evangelion - Rei Demo 1 (PD).gb";
	//const char * startRom = "roms\\Zork I - The Great Underground Empire (PD) [M].gb";
	//const char * startRom = "roms\\cpu_instrs\\individual\\01-special.gb";
	//const char * startRom = "roms\\cpu_instrs\\individual\\02-interrupts.gb";
	//const char * startRom = "roms\\cpu_instrs\\cpu_instrs.gb";
	//const char * startRom = "roms\\Kirby's Dream Land (U) [b1].gb";
	//const char * startRom = "roms\\Megami Tensei Gaiden - Last Bible (Japan).gb";
	//const char * startRom = "roms\\puyopuyo.gb";
	const char * startRom = "roms\\Pocket Puyo Puyo Tsuu (Japan).gb";
	//const char * startRom = "roms\\Final Fantasy Legend III (USA).gb";
	//const char * startRom = "roms\\Tamagotchi (USA, Europe).gb";
	//const char * startRom = "roms\\Battletoads.gb";
	//const char * startRom = "roms\\Harvest Moon GB (U) [S][!].gb";
	//const char * startRom = "roms\\Pokemonred.gb";
	//OnDragAndDrop(nullptr, 0, &startRom);

	// Init GLFW
	glfwInit();

#ifdef _WIN32
	// Set all the required options for GLFW
	//glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
	//glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
	//glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

	// Create a GLFWwindow object that we can use for GLFW's functions
	GLFWwindow* window = glfwCreateWindow(WINDOW_WIDTH, WINDOW_HEIGHT, gbcpu->GetMemory()->m_GameTitle, NULL, NULL);
	glfwMakeContextCurrent(window);
	if (window == NULL)
	{
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return -1;
	}

	// Set the required callback functions
	glfwSetKeyCallback(window, key_callback);
	glfwSetDropCallback(window, OnDragAndDrop);

	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		std::cout << "Failed to initialize OpenGL context" << std::endl;
		return -1;
	}
#elif __APPLE__
	glfwOpenWindowHint(GLFW_WINDOW_NO_RESIZE, GL_TRUE);

	GLFWvidmode vm;
	glfwGetDesktopMode(&vm);
	if (!glfwOpenWindow(WINDOW_WIDTH, WINDOW_HEIGHT, vm.RedBits, vm.GreenBits, vm.BlueBits, 0, 0, 0, GLFW_WINDOW))
	{
		printf("Failed to create GLFW window !");
		glfwTerminate();
		return 0;
	}

	glfwSetWindowTitle(gbcpu->GetMemory()->m_GameTitle);
	glfwSetKeyCallback(key_callback);
#endif
	// Define the viewport dimensions
	glViewport(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);

	// Create Vertex Array Object
	GLuint vao;
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	// Create a Vertex Buffer Object and copy the vertex data to it
	GLuint vbo;
	glGenBuffers(1, &vbo);

	GLfloat vertices[] = {
		-1.0f, 1.0f, 0.0f, 0.0f, // Top-left
		1.0f, 1.0f, 1.0f, 0.0f, // Top-right
		1.0f, -1.0f, 1.0f, 1.0f, // Bottom-right
		-1.0f, -1.0f, 0.0f, 1.0f  // Bottom-left
	};

	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	// Create an element array
	GLuint ebo;
	glGenBuffers(1, &ebo);

	GLuint elements[] = {
		0, 1, 2,
		2, 3, 0
	};

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(elements), elements, GL_STATIC_DRAW);

	// Create and compile the vertex shader
	GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertexShader, 1, &vertexSource, NULL);
	glCompileShader(vertexShader);
	GLSLShaderCheck(vertexShader, GL_COMPILE_STATUS);

	// Create and compile the fragment shader
	GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragmentShader, 1, &fragmentSource, NULL);
	glCompileShader(fragmentShader);
	GLSLShaderCheck(fragmentShader, GL_COMPILE_STATUS);

	// Link the vertex and fragment shader into a shader program
	GLuint shaderProgram = glCreateProgram();
	glAttachShader(shaderProgram, vertexShader);
	glAttachShader(shaderProgram, fragmentShader);
	glBindAttribLocation(vertexShader, 0, "position");
	//glBindFragDataLocation(shaderProgram, 0, "outColor");
	glLinkProgram(shaderProgram);
	glUseProgram(shaderProgram);

	// Specify the layout of the vertex data
	GLint posAttrib = glGetAttribLocation(shaderProgram, "position");
	glEnableVertexAttribArray(posAttrib);
	glVertexAttribPointer(posAttrib, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), 0);

	GLint uvAttrib = glGetAttribLocation(shaderProgram, "uv");
	glEnableVertexAttribArray(uvAttrib);
	glVertexAttribPointer(uvAttrib, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (void*)(2 * sizeof(GLfloat)));

	GLuint renderedTex = NULL;
	glGenTextures(1, &renderedTex);
	glBindTexture(GL_TEXTURE_2D, renderedTex);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	//glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, SCREEN_WIDTH, SCREEN_HEIGHT, 0, GL_RGB, GL_FLOAT, gbcpu->GetDisplayBuffer());

	// FPS Limit
	glfwSwapInterval(0);
	Color * displayBuffer = gbcpu->GetDisplayBuffer();
	NvGamepad::State padState;

	printf("You are using OpenGL version %s.\n", glGetString(GL_VERSION));

	bool toggleConnect = false;
	while (strcmp(Memory::m_GameTitle, "NONAME") == 0)
	{
		glfwWaitEvents();
		if (gPad.IsConnected() != toggleConnect)
		{
			toggleConnect = gPad.IsConnected();
			std::cout << (toggleConnect ? "Controller connected !" : "Controller disconnected !") << std::endl;
		}
	}

	// Game loop
	while (!glfwWindowShouldClose(window))
	{
		// Check if any events have been activated (key pressed, mouse moved etc.) and call corresponding response functions
		glfwPollEvents();

		while (!Graphics::DrawDone)
		{
			gbcpu->EmulateCycle();
			UpdateKeys();
		}
		Graphics::DrawDone = false;

		if (gPad.ReadState(padState))
			ParseNvInput(padState);

		//gbcpu->OutputDebug(false);
		//gbcpu->PaintScreen();

#ifdef _WIN32
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, SCREEN_WIDTH, SCREEN_HEIGHT, 0, GL_RGB, GL_FLOAT, displayBuffer);
#else
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, SCREEN_WIDTH, SCREEN_HEIGHT, 0, GL_RGBA, GL_UNSIGNED_INT_8_8_8_8, gbcpu->GetDisplayBuffer());
#endif

		gbcpu->GetSound()->EndFrame();

		// Clear the screen to black
		glClearColor(0, 0, 0, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT);

		// Draw a rectangle from the 2 triangles using 6 indices
		glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

		// Swap the screen buffers
#ifdef __APPLE__
		glfwSwapBuffers();
#elif _WIN32
		glfwSwapBuffers(window);
#endif
	}

	byte cart = gbcpu->GetMemory()->m_CartridgeType;
	switch (cart)
	{
	case 0x3: SaveData::WriteSave(Memory::m_GameTitle, Memory::GetMemory()->m_SaveAddress, 32 * 1024);
		break;
	case 0x6: SaveData::WriteSave(Memory::m_GameTitle, Memory::GetMemory()->m_SaveAddress, 512);
		break;
	case 0x13: SaveData::WriteSave(Memory::m_GameTitle, Memory::GetMemory()->m_SaveAddress, 32 * 1024);
		break;
	case 0x1B: SaveData::WriteSave(Memory::m_GameTitle, Memory::GetMemory()->m_SaveAddress, 128 * 1024);
		break;
	default:
		break;
	}

	delete gbcpu;
	//SoundManager::DestroyInstance();

	// Terminates GLFW, clearing any resources allocated by GLFW.
	glfwTerminate();
	return 0;
}

void UpdateKeys()
{
	if (_pressedKeys[Key::Up]) // UP
	{
		if (!BIT4(gbcpu->GetMemory()->m_Memory[cJoypad]))
		{
			gbcpu->GetMemory()->UnsetBit(cJoypad, B2);
			gbcpu->GetMemory()->SetBit(cINTFlag, B4);
		}
	}
	else if (!BIT4(gbcpu->GetMemory()->m_Memory[cJoypad]))
		gbcpu->GetMemory()->SetBit(cJoypad, B2);

	if (_pressedKeys[Key::Down]) // DOWN
	{
		if (!BIT4(gbcpu->GetMemory()->m_Memory[cJoypad]))
		{
			gbcpu->GetMemory()->UnsetBit(cJoypad, B3);
			gbcpu->GetMemory()->SetBit(cINTFlag, B4);
		}
	}
	else if (!BIT4(gbcpu->GetMemory()->m_Memory[cJoypad]))
		gbcpu->GetMemory()->SetBit(cJoypad, B3);

	if (_pressedKeys[Key::Left]) // LEFT
	{
		if (!BIT4(gbcpu->GetMemory()->m_Memory[cJoypad]))
		{
			gbcpu->GetMemory()->UnsetBit(cJoypad, B1);
			gbcpu->GetMemory()->SetBit(cINTFlag, B4);
		}
	}
	else if (!BIT4(gbcpu->GetMemory()->m_Memory[cJoypad]))
		gbcpu->GetMemory()->SetBit(cJoypad, B1);

	if (_pressedKeys[Key::Right]) // Right
	{
		if (!BIT4(gbcpu->GetMemory()->m_Memory[cJoypad]))
		{
			gbcpu->GetMemory()->UnsetBit(cJoypad, B0);
			gbcpu->GetMemory()->SetBit(cINTFlag, B4);
		}
	}
	else if (!BIT4(gbcpu->GetMemory()->m_Memory[cJoypad]))
		gbcpu->GetMemory()->SetBit(cJoypad, B0);

	if (_pressedKeys[Key::A]) // A
	{
		if (!BIT5(gbcpu->GetMemory()->m_Memory[cJoypad]))
		{
			gbcpu->GetMemory()->UnsetBit(cJoypad, B0);
			gbcpu->GetMemory()->SetBit(cINTFlag, B4);
		}
	}
	else if (!BIT5(gbcpu->GetMemory()->m_Memory[cJoypad]))
		gbcpu->GetMemory()->SetBit(cJoypad, B0);

	if (_pressedKeys[Key::B]) // B
	{
		if (!BIT5(gbcpu->GetMemory()->m_Memory[cJoypad]))
		{
			gbcpu->GetMemory()->UnsetBit(cJoypad, B1);
			gbcpu->GetMemory()->SetBit(cINTFlag, B4);
		}
	}
	else if (!BIT5(gbcpu->GetMemory()->m_Memory[cJoypad]))
		gbcpu->GetMemory()->SetBit(cJoypad, B1);


	if (_pressedKeys[Key::Start]) // Start
	{
		if (!BIT5(gbcpu->GetMemory()->m_Memory[cJoypad]))
		{
			gbcpu->GetMemory()->UnsetBit(cJoypad, B3);
			gbcpu->GetMemory()->SetBit(cINTFlag, B4);
		}
	}
	else if (!BIT5(gbcpu->GetMemory()->m_Memory[cJoypad]))
		gbcpu->GetMemory()->SetBit(cJoypad, B3);

	if (_pressedKeys[Key::Select]) // Select
	{
		if (!BIT5(gbcpu->GetMemory()->m_Memory[cJoypad]))
		{
			gbcpu->GetMemory()->UnsetBit(cJoypad, B2);
			gbcpu->GetMemory()->SetBit(cINTFlag, B4);
		}
	}
	else if (!BIT5(gbcpu->GetMemory()->m_Memory[cJoypad]))
		gbcpu->GetMemory()->SetBit(cJoypad, B2);
}

#ifdef __APPLE__
// Is called whenever a key is pressed/released via GLFW
void key_callback(int key, int action)
{
	//std::cout << key << std::endl;
	if (key == GLFW_KEY_ESC && action == GLFW_PRESS)
		glfwCloseWindow();

	if (action == GLFW_PRESS)
		switch (key)
	{
		case GLFW_KEY_UP: _pressedKeys[Key::Up] = true; break;
		case GLFW_KEY_DOWN:_pressedKeys[Key::Down] = true; break;
		case GLFW_KEY_LEFT:_pressedKeys[Key::Left] = true; break;
		case GLFW_KEY_RIGHT:_pressedKeys[Key::Right] = true; break;
		case GLFW_KEY_ENTER:_pressedKeys[Key::Start] = true; break;
		case GLFW_KEY_LSHIFT:_pressedKeys[Key::Select] = true; break;
		case 'A' :_pressedKeys[Key::A] = true; break;
		case 'Z' :_pressedKeys[Key::B] = true; break;
	}
	else if (action == GLFW_RELEASE)
		switch (key)
	{
		case GLFW_KEY_UP: _pressedKeys[Key::Up] = false; break;
		case GLFW_KEY_DOWN:_pressedKeys[Key::Down] = false; break;
		case GLFW_KEY_LEFT:_pressedKeys[Key::Left] = false; break;
		case GLFW_KEY_RIGHT:_pressedKeys[Key::Right] = false; break;
		case GLFW_KEY_ENTER:_pressedKeys[Key::Start] = false; break;
		case GLFW_KEY_LSHIFT:_pressedKeys[Key::Select] = false; break;
		case 'A':_pressedKeys[Key::A] = false; break;
		case 'Z':_pressedKeys[Key::B] = false; break;
	}
}

void OnDragAndDrop(int i, const char** f)
{
	// Open program file
	std::ifstream ifs(*f, std::ios::binary | std::ios::in);
	std::string program((std::istreambuf_iterator<char>(ifs)),
		std::istreambuf_iterator<char>());
	gbcpu->Reset();
	gbcpu->LoadGame(program);
}
#else
// Is called whenever a key is pressed/released via GLFW
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode)
{
	//std::cout << key << std::endl;
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GL_TRUE);

	if (action == GLFW_PRESS)
		switch (key)
	{
		case GLFW_KEY_UP: _pressedKeys[Key::Up] = true; break;
		case GLFW_KEY_DOWN:_pressedKeys[Key::Down] = true; break;
		case GLFW_KEY_LEFT:_pressedKeys[Key::Left] = true; break;
		case GLFW_KEY_RIGHT:_pressedKeys[Key::Right] = true; break;
		case GLFW_KEY_ENTER:_pressedKeys[Key::Start] = true; break;
		case GLFW_KEY_LEFT_SHIFT:_pressedKeys[Key::Select] = true; break;
		case GLFW_KEY_A:_pressedKeys[Key::A] = true; break;
		case GLFW_KEY_B:_pressedKeys[Key::B] = true; break;
		case GLFW_KEY_KP_MULTIPLY: gbcpu->Reset(); break;
			//case GLFW_KEY_KP_ADD: EXECUTION_SPEED += 1000; printf("Execution speed: %d\n", EXECUTION_SPEED); break;
			//case GLFW_KEY_KP_SUBTRACT: EXECUTION_SPEED -= 1000; printf("Execution speed: %d\n", EXECUTION_SPEED); break;
	}
	else if (action == GLFW_RELEASE)
		switch (key)
	{
		case GLFW_KEY_UP: _pressedKeys[Key::Up] = false; break;
		case GLFW_KEY_DOWN:_pressedKeys[Key::Down] = false; break;
		case GLFW_KEY_LEFT:_pressedKeys[Key::Left] = false; break;
		case GLFW_KEY_RIGHT:_pressedKeys[Key::Right] = false; break;
		case GLFW_KEY_ENTER:_pressedKeys[Key::Start] = false; break;
		case GLFW_KEY_LEFT_SHIFT:_pressedKeys[Key::Select] = false; break;
		case GLFW_KEY_A:_pressedKeys[Key::A] = false; break;
		case GLFW_KEY_B:_pressedKeys[Key::B] = false; break;
	}
}

void OnDragAndDrop(GLFWwindow* w, int i, const char** f)
{
	//// Open program file
	//std::ifstream ifs(*f, std::ios::binary | std::ios::in);
	//std::string program((std::istreambuf_iterator<char>(ifs)),
	//	std::istreambuf_iterator<char>());

	//return;
	std::ifstream inFile;
	inFile.open(*f, std::ios::in | std::ios::binary);

	if (inFile.is_open())
	{
		inFile.read((char *)Memory::GetMemory()->m_ROM, 8388608);
		inFile.close();
	}

	system("cls");
	gbcpu->Reset();
	gbcpu->LoadGame((char *)Memory::GetMemory()->m_ROM);
	glfwSetWindowTitle(w, gbcpu->GetMemory()->m_GameTitle);
	std::cout << "Cartridge Type : 0x" << std::hex << Memory::GetMemory()->m_CartridgeType << std::endl;
}
#endif

inline void ParseNvInput(NvGamepad::State & s)
{
	_pressedKeys[Key::Up] = MASK(s.mButtons, NvGamepad::BUTTON_DPAD_UP) > 0;
	_pressedKeys[Key::Down] = MASK(s.mButtons, NvGamepad::BUTTON_DPAD_DOWN) > 0;
	_pressedKeys[Key::Left] = MASK(s.mButtons, NvGamepad::BUTTON_DPAD_LEFT) > 0;
	_pressedKeys[Key::Right] = MASK(s.mButtons, NvGamepad::BUTTON_DPAD_RIGHT) > 0;
	_pressedKeys[Key::A] = MASK(s.mButtons, NvGamepad::BUTTON_A) > 0;
	_pressedKeys[Key::B] = MASK(s.mButtons, NvGamepad::BUTTON_B) > 0;
	_pressedKeys[Key::Start] = MASK(s.mButtons, NvGamepad::BUTTON_START) > 0;
	_pressedKeys[Key::Select] = MASK(s.mButtons, NvGamepad::BUTTON_BACK) > 0;
}