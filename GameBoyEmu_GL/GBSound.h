#pragma once
#include "defines.h"
//#include "SoundManager.h"

class Basic_Gb_Apu;
//class SoundManager;
class SoundSDL;

class GBSound
{
public:
	static GBSound * GetGBSound();
	~GBSound();

	bool SetSampleRate(int srate);
	bool Start();
	bool Stop();
	bool IsEnabled() { return m_IsEnabled; }
	void SetEnabled(bool e);
	void EndFrame();

	void WriteByte(UI16 addr, byte val);
	byte ReadByte(UI16 addr);

private:
	GBSound();
	static GBSound * pSound;

	Basic_Gb_Apu * m_Apu;
	SoundSDL * m_SoundMan;
	//SoundManager * m_SoundMan;
	bool m_IsEnabled;
	int m_SampleRate;

	bool CheckError(const char *);
};

