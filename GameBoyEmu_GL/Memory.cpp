#include "stdafx.h"
#include "Memory.h"
#include "Graphics.h"
#include "GameBoyZ80.h"
#include "GBSound.h"

Memory * Memory::m_pMemory = 0;
char Memory::m_GameTitle[16] = "NONAME\0        ";
unsigned char Memory::m_BIOS[256] =
{
	0x31, 0xFE, 0xFF, 0xAF, 0x21, 0xFF, 0x9F, 0x32, 0xCB, 0x7C, 0x20, 0xFB, 0x21, 0x26, 0xFF, 0x0E,
	0x11, 0x3E, 0x80, 0x32, 0xE2, 0x0C, 0x3E, 0xF3, 0xE2, 0x32, 0x3E, 0x77, 0x77, 0x3E, 0xFC, 0xE0,
	0x47, 0x11, 0x04, 0x01, 0x21, 0x10, 0x80, 0x1A, 0xCD, 0x95, 0x00, 0xCD, 0x96, 0x00, 0x13, 0x7B,
	0xFE, 0x34, 0x20, 0xF3, 0x11, 0xD8, 0x00, 0x06, 0x08, 0x1A, 0x13, 0x22, 0x23, 0x05, 0x20, 0xF9,
	0x3E, 0x19, 0xEA, 0x10, 0x99, 0x21, 0x2F, 0x99, 0x0E, 0x0C, 0x3D, 0x28, 0x08, 0x32, 0x0D, 0x20,
	0xF9, 0x2E, 0x0F, 0x18, 0xF3, 0x67, 0x3E, 0x64, 0x57, 0xE0, 0x42, 0x3E, 0x91, 0xE0, 0x40, 0x04,
	0x1E, 0x02, 0x0E, 0x0C, 0xF0, 0x44, 0xFE, 0x90, 0x20, 0xFA, 0x0D, 0x20, 0xF7, 0x1D, 0x20, 0xF2,
	0x0E, 0x13, 0x24, 0x7C, 0x1E, 0x83, 0xFE, 0x62, 0x28, 0x06, 0x1E, 0xC1, 0xFE, 0x64, 0x20, 0x06,
	0x7B, 0xE2, 0x0C, 0x3E, 0x87, 0xF2, 0xF0, 0x42, 0x90, 0xE0, 0x42, 0x15, 0x20, 0xD2, 0x05, 0x20,
	0x4F, 0x16, 0x20, 0x18, 0xCB, 0x4F, 0x06, 0x04, 0xC5, 0xCB, 0x11, 0x17, 0xC1, 0xCB, 0x11, 0x17,
	0x05, 0x20, 0xF5, 0x22, 0x23, 0x22, 0x23, 0xC9, 0xCE, 0xED, 0x66, 0x66, 0xCC, 0x0D, 0x00, 0x0B,
	0x03, 0x73, 0x00, 0x83, 0x00, 0x0C, 0x00, 0x0D, 0x00, 0x08, 0x11, 0x1F, 0x88, 0x89, 0x00, 0x0E,
	0xDC, 0xCC, 0x6E, 0xE6, 0xDD, 0xDD, 0xD9, 0x99, 0xBB, 0xBB, 0x67, 0x63, 0x6E, 0x0E, 0xEC, 0xCC,
	0xDD, 0xDC, 0x99, 0x9F, 0xBB, 0xB9, 0x33, 0x3E, 0x3c, 0x42, 0xB9, 0xA5, 0xB9, 0xA5, 0x42, 0x4C,
	0x21, 0x04, 0x01, 0x11, 0xA8, 0x00, 0x1A, 0x13, 0xBE, 0x20, 0xFE, 0x23, 0x7D, 0xFE, 0x34, 0x20,
	0xF5, 0x06, 0x19, 0x78, 0x86, 0x23, 0x05, 0x20, 0xFB, 0x86, 0x20, 0xFE, 0x3E, 0x01, 0xE0, 0x50
};

const UI16 Memory::m_TimerControlSpeed[] = { 1024, 16, 64, 256 };

Memory::Memory() :
m_TimerDivCounter(0),
m_WriteSaveToDisk(false),
m_SaveAddress(0),
m_SaveSize(0)
{
	m_pMemory = this;
	m_pCPU = GameBoyZ80::GetGameBoyZ80();
	m_pGPU = Graphics::GetGraphics();
	m_pSound = GBSound::GetGBSound();
}

Memory::~Memory()
{
	//delete[] m_ROM;
	//delete[] m_BIOS;
	//delete[] m_Memory;
	//delete[] m_GameTitle;
}

Memory * Memory::GetMemory()
{
	if (m_pMemory == 0)
		m_pMemory = new Memory();
	return m_pMemory;
}

void Memory::Reset()
{
	m_Model = &GameBoyZ80::GetGameBoyZ80()->model;
	m_TAC = m_TIMA = m_TMA = 0;

	for (int i = 0; i < 0xFFFF; ++i) m_Memory[i] = (rand() & 0x100); // Fill with poop
	//for (int i = 0; i < 0xFFFF; ++i) m_Memory[i] = 0;
	for (int i = 0; i < 4; i++) m_MBC1[i] = 0;
	for (int i = 0; i < 4; i++) m_MBC3[i] = 0;
	for (int i = 0; i < 2048 * 1024; i++) m_ExternalRAM[i] = 0;

	m_RomOffset = 0x4000;
	m_RamOffset = 0x0000;
	m_MBC1[MBCSet::ROMbank] = 1;

	for (int i = 0; i < 16; i++) m_MBC3_RTC[i] = 0;
	m_MBC3_RTC_Enabled = m_MBC3_RTC_Latch = false;

	m_InBios = true;

	// Init keys (active low), all not pressed
	m_Memory[cJoypad] = 0xFF;
	m_KEYS[0] = m_KEYS[1] = 0x0F;
	m_VramBank = 0;
}

byte Memory::ReadByte(unsigned short addr)
{
	//const UI16 readLocation = addr & 0xF000;
	//if (readLocation < 0x1000)
	//{
	//	if (m_InBios)
	//	{
	//		if (addr < 0x0100) return m_BIOS[addr];
	//		else if (m_pCPU->GetPC() == 0x0100) m_InBios = false;
	//	}
	//	else return m_ROM[addr];
	//}
	//if (readLocation < 0x4000) return m_ROM[addr];
	//if (readLocation < 0x8000) return m_ROM[m_RomOffset + (addr & 0x3FFF)];
	//if (readLocation < 0xA000) return m_Memory[addr];
	//if (readLocation < 0xC000)
	//{
	//	if (m_CartridgeType == 5 || m_CartridgeType == 6) // MBC2
	//	{
	//		if (addr > 0xA1FF) return 0;
	//		return m_ExternalRAM[(addr & 0x1FF) + m_RamOffset] & 0xF;
	//	}
	//	if (m_MBC3[MBC3Set::RTCMapped]) return m_MBC3_RTC[m_MBC1[MBCSet::RAMbank]];
	//	if (m_MBC1[MBCSet::Mode]) return m_ExternalRAM[(addr & 0x1FFF) + 0x2000 * m_MBC1[MBCSet::RAMbank]];
	//	return m_ExternalRAM[(addr & 0x1FFF)];
	//}
	//if (readLocation < 0xE000) return m_Memory[addr];
	//if (readLocation < 0xF000) return m_Memory[addr - 0x2000];
	//else
	//{
	//	if (addr < 0xFE00) return m_Memory[addr - 0x2000]; // WRAM
	//	if (addr < 0xFF00) return (addr < 0xFEA0) ? m_Memory[addr] : 0; // GOAM
	//	if (addr == cJoypad)
	//	{
	//		if (BIT4(m_Memory[cJoypad])) return m_KEYS[0];
	//		if (BIT5(m_Memory[cJoypad])) return m_KEYS[1];
	//	}
	//	if (addr == cINTEnable) return m_IE;
	//	if (addr == cINTFlag) return m_IF | 0xE0;
	//	if (addr == cTimerTAC) return m_TAC;
	//	if (addr == cTimerTIMA) return m_TIMA;
	//	if (addr == cTimerTMA) return m_TMA;
	//	if (addr > 0xFF7F) return m_Memory[addr]; // ZRAM
	//	if (addr >= 0xFF40) return m_pGPU->ReadByte(addr);
	//	if (addr < 0xFF40 && addr >= 0xFF10) return m_pSound->ReadByte(addr);
	//	return m_Memory[addr];
	//}

	switch (addr & 0xF000)
	{
	case 0x000:
		if (m_InBios)
		{
			if (addr < 0x0100) return m_BIOS[addr];
			else if (m_pCPU->GetPC() == 0x0100) m_InBios = false;
		}
		else return m_ROM[addr];
	case 0x1000:
	case 0x2000:
	case 0x3000:
		return m_ROM[addr];
	case 0x4000:
	case 0x5000:
	case 0x6000:
	case 0x7000:
		return m_ROM[m_RomOffset + (addr & 0x3FFF)];
	case 0x8000:
	case 0x9000:
		if (*m_Model == Device::CGB && m_Memory[0xFF4F] != 0)
			return m_CGBVram[addr & 0x1FFF];
		return m_Memory[addr];
	case 0xA000:
	case 0xB000:
		if (m_CartridgeType == 5 || m_CartridgeType == 6) // MBC2
		{
			if (addr > 0xA1FF) return 0;
			return m_ExternalRAM[(addr & 0x1FF) + m_RamOffset] & 0xF;
		}
		if (m_MBC3[MBC3Set::RTCMapped]) return m_MBC3_RTC[m_MBC1[MBCSet::RAMbank]];
		if (m_MBC1[MBCSet::Mode]) return m_ExternalRAM[(addr & 0x1FFF) + 0x2000 * m_MBC1[MBCSet::RAMbank]];
		return m_ExternalRAM[(addr & 0x1FFF)];
	case 0xC000:
	case 0xD000:
		if (*m_Model == Device::CGB && addr >= 0xD000 && m_WramBank > 0) 
			return m_CGBWram[(m_WramBank * 0x1000) + (addr & 0x0FFF)];
		return m_Memory[addr];
	case 0xE000:
		return m_Memory[addr - 0x2000];
	default:
		if (addr < 0xFE00) return m_Memory[addr - 0x2000]; // WRAM
		if (addr < 0xFF00) return (addr < 0xFEA0) ? m_Memory[addr] : 0xFF; // GOAM
		if (addr == cJoypad)
		{
			if (BIT4(m_Memory[cJoypad])) return m_KEYS[0];
			if (BIT5(m_Memory[cJoypad])) return m_KEYS[1];
		}
		if (addr == cINTEnable) return m_IE;
		if (addr == cINTFlag) return m_IF | 0xE0;
		if (addr == cTimerTAC) return m_TAC;
		if (addr == cTimerTIMA) return m_TIMA;
		if (addr == cTimerTMA) return m_TMA;
		if (addr > 0xFF7F) return m_Memory[addr]; // ZRAM
		if (addr >= 0xFF40) return m_pGPU->ReadByte(addr);
		if (addr < 0xFF40 && addr >= 0xFF10) return m_pSound->ReadByte(addr);
		return m_Memory[addr];
	}

	return 0;
}

unsigned short Memory::ReadWord(unsigned short addr)
{
	return (ReadByte(addr) + (ReadByte(addr + 1) << 8));
}

void Memory::WriteByte(unsigned short addr, unsigned char val)
{
	//if (m_CartridgeType == 0 && addr < 0x8000) { return; }
	//printf("%04X %02X\t", addr, val);

	switch (addr & 0xF000)
	{
	case 0x0000: // BIOS and ROM0
		if (m_InBios && addr < 0x100) return;
	case 0x1000: // ROM0
		switch (m_CartridgeType)
		{
		case 0x12:
		case 0x13:
			m_MBC3[MBC3Set::RTCEnable] = ((val & 0x0F) == 0x0A) ? 1 : 0;
		case 0x19:
		case 0x1A:
		case 0x1B:
		case 3:
		case 2:
		case 1:
			m_MBC1[MBCSet::RAMon] = ((val & 0x0F) == 0x0A) ? 1 : 0;
			break;
		case 5:
		case 6:
			if (addr & 0x100) m_MBC1[MBCSet::RAMon] = val;
			break;
		}
		break;
	case 0x2000:
		switch (m_CartridgeType)
		{
		case 0x19:
		case 0x1A:
		case 0x1B:
			m_MBC1[MBCSet::ROMbank] = (m_MBC1[MBCSet::ROMbank] & 0x100) | val;
			m_RomOffset = m_MBC1[MBCSet::ROMbank] * 0x4000;
			break;
		case 0x12:
		case 0x13:
			//m_Memory[addr] = val & 0x7F;
			if (val == 0) val = 1;
			m_MBC1[MBCSet::ROMbank] = val & 0x7F;
			m_RomOffset = m_MBC1[MBCSet::ROMbank] * 0x4000;
			break;
		case 3:
		case 2:
		case 1:
			WriteByte(0x3000, val); // Same
			break;
		case 5:
		case 6:
			WriteByte(addr + 0x1000, val); // Same
			break;
		}
		break;
	case 0x3000:
		switch (m_CartridgeType)
		{
		case 0x19:
		case 0x1A:
		case 0x1B:
			if (val) SET_BIT(m_MBC1[MBCSet::ROMbank], 0x100);
			else RESET_BIT(m_MBC1[MBCSet::ROMbank], 0x100);
			m_RomOffset = m_MBC1[MBCSet::ROMbank] * 0x4000;
			return;
			break;
		case 0x12:
		case 0x13: // Same as 0x2000
			//m_Memory[addr] = val & 0x7F;
			if (val == 0) val = 1;
			m_MBC1[MBCSet::ROMbank] = val & 0x7F;
			m_RomOffset = m_MBC1[MBCSet::ROMbank] * 0x4000;
			break;
		case 3:
		case 2:
		case 1:
			val &= 0x1F;
			if (val == 0) val = 1;
			m_MBC1[MBCSet::ROMbank] = (m_MBC1[MBCSet::ROMbank] & 0x60) | val;
			m_RomOffset = m_MBC1[MBCSet::ROMbank] * 0x4000;
			break;
		case 5:
		case 6:
			if (val == 0) val = 1;
			m_MBC1[MBCSet::ROMbank] = val & 0x0F;
			m_RomOffset = m_MBC1[MBCSet::ROMbank] * 0x4000;
			break;
		}
#ifdef _WIN32
		//printf("MBC1: %x Rom offset: %x\n", m_MBC1[MBCSet::ROMbank], m_RomOffset);
#endif
		break;
	case 0x4000: // ROM1 (unbanked)
	case 0x5000:
		switch (m_CartridgeType)
		{
		case 0x19:
		case 0x1A:
		case 0x1B:
			m_MBC1[MBCSet::RAMbank] = val & 0xF;
			m_RamOffset = m_MBC1[MBCSet::RAMbank] * 0x2000;
			break;
		case 0x12:
		case 0x13:
			if (val >= 0x08 && val <= 0x0C) { m_MBC3[MBC3Set::RTCMapped] = true; break; }
			m_MBC3[MBC3Set::RTCMapped] = false;
			if (m_MBC1[MBCSet::Mode]) m_RamOffset = m_MBC1[MBCSet::RAMbank] * 0x2000;
			else m_MBC1[MBCSet::RAMbank] = val & 3;
			break;
		case 3:
		case 2:
		case 1:
			if (m_MBC1[MBCSet::Mode])
			{
				val &= 0x3;
				if (!(val & 1)) val |= 1;
				m_MBC1[MBCSet::ROMbank] = (m_MBC1[MBCSet::ROMbank] & 0x1F) | (val << 5);
				m_RamOffset = m_MBC1[MBCSet::RAMbank] * 0x2000;
			}
			else m_MBC1[MBCSet::RAMbank] = val & 3;
			break;
		}
#ifdef _WIN32
		//printf("MBC1: %x Ram offset: %x\n", m_MBC1[MBCSet::RAMbank], m_RamOffset);
#endif
		break;
	case 0x6000:
	case 0x7000:
		switch (m_CartridgeType)
		{
		case 0x12:
		case 0x13:
			if (m_MBC3[MBC3Set::RTCLatchPrevValue] == 0 && val == 0x1)
				m_MBC3[MBC3Set::RTCLatch] = !m_MBC3[MBC3Set::RTCLatch];
			m_MBC3[MBC3Set::RTCLatchPrevValue] = val;
			break;
		case 3:
		case 2:
		case 1:
			m_MBC1[MBCSet::Mode] = val & 1;
			if (m_MBC1[MBCSet::Mode]) { m_MBC1[MBCSet::ROMbank] &= 0x1F; m_RomOffset = m_RomOffset = m_MBC1[MBCSet::ROMbank] * 0x4000; }
			//default:
			//m_ROM[(addr & 0x3FFF) + m_RomOffset] = val;
		}
		break;
	case 0x8000:
	case 0x9000:
		if (*m_Model == Device::CGB && m_Memory[0xFF4F] != 0)
		{
			m_CGBVram[addr & 0x1FFF] = val;
			return;
		}
		m_Memory[addr] = val; // VRAM
		break;
	case 0xA000:
	case 0xB000:
		if (m_CartridgeType == 5 || m_CartridgeType == 6) // MBC2
		{
			if (addr > 0xA1FF) return;
			m_ExternalRAM[(addr & 0x1FF) + m_RamOffset] = val & 0xF;
			SetSaveFlag(0, 512);
		}
		else if (m_MBC1[MBCSet::RAMon])
		{
			if (m_MBC1[MBCSet::Mode]) m_ExternalRAM[(addr & 0x1FFF) + 0x2000 * m_MBC1[MBCSet::RAMbank]] = val;
			else m_ExternalRAM[(addr & 0x1FFF)] = val;
			SetSaveFlag(0, 32 * 1024);
		}
		break;
	case 0xC000:
	case 0xD000:
		if (*m_Model == Device::CGB && addr >= 0xD000 && m_WramBank > 0)
		{
			m_CGBWram[(m_WramBank * 0x1000) + (addr & 0x0FFF)] = val; // Not ECHOED yet
			return;
		}
		if (addr < 0xDE00) m_Memory[addr + 0x2000] = val;
		m_Memory[addr] = val;
		break;
	case 0xE000:
		m_Memory[addr] = val; // Echo
		m_Memory[addr - 0x2000] = val; // VRAM
		break;
	case 0xF000:
		switch (addr & 0x0F00)
		{
			// Echo RAM
		case 0x000: case 0x100: case 0x200: case 0x300:
		case 0x400: case 0x500: case 0x600: case 0x700:
		case 0x800: case 0x900: case 0xA00: case 0xB00:
		case 0xC00: case 0xD00:
			m_Memory[addr] = val; // VRAM
			//m_Memory[addr - 0x2000] = val; // VRAM
			break;
			// OAM
		case 0xE00:
			if (addr < 0xFEA0) m_Memory[addr] = val; // OAM
			//m_pGPU->Updateoam(addr - 0xFE00, val);
			break;
			// Zeropage RAM, I/O

		case 0xF00:
			if (*m_Model == Device::CGB)
			{
				if (addr == 0xFF70) // SVBK
				{
					m_WramBank = val & 7;
					if (m_WramBank == 0) m_WramBank = 1;
					return;
				}
				if (addr == 0xFF4D)
				{
					m_Memory[0xFF4D] = (val & 0x80) | (val & 0x1);
				}
			}

			if (addr == cJoypad)
				m_Memory[addr] = val;
			else if (addr == cINTEnable) m_IE = val;
			else if (addr == cINTFlag) m_IF = val;
			else if (addr == cTimerTAC) m_TAC = val;
			else if (addr == cTimerTIMA) m_TIMA = val;
			else if (addr == cTimerTMA) m_TMA = val;
			else if (addr == 0xFF01) m_Memory[0xFF01] = val;
			else if (addr == 0xFF02)
			{
				//if (BIT3(m_IE)) m_IF |= B3;
				//printf("%c", m_Memory[0xFF01]); // Output to serial
				m_Memory[0xFF01] = 0xFF;
				m_Memory[0xFF02] = 0xFE;
				return;
			}
			else if (addr == cTimerDIV) { m_Memory[cTimerDIV] = 0; return; }
			else if (addr < 0xFF40) // Sound here
				m_pSound->WriteByte(addr, val);
			//break;
			else if (addr < 0xFF80)
				m_pGPU->WriteByte(addr, val);
			else
			{
				//if (val == 0x55)
				//	return;
				m_Memory[addr] = val;
			}
		}
		break;
	}
}

void Memory::WriteWord(unsigned short addr, unsigned short val)
{
	WriteByte(addr, (val & 0x00FF));
	WriteByte(addr + 1, (val & 0xFF00) >> 8);
}

void Memory::SetBit(UI16 addr, UI8 bit)
{
	if (addr == cJoypad)
	{
		if (BIT4(m_Memory[cJoypad])) m_KEYS[0] |= bit;
		else if (BIT5(m_Memory[cJoypad])) m_KEYS[1] |= bit;
	}
	else if (addr == cINTFlag) m_IF |= bit;
	else if (addr == cINTEnable) m_IE |= bit;
	else if (addr == cTimerTAC) m_TAC |= bit;
	else if (addr == cTimerTIMA) m_TIMA |= bit;
	else if (addr == cTimerTMA) m_TMA |= bit;
	else m_Memory[addr] |= bit;
}

void Memory::UnsetBit(UI16 addr, UI8 bit)
{
	switch (addr)
	{
	case cJoypad:
		if (BIT4(m_Memory[cJoypad])) m_KEYS[0] &= ~bit;
		else if (BIT5(m_Memory[cJoypad])) m_KEYS[1] &= ~bit;
		else return;
		m_pCPU->KeyPressed();
		break;
	case cINTFlag: m_IF &= ~bit; break;
	case cINTEnable: m_IE &= ~bit; break;
	case cTimerTAC: m_TAC &= ~bit; break;
	case cTimerTIMA: m_TIMA &= ~bit; break;
	case cTimerTMA: m_TMA &= ~bit; break;
	default: m_Memory[addr] &= ~bit;
	}
}

void Memory::UpdateTimer(UI8 passedCycles)
{
	// DIV runs unconditionally
	if (m_TimerDivCounter >= m_TimerControlSpeed[3]) // 16384Hz
	{
		++m_Memory[cTimerDIV];
		m_TimerDivCounter -= m_TimerControlSpeed[3];
	}
	else m_TimerDivCounter += passedCycles;

	if (BIT2(m_TAC))
	{
		m_TimerTimaCounter += passedCycles;
		while (m_TimerTimaCounter >= m_TimerControlSpeed[m_TAC & 0x3])
		{
			if (m_TIMA == 0xFF)
			{
				SetBit(cINTFlag, B2);
				m_TIMA = m_TMA; // Set timer to start value
			}
			else ++m_TIMA;
			m_TimerTimaCounter -= m_TimerControlSpeed[m_TAC & 0x3];
		}
	}

	//if (m_MBC3_RTC_Enabled)
	//{
	//	if (m_MBC3_RTC >= 128)
	//	{
	//		m_MBC3_RTC = 0;
	//	}
	//	else m_TimerTimaCounter += passedCycles;
	//}
}

void Memory::LookForSave()
{
	// http://gbdev.gg8.se/forums/viewtopic.php?id=89
	if (m_CartridgeType == 3) // 3 - ROM + MBC1 + RAM + BATT
	{
		SaveData::ReadSave(m_GameTitle, 0, 32 * 1024);
	}
	if (m_CartridgeType == 6) // 3 - ROM + MBC1 + RAM + BATT
	{
		SaveData::ReadSave(m_GameTitle, 0, 512);
	}
	else if (m_CartridgeType == 0x13)
	{
		SaveData::ReadSave(m_GameTitle, 0, 32 * 1024);
	}
	else if (m_CartridgeType == 0x1b)
	{
		SaveData::ReadSave(m_GameTitle, 0, 128 * 1024);
	}

	//if (m_CartridgeType == 0)
	{
		m_RomOffset = 0x4000;
		m_MBC1[MBCSet::ROMbank] = 1;
	}
}

byte Memory::ReadCGBBanked(UI16 addr, byte bank)
{
	return bank > 0 ? m_CGBVram[addr & 0x1FFF] : ReadByte(addr);
}