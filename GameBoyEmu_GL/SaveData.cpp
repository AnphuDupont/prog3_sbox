#include "stdafx.h"
#include "SaveData.h"
#include <fstream>

#include "Memory.h"

void SaveData::ReadSave(const char* fName, UI16 addr, UI32 size)
{
	char * _data = new char[size];

	//return;
	std::ifstream inFile;
	inFile.open(fName, std::ios::in | std::ios::binary);

	if (inFile.is_open())
	{
		inFile.read(_data, size);
		inFile.close();
	}

	memcpy(Memory::GetMemory()->GetExternalRamPtr(), _data, size);

	delete[] _data;
}

void SaveData::WriteSave(const char* fName, UI16 addr, UI32 size)
{
	//return;
	std::ofstream outFile;
	outFile.open(fName, std::ios::out | std::ios::binary);

	if (outFile.is_open())
	{
		outFile.write((char *)Memory::GetMemory()->GetExternalRamPtr(), size);
		outFile.close();
	}
}