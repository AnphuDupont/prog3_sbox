#include "GBSound.h"
//#include "SoundManager.h"
#include "Audio\SoundSDL.h"
#include "Audio\Gb_Snd_Emu-0.1.4\Basic_Gb_Apu.h"

// SDL 2.0.4

#include <stdio.h>

#define NR10 0xFF10
#define NR52 0xFF26

byte soundMask[] = {
	0x80, 0x3F, 0x00, 0xFF, 0xBF, // NR10-NR14 (0xFF10-0xFF14)
	0xFF, 0x3F, 0x00, 0xFF, 0xBF, // NR20-NR24 (0xFF15-0xFF19)
	0x7F, 0xFF, 0x9F, 0xFF, 0xBF, // NR30-NR34 (0xFF1A-0xFF1E)
	0xFF, 0xFF, 0x00, 0x00, 0xBF, // NR40-NR44 (0xFF1F-0xFF23)
	0x00, 0x00, 0x70, 0xFF, 0xFF, // NR50-NR54 (0xFF24-0xFF28)
	0xFF, 0xFF, 0xFF, 0xFF, 0xFF, // --------- (0xFF29-0xFF2D)
	0xFF, 0xFF,                   // --------- (0xFF2E-0xFF2F)
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // WaveRAM (0xFF30-0xFF37)
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // WaveRAM (0xFF38-0xFF3F)
};

GBSound * GBSound::pSound = nullptr;

GBSound::GBSound() :
m_IsEnabled(false),
m_SampleRate(44100)
{
	m_Apu = new Basic_Gb_Apu();
	m_SoundMan = new SoundSDL();

	if (!SetSampleRate(m_SampleRate))
	{
		m_IsEnabled = false;
	}
}

GBSound::~GBSound()
{
	delete m_Apu;
	delete m_SoundMan;
}

GBSound * GBSound::GetGBSound()
{
	if (pSound == 0)
		pSound = new GBSound();
	return pSound;
}

bool GBSound::CheckError(const char * err)
{
	if (err)
	{
		printf("Audio Error : ");
		printf(err);
		return false;
	}

	return true;
}

bool GBSound::SetSampleRate(int srate)
{
	m_SampleRate = srate;
	bool oldEnable = m_IsEnabled;
	if (m_IsEnabled) Stop();

	if (!CheckError(m_Apu->set_sample_rate(srate)))
		return false;

	if (oldEnable) return Start();
	return true;
}

bool GBSound::Start()
{
	if (!m_IsEnabled)
	{
		if (!m_SoundMan->Start(m_SampleRate, 2)) 
			return false;
	}

	m_IsEnabled = true;
	return true;
}

bool GBSound::Stop()
{
	if (m_IsEnabled)
	{
		m_SoundMan->Stop();
		//return true;
	}

	m_IsEnabled = false;
	return true;
}

void GBSound::SetEnabled(bool e)
{
	if (e) Start();
	else Stop();
}

void GBSound::EndFrame()
{
	m_Apu->end_frame();

	const int bufferSize = m_Apu->samples_avail();
	blip_sample_t * buffer = new blip_sample_t[bufferSize];

	int count = m_Apu->read_samples(buffer, bufferSize);
	m_SoundMan->Write(buffer, count);

	delete buffer;
}

void GBSound::WriteByte(UI16 addr, byte val)
{
	if (m_IsEnabled)
	{
		if ((addr == NR52) && (MASK(val, B7) == 0))
		{
			for (int i = 0xFF10; i <= 0xFF26; i++)
				m_Apu->write_register(i, 0);
		}
		else
		{
			if ((addr >= 0xFF30) || (addr == NR52) || MASK(m_Apu->read_register(NR52), B7))
				m_Apu->write_register(addr, val);
		}
	}
}

byte GBSound::ReadByte(UI16 addr) 
{
	byte value = 0;
	if (m_IsEnabled)
		value = m_Apu->read_register(addr);

	return value | soundMask[addr - NR10];
}