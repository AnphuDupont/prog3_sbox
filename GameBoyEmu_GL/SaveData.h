#pragma once
#include "defines.h" 

class SaveData
{
public:
	static void ReadSave(const char* file, UI16 addr, UI32 size); // Takes name from ROM (16 char)
	static void WriteSave(const char* file, UI16 addr, UI32 size); // Takes name from ROM (16 char)

private:
	SaveData() {}
	~SaveData() {}
};

