#pragma once

#include "defines.h"

namespace DMAAddr
{
#ifdef _WIN32
	enum DMAAddr
	{
		SourceL, SourceH, DestL, DestH
	};
#else // Settings for big endian cpu 
	enum DMAAddr
	{
		SourceH, SourceL, DestH, DestL 
	};
#endif
}

class Graphics
{
public:
	~Graphics();

	Color DisplayBuffer[SCREEN_WIDTH * SCREEN_HEIGHT];
	static Graphics * GetGraphics();

	void Reset();

	void RenderScanline();
	void Paint();
	void Tick(const unsigned char);

	byte ReadByte(UI16 addr);
	void WriteByte(UI16 addr, byte val);
	byte Scanline() { return m_Scanline; }
	
	byte GetMode() { return m_Mode & 0x3; }

	static UI16 FPS;
	static bool DrawDone;

private:
	void RenderBackground();
	void RenderWindow();
	void RenderSprites();
	void CGBRenderBackground();
	void CGBRenderWindow();
	void CGBRenderSprites();
	void CompareLYC();

	void RenderOAMDebug();
	void RenderTileDebug();

	Graphics();
	static Graphics * m_Graphics;
	GameBoyZ80 * m_pCpu;

	byte m_Screen[SCREEN_WIDTH * SCREEN_HEIGHT];

	unsigned short m_ModeClock;
	byte m_Mode, m_Scanline, m_ScrollY, m_ScrollX, m_LCDC;
	//byte m_WindowLine;

	//bool m_LCDcontrol[8];
	bool m_Debug;

	Memory* m_pMMU;

	static const Color m_Palette[4];
	Color m_Palettes[3][4]; // Background, Object1, Object2

	/* Gameboy Color Implementation */
	Device::Device * m_Model;
	Color m_CGBPaletteBG[8][4];
	Color m_CGBPaletteOBJ[8][4];
	byte m_ColorPalettesBG[0x3F];
	byte m_ColorPalettesOBJ[0x3F];
	byte m_BGPaletteIndex;
	byte m_OBJPaletteIndex;

	union
	{
		UI8 DMAAddr8[4];
		UI16 DMAAddr16[2];
	};

	struct CGBDMA
	{
		UI16 start;
		UI16 end;
		UI16 length;
		bool HBlankDMA;
		UI16 offset;
	} m_CGBDMA;
};

