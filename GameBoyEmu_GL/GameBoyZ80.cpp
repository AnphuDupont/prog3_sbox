#include "stdafx.h"
#include "GameBoyZ80.h"

GameBoyZ80 * GameBoyZ80::m_GameBoyZ80 = 0;
GameBoyZ80 * GameBoyZ80::GetGameBoyZ80()
{
	if (m_GameBoyZ80 == 0)
		m_GameBoyZ80 = new GameBoyZ80();
	return m_GameBoyZ80;
}

bool gInstrCB = false;

GameBoyZ80::GameBoyZ80() :
m_Halt(false),
m_Stop(false),
m_IntMasterEnable(false),
m_CanDebug(false),
m_PC(0),
m_SP(0),
m_Opcode(0)
{
	m_GameBoyZ80 = this;
	m_pGraphics = Graphics::GetGraphics();
	m_pMemory = Memory::GetMemory();
	m_pGBSound = GBSound::GetGBSound();
}

GameBoyZ80::~GameBoyZ80()
{
	delete m_pGraphics;
	delete m_pMemory;
	delete m_pGBSound;
}

#ifdef _WIN32
void GameBoyZ80::OutputDebug(bool b)
{
	//system("CLS");
	//if (gInstrCB) std::cout << "Instr CB -> ";
	printf("Current opcode: 0x%02X\n", m_Opcode);
	printf("Program counter: 0x%04X\n", m_PC);
	printf("Stack pointer: 0x%04X : 0x%04X\n", m_SP, m_pMemory->ReadWord(m_SP));

	printf("AF: %02X%02X\tROM: %03X\n", m_Register[Reg::a], m_Register[Reg::f], m_pMemory->m_MBC1[MBCSet::ROMbank]);
	printf("BC: %02X%02X\tRAM: %03X\n", m_Register[Reg::b], m_Register[Reg::c], m_pMemory->m_MBC1[MBCSet::RAMbank]);
	printf("DE: %02X%02X\t IF: %02X\n", m_Register[Reg::d], m_Register[Reg::e], m_pMemory->m_IF);
	printf("HL: %02X%02X\t IE: %02X\n", m_Register[Reg::h], m_Register[Reg::l], m_pMemory->m_IE);

	for (int i = 12; i > -4; --i)
	{
		printf("%04X - %04X\n", m_SP + i * 2, m_pMemory->ReadWord(m_SP + i * 2));
	}

	//for (size_t i = 0xC3FF; i >= 0xC000;)
	//{
	//	printf("%04X : ", i);
	//	for (int cnt = 0; cnt < 16; cnt++, --i)
	//	{
	//		printf("%02X ", m_pMemory->ReadByte(i));
	//	}
	//	printf("\n");
	//}

	if (b)
	{
		printf("\nThe gameboy has crashed.\n\n");
		system("pause");
	}
}
#else
void GameBoyZ80::OutputDebug(bool) { }
#endif

bool GameBoyZ80::LoadGame(const char * programPtr)
{
	//printf("Loading ROM : ");
	//if (programPtr.size() <= 0 || programPtr.size() > (8192 * 1024)) return false;
	//memcpy(m_pMemory->m_ROM, programPtr.data(), programPtr.size());
	memcpy(m_pMemory->m_GameTitle, programPtr + cCartName, 16);

	//printf("%X bytes.\n", programPtr.size());

	m_pMemory->m_CartridgeType = m_pMemory->m_ROM[0x0147];
	m_pMemory->m_RomSize = m_pMemory->m_ROM[0x0148];
	m_pMemory->m_RamSize = m_pMemory->m_ROM[0x0149];

	model = ((m_pMemory->m_ROM[0x0143] & 0x80) ? Device::CGB : Device::DMG);

	if (model == Device::CGB)
	{
		m_Register[Reg::a] = 0x11;
		m_Register[Reg::f] = 0xB0;
	}

	m_pMemory->LookForSave();
	return true;
}

void GameBoyZ80::Reset()
{
	m_SetInterrupt = 0;
	m_HaltInterruptDisabled = m_IsInInterruptRoutine = false;
	m_CanDebug = true;
	m_SP = 0;
	m_ClockM = m_ClockT = 0;
	m_IntMasterEnable = false;

	m_Stop = m_Halt = false;
	m_CGBSpeedMode = false;

	m_PC = 0x100;

	m_pGraphics->Reset();
	m_pMemory->Reset();

	//const UI8 _unInitializedValues[] = { 0x01, 0xB0, 0x00, 0x13, 0x00, 0xD8, 0x01, 0x4D };
	const UI8 _unInitializedValues[] = { 0xB0, 0x01, 0x13, 0x00, 0xD8, 0x00, 0x4D, 0x01 };
	memcpy(m_Register, _unInitializedValues, 8);

	if (model == Device::CGB)
	{
		m_Register[Reg::a] = 0x11;
		m_Register[Reg::f] = 0xB0;
	}

	_testVarA = true;
	m_Test = 0;

	m_pGBSound->Stop();
	m_pGBSound->Start();
	m_pGBSound->WriteByte(0xFF26, 0); // Turn off APU.
	//m_pGBSound->WriteByte(0xFF26, 0xF1); // Turn on APU.
}

void GameBoyZ80::EmulateCycle()
{
	//if (m_PC == 0x22E9)
	//{
	//	OutputDebug();
	//	_debugWinOutput = true;
	//}

	if (!m_Halt)
	{
		m_Opcode = m_pMemory->ReadByte(m_PC);
		//m_Test = m_PC;
		//if (_debugWinOutput) OutputDebug();

		//if (m_PC < 0x8000 && m_PC > 0x4000)
			//printf("%04X:%02X\t", m_PC, m_Opcode);

		//printf("STAT:%02X\t", m_pGraphics->ReadByte(cLCDStat));
		if (!m_HaltInterruptDisabled) ++m_PC;
		else m_HaltInterruptDisabled = false;

		// Exectute instruction
		switch (m_Opcode)
		{
		case 0x00: NOP();		break;
		case 0x01: LDr16nn(Reg16::bc);	break;
		case 0x02: LDBCmA();	break;
		case 0x03: INCr16(Reg16::bc);		break;
		case 0x04: INCreg(Reg::b);		break;
		case 0x05: DECreg(Reg::b);		break;
		case 0x06: LDrn(Reg::b);	break;
		case 0x07: RLCA();		break;
		case 0x08: LDnnSP();	break;
		case 0x09: ADDHLval(m_Register16[Reg16::bc]);	break;
		case 0x0A: LDABCm();	break;
		case 0x0B: DECBC();		break;
		case 0x0C: INCreg(Reg::c);		break;
		case 0x0D: DECreg(Reg::c);		break;
		case 0x0E: LDrn(Reg::c);	break;
		case 0x0F: RRCA();		break;
		case 0x10: STOP();		break;
		case 0x11: LDr16nn(Reg16::de);	break;
		case 0x12: LDDEmA();	break;
		case 0x13: INCr16(Reg16::de);		break;
		case 0x14: INCreg(Reg::d);		break;
		case 0x15: DECreg(Reg::d);		break;
		case 0x16: LDrn(Reg::d);	break;
		case 0x17: RLA();		break;
		case 0x18: JR();		break;
		case 0x19: ADDHLval(m_Register16[Reg16::de]);	break;
		case 0x1A: LDADEm();	break;
		case 0x1B: DECDE();		break;
		case 0x1C: INCreg(Reg::e);		break;
		case 0x1D: DECreg(Reg::e);		break;
		case 0x1E: LDrn(Reg::e);	break;
		case 0x1F: RRA();		break;
		case 0x20: JRNZ();		break;
		case 0x21: LDr16nn(Reg16::hl);	break;
		case 0x22: LDIAmHL();	break;
		case 0x23: INCr16(Reg16::hl);		break;
		case 0x24: INCreg(Reg::h);		break;
		case 0x25: DECreg(Reg::h);		break;
		case 0x26: LDrn(Reg::h);	break;
		case 0x27: DAA();		break;
		case 0x28: JRZ();		break;
		case 0x29: ADDHLval(m_Register16[Reg16::hl]);	break;
		case 0x2A: LDIHLmA();	break;
		case 0x2B: DECHL();		break;
		case 0x2C: INCreg(Reg::l);		break;
		case 0x2D: DECreg(Reg::l);		break;
		case 0x2E: LDrn(Reg::l);	break;
		case 0x2F: CPL();		break;
		case 0x30: JRNC();		break;
		case 0x31: LDSPnn();	break;
		case 0x32: LDDAmHL();	break;
		case 0x33: INCsp();		break;
		case 0x34: INCHLm();	break;
		case 0x35: DECHLm();	break;
		case 0x36: LDHLmn();	break;
		case 0x37: SCF();		break;
		case 0x38: JRC();		break;
		case 0x39: ADDHLval(m_SP);	break;
		case 0x3A: LDDHLmA();	break;
		case 0x3B: DECsp();		break;
		case 0x3C: INCreg(Reg::a);		break;
		case 0x3D: DECreg(Reg::a);		break;
		case 0x3E: LDrn(Reg::a);	break;
		case 0x3F: CCF();		break;
		case 0x40: LDrr(Reg::b, Reg::b);	break;
		case 0x41: LDrr(Reg::b, Reg::c);	break;
		case 0x42: LDrr(Reg::b, Reg::d);	break;
		case 0x43: LDrr(Reg::b, Reg::e);	break;
		case 0x44: LDrr(Reg::b, Reg::h);	break;
		case 0x45: LDrr(Reg::b, Reg::l);	break;
		case 0x46: LDrHLm(Reg::b);	break;
		case 0x47: LDrr(Reg::b, Reg::a);	break;
		case 0x48: LDrr(Reg::c, Reg::b);	break;
		case 0x49: LDrr(Reg::c, Reg::c);	break;
		case 0x4A: LDrr(Reg::c, Reg::d);	break;
		case 0x4B: LDrr(Reg::c, Reg::e);	break;
		case 0x4C: LDrr(Reg::c, Reg::h);	break;
		case 0x4D: LDrr(Reg::c, Reg::l);	break;
		case 0x4E: LDrHLm(Reg::c);	break;
		case 0x4F: LDrr(Reg::c, Reg::a);	break;
		case 0x50: LDrr(Reg::d, Reg::b);	break;
		case 0x51: LDrr(Reg::d, Reg::c);	break;
		case 0x52: LDrr(Reg::d, Reg::d);	break;
		case 0x53: LDrr(Reg::d, Reg::e);	break;
		case 0x54: LDrr(Reg::d, Reg::h);	break;
		case 0x55: LDrr(Reg::d, Reg::l);	break;
		case 0x56: LDrHLm(Reg::d);	break;
		case 0x57: LDrr(Reg::d, Reg::a);	break;
		case 0x58: LDrr(Reg::e, Reg::b);	break;
		case 0x59: LDrr(Reg::e, Reg::c);	break;
		case 0x5A: LDrr(Reg::e, Reg::d);	break;
		case 0x5B: LDrr(Reg::e, Reg::e);	break;
		case 0x5C: LDrr(Reg::e, Reg::h);	break;
		case 0x5D: LDrr(Reg::e, Reg::l);	break;
		case 0x5E: LDrHLm(Reg::e);	break;
		case 0x5F: LDrr(Reg::e, Reg::a);	break;
		case 0x60: LDrr(Reg::h, Reg::b);	break;
		case 0x61: LDrr(Reg::h, Reg::c);	break;
		case 0x62: LDrr(Reg::h, Reg::d);	break;
		case 0x63: LDrr(Reg::h, Reg::e);	break;
		case 0x64: LDrr(Reg::h, Reg::h);	break;
		case 0x65: LDrr(Reg::h, Reg::l);	break;
		case 0x66: LDrHLm(Reg::h);	break;
		case 0x67: LDrr(Reg::h, Reg::a);	break;
		case 0x68: LDrr(Reg::l, Reg::b);	break;
		case 0x69: LDrr(Reg::l, Reg::c);	break;
		case 0x6A: LDrr(Reg::l, Reg::d);	break;
		case 0x6B: LDrr(Reg::l, Reg::e);	break;
		case 0x6C: LDrr(Reg::l, Reg::h);	break;
		case 0x6D: LDrr(Reg::l, Reg::l);	break;
		case 0x6E: LDrHLm(Reg::l);	break;
		case 0x6F: LDrr(Reg::l, Reg::a);	break;
		case 0x70: LDHLmr(Reg::b);	break;
		case 0x71: LDHLmr(Reg::c);	break;
		case 0x72: LDHLmr(Reg::d);	break;
		case 0x73: LDHLmr(Reg::e);	break;
		case 0x74: LDHLmr(Reg::h);	break;
		case 0x75: LDHLmr(Reg::l);	break;
		case 0x76: HALT();		break;
		case 0x77: LDHLmr(Reg::a);	break;
		case 0x78: LDrr(Reg::a, Reg::b);	break;
		case 0x79: LDrr(Reg::a, Reg::c);	break;
		case 0x7A: LDrr(Reg::a, Reg::d);	break;
		case 0x7B: LDrr(Reg::a, Reg::e);	break;
		case 0x7C: LDrr(Reg::a, Reg::h);	break;
		case 0x7D: LDrr(Reg::a, Reg::l);	break;
		case 0x7E: LDrHLm(Reg::a);	break;
		case 0x7F: LDrr(Reg::a, Reg::a);	break;
		case 0x80: ADDr(Reg::b);		break;
		case 0x81: ADDr(Reg::c);		break;
		case 0x82: ADDr(Reg::d);		break;
		case 0x83: ADDr(Reg::e);		break;
		case 0x84: ADDr(Reg::h);		break;
		case 0x85: ADDr(Reg::l);		break;
		case 0x86: ADDHL();		break;
		case 0x87: ADDr(Reg::a);		break;
		case 0x88: ADCr(Reg::b);		break;
		case 0x89: ADCr(Reg::c);		break;
		case 0x8A: ADCr(Reg::d);		break;
		case 0x8B: ADCr(Reg::e);		break;
		case 0x8C: ADCr(Reg::h);		break;
		case 0x8D: ADCr(Reg::l);		break;
		case 0x8E: ADCHL();		break;
		case 0x8F: ADCr(Reg::a);	break;
		case 0x90: SUBr(Reg::b);		break;
		case 0x91: SUBr(Reg::c);		break;
		case 0x92: SUBr(Reg::d);		break;
		case 0x93: SUBr(Reg::e);		break;
		case 0x94: SUBr(Reg::h);		break;
		case 0x95: SUBr(Reg::l);		break;
		case 0x96: SUBHL();		break;
		case 0x97: SUBr(Reg::a);		break;
		case 0x98: SBCr(Reg::b);		break;
		case 0x99: SBCr(Reg::c);		break;
		case 0x9A: SBCr(Reg::d);		break;
		case 0x9B: SBCr(Reg::e);		break;
		case 0x9C: SBCr(Reg::h);		break;
		case 0x9D: SBCr(Reg::l);		break;
		case 0x9E: SBCHL();		break;
		case 0x9F: SBCr(Reg::a);		break;
		case 0xA0: AND(Reg::b);		break;
		case 0xA1: AND(Reg::c);		break;
		case 0xA2: AND(Reg::d);		break;
		case 0xA3: AND(Reg::e);		break;
		case 0xA4: AND(Reg::h);		break;
		case 0xA5: AND(Reg::l);		break;
		case 0xA6: ANDHL();		break;
		case 0xA7: AND(Reg::a);		break;
		case 0xA8: XOR(Reg::b);		break;
		case 0xA9: XOR(Reg::c);		break;
		case 0xAA: XOR(Reg::d);		break;
		case 0xAB: XOR(Reg::e);		break;
		case 0xAC: XOR(Reg::h);		break;
		case 0xAD: XOR(Reg::l);		break;
		case 0xAE: XORHL();		break;
		case 0xAF: XOR(Reg::a);		break;
		case 0xB0: OR(Reg::b);		break;
		case 0xB1: OR(Reg::c);		break;
		case 0xB2: OR(Reg::d);		break;
		case 0xB3: OR(Reg::e);		break;
		case 0xB4: OR(Reg::h);		break;
		case 0xB5: OR(Reg::l);		break;
		case 0xB6: ORHL();		break;
		case 0xB7: OR(Reg::a);		break;
		case 0xB8: CPval(m_Register[Reg::b]);		break;
		case 0xB9: CPval(m_Register[Reg::c]);		break;
		case 0xBA: CPval(m_Register[Reg::d]);		break;
		case 0xBB: CPval(m_Register[Reg::e]);		break;
		case 0xBC: CPval(m_Register[Reg::h]);		break;
		case 0xBD: CPval(m_Register[Reg::l]);		break;
		case 0xBE: CPHL();		break;
		case 0xBF: CPval(m_Register[Reg::a]);		break;
		case 0xC0: RETNZ();		break;
		case 0xC1: POPBC();		break;
		case 0xC2: JPNZ();		break;
		case 0xC3: JP();		break;
		case 0xC4: CALLNZ();	break;
		case 0xC5: PUSHBC();	break;
		case 0xC6: ADDn();		break;
		case 0xC7: RST00();		break;
		case 0xC8: RETZ();		break;
		case 0xC9: RET();		break;
		case 0xCA: JPZ();		break;
		case 0xCB: InstrCB();	break;
		case 0xCC: CALLZ();		break;
		case 0xCD: CALLnn();	break;
		case 0xCE: ADCn();		break;
		case 0xCF: RST08();		break;
		case 0xD0: RETNC();		break;
		case 0xD1: POPDE();		break;
		case 0xD2: JPNC();		break;
			//case 0xD3: XX();		break;
		case 0xD4: CALLNC();	break;
		case 0xD5: PUSHDE();	break;
		case 0xD6: SUBn();		break;
		case 0xD7: RST10();		break;
		case 0xD8: RETC();		break;
		case 0xD9: RETI();		break;
		case 0xDA: JPC();		break;
			//case 0xDB: XX();		break;
		case 0xDC: CALLC();		break;
			//case 0xDD: XX();		break;
		case 0xDE: SBCn();		break;
		case 0xDF: RST18();		break;
		case 0xE0: LDHAm();		break;
		case 0xE1: POPHL();		break;
		case 0xE2: LDACm();		break;
			//case 0xE3: XX();		break;
			//case 0xE4: XX();		break;
		case 0xE5: PUSHHL();	break;
		case 0xE6: ANDn();		break;
		case 0xE7: RST20();		break;
		case 0xE8: ADDspn();	break;
		case 0xE9: JPHL();		break;
		case 0xEA: LDmmA();		break;
			//case 0xEB: XX();		break;
			//case 0xEC: XX();		break;
			//case 0xED: XX();		break;
		case 0xEE: XORn();		break;
		case 0xEF: RST28();		break;
		case 0xF0: LDHmA();		break;
		case 0xF1: POPAF();		break;
		case 0xF2: LDCmA();		break;
		case 0xF3: DI();		break;
			//case 0xF4: XX();		break;
		case 0xF5: PUSHAF();	break;
		case 0xF6: ORn();		break;
		case 0xF7: RST30();		break;
		case 0xF8: LDHLSPn();	break;
		case 0xF9: LDSPHL();	break;
		case 0xFA: LDAmm();		break;
		case 0xFB: EI();		break;
			//case 0xFC: XX();		break;
			//case 0xFD: XX();		break;
		case 0xFE: CPn();		break;
		case 0xFF: RST38();		break;
		default: OutputDebug();
			break;
		}
	}

	CheckInterrupts();
	m_pMemory->UpdateTimer(m_ClockM);
	if (m_CGBSpeedMode) m_pGraphics->Tick(m_ClockM >> 1);
	else m_pGraphics->Tick(m_ClockM);
}

void GameBoyZ80::LDDHLmA()
{
	m_Register[Reg::a] = m_pMemory->ReadByte(m_Register16[Reg16::hl]);
	--m_Register16[Reg16::hl];
	m_ClockM = 8;
}

void GameBoyZ80::LDIHLmA()
{
	m_Register[Reg::a] = m_pMemory->ReadByte(m_Register16[Reg16::hl]);
	++m_Register16[Reg16::hl];
	m_ClockM = 8;
}

void GameBoyZ80::LDDAmHL()
{
	m_pMemory->WriteByte(m_Register16[Reg16::hl], m_Register[Reg::a]);
	--m_Register16[Reg16::hl];
	m_ClockM = 8;
}

void GameBoyZ80::LDIAmHL()
{
	m_pMemory->WriteByte(m_Register16[Reg16::hl], m_Register[Reg::a]);
	++m_Register16[Reg16::hl];
	m_ClockM = 8;
}

void GameBoyZ80::LDHLSPn()
{
	byte immVal = m_pMemory->ReadByte(m_PC++);
	m_Register[Reg::f] = 0;

	if (((m_SP & 0x0F) + (immVal & 0x0F)) > 0x0F) m_Register[Reg::f] |= FLAG_H;
	if (((m_SP & 0xFF) + immVal) > 0xFF) m_Register[Reg::f] |= FLAG_C;

	m_Register16[Reg16::hl] = m_SP + (signed char)immVal;

	m_ClockM = 12;
}

void GameBoyZ80::ADDvalToA(const unsigned char value)
{
	unsigned char oldA = m_Register[Reg::a];
	m_Register[Reg::a] += value;
	m_Register[Reg::f] = (m_Register[Reg::a] < oldA) ? 0x10 : 0;
	m_Register[Reg::f] |= ((m_Register[Reg::a] & 0xF) < (oldA & 0xF)) ? 0x20 : 0;
	m_Register[Reg::f] |= (m_Register[Reg::a] == 0) ? 0x80 : 0;
}

void GameBoyZ80::ADCvalToA(const unsigned char value)
{
	byte oldC = (m_Register[Reg::f] & 0x10) ? 1 : 0;
	UI16 result = m_Register[Reg::a] + value + oldC;

	m_Register[Reg::f] = 0;
	if (!(result & 0xFF)) m_Register[Reg::f] |= FLAG_Z;
	if ((oldC + (value & 0x0F) + (m_Register[Reg::a] & 0x0F)) > 0x0F) m_Register[Reg::f] |= FLAG_H;
	if (result > 0xFF) m_Register[Reg::f] |= FLAG_C;

	m_Register[Reg::a] = (byte)result;
}

void GameBoyZ80::SUBvalFromA(const unsigned char value)
{
	unsigned char oldA = m_Register[Reg::a];
	m_Register[Reg::a] -= value;
	m_Register[Reg::f] = (m_Register[Reg::a] > oldA) ? 0x50 : 0x40;
	// H flag set if NO borrow
	m_Register[Reg::f] |= ((m_Register[Reg::a] ^ value ^ oldA) & 0x10) ? 0x20 : 0;
	m_Register[Reg::f] |= (m_Register[Reg::a] == 0) ? 0x80 : 0;
}

void GameBoyZ80::SBCvalFromA(const unsigned char value)
{
	byte oldC = (m_Register[Reg::f] & FLAG_C) ? 1 : 0;
	unsigned short sum = value + oldC;
	byte result = byte(m_Register[Reg::a] - sum);

	m_Register[Reg::f] = FLAG_N;
	if (!result) m_Register[Reg::f] |= FLAG_Z;
	if ((m_Register[Reg::a] & 0x0F) < (value & 0x0F)) m_Register[Reg::f] |= FLAG_H;
	else if ((m_Register[Reg::a] & 0x0F) < (sum & 0x0F)) m_Register[Reg::f] |= FLAG_H;
	else if (((m_Register[Reg::a] & 0x0F) == (value & 0x0F)) && ((value & 0x0F) == 0x0F) && oldC) m_Register[Reg::f] |= FLAG_H;

	if (m_Register[Reg::a] < sum) m_Register[Reg::f] |= FLAG_C;

	m_Register[Reg::a] = result;
}

void GameBoyZ80::CPval(const unsigned char value)
{
	unsigned char oldA = m_Register[Reg::a];
	oldA -= value;
	m_Register[Reg::f] = (m_Register[Reg::a] < oldA) ? 0x50 : 0x40;
	m_Register[Reg::f] |= ((m_Register[Reg::a] ^ value ^ oldA) & 0x10) ? 0x20 : 0;
	m_Register[Reg::f] |= (oldA == 0) ? 0x80 : 0;

	m_ClockM = 4;
}

void GameBoyZ80::INCreg(Reg::Reg r)
{
	unsigned char olVal = m_Register[r];
	++m_Register[r];
	m_Register[Reg::f] &= FLAG_C;
	//if (m_Register[r] & 0xF == 0) m_Register[Reg::f] |= 0x20;
	m_Register[Reg::f] |= ((olVal & 0xF0) != (m_Register[r] & 0xF0)) ? FLAG_H : 0;
	m_Register[Reg::f] |= (m_Register[r] == 0) ? FLAG_Z : 0;

	m_ClockM = 4;
}

void GameBoyZ80::INCHLm()
{
	unsigned char olVal = m_pMemory->ReadByte(m_Register16[Reg16::hl]);
	unsigned char newVal = olVal + 1;
	m_pMemory->WriteByte(m_Register16[Reg16::hl], newVal);
	m_Register[Reg::f] &= 0x10;
	m_Register[Reg::f] |= ((olVal & 0xF0) != (newVal & 0xF0)) ? 0x20 : 0;
	//if (olVal & 0x) m_Register[Reg::f] |= 0x20;
	if (newVal == 0) m_Register[Reg::f] |= 0x80;
	m_ClockM = 12;
}

void GameBoyZ80::DECreg(Reg::Reg r)
{
	unsigned char olVal = m_Register[r];
	--m_Register[r];
	m_Register[Reg::f] &= FLAG_C;
	m_Register[Reg::f] |= ((olVal & 0xF0) != (m_Register[r] & 0xF0)) ? 0x60 : 0x40;
	m_Register[Reg::f] |= (m_Register[r] == 0) ? FLAG_Z : 0;

	m_ClockM = 4;
}

void GameBoyZ80::DECHLm()
{
	unsigned char olVal = m_pMemory->ReadByte(m_Register16[Reg16::hl]);
	unsigned char newVal = olVal - 1;
	m_pMemory->WriteByte(m_Register16[Reg16::hl], newVal);
	m_Register[Reg::f] &= 0x10;
	m_Register[Reg::f] |= ((olVal & 0xF0) != (newVal & 0xF0)) ? 0x60 : 0x40;
	m_Register[Reg::f] |= ((olVal - 1) == 0) ? 0x80 : 0;
	m_ClockM = 12;
}

void GameBoyZ80::ADDHLval(const UI16 v)
{
	unsigned short oldHL = m_Register16[Reg16::hl];
	unsigned short temp = v;

	m_Register[Reg::f] &= FLAG_Z;
	if ((oldHL & 0xFFF) + (temp & 0xFFF) > 0xFFF) m_Register[Reg::f] |= FLAG_H;
	temp += oldHL;
	if (temp < oldHL) m_Register[Reg::f] |= FLAG_C;

	m_Register16[Reg16::hl] = temp;
	m_ClockM = 8;
}

void GameBoyZ80::ADDspn()
{
	byte immVal = m_pMemory->ReadByte(m_PC++);

	m_Register[Reg::f] = 0;
	if (((m_SP & 0x0F) + (immVal & 0x0F)) > 0x0F) m_Register[Reg::f] |= FLAG_H;
	if (((m_SP & 0xFF) + immVal) > 0xFF) m_Register[Reg::f] |= FLAG_C;

	m_SP += (signed char)immVal;
	m_ClockM = 16;
}


void GameBoyZ80::INCr16(Reg16::Reg16 r16)
{
	++m_Register16[r16];
	m_ClockM = 8;
}

void GameBoyZ80::DECBC()
{
	--m_Register16[Reg16::bc];
	m_ClockM = 8;
}

void GameBoyZ80::DECDE()
{
	--m_Register16[Reg16::de];
	m_ClockM = 8;
}

void GameBoyZ80::DECHL()
{
	--m_Register16[Reg16::hl];
	m_ClockM = 8;
}

void GameBoyZ80::DAA() // BGBboy
{
	int regA = m_Register[Reg::a];
	if ((m_Register[Reg::f] & FLAG_N) == 0)
	{
		if (((m_Register[Reg::f] & FLAG_H) > 0) || ((regA & 0xF) > 9)) regA += 0x06;
		if (((m_Register[Reg::f] & FLAG_C) > 0) || (regA > 0x9F)) regA += 0x60;
	}
	else
	{
		if ((m_Register[Reg::f] & FLAG_H) > 0) regA = (regA - 6) & 0xFF;
		if ((m_Register[Reg::f] & FLAG_C) > 0) regA -= 0x60;
	}

	m_Register[Reg::f] &= FLAG_N;
	if ((regA & 0x100) == 0x100) m_Register[Reg::f] |= FLAG_C;

	regA &= 0xFF;

	if (regA == 0) m_Register[Reg::f] |= FLAG_Z;
	m_Register[Reg::a] = byte(regA);

	m_ClockM = 4;
}

void GameBoyZ80::RLCr(Reg::Reg r)
{
	m_Register[Reg::f] = (m_Register[r] & 0x80) ? 0x10 : 0;
	m_Register[r] = (unsigned char)(m_Register[r] << 1);
	if (m_Register[Reg::f] & 0x10) ++m_Register[r];
	if (m_Register[r] == 0) m_Register[Reg::f] |= 0x80;

	m_ClockM = 8;
}

void GameBoyZ80::RLr(Reg::Reg r)
{
	unsigned char oldCarry = (m_Register[Reg::f] & 0x10) ? 1 : 0;
	m_Register[Reg::f] = (m_Register[r] & 0x80) ? 0x10 : 0;
	m_Register[r] = (unsigned char)(m_Register[r] << 1) + oldCarry;
	if (m_Register[r] == 0) m_Register[Reg::f] |= 0x80;

	m_ClockM = 8;
}

void GameBoyZ80::RRCr(Reg::Reg r)
{
	m_Register[Reg::f] = (m_Register[r] & 0x1) ? FLAG_C : 0;
	m_Register[r] = (unsigned char)(m_Register[r] >> 1);
	if (m_Register[Reg::f] & 0x10) m_Register[r] |= 0x80;
	if (m_Register[r] == 0) m_Register[Reg::f] |= 0x80;

	m_ClockM = 8;
}

void GameBoyZ80::RRr(Reg::Reg r)
{
	unsigned char oldCarry = (m_Register[Reg::f] & 0x10) ? 1 : 0;
	m_Register[Reg::f] = (m_Register[r] & 0x1) ? 0x10 : 0;
	m_Register[r] = (unsigned char)(m_Register[r] >> 1) + (oldCarry << 7);
	if (m_Register[r] == 0) m_Register[Reg::f] |= 0x80;

	m_ClockM = 8;
}

void GameBoyZ80::RLCHL()
{
	unsigned char hl = m_pMemory->ReadByte(m_Register16[Reg16::hl]);
	m_Register[Reg::f] = (hl & 0x80) ? 0x10 : 0;
	hl = (unsigned char)(hl << 1);
	if (m_Register[Reg::f] & 0x10) ++hl;
	if (hl == 0) m_Register[Reg::f] |= 0x80;
	m_pMemory->WriteByte(m_Register16[Reg16::hl], hl);

	m_ClockM = 16;
}

void GameBoyZ80::RLHL()
{
	unsigned char hl = m_pMemory->ReadByte(m_Register16[Reg16::hl]);
	unsigned char oldCarry = (m_Register[Reg::f] & 0x10) ? 1 : 0;
	m_Register[Reg::f] = (hl & 0x80) ? 0x10 : 0;
	hl = (unsigned char)(hl << 1) + oldCarry;
	if (hl == 0) m_Register[Reg::f] |= 0x80;
	m_pMemory->WriteByte(m_Register16[Reg16::hl], hl);

	m_ClockM = 16;
}

void GameBoyZ80::RRCHL()
{
	unsigned char hl = m_pMemory->ReadByte(m_Register16[Reg16::hl]);
	m_Register[Reg::f] = (hl & 0x1) ? 0x10 : 0;
	hl = (unsigned char)(hl >> 1);
	if (m_Register[Reg::f] & 0x10) hl |= 0x80;
	if (hl == 0) m_Register[Reg::f] |= 0x80;
	m_pMemory->WriteByte(m_Register16[Reg16::hl], hl);

	m_ClockM = 16;
}

void GameBoyZ80::RRHL()
{
	unsigned char hl = m_pMemory->ReadByte(m_Register16[Reg16::hl]);
	unsigned char oldCarry = (m_Register[Reg::f] & 0x10) ? 1 : 0;
	m_Register[Reg::f] = (hl & 0x1) ? 0x10 : 0;
	hl = (unsigned char)(hl >> 1) + (oldCarry << 7);
	if (hl == 0) m_Register[Reg::f] |= 0x80;
	m_pMemory->WriteByte(m_Register16[Reg16::hl], hl);

	m_ClockM = 16;
}

void GameBoyZ80::SLAr_r(Reg::Reg r)
{
	m_Register[Reg::f] = (m_Register[r] & 0x80) ? 0x10 : 0;
	m_Register[r] = m_Register[r] << 1;
	if (!m_Register[r]) m_Register[Reg::f] |= 0x80;
}

void GameBoyZ80::SLAHL()
{
	unsigned char hl = m_pMemory->ReadByte(m_Register16[Reg16::hl]);

	m_Register[Reg::f] = (hl & 0x80) ? 0x10 : 0;
	hl = hl << 1;
	if (!hl) m_Register[Reg::f] |= 0x80;

	m_pMemory->WriteByte(m_Register16[Reg16::hl], hl);
	m_ClockM = 16;
}

void GameBoyZ80::SRAr_r(Reg::Reg r)
{
	m_Register[Reg::f] = (m_Register[r] & 0x1) ? 0x10 : 0;
	m_Register[r] = (m_Register[r] & 0x80) + (m_Register[r] >> 1);
	//if (m_Register[r] & 0x40) m_Register[r] |= 0x80;
	if (m_Register[r] == 0) m_Register[Reg::f] |= 0x80;
}

void GameBoyZ80::SRAHL()
{
	unsigned char hl = m_pMemory->ReadByte(m_Register16[Reg16::hl]);

	m_Register[Reg::f] = (hl & 0x1) ? 0x10 : 0;
	hl = (hl & 0x80) + (hl >> 1);
	//if (hl & 0x40) hl |= 0x80;
	if (hl == 0) m_Register[Reg::f] |= 0x80;

	m_pMemory->WriteByte(m_Register16[Reg16::hl], hl);
	m_ClockM = 16;
}

void GameBoyZ80::SRLr_r(Reg::Reg r)
{
	m_Register[Reg::f] = (m_Register[r] & 0x1) ? 0x10 : 0;
	m_Register[r] = m_Register[r] >> 1;
	if (!m_Register[r]) m_Register[Reg::f] |= 0x80;

	m_ClockM = 8;
}

void GameBoyZ80::SRLHL()
{
	unsigned char hl = m_pMemory->ReadByte(m_Register16[Reg16::hl]);

	m_Register[Reg::f] = (hl & 0x1) ? 0x10 : 0;
	hl = hl >> 1;
	if (!hl) m_Register[Reg::f] |= 0x80;

	m_pMemory->WriteByte(m_Register16[Reg16::hl], hl);
	m_ClockM = 16;
}

void GameBoyZ80::SWAPnibble(unsigned char * var)
{
	unsigned char newValue = ((*var & 0x0F) << 4) | ((*var & 0xF0) >> 4);
	m_Register[Reg::f] = (newValue == 0) ? FLAG_Z : 0;
	*var = newValue;
}

void GameBoyZ80::POPAF()
{
	m_Register16[Reg16::af] = m_pMemory->ReadWord(m_SP);
	m_Register[Reg::f] &= 0xF0;
	m_SP += 2;
	m_ClockM = 12;
}
void GameBoyZ80::InstrCB()
{
	gInstrCB = true;
	m_Opcode = m_pMemory->ReadByte(m_PC++);

	switch (m_Opcode)
	{
	case 0x00: RLCr(Reg::b);	break;
	case 0x01: RLCr(Reg::c);	break;
	case 0x02: RLCr(Reg::d);	break;
	case 0x03: RLCr(Reg::e);	break;
	case 0x04: RLCr(Reg::h);	break;
	case 0x05: RLCr(Reg::l);	break;
	case 0x06: RLCHL();		break;
	case 0x07: RLCr(Reg::a);	break;
	case 0x08: RRCr(Reg::b);	break;
	case 0x09: RRCr(Reg::c);	break;
	case 0x0A: RRCr(Reg::d);	break;
	case 0x0B: RRCr(Reg::e);	break;
	case 0x0C: RRCr(Reg::h);	break;
	case 0x0D: RRCr(Reg::l);	break;
	case 0x0E: RRCHL();		break;
		// 0x10
	case 0x0F: RRCr(Reg::a);	break;
	case 0x10: RLr(Reg::b);		break;
	case 0x11: RLr(Reg::c);		break;
	case 0x12: RLr(Reg::d);		break;
	case 0x13: RLr(Reg::e);		break;
	case 0x14: RLr(Reg::h);		break;
	case 0x15: RLr(Reg::l);		break;
	case 0x16: RLHL();		break;
	case 0x17: RLr(Reg::a);		break;
	case 0x18: RRr(Reg::b);		break;
	case 0x19: RRr(Reg::c);		break;
	case 0x1A: RRr(Reg::d);		break;
	case 0x1B: RRr(Reg::e);		break;
	case 0x1C: RRr(Reg::h);		break;
	case 0x1D: RRr(Reg::l);		break;
	case 0x1E: RRHL();		break;
	case 0x1F: RRr(Reg::a);		break;
		// 0x20
	case 0x20: SLAr_b();	break;
	case 0x21: SLAr_c();	break;
	case 0x22: SLAr_d();	break;
	case 0x23: SLAr_e();	break;
	case 0x24: SLAr_h();	break;
	case 0x25: SLAr_l();	break;
	case 0x26: SLAHL();		break;
	case 0x27: SLAr_a();	break;
	case 0x28: SRAr_b();	break;
	case 0x29: SRAr_c();	break;
	case 0x2A: SRAr_d();	break;
	case 0x2B: SRAr_e();	break;
	case 0x2C: SRAr_h();	break;
	case 0x2D: SRAr_l();	break;
	case 0x2E: SRAHL();		break;
	case 0x2F: SRAr_a();	break;
		// 0x30
	case 0x30: SWAPr_b();	break;
	case 0x31: SWAPr_c();	break;
	case 0x32: SWAPr_d();	break;
	case 0x33: SWAPr_e();	break;
	case 0x34: SWAPr_h();	break;
	case 0x35: SWAPr_l();	break;
	case 0x36: SWAPHLm();	break;
	case 0x37: SWAPr_a();	break;
	case 0x38: SRLr_r(Reg::b);	break;
	case 0x39: SRLr_r(Reg::c);	break;
	case 0x3A: SRLr_r(Reg::d);	break;
	case 0x3B: SRLr_r(Reg::e);	break;
	case 0x3C: SRLr_r(Reg::h);	break;
	case 0x3D: SRLr_r(Reg::l);	break;
	case 0x3E: SRLHL();		break;
	case 0x3F: SRLr_r(Reg::a);	break;
		// 0x40
	case 0x40: BITbr(m_Register[Reg::b], B0);	break;
	case 0x41: BITbr(m_Register[Reg::c], B0);	break;
	case 0x42: BITbr(m_Register[Reg::d], B0);	break;
	case 0x43: BITbr(m_Register[Reg::e], B0);	break;
	case 0x44: BITbr(m_Register[Reg::h], B0);	break;
	case 0x45: BITbr(m_Register[Reg::l], B0);	break;
	case 0x46: BIT0_HL();	break;
	case 0x47: BITbr(m_Register[Reg::a], B0);	break;
	case 0x48: BITbr(m_Register[Reg::b], B1);	break;
	case 0x49: BITbr(m_Register[Reg::c], B1);	break;
	case 0x4A: BITbr(m_Register[Reg::d], B1);	break;
	case 0x4B: BITbr(m_Register[Reg::e], B1);	break;
	case 0x4C: BITbr(m_Register[Reg::h], B1);	break;
	case 0x4D: BITbr(m_Register[Reg::l], B1);	break;
	case 0x4E: BIT1_HL();	break;
	case 0x4F: BITbr(m_Register[Reg::a], B1);	break;
		// 0x50
	case 0x50: BITbr(m_Register[Reg::b], B2);	break;
	case 0x51: BITbr(m_Register[Reg::c], B2);	break;
	case 0x52: BITbr(m_Register[Reg::d], B2);	break;
	case 0x53: BITbr(m_Register[Reg::e], B2);	break;
	case 0x54: BITbr(m_Register[Reg::h], B2);	break;
	case 0x55: BITbr(m_Register[Reg::l], B2);	break;
	case 0x56: BIT2_HL();	break;
	case 0x57: BITbr(m_Register[Reg::a], B2);	break;
	case 0x58: BITbr(m_Register[Reg::b], B3);	break;
	case 0x59: BITbr(m_Register[Reg::c], B3);	break;
	case 0x5A: BITbr(m_Register[Reg::d], B3);	break;
	case 0x5B: BITbr(m_Register[Reg::e], B3);	break;
	case 0x5C: BITbr(m_Register[Reg::h], B3);	break;
	case 0x5D: BITbr(m_Register[Reg::l], B3);	break;
	case 0x5E: BIT3_HL();	break;
	case 0x5F: BITbr(m_Register[Reg::a], B3);	break;
		// 0x60
	case 0x60: BITbr(m_Register[Reg::b], B4);	break;
	case 0x61: BITbr(m_Register[Reg::c], B4);	break;
	case 0x62: BITbr(m_Register[Reg::d], B4);	break;
	case 0x63: BITbr(m_Register[Reg::e], B4);	break;
	case 0x64: BITbr(m_Register[Reg::h], B4);	break;
	case 0x65: BITbr(m_Register[Reg::l], B4);	break;
	case 0x66: BIT4_HL();	break;
	case 0x67: BITbr(m_Register[Reg::a], B4);	break;
	case 0x68: BITbr(m_Register[Reg::b], B5);	break;
	case 0x69: BITbr(m_Register[Reg::c], B5);	break;
	case 0x6A: BITbr(m_Register[Reg::d], B5);	break;
	case 0x6B: BITbr(m_Register[Reg::e], B5);	break;
	case 0x6C: BITbr(m_Register[Reg::h], B5);	break;
	case 0x6D: BITbr(m_Register[Reg::l], B5);	break;
	case 0x6E: BIT5_HL();	break;
	case 0x6F: BITbr(m_Register[Reg::a], B5);	break;
		// 0x70
	case 0x70: BITbr(m_Register[Reg::b], B6);	break;
	case 0x71: BITbr(m_Register[Reg::c], B6);	break;
	case 0x72: BITbr(m_Register[Reg::d], B6);	break;
	case 0x73: BITbr(m_Register[Reg::e], B6);	break;
	case 0x74: BITbr(m_Register[Reg::h], B6);	break;
	case 0x75: BITbr(m_Register[Reg::l], B6);	break;
	case 0x76: BIT6_HL();	break;
	case 0x77: BITbr(m_Register[Reg::a], B6);	break;
	case 0x78: BITbr(m_Register[Reg::b], B7);	break;
	case 0x79: BITbr(m_Register[Reg::c], B7);	break;
	case 0x7A: BITbr(m_Register[Reg::d], B7);	break;
	case 0x7B: BITbr(m_Register[Reg::e], B7);	break;
	case 0x7C: BITbr(m_Register[Reg::h], B7);	break;
	case 0x7D: BITbr(m_Register[Reg::l], B7);	break;
	case 0x7E: BIT7_HL();	break;
	case 0x7F: BITbr(m_Register[Reg::a], B7);	break;
		// 0x80
	case 0x80: RESET_BIT(m_Register[Reg::b], B0); m_ClockM = 8;	break;
	case 0x81: RESET_BIT(m_Register[Reg::c], B0); m_ClockM = 8;	break;
	case 0x82: RESET_BIT(m_Register[Reg::d], B0); m_ClockM = 8;	break;
	case 0x83: RESET_BIT(m_Register[Reg::e], B0); m_ClockM = 8;	break;
	case 0x84: RESET_BIT(m_Register[Reg::h], B0); m_ClockM = 8;	break;
	case 0x85: RESET_BIT(m_Register[Reg::l], B0); m_ClockM = 8;	break;
	case 0x86: RES0_HL();	break;
	case 0x87: RESET_BIT(m_Register[Reg::a], B0); m_ClockM = 8;	break;
	case 0x88: RESET_BIT(m_Register[Reg::b], B1); m_ClockM = 8;	break;
	case 0x89: RESET_BIT(m_Register[Reg::c], B1); m_ClockM = 8;	break;
	case 0x8A: RESET_BIT(m_Register[Reg::d], B1); m_ClockM = 8;	break;
	case 0x8B: RESET_BIT(m_Register[Reg::e], B1); m_ClockM = 8;	break;
	case 0x8C: RESET_BIT(m_Register[Reg::h], B1); m_ClockM = 8;	break;
	case 0x8D: RESET_BIT(m_Register[Reg::l], B1); m_ClockM = 8;	break;
	case 0x8E: RES1_HL();	break;
	case 0x8F: RESET_BIT(m_Register[Reg::a], B1); m_ClockM = 8;	break;
		// 0x90
	case 0x90: RESET_BIT(m_Register[Reg::b], B2); m_ClockM = 8;	break;
	case 0x91: RESET_BIT(m_Register[Reg::c], B2); m_ClockM = 8;	break;
	case 0x92: RESET_BIT(m_Register[Reg::d], B2); m_ClockM = 8;	break;
	case 0x93: RESET_BIT(m_Register[Reg::e], B2); m_ClockM = 8;	break;
	case 0x94: RESET_BIT(m_Register[Reg::h], B2); m_ClockM = 8;	break;
	case 0x95: RESET_BIT(m_Register[Reg::l], B2); m_ClockM = 8;	break;
	case 0x96: RES2_HL();	break;
	case 0x97: RESET_BIT(m_Register[Reg::a], B2); m_ClockM = 8;	break;
	case 0x98: RESET_BIT(m_Register[Reg::b], B3); m_ClockM = 8;	break;
	case 0x99: RESET_BIT(m_Register[Reg::c], B3); m_ClockM = 8;	break;
	case 0x9A: RESET_BIT(m_Register[Reg::d], B3); m_ClockM = 8;	break;
	case 0x9B: RESET_BIT(m_Register[Reg::e], B3); m_ClockM = 8;	break;
	case 0x9C: RESET_BIT(m_Register[Reg::h], B3); m_ClockM = 8;	break;
	case 0x9D: RESET_BIT(m_Register[Reg::l], B3); m_ClockM = 8;	break;
	case 0x9E: RES3_HL();	break;
	case 0x9F: RESET_BIT(m_Register[Reg::a], B3); m_ClockM = 8;	break;
		// 0xA0
	case 0xA0: RESET_BIT(m_Register[Reg::b], B4); m_ClockM = 8;	break;
	case 0xA1: RESET_BIT(m_Register[Reg::c], B4); m_ClockM = 8;	break;
	case 0xA2: RESET_BIT(m_Register[Reg::d], B4); m_ClockM = 8;	break;
	case 0xA3: RESET_BIT(m_Register[Reg::e], B4); m_ClockM = 8;	break;
	case 0xA4: RESET_BIT(m_Register[Reg::h], B4); m_ClockM = 8;	break;
	case 0xA5: RESET_BIT(m_Register[Reg::l], B4); m_ClockM = 8;	break;
	case 0xA6: RES4_HL();	break;
	case 0xA7: RESET_BIT(m_Register[Reg::a], B4); m_ClockM = 8;	break;
	case 0xA8: RESET_BIT(m_Register[Reg::b], B5); m_ClockM = 8;	break;
	case 0xA9: RESET_BIT(m_Register[Reg::c], B5); m_ClockM = 8;	break;
	case 0xAA: RESET_BIT(m_Register[Reg::d], B5); m_ClockM = 8;	break;
	case 0xAB: RESET_BIT(m_Register[Reg::e], B5); m_ClockM = 8;	break;
	case 0xAC: RESET_BIT(m_Register[Reg::h], B5); m_ClockM = 8;	break;
	case 0xAD: RESET_BIT(m_Register[Reg::l], B5); m_ClockM = 8;	break;
	case 0xAE: RES5_HL();	break;
	case 0xAF: RESET_BIT(m_Register[Reg::a], B5); m_ClockM = 8;	break;
		// 0xB0
	case 0xB0: RESET_BIT(m_Register[Reg::b], B6); m_ClockM = 8;	break;
	case 0xB1: RESET_BIT(m_Register[Reg::c], B6); m_ClockM = 8;	break;
	case 0xB2: RESET_BIT(m_Register[Reg::d], B6); m_ClockM = 8;	break;
	case 0xB3: RESET_BIT(m_Register[Reg::e], B6); m_ClockM = 8;	break;
	case 0xB4: RESET_BIT(m_Register[Reg::h], B6); m_ClockM = 8;	break;
	case 0xB5: RESET_BIT(m_Register[Reg::l], B6); m_ClockM = 8;	break;
	case 0xB6: RES6_HL();	break;
	case 0xB7: RESET_BIT(m_Register[Reg::a], B6); m_ClockM = 8;	break;
	case 0xB8: RESET_BIT(m_Register[Reg::b], B7); m_ClockM = 8;	break;
	case 0xB9: RESET_BIT(m_Register[Reg::c], B7); m_ClockM = 8;	break;
	case 0xBA: RESET_BIT(m_Register[Reg::d], B7); m_ClockM = 8;	break;
	case 0xBB: RESET_BIT(m_Register[Reg::e], B7); m_ClockM = 8;	break;
	case 0xBC: RESET_BIT(m_Register[Reg::h], B7); m_ClockM = 8;	break;
	case 0xBD: RESET_BIT(m_Register[Reg::l], B7); m_ClockM = 8;	break;
	case 0xBE: RES7_HL();	break;
	case 0xBF: RESET_BIT(m_Register[Reg::a], B7); m_ClockM = 8;	break;
		// 0xC0
	case 0xC0: SET_BIT(m_Register[Reg::b], B0); m_ClockM = 8;	break;
	case 0xC1: SET_BIT(m_Register[Reg::c], B0); m_ClockM = 8;	break;
	case 0xC2: SET_BIT(m_Register[Reg::d], B0); m_ClockM = 8;	break;
	case 0xC3: SET_BIT(m_Register[Reg::e], B0); m_ClockM = 8;	break;
	case 0xC4: SET_BIT(m_Register[Reg::h], B0); m_ClockM = 8;	break;
	case 0xC5: SET_BIT(m_Register[Reg::l], B0); m_ClockM = 8;	break;
	case 0xC6: SET0_HL();	break;
	case 0xC7: SET_BIT(m_Register[Reg::a], B0); m_ClockM = 8;	break;
	case 0xC8: SET_BIT(m_Register[Reg::b], B1); m_ClockM = 8;	break;
	case 0xC9: SET_BIT(m_Register[Reg::c], B1); m_ClockM = 8;	break;
	case 0xCA: SET_BIT(m_Register[Reg::d], B1); m_ClockM = 8;	break;
	case 0xCB: SET_BIT(m_Register[Reg::e], B1); m_ClockM = 8;	break;
	case 0xCC: SET_BIT(m_Register[Reg::h], B1); m_ClockM = 8;	break;
	case 0xCD: SET_BIT(m_Register[Reg::l], B1); m_ClockM = 8;	break;
	case 0xCE: SET1_HL();	break;
	case 0xCF: SET_BIT(m_Register[Reg::a], B1); m_ClockM = 8;	break;
		// 0xD0
	case 0xD0: SET_BIT(m_Register[Reg::b], B2); m_ClockM = 8;	break;
	case 0xD1: SET_BIT(m_Register[Reg::c], B2); m_ClockM = 8;	break;
	case 0xD2: SET_BIT(m_Register[Reg::d], B2); m_ClockM = 8;	break;
	case 0xD3: SET_BIT(m_Register[Reg::e], B2); m_ClockM = 8;	break;
	case 0xD4: SET_BIT(m_Register[Reg::h], B2); m_ClockM = 8;	break;
	case 0xD5: SET_BIT(m_Register[Reg::l], B2); m_ClockM = 8;	break;
	case 0xD6: SET2_HL();	break;
	case 0xD7: SET_BIT(m_Register[Reg::a], B2); m_ClockM = 8;	break;
	case 0xD8: SET_BIT(m_Register[Reg::b], B3); m_ClockM = 8;	break;
	case 0xD9: SET_BIT(m_Register[Reg::c], B3); m_ClockM = 8;	break;
	case 0xDA: SET_BIT(m_Register[Reg::d], B3); m_ClockM = 8;	break;
	case 0xDB: SET_BIT(m_Register[Reg::e], B3); m_ClockM = 8;	break;
	case 0xDC: SET_BIT(m_Register[Reg::h], B3); m_ClockM = 8;	break;
	case 0xDD: SET_BIT(m_Register[Reg::l], B3); m_ClockM = 8;	break;
	case 0xDE: SET3_HL();	break;
	case 0xDF: SET_BIT(m_Register[Reg::a], B3); m_ClockM = 8;	break;
		// 0xE0
	case 0xE0: SET_BIT(m_Register[Reg::b], B4); m_ClockM = 8;	break;
	case 0xE1: SET_BIT(m_Register[Reg::c], B4); m_ClockM = 8;	break;
	case 0xE2: SET_BIT(m_Register[Reg::d], B4); m_ClockM = 8;	break;
	case 0xE3: SET_BIT(m_Register[Reg::e], B4); m_ClockM = 8;	break;
	case 0xE4: SET_BIT(m_Register[Reg::h], B4); m_ClockM = 8;	break;
	case 0xE5: SET_BIT(m_Register[Reg::l], B4); m_ClockM = 8;	break;
	case 0xE6: SET4_HL();	break;
	case 0xE7: SET_BIT(m_Register[Reg::a], B4); m_ClockM = 8;	break;
	case 0xE8: SET_BIT(m_Register[Reg::b], B5); m_ClockM = 8;	break;
	case 0xE9: SET_BIT(m_Register[Reg::c], B5); m_ClockM = 8;	break;
	case 0xEA: SET_BIT(m_Register[Reg::d], B5); m_ClockM = 8;	break;
	case 0xEB: SET_BIT(m_Register[Reg::e], B5); m_ClockM = 8;	break;
	case 0xEC: SET_BIT(m_Register[Reg::h], B5); m_ClockM = 8;	break;
	case 0xED: SET_BIT(m_Register[Reg::l], B5); m_ClockM = 8;	break;
	case 0xEE: SET5_HL();	break;
	case 0xEF: SET_BIT(m_Register[Reg::a], B5); m_ClockM = 8;	break;
		// 0xF0
	case 0xF0: SET_BIT(m_Register[Reg::b], B6); m_ClockM = 8;	break;
	case 0xF1: SET_BIT(m_Register[Reg::c], B6); m_ClockM = 8;	break;
	case 0xF2: SET_BIT(m_Register[Reg::d], B6); m_ClockM = 8;	break;
	case 0xF3: SET_BIT(m_Register[Reg::e], B6); m_ClockM = 8;	break;
	case 0xF4: SET_BIT(m_Register[Reg::h], B6); m_ClockM = 8;	break;
	case 0xF5: SET_BIT(m_Register[Reg::l], B6); m_ClockM = 8;	break;
	case 0xF6: SET6_HL();	break;
	case 0xF7: SET_BIT(m_Register[Reg::a], B6); m_ClockM = 8;	break;
	case 0xF8: SET_BIT(m_Register[Reg::b], B7); m_ClockM = 8;	break;
	case 0xF9: SET_BIT(m_Register[Reg::c], B7); m_ClockM = 8;	break;
	case 0xFA: SET_BIT(m_Register[Reg::d], B7); m_ClockM = 8;	break;
	case 0xFB: SET_BIT(m_Register[Reg::e], B7); m_ClockM = 8;	break;
	case 0xFC: SET_BIT(m_Register[Reg::h], B7); m_ClockM = 8;	break;
	case 0xFD: SET_BIT(m_Register[Reg::l], B7); m_ClockM = 8;	break;
	case 0xFE: SET7_HL();	break;
	case 0xFF: SET_BIT(m_Register[Reg::a], B7); m_ClockM = 8;	break;
	default: OutputDebug();
		break;
	}
}

#pragma region Load_Store_8b

// Read byte from memory at HL and store in register
void GameBoyZ80::LDrHLm(Reg::Reg r) { m_Register[r] = m_pMemory->ReadByte(m_Register16[Reg16::hl]); m_ClockM = 8; }

// Store number(byte) in register
void GameBoyZ80::LDrn(Reg::Reg r) { m_Register[r] = m_pMemory->ReadByte(m_PC); ++m_PC; m_ClockM = 8; }

// Write byte to memory at HL from register
void GameBoyZ80::LDHLmr(Reg::Reg r) { m_pMemory->WriteByte(m_Register16[Reg16::hl], m_Register[r]); m_ClockM = 8; }

// Store value at number to memory at HL
void GameBoyZ80::LDHLmn() { m_pMemory->WriteByte(m_Register16[Reg16::hl], m_pMemory->ReadByte(m_PC)); m_PC++; m_ClockM = 12; }

// Load register 'A' from memory at register-pair
void GameBoyZ80::LDABCm() { m_Register[Reg::a] = m_pMemory->ReadByte(m_Register16[Reg16::bc]); m_ClockM = 8; }
void GameBoyZ80::LDADEm() { m_Register[Reg::a] = m_pMemory->ReadByte(m_Register16[Reg16::de]); m_ClockM = 8; }

// Store register 'A' in memory at register-pair
void GameBoyZ80::LDBCmA() { m_pMemory->WriteByte(m_Register16[Reg16::bc], m_Register[Reg::a]); m_ClockM = 8; }
void GameBoyZ80::LDDEmA() { m_pMemory->WriteByte(m_Register16[Reg16::de], m_Register[Reg::a]); m_ClockM = 8; }

// Load register 'A' from memory at position
void GameBoyZ80::LDAmm() { m_Register[Reg::a] = m_pMemory->ReadByte(m_pMemory->ReadWord(m_PC)); m_PC += 2; m_ClockM = 16; }

// Store register 'A' in memory at position
void GameBoyZ80::LDmmA() { m_pMemory->WriteByte(m_pMemory->ReadWord(m_PC), m_Register[Reg::a]); m_PC += 2; m_ClockM = 16; }

// Load register 'A' from 0xFF00 + register 'C'
void GameBoyZ80::LDCmA() { m_Register[Reg::a] = m_pMemory->ReadByte(0xFF00 + m_Register[Reg::c]); m_ClockM = 8; }

// Store register 'A' in memory at 0xFF00 + register 'C'
void GameBoyZ80::LDACm() { m_pMemory->WriteByte(0xFF00 + m_Register[Reg::c], m_Register[Reg::a]); m_ClockM = 8; }

// Load register 'A' from memory at 0xFF00 + number 
void GameBoyZ80::LDHmA() { m_Register[Reg::a] = m_pMemory->ReadByte(0xFF00 + m_pMemory->ReadByte(m_PC)); ++m_PC; m_ClockM = 12; }

// Write register 'A' to memory at 0xFF00 + number 
void GameBoyZ80::LDHAm() { m_pMemory->WriteByte(0xFF00 + m_pMemory->ReadByte(m_PC), m_Register[Reg::a]); ++m_PC; m_ClockM = 12; }

#pragma endregion

#pragma region Load_Store_16b

// Store register-pair from memory at number
void GameBoyZ80::LDr16nn(Reg16::Reg16 r16) { m_Register16[r16] = m_pMemory->ReadWord(m_PC); m_PC += 2; m_ClockM = 12; }
void GameBoyZ80::LDSPnn() { m_SP = m_pMemory->ReadWord(m_PC); m_PC += 2; m_ClockM = 12; }

// Write stack pointer to memory
void GameBoyZ80::LDnnSP() { m_pMemory->WriteWord(m_pMemory->ReadWord(m_PC), m_SP); m_PC += 2; m_ClockM = 20; }

// Copy HL to SP 
void GameBoyZ80::LDSPHL() { m_SP = m_Register16[Reg16::hl]; m_ClockM = 8; }

// Push register-pair to memory at stack-pointer -2 
void GameBoyZ80::PUSHBC() { m_SP -= 2; m_pMemory->WriteWord(m_SP, m_Register16[Reg16::bc]); m_ClockM = 16; }
void GameBoyZ80::PUSHDE() { m_SP -= 2; m_pMemory->WriteWord(m_SP, m_Register16[Reg16::de]); m_ClockM = 16; }
void GameBoyZ80::PUSHHL() { m_SP -= 2; m_pMemory->WriteWord(m_SP, m_Register16[Reg16::hl]); m_ClockM = 16; }
void GameBoyZ80::PUSHAF() { m_SP -= 2; m_pMemory->WriteWord(m_SP, m_Register16[Reg16::af]); m_ClockM = 16; }
void GameBoyZ80::PUSHPC() { m_SP -= 2; m_pMemory->WriteWord(m_SP, m_PC); m_ClockM = 16; }

// Pop stack-pointer to register-pair
void GameBoyZ80::POPBC() { m_Register16[Reg16::bc] = m_pMemory->ReadWord(m_SP); m_SP += 2; m_ClockM = 12; }
void GameBoyZ80::POPDE() { m_Register16[Reg16::de] = m_pMemory->ReadWord(m_SP); m_SP += 2; m_ClockM = 12; }
void GameBoyZ80::POPHL() { m_Register16[Reg16::hl] = m_pMemory->ReadWord(m_SP); m_SP += 2; m_ClockM = 12; }

#pragma endregion

#pragma region Arthmetic_8b
// ADD register or value from memory to register 'A'
void GameBoyZ80::ADDr(Reg::Reg r) { ADDvalToA(m_Register[r]); m_ClockM = 4; }
void GameBoyZ80::ADDHL() { ADDvalToA(m_pMemory->ReadByte(m_Register16[Reg16::hl])); m_ClockM = 8; }
void GameBoyZ80::ADDn() { ADDvalToA(m_pMemory->ReadByte(m_PC++)); m_ClockM = 8; }

// ADD register or value from memory to register 'A' and add carry
void GameBoyZ80::ADCr(Reg::Reg r) { ADCvalToA(m_Register[r]); m_ClockM = 4; }
void GameBoyZ80::ADCHL() { ADCvalToA(m_pMemory->ReadByte(m_Register16[Reg16::hl])); m_ClockM = 8; }
void GameBoyZ80::ADCn()
{
	ADCvalToA(m_pMemory->ReadByte(m_PC++));
	m_ClockM = 8;
}

// SUB register or value from memory from register 'A'
void GameBoyZ80::SUBr(Reg::Reg r) { SUBvalFromA(m_Register[r]); m_ClockM = 4; }
void GameBoyZ80::SUBHL() { SUBvalFromA(m_pMemory->ReadByte(m_Register16[Reg16::hl])); m_ClockM = 8; }
void GameBoyZ80::SUBn() { SUBvalFromA(m_pMemory->ReadByte(m_PC++)); m_ClockM = 8; }

// SUB register or value from memory from register 'A' and sub carry
void GameBoyZ80::SBCr(const Reg::Reg r) { SBCvalFromA(m_Register[r]); m_ClockM = 4; }
void GameBoyZ80::SBCHL() { SBCvalFromA(m_pMemory->ReadByte(m_Register16[Reg16::hl])); m_ClockM = 8; }
void GameBoyZ80::SBCn() { SBCvalFromA(m_pMemory->ReadByte(m_PC++)); m_ClockM = 8; }

// AND register or value from memory with register 'A'
void GameBoyZ80::AND(Reg::Reg r) { m_Register[Reg::a] &= m_Register[r]; m_Register[Reg::f] = (m_Register[Reg::a] ? FLAG_H : 0xA0); m_ClockM = 4; }
void GameBoyZ80::ANDHL() { m_Register[Reg::a] &= m_pMemory->ReadByte(m_Register16[Reg16::hl]); m_Register[Reg::f] = (m_Register[Reg::a] ? 0x20 : 0xA0); m_ClockM = 8; }
void GameBoyZ80::ANDn() { m_Register[Reg::a] &= m_pMemory->ReadByte(m_PC++); m_Register[Reg::f] = (m_Register[Reg::a] ? 0x20 : 0xA0); m_ClockM = 8; }

// OR register or value from memory with register 'A'
void GameBoyZ80::OR(Reg::Reg r) { m_Register[Reg::a] |= m_Register[r]; m_Register[Reg::f] = (m_Register[Reg::a] ? 0 : FLAG_Z); m_ClockM = 4; }
void GameBoyZ80::ORHL() { m_Register[Reg::a] |= m_pMemory->ReadByte(m_Register16[Reg16::hl]); m_Register[Reg::f] = (m_Register[Reg::a] ? 0 : FLAG_Z); m_ClockM = 8; }
void GameBoyZ80::ORn() { m_Register[Reg::a] |= m_pMemory->ReadByte(m_PC++); m_Register[Reg::f] = (m_Register[Reg::a] ? 0 : FLAG_Z); m_ClockM = 8; }

// XOR register or value from memory with register 'A'
void GameBoyZ80::XOR(const Reg::Reg r) { m_Register[Reg::a] ^= m_Register[r]; m_Register[Reg::f] = (m_Register[Reg::a] ? 0 : FLAG_Z); m_ClockM = 4; }
void GameBoyZ80::XORHL() { m_Register[Reg::a] ^= m_pMemory->ReadByte(m_Register16[Reg16::hl]); m_Register[Reg::f] = (m_Register[Reg::a] ? 0 : FLAG_Z); m_ClockM = 8; }
void GameBoyZ80::XORn() { m_Register[Reg::a] ^= m_pMemory->ReadByte(m_PC++); m_Register[Reg::f] = (m_Register[Reg::a] ? 0 : FLAG_Z); m_ClockM = 8; }

// SUB register or value from memory from register 'A' and sub carry
void GameBoyZ80::CPHL() { CPval(m_pMemory->ReadByte(m_Register16[Reg16::hl])); m_ClockM = 8; }
void GameBoyZ80::CPn() { CPval(m_pMemory->ReadByte(m_PC++)); m_ClockM = 8; }

#pragma endregion

#pragma region Arthmetic_16b

// INC word
void GameBoyZ80::INCsp() { m_SP += 1; m_ClockM = 8; }

//DEC word
void GameBoyZ80::DECsp() { m_SP -= 1; m_ClockM = 8; }
#pragma endregion

#pragma region Miscellaneous
// SWAP nibbles
void GameBoyZ80::SWAPr_a() { SWAPnibble(&m_Register[Reg::a]); m_ClockM = 8; };
void GameBoyZ80::SWAPr_b() { SWAPnibble(&m_Register[Reg::b]); m_ClockM = 8; };
void GameBoyZ80::SWAPr_c() { SWAPnibble(&m_Register[Reg::c]); m_ClockM = 8; };
void GameBoyZ80::SWAPr_d() { SWAPnibble(&m_Register[Reg::d]); m_ClockM = 8; };
void GameBoyZ80::SWAPr_e() { SWAPnibble(&m_Register[Reg::e]); m_ClockM = 8; };
void GameBoyZ80::SWAPr_h() { SWAPnibble(&m_Register[Reg::h]); m_ClockM = 8; };
void GameBoyZ80::SWAPr_l() { SWAPnibble(&m_Register[Reg::l]); m_ClockM = 8; };
void GameBoyZ80::SWAPHLm() 
{
	unsigned char v = m_pMemory->ReadByte(m_Register16[Reg16::hl]); 
	SWAPnibble(&v); m_pMemory->WriteByte(m_Register16[Reg16::hl], v);
	m_ClockM = 16; 
}

// Invert a
void GameBoyZ80::CPL() { m_Register[Reg::a] ^= 0xFF; m_Register[Reg::f] |= 0x60; m_ClockM = 4; }

// Invert carry flag
void GameBoyZ80::CCF() { unsigned char cFlag = (m_Register[Reg::f] & FLAG_C) ? 0 : FLAG_C; m_Register[Reg::f] = (m_Register[Reg::f] & FLAG_Z) | cFlag; m_ClockM = 4; }

// Set carry flag
void GameBoyZ80::SCF() { m_Register[Reg::f] &= 0x80; m_Register[Reg::f] |= 0x10; m_ClockM = 4; }

// No operation
void GameBoyZ80::NOP() { m_ClockM = 4; }

// Halt CPU until an interrupt occurs
void GameBoyZ80::HALT()
{
	if (m_IntMasterEnable) m_Halt = true;
	else m_HaltInterruptDisabled = true;
	//m_Halt = true;
	//m_HaltInterruptDisabled = true;
	m_ClockM = 4;
}

// Halt CPU until button pressed
void GameBoyZ80::STOP() 
{
	m_Stop = true; m_ClockM = 4;

	if (model == Device::CGB)
	{
		m_CGBSpeedMode = !m_CGBSpeedMode;
		byte * f = &m_pMemory->m_Memory[0xFF4D];
		*f = m_CGBSpeedMode ? 0x80 : 0x00;
		//printf("CGB Speed : %01d", m_CGBSpeedMode);
	}
}

// Disable interrupts
void GameBoyZ80::DI() { m_SetInterrupt = B7 | B6; m_ClockM = 4; }

// Enable interrupts
void GameBoyZ80::EI() { m_SetInterrupt = B7 | B6 | B0; m_ClockM = 4; }

#pragma endregion

#pragma region Rotates_Shifts

void GameBoyZ80::RLCA()		{ RLCr(Reg::a);	m_Register[Reg::f] &= FLAG_C; m_ClockM = 4; }
void GameBoyZ80::RLA()		{ RLr(Reg::a);	m_Register[Reg::f] &= FLAG_C; m_ClockM = 4; }
void GameBoyZ80::RRCA()		{ RRCr(Reg::a);	m_Register[Reg::f] &= FLAG_C; m_ClockM = 4; }
void GameBoyZ80::RRA()		{ RRr(Reg::a);	m_Register[Reg::f] &= FLAG_C; m_ClockM = 4; }

// Shift n left into carry. LSB set to 0
void GameBoyZ80::SLAr_a() { SLAr_r(Reg::a); m_ClockM = 8; }
void GameBoyZ80::SLAr_b() { SLAr_r(Reg::b); m_ClockM = 8; }
void GameBoyZ80::SLAr_c() { SLAr_r(Reg::c); m_ClockM = 8; }
void GameBoyZ80::SLAr_d() { SLAr_r(Reg::d); m_ClockM = 8; }
void GameBoyZ80::SLAr_e() { SLAr_r(Reg::e); m_ClockM = 8; }
void GameBoyZ80::SLAr_h() { SLAr_r(Reg::h); m_ClockM = 8; }
void GameBoyZ80::SLAr_l() { SLAr_r(Reg::l); m_ClockM = 8; }

void GameBoyZ80::SRAr_a() { SRAr_r(Reg::a); m_ClockM = 8; }
void GameBoyZ80::SRAr_b() { SRAr_r(Reg::b); m_ClockM = 8; }
void GameBoyZ80::SRAr_c() { SRAr_r(Reg::c); m_ClockM = 8; }
void GameBoyZ80::SRAr_d() { SRAr_r(Reg::d); m_ClockM = 8; }
void GameBoyZ80::SRAr_e() { SRAr_r(Reg::e); m_ClockM = 8; }
void GameBoyZ80::SRAr_h() { SRAr_r(Reg::h); m_ClockM = 8; }
void GameBoyZ80::SRAr_l() { SRAr_r(Reg::l); m_ClockM = 8; }

#pragma endregion

#pragma region Bit_Opcodes

// Check bit in register or HL
void GameBoyZ80::BIT0_HL() { BITbr(0x1, m_pMemory->ReadByte(m_Register16[Reg16::hl])); m_ClockM = 12; }
void GameBoyZ80::BIT1_HL() { BITbr(0x2, m_pMemory->ReadByte(m_Register16[Reg16::hl])); m_ClockM = 12; }
void GameBoyZ80::BIT2_HL() { BITbr(0x4, m_pMemory->ReadByte(m_Register16[Reg16::hl])); m_ClockM = 12; }
void GameBoyZ80::BIT3_HL() { BITbr(0x8, m_pMemory->ReadByte(m_Register16[Reg16::hl])); m_ClockM = 12; }
void GameBoyZ80::BIT4_HL() { BITbr(0x10, m_pMemory->ReadByte(m_Register16[Reg16::hl])); m_ClockM = 12; }
void GameBoyZ80::BIT5_HL() { BITbr(0x20, m_pMemory->ReadByte(m_Register16[Reg16::hl])); m_ClockM = 12; }
void GameBoyZ80::BIT6_HL() { BITbr(0x40, m_pMemory->ReadByte(m_Register16[Reg16::hl])); m_ClockM = 12; }
void GameBoyZ80::BIT7_HL() { BITbr(0x80, m_pMemory->ReadByte(m_Register16[Reg16::hl])); m_ClockM = 12; }

// Set bit in register or HL
void GameBoyZ80::SET0_HL() { m_pMemory->WriteByte(m_Register16[Reg16::hl], m_pMemory->ReadByte(m_Register16[Reg16::hl]) | 0x1); m_ClockM = 16; }
void GameBoyZ80::SET1_HL() { m_pMemory->WriteByte(m_Register16[Reg16::hl], m_pMemory->ReadByte(m_Register16[Reg16::hl]) | 0x2); m_ClockM = 16; }
void GameBoyZ80::SET2_HL() { m_pMemory->WriteByte(m_Register16[Reg16::hl], m_pMemory->ReadByte(m_Register16[Reg16::hl]) | 0x4); m_ClockM = 16; }
void GameBoyZ80::SET3_HL() { m_pMemory->WriteByte(m_Register16[Reg16::hl], m_pMemory->ReadByte(m_Register16[Reg16::hl]) | 0x8); m_ClockM = 16; }
void GameBoyZ80::SET4_HL() { m_pMemory->WriteByte(m_Register16[Reg16::hl], m_pMemory->ReadByte(m_Register16[Reg16::hl]) | 0x10); m_ClockM = 16; }
void GameBoyZ80::SET5_HL() { m_pMemory->WriteByte(m_Register16[Reg16::hl], m_pMemory->ReadByte(m_Register16[Reg16::hl]) | 0x20); m_ClockM = 16; }
void GameBoyZ80::SET6_HL() { m_pMemory->WriteByte(m_Register16[Reg16::hl], m_pMemory->ReadByte(m_Register16[Reg16::hl]) | 0x40); m_ClockM = 16; }
void GameBoyZ80::SET7_HL() { m_pMemory->WriteByte(m_Register16[Reg16::hl], m_pMemory->ReadByte(m_Register16[Reg16::hl]) | 0x80); m_ClockM = 16; }

//// Reset bit in regsiter or HL
void GameBoyZ80::RES0_HL() { m_pMemory->WriteByte(m_Register16[Reg16::hl], m_pMemory->ReadByte(m_Register16[Reg16::hl]) & 0xFE); m_ClockM = 16; }
void GameBoyZ80::RES1_HL() { m_pMemory->WriteByte(m_Register16[Reg16::hl], m_pMemory->ReadByte(m_Register16[Reg16::hl]) & 0xFD); m_ClockM = 16; }
void GameBoyZ80::RES2_HL() { m_pMemory->WriteByte(m_Register16[Reg16::hl], m_pMemory->ReadByte(m_Register16[Reg16::hl]) & 0xFB); m_ClockM = 16; }
void GameBoyZ80::RES3_HL() { m_pMemory->WriteByte(m_Register16[Reg16::hl], m_pMemory->ReadByte(m_Register16[Reg16::hl]) & 0xF7); m_ClockM = 16; }
void GameBoyZ80::RES4_HL() { m_pMemory->WriteByte(m_Register16[Reg16::hl], m_pMemory->ReadByte(m_Register16[Reg16::hl]) & 0xEF); m_ClockM = 16; }
void GameBoyZ80::RES5_HL() { m_pMemory->WriteByte(m_Register16[Reg16::hl], m_pMemory->ReadByte(m_Register16[Reg16::hl]) & 0xDF); m_ClockM = 16; }
void GameBoyZ80::RES6_HL() { m_pMemory->WriteByte(m_Register16[Reg16::hl], m_pMemory->ReadByte(m_Register16[Reg16::hl]) & 0xBF); m_ClockM = 16; }
void GameBoyZ80::RES7_HL() { m_pMemory->WriteByte(m_Register16[Reg16::hl], m_pMemory->ReadByte(m_Register16[Reg16::hl]) & 0x7F); m_ClockM = 16; }

#pragma endregion

#pragma region Jumps

// Jump to addr
void GameBoyZ80::JP() { m_PC = m_pMemory->ReadWord(m_PC); m_ClockM = 16; /*printf("Jump %d\n", m_PC);*/ }

// Jump to addr if condition is true
void GameBoyZ80::JPNZ()	{ m_ClockM = 12; if ((m_Register[Reg::f] & 0x80) == 0x00) JP(); else m_PC += 2; }
void GameBoyZ80::JPZ()	{ m_ClockM = 12; if ((m_Register[Reg::f] & 0x80) == 0x80) JP(); else m_PC += 2; }
void GameBoyZ80::JPNC() { m_ClockM = 12; if ((m_Register[Reg::f] & 0x10) == 0x00) JP(); else m_PC += 2; }
void GameBoyZ80::JPC()	{ m_ClockM = 12; if ((m_Register[Reg::f] & 0x10) == 0x10) JP(); else m_PC += 2; }

// Jump to addr in HL
void GameBoyZ80::JPHL() { m_PC = m_Register16[Reg16::hl]; m_ClockM = 4; }

// Add n to addr, then jump
void GameBoyZ80::JR() { unsigned short e = m_pMemory->ReadByte(m_PC++); if (e > 127) e = -((~e + 1) & 0xFF); m_PC += e; m_ClockM = 12; }

// If condition true, add n to addr, then jump
void GameBoyZ80::JRNZ() { if ((m_Register[Reg::f] & FLAG_Z) == 0x00)	JR(); else { ++m_PC; m_ClockM = 8; } }
void GameBoyZ80::JRZ()	{ if ((m_Register[Reg::f] & FLAG_Z) == 0x80)	JR(); else { ++m_PC; m_ClockM = 8; } }
void GameBoyZ80::JRNC() { if ((m_Register[Reg::f] & FLAG_C) == 0x00)	JR(); else { ++m_PC; m_ClockM = 8; } }
void GameBoyZ80::JRC()	{ if ((m_Register[Reg::f] & FLAG_C) == 0x10)	JR(); else { ++m_PC; m_ClockM = 8; } }

#pragma endregion

#pragma region Calls

// Push next instr onto stack, then jump
void GameBoyZ80::CALLnn() { m_SP -= 2; m_pMemory->WriteWord(m_SP, m_PC + 2); m_PC = m_pMemory->ReadWord(m_PC); m_ClockM = 24; }

// If condition true, push next instr onto stack, then jump
void GameBoyZ80::CALLNZ()	{ if ((m_Register[Reg::f] & 0x80) == 0x00)	CALLnn(); else { m_PC += 2; m_ClockM = 12; } }
void GameBoyZ80::CALLZ()	{ if ((m_Register[Reg::f] & 0x80) == 0x80)	CALLnn(); else { m_PC += 2; m_ClockM = 12; } }
void GameBoyZ80::CALLNC()	{ if ((m_Register[Reg::f] & 0x10) == 0x00)	CALLnn(); else { m_PC += 2; m_ClockM = 12; } }
void GameBoyZ80::CALLC()	{ if ((m_Register[Reg::f] & 0x10) == 0x10)	CALLnn(); else { m_PC += 2; m_ClockM = 12; } }

#pragma endregion

#pragma region Restarts

// Push addr to stack, jump to addr $0000 + n
void GameBoyZ80::RST00() { PUSHPC(); m_PC = 0x00; m_ClockM = 16; }
void GameBoyZ80::RST08() { PUSHPC(); m_PC = 0x08; m_ClockM = 16; }
void GameBoyZ80::RST10() { PUSHPC(); m_PC = 0x10; m_ClockM = 16; }
void GameBoyZ80::RST18() { PUSHPC(); m_PC = 0x18; m_ClockM = 16; }
void GameBoyZ80::RST20() { PUSHPC(); m_PC = 0x20; m_ClockM = 16; }
void GameBoyZ80::RST28() { PUSHPC(); m_PC = 0x28; m_ClockM = 16; }
void GameBoyZ80::RST30() { PUSHPC(); m_PC = 0x30; m_ClockM = 16; }
void GameBoyZ80::RST38() { PUSHPC(); m_PC = 0x38; m_ClockM = 16; }

// Handle vblank 
void GameBoyZ80::RST40() { m_IntMasterEnable = false; PUSHPC(); m_PC = 0x40; m_ClockM = 12; }

#pragma endregion

#pragma region Returns

// Pop two bytes from stack, jump to that addrs
void GameBoyZ80::RET() { m_PC = m_pMemory->ReadWord(m_SP); m_SP += 2; m_ClockM = 16; }

// RET condition
void GameBoyZ80::RETCon() { m_PC = m_pMemory->ReadWord(m_SP); m_SP += 2; m_ClockM = 20; }

// Return if conditio is met
void GameBoyZ80::RETNZ() { if (!(m_Register[Reg::f] & 0x80)) RETCon(); else { m_ClockM = 8; } }
void GameBoyZ80::RETZ() { if (m_Register[Reg::f] & 0x80) RETCon(); else { m_ClockM = 8; } }
void GameBoyZ80::RETNC() { if (!(m_Register[Reg::f] & 0x10)) RETCon(); else { m_ClockM = 8; } }
void GameBoyZ80::RETC() { if (m_Register[Reg::f] & 0x10) RETCon(); else { m_ClockM = 8; } }

// Retun and enable interrupts
void GameBoyZ80::RETI() { EI(); RET(); }

#pragma endregion

void GameBoyZ80::CheckInterrupts()
{
	if (BIT7(m_SetInterrupt))
	{
		if (BIT6(m_SetInterrupt))
		{
			RESET_BIT(m_SetInterrupt, B6);
			return;
		}
		else
		{
			m_IntMasterEnable = BIT0(m_SetInterrupt) ? true : false;
			m_SetInterrupt = 0;
		}
	}

	if (!m_IntMasterEnable && !m_Halt) return;

	byte _iEnable = m_pMemory->m_IE;
	byte _iFlag = m_pMemory->m_IF;

	if (!(_iEnable & _iFlag)) return;

	if (BIT0(_iEnable) & BIT0(_iFlag)) // V-Blank
	{
		if (!BIT7(m_pMemory->ReadByte(0xFF40))) return;
		m_IntMasterEnable = false;
		m_pMemory->UnsetBit(cINTFlag, B0);
		PUSHPC();
		m_PC = 0x40;
		m_Halt = false;
		return;
	}

	if (BIT1(_iEnable) & BIT1(_iFlag)) // LCD STAT
	{
		if (!BIT7(m_pMemory->ReadByte(0xFF40))) return;
		m_IntMasterEnable = false;
		m_pMemory->UnsetBit(cINTFlag, B1);
		PUSHPC();
		m_PC = 0x48;
		m_Halt = false;
		return;
	}

	if (BIT2(_iEnable) & BIT2(_iFlag)) // Timer
	{
		m_IntMasterEnable = false;
		m_pMemory->UnsetBit(cINTFlag, B2);
		PUSHPC();
		m_PC = 0x50;
		m_Halt = false;
		return;
	}

	if (BIT3(_iEnable) & BIT3(_iFlag)) // Serial
	{
		m_IntMasterEnable = false;
		m_pMemory->UnsetBit(cINTFlag, B3);
		PUSHPC();
		m_PC = 0x58;
		m_Halt = false;
		return;
	}

	if (BIT4(_iEnable) & BIT4(_iFlag)) // Joypad
	{
		m_IntMasterEnable = false;
		m_pMemory->UnsetBit(cINTFlag, B4);
		PUSHPC();
		m_PC = 0x60;
		m_Halt = false;
		return;
	}
}

void GameBoyZ80::UpdateTimingDuringInstr(byte tim)
{
	m_ClockM = 4;
	tim -= 4;
	m_pMemory->UpdateTimer(tim);
	m_pGraphics->Tick(tim);
}