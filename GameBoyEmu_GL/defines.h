#pragma once
#include "stdafx.h"

// Screen dimension constants 
const int SCREEN_WIDTH = 160;
const int SCREEN_HEIGHT = 144;

// Window dimension constants 
const int WINDOW_WIDTH = SCREEN_WIDTH * 5;
const int WINDOW_HEIGHT = SCREEN_HEIGHT * 5;

// Typedefs
typedef unsigned short UI16;
typedef unsigned char byte;
typedef unsigned char UI8;
typedef unsigned int UI32;
typedef signed char SI8;

// Gameboy memory locations
#define cINTEnable 0xFFFF
#define cINTFlag 0xFF0F
#define cSERData 0xFF01
#define cSERControl 0xFF02
#define cZeroPage 0xFF80
#define cJoypad 0xFF00
#define cOAM 0xFE00
#define cEchoRAM 0xE000
#define cIRAM1 0xD000
#define cIRAM0 0xC000
#define cCartRAM 0xA000
#define cBGData2 0x9C00
#define cBGData1 0x9800
#define cCharRam 0x8000
#define cCartROM1 0x4000
#define cCartROM0 0x0150
#define cCartHead 0x0100
#define cTimerDIV 0xFF04
#define cTimerTIMA 0xFF05
#define cTimerTMA 0xFF06
#define cTimerTAC 0xFF07

#define cLCDStat 0xFF41
#define cLYCompare 0xFF45

#define cCartName 0x0134

// Color for drawing
#ifdef _WIN32
struct Color
{
	Color() : R(0), B(0), G(0) {}
	Color(float r, float g, float b) : R(r), B(b), G(g) { }
	Color(int r, int g, int b) : R(r / 255.0f), B(b / 255.0f), G(g / 255.0f) { }
	float R, G, B;
};
#else
struct Color
{
	Color() : R(0), B(0), G(0), A(255) {}
	Color(float r, float g, float b) : R(byte(r * 255)), G(byte(g * 255)), B(byte(b * 255)), A(255) { }
	Color(int r, int g, int b) : R(byte(r)), G(byte(g)), B(byte(b)), A(255) { }
	Color(byte r, byte g, byte b) : R(r), G(g), B(b), A(255) { }
	byte R, G, B, A;
};
#endif

class Memory;
class Graphics;
class GameBoyZ80;
class GBSound;

#define BIT0(value)	((value) & 0x01)
#define BIT1(value)	((value) & 0x02)
#define BIT2(value)	((value) & 0x04)
#define BIT3(value)	((value) & 0x08)
#define BIT4(value)	((value) & 0x10)
#define BIT5(value)	((value) & 0x20)
#define BIT6(value)	((value) & 0x40)
#define BIT7(value)	((value) & 0x80)

#define B0 0x01
#define B1 0x02
#define B2 0x04
#define B3 0x08
#define B4 0x10
#define B5 0x20
#define B6 0x40
#define B7 0x80

#define FLAG_Z 0x80
#define FLAG_N 0x40
#define FLAG_H 0x20
#define FLAG_C 0x10

#define SET_BIT(value, bit) (value |= bit)
#define RESET_BIT(value, bit) (value &= ~bit)
#define MASK(value, bits) (value & bits)

namespace Device
{
	enum Device
	{
		DMG, SGB, CGB
	};
}