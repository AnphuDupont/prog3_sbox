#pragma once

#include "defines.h"  
#include "SaveData.h"

namespace MBCSet
{
	enum MBCSet
	{
		ROMbank, RAMbank, RAMon, Mode
	};
}

namespace MBC3Set
{
	enum MBC3Set
	{
		RTCEnable, RTCLatch, RTCMapped, RTCLatchPrevValue
	};
}

class Memory
{
public:
	~Memory();

	static Memory * GetMemory();
	void Reset();

	void * GetExternalRamPtr() { return &m_ExternalRAM; }

	void SetBit(UI16 addr, UI8 bit);
	void UnsetBit(UI16 addr, UI8 bit);

	unsigned char ReadByte(unsigned short addr);
	unsigned short ReadWord(unsigned short addr);
	byte ReadCGBBanked(UI16 addr, byte bank);

	void WriteByte(unsigned short addr, unsigned char val);
	void WriteWord(unsigned short addr, unsigned short val);

	void LookForSave();

	/* Timer functions */
	void UpdateTimer(UI8 passedCycles);

	inline void SetSaveFlag(UI16 addr, UI16 size) { m_WriteSaveToDisk = true; m_SaveAddress = addr; m_SaveSize = size; }
	bool m_WriteSaveToDisk;
	UI16 m_SaveAddress, m_SaveSize;

	/* Public datamembers */
	static	byte	m_BIOS[256];	// BIOS 
	byte	m_Memory[0xFFFF];		// RAM
	byte	m_ROM[8192 * 1024];		// ROM0 data (support only 4MB, roms exist up to 8MB)

	byte	m_ExternalRAM[2048 * 1024]; // ERAM max 2MB

	bool	m_InBios;
	int m_CartridgeType, m_RomSize, m_RamSize;
	static char m_GameTitle[16];

	byte m_IE, m_IF, m_TAC, m_TIMA, m_TMA;

	UI16 m_MBC1[4];		// MBC settings

	// CGB
	byte m_VramBank;
	byte m_WramBank;
	byte m_CGBVram[0x2000];
	byte m_CGBWram[0x8000];
	Device::Device * m_Model;

private :
	Memory();
	static Memory * m_pMemory;

	GBSound * m_pSound;
	Graphics * m_pGPU;
	GameBoyZ80 * m_pCPU;
	byte m_KEYS[2];

	unsigned int m_RomOffset, m_RamOffset;


	/* Timer */
	static const UI16 m_TimerControlSpeed[4];
	UI16 m_TimerDivCounter, m_TimerTimaCounter;

#define _RTC_S 0x08
#define _RTC_M 0x09
#define _RTC_H 0x0A
#define _RTC_DL 0x0B
#define _RTC_DH 0x0C

	// MBC3 RealTimeClock
	UI8 m_MBC3_RTC[16]; // Registers
	UI8 m_MBC3[4];		// MBC settings
	bool m_MBC3_RTC_Enabled, m_MBC3_RTC_Latch;
};

