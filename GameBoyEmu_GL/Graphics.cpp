#include "stdafx.h"
#include "Graphics.h"
#include "Memory.h"
#include "defines.h"
#include "GameBoyZ80.h"

#define LCDC 0xFF40

bool Graphics::DrawDone = false;
Graphics * Graphics::m_Graphics = 0;
const Color Graphics::m_Palette[4] =
{
	//Color(1.0f, 1.0f, 1.0f),
	//	Color(0.75f, 0.75f, 0.75f),
	//	Color(0.35f, 0.35f, 0.35f),
	//	Color(0, 0, 0)
	//Color(0.843f, 0.91f, 0.58f),
	//Color(0.682f, 0.769f, 0.251f),
	//Color(0.322f, 0.498f, 0.224f),
	//Color(0.125f, 0.275f, 0.192f)
	Color(224, 248, 208),
	Color(136, 192, 112),
	Color(52, 104, 86),
	Color(8, 24, 32)
};

Graphics::Graphics() :
m_ModeClock(0),
m_Mode(0),
m_Scanline(0),
m_ScrollY(0),
m_ScrollX(0),
m_Debug(false)
{
	m_Graphics = this;
	m_pMMU = Memory::GetMemory();
}

Graphics::~Graphics()
{
}

void Graphics::Reset()
{
	m_Model = &GameBoyZ80::GetGameBoyZ80()->model;
	DrawDone = false;
	m_Mode = 1;
	m_LCDC = 0x91;
	m_Scanline = 0x90;
	//m_WindowLine = m_Scanline;

	for (int i = 0, n = cOAM; i < 40; ++i, n += 4)
	{
		m_pMMU->m_Memory[n] = 0;
		m_pMMU->m_Memory[n + 1] = 0;
		m_pMMU->m_Memory[n + 2] = 0;
		m_pMMU->m_Memory[n + 3] = 0;
	}

	// Init palettes with default colors, otherwise all palettes are black
	for (byte p = 0; p < 3; p++)
		for (byte i = 0; i < 4; i++)
			m_Palettes[p][i] = m_Palette[i];

	// Init CGB Palette
	//for (byte p = 0; p < 3; p++)
	//	for (byte i = 0; i < 4; i++)
	//		m_Palettes[p][i] = m_Palette[i];
	m_BGPaletteIndex = m_OBJPaletteIndex = 0;
	m_CGBDMA.HBlankDMA = false;
}

Graphics * Graphics::GetGraphics()
{
	if (m_Graphics == 0)
		m_Graphics = new Graphics();
	return m_Graphics;
}

// http://gameboy.mongenel.com/dmg/istat98.txt
void Graphics::Tick(const byte clockM)
{
	if (!MASK(m_LCDC, B7)) return;
	//printf("%02X\t", m_Mode);
	m_ModeClock += clockM;

	switch (m_Mode & 0x3) // 0xFF41 LCDSTAT
	{
	case 2: // OAM read mode
		if (m_ModeClock >= 80)
		{
			m_ModeClock -= 80;
			m_Mode |= 3;
		}
		return;
	case 3: // VRAM read mode
		if (m_ModeClock >= 172)
		{
			m_ModeClock -= 172;
			m_Mode &= 0xFC;
			if (BIT3(m_Mode)) m_pMMU->SetBit(cINTFlag, B1);

			if (BIT7(m_LCDC))
			{
				RenderScanline();
			}
			else
			{
				int ydex = m_Scanline * SCREEN_WIDTH;
				for (int i = 0; i < SCREEN_WIDTH; i++)
				{
					const int index = ydex + i;
					if (index > SCREEN_WIDTH * SCREEN_HEIGHT) continue;
					m_Screen[ydex + i] = 0;
					DisplayBuffer[ydex + i] = m_Palette[0];
				}
			}
		}
		return;
	case 0: // Horizntal blank
		if (m_ModeClock >= 204)
		{
			m_ModeClock -= 204;
			++m_Scanline;

			if (m_Scanline >= SCREEN_HEIGHT)
			{
				m_Mode |= 1;
				if (m_Scanline == SCREEN_HEIGHT)
				{
					DrawDone = true;
					m_pMMU->SetBit(cINTFlag, B0);
					if (BIT4(m_Mode)) m_pMMU->SetBit(cINTFlag, B1);
				}
			}
			else
			{
				m_Mode |= 2;
				if (BIT5(m_Mode)) m_pMMU->SetBit(cINTFlag, B1);
			}

			CompareLYC();

			if (m_CGBDMA.HBlankDMA)
			{
				memcpy(m_pMMU->m_Memory + m_CGBDMA.end,
					m_pMMU->m_Memory + m_CGBDMA.start + m_CGBDMA.offset, 0x10);
				m_CGBDMA.offset += 0x10;
				if (m_CGBDMA.length == m_CGBDMA.offset)
				{
					m_CGBDMA.HBlankDMA = false;
					m_pMMU->m_Memory[0xFF55] = 0;
				}
			}
		}
		return;
	case 1: // Vertical blank
		if (m_ModeClock >= 456)
		{
			m_ModeClock -= 456;
			++m_Scanline;

			if (m_Scanline >= 154)
			{
				m_Mode |= 2;
				m_Scanline = 0;
			}
			else if (m_Scanline == 1)
			{
				m_Scanline = 0;
				m_Mode &= 0xFE;
				if (BIT4(m_Mode)) m_pMMU->SetBit(cINTFlag, B1);
			}
			CompareLYC();
		}
		return;
	}
}

void Graphics::CompareLYC()
{
	// Check if LYC == scanline
	if (m_Scanline == m_pMMU->m_Memory[cLYCompare])
	{
		m_Mode |= B2;
		if (BIT6(m_Mode)) m_pMMU->SetBit(cINTFlag, B1);
	}
	else RESET_BIT(m_Mode, B2);
}

byte Graphics::ReadByte(unsigned short addr)
{
	switch (addr)
	{
	case 0xFF40:
		return m_LCDC;
	case cLCDStat:
		//if (BIT6(m_Mode)) m_Mode |= B3;
		if (!BIT7(m_LCDC)) return m_Mode & 0xFC;
		return m_Mode;
	case 0xFF42:
		return m_ScrollY;
	case 0xFF43:
		return m_ScrollX;
	case 0xFF44:
		if (!BIT7(m_LCDC)) return 0;
		return m_Scanline;
	case cLYCompare:
		return m_pMMU->m_Memory[cLYCompare];
	case 0xFF4A:
	case 0xFF4B:
		return m_pMMU->m_Memory[addr];
	case 0xFF55:
		if (*m_Model == Device::CGB)
		{
			if (m_CGBDMA.HBlankDMA)
				return (m_CGBDMA.length - m_CGBDMA.offset) / 0x10 - 1;
			return m_pMMU->m_Memory[addr];
		}
	default: // Unhandled -> crash !
		return m_pMMU->m_Memory[addr];
	}
}

void Graphics::WriteByte(unsigned short addr, byte val)
{
	switch (addr)
	{
	case 0xFF40:
		m_LCDC = val;
		break;
	case cLCDStat:
		m_Mode = (m_Mode & 0x03) | (val & 0x78);
		break;
	case 0xFF42:
		m_ScrollY = val;
		break;
	case 0xFF43:
		m_ScrollX = val;
		break;
	case 0xFF44:
		m_Scanline = 0;
		m_pMMU->m_Memory[cLCDStat] = (m_pMMU->m_Memory[cLCDStat] & 0xF8) | 2;
		break;
	case cLYCompare:
		m_pMMU->m_Memory[cLYCompare] = val;
		break;
	case 0xFF46: // DMA transfer
	{
		UI16 startAddr = ((val % 0xF2) << 8);
		memcpy(m_pMMU->m_Memory + cOAM, m_pMMU->m_Memory + startAddr, 0xA0);
	}
		break;
	case 0xFF47:
		for (int i = 0; i < 4; i++)
		{
			byte colorSelect = (val >> (i * 2)) & 3;
			m_Palettes[0][i] = m_Palette[colorSelect];
		}
		//printf("BG : %02X\n", val);
		break;
	case 0xFF48:
		for (int i = 0; i < 4; i++)
		{
			byte colorSelect = (val >> (i * 2)) & 3;
			m_Palettes[1][i] = m_Palette[colorSelect];
		}
		//printf("O1 : %02X\n", val);
		break;
	case 0xFF49:
		for (int i = 0; i < 4; i++)
		{
			byte colorSelect = (val >> (i * 2)) & 3;
			m_Palettes[2][i] = m_Palette[colorSelect];
		}
		//printf("O2 : %02X\n", val);
		break;
	case 0xFF4A:
	case 0xFF4B:
		m_pMMU->m_Memory[addr] = val;
		break;

		/* ---------- CGB ----------- */

	case 0xFF51:	// FF51 - HDMA1 - CGB Mode Only - New DMA Source, High
		DMAAddr8[DMAAddr::SourceH] = val;
		//m_pMMU->m_Memory[addr] = val;
		break;
	case 0xFF52:	// FF52 - HDMA2 - CGB Mode Only - New DMA Source, Low
		DMAAddr8[DMAAddr::SourceL] = val;
		//m_pMMU->m_Memory[addr] = val;
		break;
	case 0xFF53:	// FF53 - HDMA3 - CGB Mode Only - New DMA Destination, High
		DMAAddr8[DMAAddr::DestH] = val;
		//m_pMMU->m_Memory[addr] = val;
		break;
	case 0xFF54:	// FF54 - HDMA4 - CGB Mode Only - New DMA Destination, Low
		DMAAddr8[DMAAddr::DestL] = val;
		//m_pMMU->m_Memory[addr] = val;
		break;
	case 0xFF55:	// FF55 - HDMA5 - CGB Mode Only - New DMA Length / Mode / Start
	{
		m_CGBDMA.start = DMAAddr16[0] & 0xFFF0;
		m_CGBDMA.end = (DMAAddr16[1] & 0x1FF0) + 0x8000;
		m_CGBDMA.length = ((val & 0x7F) + 1) * 0x10;
		m_CGBDMA.HBlankDMA = BIT7(val) > 0;
		if (m_CGBDMA.HBlankDMA)
		{
			m_CGBDMA.offset = 0;
			m_pMMU->m_Memory[0xFF55] = val;
		}
		else
		{
			memcpy(m_pMMU->m_Memory + m_CGBDMA.end, m_pMMU->m_Memory + m_CGBDMA.start, m_CGBDMA.length);
			m_pMMU->m_Memory[0xFF55] = 0xFF;
		}
		return;
	}
		break;
	case 0xFF68:	// FF68 - BCPS/BGPI - CGB Mode Only - Background Palette Index
		if (*m_Model == Device::CGB)
			m_BGPaletteIndex = val & 0x3F; // Only Bit 0-5
		m_pMMU->m_Memory[0xFF68] = val;
		break;
	case 0xFF69:	// FF69 - BCPD/BGPD - CGB Mode Only - Background Palette Data
		if (*m_Model == Device::CGB)
		{
			m_ColorPalettesBG[m_BGPaletteIndex] = val;

			// Recalculate color
			const byte idx = m_BGPaletteIndex >> 1;
			const byte idx2 = idx << 1;
			const byte palIndex = m_BGPaletteIndex >> 3;
			const byte palIndex2 = idx % 4;
			m_CGBPaletteBG[palIndex][palIndex2] = Color(m_ColorPalettesBG[idx2] & 0x1F,
				((m_ColorPalettesBG[idx2] & 0xE0) >> 5) | ((m_ColorPalettesBG[idx2 + 1] & 0x3) << 3),
				(m_ColorPalettesBG[idx2 + 1] & 0x7C) >> 2);
			m_CGBPaletteBG[palIndex][palIndex2].R *= 8;
			m_CGBPaletteBG[palIndex][palIndex2].G *= 8;
			m_CGBPaletteBG[palIndex][palIndex2].B *= 8;

			if (BIT7(m_pMMU->m_Memory[0xFF68])) ++m_BGPaletteIndex;
			if (m_BGPaletteIndex == 0x40) m_BGPaletteIndex = 0;
		}
		m_pMMU->m_Memory[addr] = val;
		break;
	case 0xFF6A:	// FF6A - OCPS/OBPI - CGB Mode Only - Sprite Palette Index
		if (*m_Model == Device::CGB)
			m_OBJPaletteIndex = val & 0x3F; // Only Bit 0-5
		m_pMMU->m_Memory[0xFF6A] = val; // Store as copy in memory
		break;
	case 0xFF6B:	// FF6B - OCPD/OBPD - CGB Mode Only - Sprite Palette Data
		if (*m_Model == Device::CGB)
		{
			m_ColorPalettesOBJ[m_OBJPaletteIndex] = val;

			// Recalculate color
			const byte idx = m_OBJPaletteIndex >> 1;
			const byte idx2 = idx << 1;
			const byte palIndex = m_OBJPaletteIndex >> 3;
			const byte palIndex2 = idx % 4;
			m_CGBPaletteOBJ[palIndex][palIndex2] = Color(m_ColorPalettesOBJ[idx2] & 0x1F,
				((m_ColorPalettesOBJ[idx2] & 0xE0) >> 5) | ((m_ColorPalettesOBJ[idx2 + 1] & 0x3) << 3),
				(m_ColorPalettesOBJ[idx2 + 1] & 0x7C) >> 2);
			m_CGBPaletteOBJ[palIndex][palIndex2].R *= 8;
			m_CGBPaletteOBJ[palIndex][palIndex2].G *= 8;
			m_CGBPaletteOBJ[palIndex][palIndex2].B *= 8;

			if (BIT7(m_pMMU->m_Memory[0xFF6A])) ++m_OBJPaletteIndex;
			if (m_OBJPaletteIndex == 0x40) m_OBJPaletteIndex = 0;
		}
		m_pMMU->m_Memory[addr] = val;
		break;
	default: // Unhandled -> crash !
		m_pMMU->m_Memory[addr] = val;
		break;
	}
}

void Graphics::RenderScanline()
{
	if (*m_Model == Device::DMG)
	{
		RenderBackground();
		RenderWindow();
		RenderSprites();
	}
	else if (*m_Model == Device::CGB)
	{
		CGBRenderBackground();
		CGBRenderWindow();
		CGBRenderSprites();
	}
}

void Graphics::RenderBackground()
{
	if (BIT0(m_LCDC))
	{
		UI16 tileDataMemory = 0;
		bool useSignedValues = false;
		if (BIT4(m_LCDC)) tileDataMemory = 0x8000;
		else
		{
			tileDataMemory = 0x8800;
			useSignedValues = true;
		}

		const UI16 backgroundMemory = BIT3(m_LCDC) ? cBGData2 : cBGData1;
		const byte yPos = m_ScrollY + m_Scanline;
		const byte lineNr = (yPos % 8) * 2;
		const UI16 tileRow = byte(yPos / 8) * 32;
		for (byte _pixel = 0; _pixel < SCREEN_WIDTH; _pixel++)
		{
			const byte xPos = m_ScrollX + _pixel;

			const unsigned short tileAddress = backgroundMemory + tileRow + (xPos / 8);
			const signed short tileNumber = useSignedValues ? (signed char)m_pMMU->ReadByte(tileAddress) : m_pMMU->ReadByte(tileAddress);

			unsigned short tileLocation = tileDataMemory;
			if (useSignedValues) tileLocation += (tileNumber + 128) * 16;
			else tileLocation += tileNumber * 16;

			const byte data1 = m_pMMU->ReadByte(tileLocation + lineNr);
			const byte data2 = m_pMMU->ReadByte(tileLocation + lineNr + 1);

			const int colorBit = 1 << (7 - (xPos % 8));
			const UI16 displayAddress = _pixel + m_Scanline * SCREEN_WIDTH;
			if (displayAddress >= SCREEN_WIDTH * SCREEN_HEIGHT) continue;

			const byte color = ((data1 & colorBit) ? 1 : 0) | ((data2 & colorBit) ? 2 : 0);
			m_Screen[displayAddress] = color;
			DisplayBuffer[displayAddress] = m_Palettes[0][color];
		}
	}
	else
	{
		for (byte _pixel = 0; _pixel < SCREEN_WIDTH; _pixel++)
		{
			const UI16 displayAddress = _pixel + m_Scanline * SCREEN_WIDTH;
			m_Screen[displayAddress] = 0;
			DisplayBuffer[displayAddress] = m_Palettes[0][0];
		}
	}
}

void Graphics::RenderWindow()
{
	if (BIT5(m_LCDC))
	{
		UI16 tileDataMemory = 0;
		bool useSignedValues = false;
		if (BIT4(m_LCDC)) tileDataMemory = 0x8000;
		else
		{
			tileDataMemory = 0x8800;
			useSignedValues = true;
		}

		const int windowY = ReadByte(0xFF4A);
		const int windowX = ReadByte(0xFF4B) - 7;

		const UI16 backgroundMemory = BIT6(m_LCDC) ? cBGData2 : cBGData1;
		const byte yPos = m_Scanline - windowY;
		const byte lineNr = (yPos % 8) * 2;
		const UI16 tileRow = byte(yPos / 8) * 32;

		for (byte _pixel = 0; _pixel < SCREEN_WIDTH; _pixel++)
		{
			const byte xPos = _pixel - windowX;
			const int tileAddress = backgroundMemory + tileRow + (xPos / 8);
			const int tileNumber = useSignedValues ? (signed char)m_pMMU->ReadByte(tileAddress) : m_pMMU->ReadByte(tileAddress);

			unsigned short tileLocation = tileDataMemory;
			if (useSignedValues) tileLocation += (tileNumber + 128) * 16;
			else tileLocation += tileNumber * 16;

			const byte data1 = m_pMMU->ReadByte(tileLocation + lineNr);
			const byte data2 = m_pMMU->ReadByte(tileLocation + lineNr + 1);

			const int wx = windowX + xPos;
			const int wy = windowY + yPos;
			if (wx > 159 || wy > 143) continue;

			const UI16 displayAddress = wx + wy * SCREEN_WIDTH;
			if (displayAddress >= SCREEN_WIDTH * SCREEN_HEIGHT) continue;

			const int colorBit = 1 << (7 - (xPos % 8));
			const byte color = ((data1 & colorBit) ? 1 : 0) | ((data2 & colorBit) ? 2 : 0);
			m_Screen[displayAddress] = color;
			DisplayBuffer[displayAddress] = m_Palettes[0][color];
		}
	}
}

void Graphics::RenderSprites()
{
	if (BIT1(m_LCDC))
	{
		for (byte sprite = 0; sprite < 40; sprite++) // Does not take into account that hardware only allows for 10 sprites per line
		{
			const UI16 index = cOAM + sprite * 4;
			const byte yPos = m_pMMU->ReadByte(index) - 16;
			if (yPos >= 160) continue;
			const byte xPos = m_pMMU->ReadByte(index + 1) - 8;
			if (xPos >= 168) continue;
			const byte patternNum = m_pMMU->ReadByte(index + 2);
			const byte attributes = m_pMMU->ReadByte(index + 3);

			byte ySize = (BIT2(m_LCDC) ? 16 : 8);

			if ((m_Scanline >= yPos) && (m_Scanline < (yPos + ySize))) // On screen ?
			{
				SI8 spriteVertical = m_Scanline - yPos; // Y coordinate in the 8x? sprite
				if (BIT6(attributes)) spriteVertical = ySize - spriteVertical - 1;
				UI16 readAddr = cCharRam + spriteVertical * 2;

				UI8 data1 = 0, data2 = 0;
				if (ySize == 16) // Check 8x8 or 8x16
				{
					data1 = m_pMMU->ReadByte(readAddr + (patternNum & 0xFE) * 16);
					data2 = m_pMMU->ReadByte(readAddr + (patternNum & 0xFE) * 16 + 1);
				}
				else
				{
					data1 = m_pMMU->ReadByte(readAddr + patternNum * 16);
					data2 = m_pMMU->ReadByte(readAddr + patternNum * 16 + 1);
				}

				for (UI8 _xPixel = 0; _xPixel < 8; ++_xPixel)
				{
					const UI8 xDex = (BIT5(attributes) ? (xPos + 8 - _xPixel - 1) : xPos + _xPixel); // Only used to draw on screen
					const UI8 colorBit = 1 << (7 - _xPixel);
					const UI8 colorSelect = ((data1 & colorBit) ? 1 : 0) | ((data2 & colorBit) ? 2 : 0);

					if (colorSelect == 0) continue;
					UI16 displayAddress = xDex + m_Scanline * SCREEN_WIDTH;
					if (displayAddress >= SCREEN_WIDTH * SCREEN_HEIGHT) continue;

					//printf("%02X ", BIT4(attributes));

					if ((BIT7(attributes) == 0) || (m_Screen[displayAddress] == 0))
					{
						m_Screen[displayAddress] = colorSelect;
						DisplayBuffer[displayAddress] = m_Palettes[BIT4(attributes) ? 2 : 1][colorSelect];
					}
				}
			}
		}
	}
}

void Graphics::CGBRenderBackground()
{
	if (BIT0(m_LCDC))
	{
		UI16 tileDataMemory = 0;
		bool useSignedValues = false;
		if (BIT4(m_LCDC)) tileDataMemory = 0x8000;
		else
		{
			tileDataMemory = 0x8800;
			useSignedValues = true;
		}

		const UI16 backgroundMemory = BIT3(m_LCDC) ? cBGData2 : cBGData1;
		const byte yPos = m_ScrollY + m_Scanline;
		const byte lineNr = (yPos % 8) * 2;
		const UI16 tileRow = byte(yPos / 8) * 32;
		for (byte _pixel = 0; _pixel < SCREEN_WIDTH; ++_pixel)
		{
			const byte xPos = m_ScrollX + _pixel;

			const unsigned short tileAddress = backgroundMemory + tileRow + (xPos / 8);
			const byte cgb_attrib = m_pMMU->ReadCGBBanked(tileAddress, 1);
			const signed short tileNumber = useSignedValues ? (signed char)m_pMMU->ReadCGBBanked(tileAddress, BIT3(cgb_attrib)) : m_pMMU->ReadCGBBanked(tileAddress, BIT3(cgb_attrib));

			unsigned short tileLocation = tileDataMemory;
			if (useSignedValues) tileLocation += (tileNumber + 128) * 16;
			else tileLocation += tileNumber * 16;

			const byte data1 = m_pMMU->ReadByte(tileLocation + lineNr);
			const byte data2 = m_pMMU->ReadByte(tileLocation + lineNr + 1);

			const int colorBit = 1 << (7 - (xPos % 8));
			const UI16 displayAddress = _pixel + m_Scanline * SCREEN_WIDTH;
			if (displayAddress >= SCREEN_WIDTH * SCREEN_HEIGHT) continue;

			const byte color = ((data1 & colorBit) ? 1 : 0) | ((data2 & colorBit) ? 2 : 0);
			m_Screen[displayAddress] = color;
			DisplayBuffer[displayAddress] = m_CGBPaletteBG[cgb_attrib & 0x7][color];
		}
	}
}

void Graphics::CGBRenderWindow()
{
	if (BIT5(m_LCDC))
	{
		UI16 tileDataMemory = 0;
		bool useSignedValues = false;
		if (BIT4(m_LCDC)) tileDataMemory = 0x8000;
		else
		{
			tileDataMemory = 0x8800;
			useSignedValues = true;
		}

		const int windowY = ReadByte(0xFF4A);
		const int windowX = ReadByte(0xFF4B) - 7;

		const UI16 backgroundMemory = BIT6(m_LCDC) ? cBGData2 : cBGData1;
		const byte yPos = m_Scanline - windowY;
		const byte lineNr = (yPos % 8) * 2;
		const UI16 tileRow = byte(yPos / 8) * 32;

		for (byte _pixel = 0; _pixel < SCREEN_WIDTH; _pixel++)
		{
			const byte xPos = _pixel - windowX;
			const int tileAddress = backgroundMemory + tileRow + (xPos / 8);
			const byte cgb_attrib = m_pMMU->ReadCGBBanked(tileAddress, 1);
			const signed short tileNumber = useSignedValues ? (signed char)m_pMMU->ReadCGBBanked(tileAddress, BIT3(cgb_attrib)) : m_pMMU->ReadCGBBanked(tileAddress, BIT3(cgb_attrib));

			unsigned short tileLocation = tileDataMemory;
			if (useSignedValues) tileLocation += (tileNumber + 128) * 16;
			else tileLocation += tileNumber * 16;

			const byte data1 = m_pMMU->ReadByte(tileLocation + lineNr);
			const byte data2 = m_pMMU->ReadByte(tileLocation + lineNr + 1);

			const int wx = windowX + xPos;
			const int wy = windowY + yPos;
			if (wx > 159 || wy > 143) continue;

			const UI16 displayAddress = wx + wy * SCREEN_WIDTH;
			if (displayAddress >= SCREEN_WIDTH * SCREEN_HEIGHT) continue;

			const int colorBit = 1 << (7 - (xPos % 8));
			const byte color = ((data1 & colorBit) ? 1 : 0) | ((data2 & colorBit) ? 2 : 0);
			m_Screen[displayAddress] = color;
			DisplayBuffer[displayAddress] = m_CGBPaletteBG[cgb_attrib & 0x7][color];
		}
	}
}

void Graphics::CGBRenderSprites()
{
	if (BIT1(m_LCDC))
	{
		for (byte sprite = 0; sprite < 40; sprite++) // Does not take into account that hardware only allows for 10 sprites per line
		{
			const UI16 index = cOAM + sprite * 4;
			const byte yPos = m_pMMU->ReadByte(index) - 16;
			if (yPos >= 160) continue;
			const byte xPos = m_pMMU->ReadByte(index + 1) - 8;
			if (xPos >= 168) continue;
			const byte patternNum = m_pMMU->ReadByte(index + 2);
			const byte attributes = m_pMMU->ReadByte(index + 3);

			byte ySize = (BIT2(m_LCDC) ? 16 : 8);

			if ((m_Scanline >= yPos) && (m_Scanline < (yPos + ySize))) // On screen ?
			{
				SI8 spriteVertical = m_Scanline - yPos; // Y coordinate in the 8x? sprite
				if (BIT6(attributes)) spriteVertical = ySize - spriteVertical - 1;
				UI16 readAddr = cCharRam + spriteVertical * 2;

				UI8 data1 = 0, data2 = 0;
				if (ySize == 16) // Check 8x8 or 8x16
				{
					data1 = m_pMMU->ReadCGBBanked(readAddr + (patternNum & 0xFE) * 16, BIT3(attributes));
					data2 = m_pMMU->ReadCGBBanked(readAddr + (patternNum & 0xFE) * 16 + 1, BIT3(attributes));
				}
				else
				{
					data1 = m_pMMU->ReadCGBBanked(readAddr + patternNum * 16, BIT3(attributes));
					data2 = m_pMMU->ReadCGBBanked(readAddr + patternNum * 16 + 1, BIT3(attributes));
				}

				for (UI8 _xPixel = 0; _xPixel < 8; ++_xPixel)
				{
					const UI8 xDex = (BIT5(attributes) ? (xPos + 8 - _xPixel - 1) : xPos + _xPixel); // Only used to draw on screen
					const UI8 colorBit = 1 << (7 - _xPixel);
					const UI8 colorSelect = ((data1 & colorBit) ? 1 : 0) | ((data2 & colorBit) ? 2 : 0);

					if (colorSelect == 0) continue;
					UI16 displayAddress = xDex + m_Scanline * SCREEN_WIDTH;
					if (displayAddress >= SCREEN_WIDTH * SCREEN_HEIGHT) continue;

					//printf("%02X ", BIT4(attributes));

					if ((BIT7(attributes) == 0) || (m_Screen[displayAddress] == 0))
					{
						m_Screen[displayAddress] = colorSelect;
						DisplayBuffer[displayAddress] = m_CGBPaletteOBJ[attributes & 7][colorSelect];
					}
				}
			}
		}
	}
}

void Graphics::Paint()
{
	//for (UI32 i = 0; i < SCREEN_WIDTH * SCREEN_HEIGHT; ++i)
	//{
	//	int i3 = i * 3 + 0x8000;
	//	if (i3 > 0xFFFF) break;
	//	DisplayBuffer[i].R = m_pMMU->m_Memory[i3] / 255.0f;
	//	DisplayBuffer[i].G = m_pMMU->m_Memory[i3 + 1] / 255.0f;
	//	DisplayBuffer[i].B = m_pMMU->m_Memory[i3 + 2] / 255.0f;
	//}

	RenderOAMDebug();
	RenderTileDebug();
}

void Graphics::RenderOAMDebug()
{
	for (byte sprite = 0; sprite < 40; sprite++) // Does not take into account that hardware only allows for 10 sprites per line
	{
		const UI16 index = cOAM + sprite * 4;
		const byte yPos = (sprite / 20) * 16;
		const byte xPos = (sprite % 20) * 8;
		const byte patternNum = m_pMMU->ReadByte(index + 2);
		const byte attributes = m_pMMU->ReadByte(index + 3);

		byte ySize = (BIT2(m_LCDC) ? 16 : 8);

		for (byte scanline = 0; scanline < ySize; ++scanline)
		{
			SI8 spriteVertical = scanline; // Y coordinate in the 8x? sprite
			if (BIT6(attributes)) spriteVertical = ySize - spriteVertical - 1;
			UI16 readAddr = cCharRam + spriteVertical * 2;

			UI8 data1 = 0, data2 = 0;
			if (ySize == 16) // Check 8x8 or 8x16
			{
				data1 = m_pMMU->ReadByte(readAddr + (patternNum & 0xFE) * 16);
				data2 = m_pMMU->ReadByte(readAddr + (patternNum & 0xFE) * 16 + 1);
			}
			else
			{
				data1 = m_pMMU->ReadByte(readAddr + patternNum * 16);
				data2 = m_pMMU->ReadByte(readAddr + patternNum * 16 + 1);
			}

			for (UI8 _xPixel = 0; _xPixel < 8; ++_xPixel)
			{
				const UI8 xDex = (BIT5(attributes) ? (xPos + 8 - _xPixel - 1) : xPos + _xPixel); // Only used to draw on screen
				const UI8 colorBit = 1 << (7 - _xPixel);
				const UI8 colorSelect = ((data1 & colorBit) ? 1 : 0) | ((data2 & colorBit) ? 2 : 0);

				UI16 displayAddress = xDex + (scanline + yPos) * SCREEN_WIDTH;
				if (displayAddress >= SCREEN_WIDTH * SCREEN_HEIGHT)
					continue;

				if (colorSelect == 0) DisplayBuffer[displayAddress] = Color(255, 0, 255);
				else
				{
					m_Screen[displayAddress] = colorSelect;
					DisplayBuffer[displayAddress] = m_Palettes[BIT4(attributes) ? 2 : 1][colorSelect];
				}
			}
		}
	}
}

void Graphics::RenderTileDebug()
{
	for (byte tile = 0; tile < 255; tile++)
	{
		const byte yPos = (tile / 20) * 8;
		const byte xPos = (tile % 20) * 8;
		UI16 addr = 0x8000 + tile * 16;

		for (byte scanline = 0; scanline < 8; scanline++)
		{
			addr += 2;
			const byte data1 = m_pMMU->ReadByte(addr);
			const byte data2 = m_pMMU->ReadByte(addr + 1);

			for (UI8 _xPixel = 0; _xPixel < 8; ++_xPixel)
			{
				const UI8 colorBit = 1 << (7 - _xPixel);
				const UI8 colorSelect = ((data1 & colorBit) ? 1 : 0) | ((data2 & colorBit) ? 2 : 0);

				UI16 displayAddress = (xPos + _xPixel) + (scanline + yPos + 32) * SCREEN_WIDTH;
				if (displayAddress >= SCREEN_WIDTH * SCREEN_HEIGHT) return;

				if (colorSelect == 0) DisplayBuffer[displayAddress] = Color(255, 0, 255);
				else
				{
					m_Screen[displayAddress] = colorSelect;
					DisplayBuffer[displayAddress] = m_Palettes[0][colorSelect];
				}
			}
		}
	}
}
