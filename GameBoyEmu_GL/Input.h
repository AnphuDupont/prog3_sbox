#pragma once
#include "NvGamePad\include\NvGamepad.h"

#ifdef WIN32
#include "NvGamePad\include\NvGamepadXInput.h"
#elif LINUX
#include "NvGamePad\include\NvGamepadLinux.h"
#endif

class GamePad
{
public:
	GamePad();
	~GamePad();

	bool ReadState(NvGamepad::State & out) { return m_pGamePad->getState(0, out); }
	bool IsConnected() 
	{
		NvGamepad::State s;
		return m_pGamePad->getState(0, s); 
	}

private:
	NvGamepad * m_pGamePad;
};

GamePad::GamePad()
{

#ifdef WIN32
	m_pGamePad = new NvGamepadXInput();
#elif LINUX
	m_pGamePad = new NvGamepadLinux();
#endif

}

GamePad::~GamePad()
{
	delete m_pGamePad;
}