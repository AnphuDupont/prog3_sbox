#pragma once

#include "defines.h"
#include "Memory.h"
#include "Graphics.h"
#include "GBSound.h"

#include <iostream>

namespace Reg
{
#ifdef _WIN32
	enum Reg
	{
		f, a, c, b, e, d, l, h
	};
#else // Settings for big endian cpu 
	enum Reg
	{
		a, f, b, c, d, e, h, l
	};
#endif
}

namespace Reg16
{
	enum Reg16
	{
		af, bc, de, hl
	};
}

class GameBoyZ80
{
public:
	~GameBoyZ80();

	void Reset();
	bool LoadGame(const char *);
	void EmulateCycle();
	void UpdateTimingDuringInstr(byte timing);

	Color * GetDisplayBuffer(){ return m_pGraphics->DisplayBuffer; }
	void OutputDebug(bool b = true);
	void PaintScreen() { m_pGraphics->Paint(); }

	static GameBoyZ80 * GetGameBoyZ80();
	Memory * GetMemory() { return m_pMemory; }
	GBSound * GetSound() { return m_pGBSound; }

	UI16 GetPC() { return m_PC; }

	void KeyPressed() { m_Stop = false; }
	bool GetInterruptMasterEnable() { return m_IntMasterEnable; }

#ifdef _WIN32
	bool _debugWinOutput = false;
	bool _testVarA = false, _testVarB = false;
#endif

	Device::Device model;
	bool m_CGBSpeedMode;

private:
	GameBoyZ80();
	static GameBoyZ80 * m_GameBoyZ80;

	Graphics * m_pGraphics;
	Memory * m_pMemory;
	GBSound * m_pGBSound;

	union
	{
		UI8 m_Register[8];
		UI16 m_Register16[4];
	};

	UI8 m_TimerT,
		m_TimerM,
		m_ClockM,				// Clock for last instruction
		m_ClockT;				// Clock for last instruction

	UI16 m_Test;
	UI8 m_SetInterrupt;
	bool m_HaltInterruptDisabled;

	UI16 m_PC,					// Program counter
		m_SP,					// Stack pointer
		m_Opcode;				// Current opcode
	UI16 m_OldPC;

	bool m_Halt, m_Stop, m_IntMasterEnable, m_CanDebug, m_IsInInterruptRoutine;

	void InstrCB();

	void XX()
	{
		// Crash
		m_Stop = true;
	}

	// CPU functions
#pragma region Load_Store_8b

	// Load register from register
	inline void LDrr(Reg::Reg rDest, Reg::Reg rSrc) { m_Register[rDest] = m_Register[rSrc]; m_ClockM = 4; }

	// Read byte from memory at HL and store in register
	inline void LDrHLm(Reg::Reg);

	// Store number(byte) in register
	inline void LDrn(Reg::Reg);

	// Write byte to memory at HL from register
	inline void LDHLmr(Reg::Reg);

	// Store value at number to memory at HL
	void LDHLmn();

	// Load register 'A' from memory at register-pair
	void LDABCm();
	void LDADEm();

	// Store register 'A' in memory at register-pair
	void LDBCmA();
	void LDDEmA();

	// Load register 'A' from memory at position
	void LDAmm();

	// Store register 'A' in memory at position
	void LDmmA();

	// Load register 'A' from 0xFF00 + register 'C'
	void LDCmA();

	// Store register 'A' in memory at 0xFF00 + register 'C'
	void LDACm();

	// Load register 'A' from HL in memory,
	void LDDHLmA();						// decrement HL
	void LDIHLmA();						// increment HL

	// Store register 'A' to memory at HL,
	void LDDAmHL();						// decrement HL
	void LDIAmHL();						// increment HL

	// Load register 'A' from memory at 0xFF00 + number 
	void LDHmA();

	// Write register 'A' to memory at 0xFF00 + number 
	void LDHAm();

#pragma endregion

#pragma region Load_Store_16b

	// Store register-pair from memory at number
	void LDr16nn(Reg16::Reg16);
	void LDSPnn();

	// Write stack pointer to memory
	void LDnnSP();

	// Copy HL to SP 
	void LDSPHL();

	// Copy SP to HL, with adding value using two's complement displacement from memory
	void LDHLSPn();

	// Push register-pair to memory at stack-pointer -2 
	void PUSHBC();
	void PUSHDE();
	void PUSHHL();
	void PUSHAF();
	void PUSHPC();

	// Pop stack-pointer to register-pair
	void POPBC();
	void POPDE();
	void POPHL();
	void POPAF();

#pragma endregion

#pragma region Arthmetic_8b
	// ADD register or value from memory to register 'A'
	void ADDvalToA(const unsigned char value);
	inline void ADDr(const Reg::Reg);
	void ADDHL();
	void ADDn();

	// ADD register or value from memory to register 'A' and add carry
	void ADCvalToA(const unsigned char value);
	inline void ADCr(const Reg::Reg);
	void ADCHL();
	void ADCn();

	// SUB register or value from memory from register 'A'
	void SUBvalFromA(const unsigned char value);
	inline void SUBr(const Reg::Reg);
	void SUBHL();
	void SUBn();

	// SUB register or value from memory from register 'A' and sub carry
	void SBCvalFromA(const unsigned char value);
	inline void SBCr(const Reg::Reg);
	void SBCHL();
	void SBCn();

	// AND register or value from memory with register 'A'
	inline void AND(const Reg::Reg);
	void ANDHL();
	void ANDn();

	// OR register or value from memory with register 'A'
	inline void OR(const Reg::Reg);
	void ORHL();
	void ORn();

	// XOR register or value from memory with register 'A'
	inline void XOR(const Reg::Reg);
	void XORHL();
	void XORn();


	// SUB register or value from memory from register 'A' and sub carry
	void CPval(const unsigned char value);
	void CPHL();
	void CPn();

	// INC register or value from HL from register 'A' and sub carry
	void INCreg(Reg::Reg);
	void INCHLm();

	// DEC register or value from HL from register 'A' and sub carry
	void DECreg(Reg::Reg);
	void DECHLm();

#pragma endregion

#pragma region Arthmetic_16b
	// ADD register pair or value16b from memory to HL
	void ADDHLval(const UI16);
	// ADD two's complement value from memory to SP
	void ADDspn();

	// INC word
	inline void INCr16(Reg16::Reg16);
	void INCsp();

	//DEC word
	void DECBC();
	void DECDE();
	void DECHL();
	void DECsp();
#pragma endregion

#pragma region Miscellaneous
	// SWAP nibbles
	void SWAPnibble(unsigned char * var);
	void SWAPr_a();
	void SWAPr_b();
	void SWAPr_c();
	void SWAPr_d();
	void SWAPr_e();
	void SWAPr_h();
	void SWAPr_l();
	void SWAPHLm();

	// Converts a into packed BCD
	void DAA();

	// Invert a
	void CPL();

	// Invert carry flag
	void CCF();

	// Set carry flag
	void SCF();

	// No operation
	void NOP();

	// Halt CPU until an interrupt occurs
	void HALT();

	// Halt CPU
	void STOP();

	// Disable interrupts
	void DI();

	// Enable interrupts
	void EI();

#pragma endregion

#pragma region Rotates_Shifts

	// Info check URLs at top. 
	void RLCr(Reg::Reg);
	void RLr(Reg::Reg);
	void RRCr(Reg::Reg);
	void RRr(Reg::Reg);

	void RLCA();
	void RLA();
	void RRCA();
	void RRA();

	void RLCHL();
	void RLHL();
	void RRCHL();
	void RRHL();

	// Shift n left into carry. LSB set to 0
	void SLAr_r(Reg::Reg);
	void SLAr_a();
	void SLAr_b();
	void SLAr_c();
	void SLAr_d();
	void SLAr_e();
	void SLAr_h();
	void SLAr_l();
	void SLAHL();

	void SRAr_r(Reg::Reg);
	void SRAr_a();
	void SRAr_b();
	void SRAr_c();
	void SRAr_d();
	void SRAr_e();
	void SRAr_h();
	void SRAr_l();
	void SRAHL();

	void SRLr_r(Reg::Reg);
	void SRLHL();

#pragma endregion

#pragma region Bit_Opcodes

	// Check bit in register or HL
	inline void BITbr(const UI8 val, const UI8 bit) { m_Register[Reg::f] &= 0x10; m_Register[Reg::f] |= (val & bit) ? 0x20 : 0xA0; m_ClockM = 8; }
	
	inline void BIT0_HL();
	inline void BIT1_HL();
	inline void BIT2_HL();
	inline void BIT3_HL();
	inline void BIT4_HL();
	inline void BIT5_HL();
	inline void BIT6_HL();
	inline void BIT7_HL();

	// Set bit in register or HL
	inline void SET0_HL();
	inline void SET1_HL();
	inline void SET2_HL();
	inline void SET3_HL();
	inline void SET4_HL();
	inline void SET5_HL();
	inline void SET6_HL();
	inline void SET7_HL();

	// Reset bit in regsiter or HL
	inline void RES0_HL();
	inline void RES1_HL();
	inline void RES2_HL();
	inline void RES3_HL();
	inline void RES4_HL();
	inline void RES5_HL();
	inline void RES6_HL();
	inline void RES7_HL();

#pragma endregion

#pragma region Jumps

	// Jump to addr
	void JP();

	// Jump to addr if condition is true
	void JPNZ();
	void JPZ();
	void JPNC();
	void JPC();

	// Jump to addr in HL
	void JPHL();

	// Add n to addr, then jump
	void JR();

	// If condition true, add n to addr, then jump
	void JRNZ();
	void JRZ();
	void JRNC();
	void JRC();

#pragma endregion

#pragma region Calls

	// Push next instr onto stack, then jump
	void CALLnn();

	// If condition true, push next instr onto stack, then jump
	void CALLNZ();
	void CALLZ();
	void CALLNC();
	void CALLC();

#pragma endregion

#pragma region Restarts

	// Push addr to stack, jump to addr $0000 + n
	void RST00();
	void RST08();
	void RST10();
	void RST18();
	void RST20();
	void RST28();
	void RST30();
	void RST38();

	// Handle vblank 
	void RST40();

#pragma endregion

#pragma region Returns

	// Pop two bytes from stack, jump to that addrs
	void RET();

	// RET condition
	void RETCon();

	// Return if conditio is met
	void RETNZ();
	void RETZ();
	void RETNC();
	void RETC();

	// Retun and enable interrupts
	void RETI();

	void CheckInterrupts();

#pragma endregion

};

