// Dupont An Phu (c)2014

// Gameboy CPU (LR35902) instruction set
// http://www.pastraiser.com/cpu/gameboy/gameboy_opcodes.html
// http://codeslinger.co.uk

#include "GameBoyZ80.h"
#include "defines.h"

unsigned char GameBoyZ80::m_Register[8] = { 0x01, 0xB0, 0x00, 0x13, 0x00, 0xD8, 0x01, 0x4D };
unsigned char GameBoyZ80::m_TimerT = 0;
unsigned char GameBoyZ80::m_TimerM = 0;
unsigned char GameBoyZ80::m_ClockM = 0;
unsigned char GameBoyZ80::m_ClockT = 0;

unsigned short GameBoyZ80::m_PC = 0;
unsigned short GameBoyZ80::m_SP = 0;
unsigned short GameBoyZ80::m_Opcode = 0;

GameBoyZ80::GameBoyZ80() :
m_Halt(false),
m_Stop(false),
m_IntMasterEnable(true),
m_CanDebug(false),
m_pGPU(nullptr)
{
}

GameBoyZ80::~GameBoyZ80()
{
	delete m_pGPU;
	delete m_pTimer;
	//delete m_pSound;
}

void GameBoyZ80::Reset()
{
	//m_pSound = new Sound();
	//m_pSound->Initialize();
	m_pGPU = new Graphics(&m_MMU);
	m_MMU.SetGraphicsPtr(m_pGPU);
	//m_MMU.SetSoundPtr(m_pSound);

	m_pTimer = new Timer(&m_MMU, this);
	m_MMU.SetTimer(m_pTimer);

	//for (int i = 0; i < 8; ++i)
	//	m_Register[i] = 0;
	m_SP = 0xFFFE;
	m_ClockM = m_ClockT = 0;
	//m_IntMasterEnable = 1;

	m_PC = 0x100;
	oldOp = 0x100;

	m_MMU.Reset(); // Real hardware does not initialize the values on start
}

bool GameBoyZ80::LoadGame(std::string * programPtr)
{
	if (programPtr->size() <= 0) return false;
	m_MMU.m_ROM.reserve(programPtr->size());
	for (unsigned int i = 0; i < programPtr->size(); i++)
		m_MMU.WriteToROM(programPtr->c_str()[i]);

	m_MMU.m_CartridgeType = m_MMU.m_ROM[0x0147];
	m_MMU.m_RomSize = m_MMU.m_ROM[0x0148];
	m_MMU.m_RamSize = m_MMU.m_ROM[0x0149];

	// Read game name from ROM
	// 0134-0143h
	for (unsigned int i = 0; i < 16; ++i)
		Memory::m_GameTitle[i] = m_MMU.m_ROM[0x0134 + i];

	printf("Loaded ROM: %d byte(s).\n", (int)programPtr->size());
	printf("Cartridge type: %d\n", m_MMU.m_CartridgeType);
	printf("\nInstructions executed per second:\n");
	return true;
}

void GameBoyZ80::PushPC()
{
	m_SP -= 2;
	m_MMU.WriteWord(m_SP, m_PC);
	m_ClockM = 16; 
}

void GameBoyZ80::Interrupts()
{
	if (!m_IntMasterEnable) return;

	byte interrupts = m_MMU.ReadByte(cINTenable) & m_MMU.ReadByte(cINTflag);
	if (!interrupts) return;

	m_IntMasterEnable = false;
	m_Halt = false; 
	PushPC();
	//newInterrupt = true;

	if (BIT0(interrupts))	//V-Blank
	{
		m_PC = 0x40;
		m_MMU.m_Memory[cINTflag] &= ~0x01;
		//VBlankIntPending = false;
	}
	else if (BIT1(interrupts))	//LCD-Stat
	{
		m_PC = 0x48;
		m_MMU.m_Memory[cINTflag] &= ~0x02;
	}
	else if (BIT2(interrupts))	//Timer
	{
		m_PC = 0x50;
		m_MMU.m_Memory[cINTflag] &= ~0x04;
	}
	else if (BIT3(interrupts))	//Serial
	{
		m_PC = 0x58;
		m_MMU.m_Memory[cINTflag] &= ~0x08;
	}
	else if (BIT4(interrupts))	//Joypad
	{
		m_PC = 0x60;
		m_MMU.m_Memory[cINTflag] &= ~0x10;
	}
}

void GameBoyZ80::EmulateCycle()
{
	//if (m_PC == oldOp) m_CanDebug = true;
	//if (m_PC == 0xFFFF) 
	//OutputDebug();

	if (m_Halt)
	{
		m_Halt = false;
		m_ClockM = 4;
		m_IntMasterEnable = true;
	}
	else
	{
		m_Opcode = m_MMU.ReadByte(m_PC);
		//OutputDebug();
		++m_PC;
		// Exectute instruction
		switch (m_Opcode & 0xF0)
		{
		case 0x00:
			switch (m_Opcode & 0xF)
			{
			case 0x00: NOP();		break;
			case 0x01: LDBCnn();	break;
			case 0x02: LDBCmA();	break;
			case 0x03: INCBC();		break;
			case 0x04: INCb();		break;
			case 0x05: DECb();		break;
			case 0x06: LDrn_b();	break;
			case 0x07: RLCA();		break;
			case 0x08: LDnnSP();	break;
			case 0x09: ADDHLBC();	break;
			case 0x0A: LDABCm();	break;
			case 0x0B: DECBC();		break;
			case 0x0C: INCc();		break;
			case 0x0D: DECc();		break;
			case 0x0E: LDrn_c();	break;
			case 0x0F: RRCA();		break;
			default: OutputDebug();
			}
			break;
		case 0x10:
			switch (m_Opcode & 0xF)
			{
			case 0x00: STOP();		break;
			case 0x01: LDDEnn();	break;
			case 0x02: LDDEmA();	break;
			case 0x03: INCDE();		break;
			case 0x04: INCd();		break;
			case 0x05: DECd();		break;
			case 0x06: LDrn_d();	break;
			case 0x07: RLA();		break;
			case 0x08: JR();		break;
			case 0x09: ADDHLDE();	break;
			case 0x0A: LDADEm();	break;
			case 0x0B: DECDE();		break;
			case 0x0C: INCe();		break;
			case 0x0D: DECe();		break;
			case 0x0E: LDrn_e();	break;
			case 0x0F: RRA();		break;
			default: OutputDebug();
			}
			break;
		case 0x20:
			switch (m_Opcode & 0xF)
			{
			case 0x00: JRNZ();		break;
			case 0x01: LDHLnn();	break;
			case 0x02: LDIAmHL();	break;
			case 0x03: INCHL();		break;
			case 0x04: INCh();		break;
			case 0x05: DECh();		break;
			case 0x06: LDrn_h();	break;
			case 0x07: DAA();		break;
			case 0x08: JRZ();		break;
			case 0x09: ADDHLHL();	break;
			case 0x0A: LDIHLmA();	break;
			case 0x0B: DECHL();		break;
			case 0x0C: INCl();		break;
			case 0x0D: DECl();		break;
			case 0x0E: LDrn_l();	break;
			case 0x0F: CPL();		break;
			default: OutputDebug();
			}
			break;
		case 0x30:
			switch (m_Opcode & 0xF)
			{
			case 0x00: JRNC();		break;
			case 0x01: LDSPnn();	break;
			case 0x02: LDDAmHL();	break;
			case 0x03: INCsp();		break;
			case 0x04: INCHLm();	break;
			case 0x05: DECHLm();	break;
			case 0x06: LDHLmn();	break;
			case 0x07: SCF();		break;
			case 0x08: JRC();		break;
			case 0x09: ADDHLsp();	break;
			case 0x0A: LDDHLmA();	break;
			case 0x0B: DECsp();		break;
			case 0x0C: INCa();		break;
			case 0x0D: DECa();		break;
			case 0x0E: LDrn_a();	break;
			case 0x0F: CCF();		break;
			default: OutputDebug();
			}
			break;
		case 0x40:
			switch (m_Opcode & 0xF)
			{
			case 0x00: LDrr_bb();	break;
			case 0x01: LDrr_bc();	break;
			case 0x02: LDrr_bd();	break;
			case 0x03: LDrr_be();	break;
			case 0x04: LDrr_bh();	break;
			case 0x05: LDrr_bl();	break;
			case 0x06: LDrHLm_b();	break;
			case 0x07: LDrr_ba();	break;
			case 0x08: LDrr_cb();	break;
			case 0x09: LDrr_cc();	break;
			case 0x0A: LDrr_cd();	break;
			case 0x0B: LDrr_ce();	break;
			case 0x0C: LDrr_ch();	break;
			case 0x0D: LDrr_cl();	break;
			case 0x0E: LDrHLm_c();	break;
			case 0x0F: LDrr_ca();	break;
			default: OutputDebug();
			}
			break;
		case 0x50:
			switch (m_Opcode & 0xF)
			{
			case 0x00: LDrr_db();	break;
			case 0x01: LDrr_dc();	break;
			case 0x02: LDrr_dd();	break;
			case 0x03: LDrr_de();	break;
			case 0x04: LDrr_dh();	break;
			case 0x05: LDrr_dl();	break;
			case 0x06: LDrHLm_d();	break;
			case 0x07: LDrr_da();	break;
			case 0x08: LDrr_eb();	break;
			case 0x09: LDrr_ec();	break;
			case 0x0A: LDrr_ed();	break;
			case 0x0B: LDrr_ee();	break;
			case 0x0C: LDrr_eh();	break;
			case 0x0D: LDrr_el();	break;
			case 0x0E: LDrHLm_e();	break;
			case 0x0F: LDrr_ea();	break;
			default: OutputDebug();
			}
			break;
		case 0x60:
			switch (m_Opcode & 0xF)
			{
			case 0x00: LDrr_hb();	break;
			case 0x01: LDrr_hc();	break;
			case 0x02: LDrr_hd();	break;
			case 0x03: LDrr_he();	break;
			case 0x04: LDrr_hh();	break;
			case 0x05: LDrr_hl();	break;
			case 0x06: LDrHLm_h();	break;
			case 0x07: LDrr_ha();	break;
			case 0x08: LDrr_lb();	break;
			case 0x09: LDrr_lc();	break;
			case 0x0A: LDrr_ld();	break;
			case 0x0B: LDrr_le();	break;
			case 0x0C: LDrr_lh();	break;
			case 0x0D: LDrr_ll();	break;
			case 0x0E: LDrHLm_l();	break;
			case 0x0F: LDrr_la();	break;
			default: OutputDebug();
			}
			break;
		case 0x70:
			switch (m_Opcode & 0xF)
			{
			case 0x00: LDHLmr_b();	break;
			case 0x01: LDHLmr_c();	break;
			case 0x02: LDHLmr_d();	break;
			case 0x03: LDHLmr_e();	break;
			case 0x04: LDHLmr_h();	break;
			case 0x05: LDHLmr_l();	break;
			case 0x06: HALT();		break;
			case 0x07: LDHLmr_a();	break;
			case 0x08: LDrr_ab();	break;
			case 0x09: LDrr_ac();	break;
			case 0x0A: LDrr_ad();	break;
			case 0x0B: LDrr_ae();	break;
			case 0x0C: LDrr_ah();	break;
			case 0x0D: LDrr_al();	break;
			case 0x0E: LDrHLm_a();	break;
			case 0x0F: LDrr_aa();	break;
			default: OutputDebug();
			}
			break;
		case 0x80:
			switch (m_Opcode & 0xF)
			{
			case 0x00: ADDb();		break;
			case 0x01: ADDc();		break;
			case 0x02: ADDd();		break;
			case 0x03: ADDe();		break;
			case 0x04: ADDh();		break;
			case 0x05: ADDl();		break;
			case 0x06: ADDHL();		break;
			case 0x07: ADDa();		break;
			case 0x08: ADCb();		break;
			case 0x09: ADCc();		break;
			case 0x0A: ADCd();		break;
			case 0x0B: ADCe();		break;
			case 0x0C: ADCh();		break;
			case 0x0D: ADCl();		break;
			case 0x0E: ADCHL();		break;
			case 0x0F: ADCa();		break;
			default: OutputDebug();
			}
			break;
		case 0x90:
			switch (m_Opcode & 0xF)
			{
			case 0x00: SUBb();		break;
			case 0x01: SUBc();		break;
			case 0x02: SUBd();		break;
			case 0x03: SUBe();		break;
			case 0x04: SUBh();		break;
			case 0x05: SUBl();		break;
			case 0x06: SUBHL();		break;
			case 0x07: SUBa();		break;
			case 0x08: SBCb();		break;
			case 0x09: SBCc();		break;
			case 0x0A: SBCd();		break;
			case 0x0B: SBCe();		break;
			case 0x0C: SBCh();		break;
			case 0x0D: SBCl();		break;
			case 0x0E: SBCHL();		break;
			case 0x0F: SBCa();		break;
			default: OutputDebug();
			}
			break;
		case 0xA0:
			switch (m_Opcode & 0xF)
			{
			case 0x00: ANDb();		break;
			case 0x01: ANDc();		break;
			case 0x02: ANDd();		break;
			case 0x03: ANDe();		break;
			case 0x04: ANDh();		break;
			case 0x05: ANDl();		break;
			case 0x06: ANDHL();		break;
			case 0x07: ANDa();		break;
			case 0x08: XORb();		break;
			case 0x09: XORc();		break;
			case 0x0A: XORd();		break;
			case 0x0B: XORe();		break;
			case 0x0C: XORh();		break;
			case 0x0D: XORl();		break;
			case 0x0E: XORHL();		break;
			case 0x0F: XORa();		break;
			default: OutputDebug();
			}
			break;
		case 0xB0:
			switch (m_Opcode & 0xF)
			{
			case 0x00: ORb();		break;
			case 0x01: ORc();		break;
			case 0x02: ORd();		break;
			case 0x03: ORe();		break;
			case 0x04: ORh();		break;
			case 0x05: ORl();		break;
			case 0x06: ORHL();		break;
			case 0x07: ORa();		break;
			case 0x08: CPb();		break;
			case 0x09: CPc();		break;
			case 0x0A: CPd();		break;
			case 0x0B: CPe();		break;
			case 0x0C: CPh();		break;
			case 0x0D: CPl();		break;
			case 0x0E: CPHL();		break;
			case 0x0F: CPa();		break;
			default: OutputDebug();
			}
			break;
		case 0xC0:
			switch (m_Opcode & 0xF)
			{
			case 0x00: RETNZ();		break;
			case 0x01: POPBC();		break;
			case 0x02: JPNZ();		break;
			case 0x03: JP();		break;
			case 0x04: CALLNZ();	break;
			case 0x05: PUSHBC();	break;
			case 0x06: ADDn();		break;
			case 0x07: RST00();		break;
			case 0x08: RETZ();		break;
			case 0x09: RET();		break;
			case 0x0A: JPZ();		break;
			case 0x0B: InstrCB();	break;
			case 0x0C: CALLZ();		break;
			case 0x0D: CALLnn();	break;
			case 0x0E: ADCn();		break;
			case 0x0F: RST08();		break;
			default: OutputDebug();
			}
			break;
		case 0xD0:
			switch (m_Opcode & 0xF)
			{
			case 0x00: RETNC();		break;
			case 0x01: POPDE();		break;
			case 0x02: JPNC();		break;
			case 0x03: XX();		break;
			case 0x04: CALLNC();	break;
			case 0x05: PUSHDE();	break;
			case 0x06: SUBn();		break;
			case 0x07: RST10();		break;
			case 0x08: RETC();		break;
			case 0x09: RETI();		break;
			case 0x0A: JPC();		break;
			case 0x0B: XX();		break;
			case 0x0C: CALLC();		break;
			case 0x0D: XX();		break;
			case 0x0E: SBCn();		break;
			case 0x0F: RST18();		break;
			default: OutputDebug();
			}
			break;
		case 0xE0:
			switch (m_Opcode & 0xF)
			{
			case 0x00: LDHAm();		break;
			case 0x01: POPHL();		break;
			case 0x02: LDACm();		break;
			case 0x03: XX();		break;
			case 0x04: XX();		break;
			case 0x05: PUSHHL();	break;
			case 0x06: ANDn();		break;
			case 0x07: RST20();		break;
			case 0x08: ADDspn();	break;
			case 0x09: JPHL();		break;
			case 0x0A: LDmmA();		break;
			case 0x0B: XX();		break;
			case 0x0C: XX();		break;
			case 0x0D: XX();		break;
			case 0x0E: XORn();		break;
			case 0x0F: RST28();		break;
			default: OutputDebug();
			}
			break;
		case 0xF0:
			switch (m_Opcode & 0xF)
			{
			case 0x00: LDHmA();		break;
			case 0x01: POPAF();		break;
			case 0x02: LDCmA();		break;
			case 0x03: DI();		break;
			case 0x04: XX();		break;
			case 0x05: PUSHAF();	break;
			case 0x06: ORn();		break;
			case 0x07: RST30();		break;
			case 0x08: LDHLSPn();	break;
			case 0x09: LDSPHL();	break;
			case 0x0A: LDAmm();		break;
			case 0x0B: EI();		break;
			case 0x0C: XX();		break;
			case 0x0D: XX();		break;
			case 0x0E: CPn();		break;
			case 0x0F: RST38();		break;
			default: OutputDebug();
			}
			break;
		}
	}

	m_TimerM += m_ClockM;
	m_TimerT += m_ClockT;
	m_pTimer->Inc();

	m_pGPU->Tick(m_ClockM);
	//m_pSound->AddCpuTime(m_ClockM);
	//m_pSound->EndFrame(m_ClockM);

	//m_ClockM = m_ClockT = 0;

	//Interrupts();

	//m_TimerM += m_ClockM;
	//m_TimerT += m_ClockT;
	//m_pTimer->Inc();
}

void GameBoyZ80::LDDHLmA()
{
	m_Register[a] = m_MMU.ReadByte((m_Register[h] << 8) | m_Register[l]);
	if (--m_Register[l] == 0xFF) // Looped, because (0 - 1) == 255 unsigned
		--m_Register[h];
	m_ClockM = 8;
}

void GameBoyZ80::LDIHLmA()
{
	m_Register[a] = m_MMU.ReadByte((m_Register[h] << 8) | m_Register[l]);
	if (++m_Register[l] == 0) // Looped, because (255 + 1) == 0 unsigned
		++m_Register[h];
	m_ClockM = 8;
}

void GameBoyZ80::LDDAmHL()
{
	m_MMU.WriteByte((m_Register[h] << 8) | m_Register[l], m_Register[a]);
	if (--m_Register[l] == 0xFF) // Looped, because (0 - 1) == 255 unsigned
		--m_Register[h];
	m_ClockM = 8;
}

void GameBoyZ80::LDIAmHL()
{
	m_MMU.WriteByte((m_Register[h] << 8) | m_Register[l], m_Register[a]);
	if (++m_Register[l] == 0) // Looped, because (255 + 1) == 0 unsigned
		++m_Register[h];
	m_ClockM = 8;
}

void GameBoyZ80::LDHLSPn()
{
	char immVal = m_MMU.ReadByte(m_PC);
	++m_PC;

	m_Register[f] &= 0x0F;
	if (((m_SP & 0x0F) + (immVal & 0x0F)) > 0x0F) m_Register[f] |= 0x20;
	if ((m_SP + immVal) < m_SP) m_Register[f] |= 0x10;

	unsigned short val = m_SP + immVal;
	m_Register[h] = (val & 0xFF00) >> 8;
	m_Register[l] = (val & 0xFF);

	m_ClockM = 12;
}

void GameBoyZ80::ADDvalToA(const unsigned char value)
{
	unsigned char oldA = m_Register[a];
	m_Register[a] += value;
	m_Register[f] = (m_Register[a] < oldA) ? 0x10 : 0;
	m_Register[f] |= ((m_Register[a] & 0xF) < (oldA & 0xF)) ? 0x20 : 0;
	m_Register[f] |= (m_Register[a] == 0) ? 0x80 : 0;
}

void GameBoyZ80::ADCvalToA(const unsigned char value)
{
	//OutputDebug();

	byte oldC = (m_Register[f] & 0x10) ? 1 : 0;
	unsigned short result = m_Register[a] + value + oldC;

	m_Register[f] = 0;
	if (!(result & 0xFF)) m_Register[f] |= 0x80;
	if ((oldC + (value & 0x0F) + (m_Register[a] & 0x0F)) > 0x0F) m_Register[f] |= 0x20;
	if (result > 0xFF) m_Register[f] |= 0x10;

	m_Register[a] = (byte)result;

	//OutputDebug();
}

void GameBoyZ80::SUBvalFromA(const unsigned char value)
{
	unsigned char oldA = m_Register[a];
	m_Register[a] -= value;
	m_Register[f] = (m_Register[a] > oldA) ? 0x50 : 0x40;
	// H flag set if NO borrow
	m_Register[f] |= ((m_Register[a] ^ value ^ oldA) & 0x10) ? 0x20 : 0;
	m_Register[f] |= (m_Register[a] == 0) ? 0x80 : 0;
}

void GameBoyZ80::SBCvalFromA(const unsigned char value)
{
	byte oldC = (m_Register[f] & 0x10) ? 1 : 0;
	unsigned short sum = value + oldC;
	byte result = byte(m_Register[a] - sum);

	m_Register[Reg::f] = 0x40;
	if (!result) m_Register[f] |= 0x80;
	if ((m_Register[a] & 0x0F) < (value & 0x0F)) m_Register[Reg::f] |= 0x20;
	else if ((m_Register[a] & 0x0F) < (sum & 0x0F)) m_Register[Reg::f] |= 0x20;
	else if (((m_Register[a] & 0x0F) == (value & 0x0F)) && ((value & 0x0F) == 0x0F) && oldC) m_Register[Reg::f] |= 0x20;
	if (m_Register[a] < sum) m_Register[Reg::f] |= 0x10;

	m_Register[a] = result;
}

void GameBoyZ80::CPval(const unsigned char value)
{
	unsigned char oldA = m_Register[a];
	oldA -= value;
	m_Register[f] = (m_Register[a] < oldA) ? 0x50 : 0x40;
	// set H if n
	m_Register[f] |= ((m_Register[a] ^ value ^ oldA) & 0x10) ? 0x20 : 0;
	m_Register[f] |= (oldA == 0) ? 0x80 : 0;
}

void GameBoyZ80::INCreg(Reg r)
{
	unsigned char olVal = m_Register[r];
	++m_Register[r];
	m_Register[f] &= 0x10;
	//if (m_Register[r] & 0xF == 0) m_Register[f] |= 0x20;
	m_Register[f] |= ((olVal & 0xF0) != (m_Register[r] & 0xF0)) ? 0x20 : 0;
	m_Register[f] |= (m_Register[r] == 0) ? 0x80 : 0;
}

void GameBoyZ80::INCHLm()
{
	unsigned char olVal = m_MMU.ReadByte((m_Register[h] << 8) + m_Register[l]);
	unsigned char newVal = olVal + 1;
	m_MMU.WriteByte((m_Register[h] << 8) + m_Register[l], newVal);
	m_Register[f] &= 0x10;
	m_Register[f] |= ((olVal & 0xF0) != (newVal & 0xF0)) ? 0x20 : 0;
	//if (olVal & 0x) m_Register[f] |= 0x20;
	if (newVal == 0) m_Register[f] |= 0x80;
	m_ClockM = 12;
}

void GameBoyZ80::DECreg(Reg r)
{
	unsigned char olVal = m_Register[r];
	--m_Register[r];
	m_Register[f] &= 0x10;
	m_Register[f] |= ((olVal & 0xF0) != (m_Register[r] & 0xF0)) ? 0x60 : 0x40;
	m_Register[f] |= (m_Register[r] == 0) ? 0x80 : 0;
}

void GameBoyZ80::DECHLm()
{
	unsigned char olVal = m_MMU.ReadByte((m_Register[h] << 8) | m_Register[l]);
	unsigned char newVal = olVal - 1;
	m_MMU.WriteByte((m_Register[h] << 8) + m_Register[l], newVal);
	m_Register[f] &= 0x10;
	m_Register[f] |= ((olVal & 0xF0) != (newVal & 0xF0)) ? 0x60 : 0x40;
	m_Register[f] |= ((olVal - 1) == 0) ? 0x80 : 0;
	m_ClockM = 12;
}

void GameBoyZ80::ADDHLval(const unsigned char bHigh, const unsigned char bLow)
{
	unsigned short oldHL = (m_Register[h] << 8) | m_Register[l];
	unsigned short temp = (bHigh << 8) | bLow;

	m_Register[f] &= 0x80;
	if ((oldHL & 0xFFF) + (temp & 0xFFF) > 0xFFF) m_Register[f] |= 0x20;
	temp += oldHL;
	if (temp < oldHL) m_Register[f] |= 0x10;
	//(m_Register[a] & 0xF) < (oldA & 0xF)
	//if ((bHigh & 0xF00 + (m_Register[h] & 0xF) << 8) > 0xFFF) m_Register[f] |= 0x20;

	m_Register[h] = (temp & 0xFF00) >> 8;
	m_Register[l] = (temp & 0xFF);
}

void GameBoyZ80::ADDspn()
{
	byte immVal = m_MMU.ReadByte(m_PC++);

	m_Register[f] = 0;
	if (((m_SP & 0x0F) + (immVal & 0x0F)) > 0x0F) m_Register[f] |= FLAG_H;
	if ((m_SP + immVal) > 0xFF) m_Register[f] |= FLAG_C;

	m_SP += immVal;
	m_ClockM = 16;
}

void GameBoyZ80::INCBC()
{
	++m_Register[c];
	if (m_Register[c] == 0) ++m_Register[b];
	/*unsigned short val = ((m_Register[b] << 8) | m_Register[c]) + 1;
	m_Register[b] = (val & 0xFF00) >> 8;
	m_Register[c] = val & 0xFF; */
	m_ClockM = 8;
}

void GameBoyZ80::INCDE()
{
	++m_Register[e];
	if (m_Register[e] == 0) ++m_Register[d];
	//unsigned short val = (m_Register[d] << 8) + m_Register[e] + 1;
	//m_Register[d] = (val & 0xFF00) >> 8;
	//m_Register[e] = val & 0xFF;
	m_ClockM = 8;
}

void GameBoyZ80::INCHL()
{
	++m_Register[l];
	if (m_Register[l] == 0) ++m_Register[h];
	//unsigned short val = (m_Register[h] << 8) + m_Register[l] + 1;
	//m_Register[h] = (val & 0xFF00) >> 8;
	//m_Register[l] = val & 0xFF;
	m_ClockM = 8;
}

void GameBoyZ80::DECBC()
{
	--m_Register[c];
	if (m_Register[c] == 255) --m_Register[b];
	//unsigned short val = (m_Register[b] << 8) + m_Register[c] - 1;
	//m_Register[b] = (val & 0xFF00) >> 8;
	//m_Register[c] = val & 0xFF;
	m_ClockM = 8;
}

void GameBoyZ80::DECDE()
{
	--m_Register[e];
	if (m_Register[e] == 255) --m_Register[d];
	//unsigned short val = (m_Register[d] << 8) + m_Register[e] - 1;
	//m_Register[d] = (val & 0xFF00) >> 8;
	//m_Register[e] = val & 0xFF;
	m_ClockM = 8;
}

void GameBoyZ80::DECHL()
{
	--m_Register[l];
	if (m_Register[l] == 255) --m_Register[h];
	//unsigned short val = (m_Register[h] << 8) + m_Register[l] - 1;
	//m_Register[h] = (val & 0xFF00) >> 8;
	//m_Register[l] = val & 0xFF;
	m_ClockM = 8;
}

void GameBoyZ80::DAA() // BGBboy
{
	UI16 regA = m_Register[Reg::a];
	if ((m_Register[Reg::f] & FLAG_N) == 0)
	{
		if ((m_Register[Reg::f] & FLAG_H) > 0 || (regA & 0xF) > 9) regA += 0x06;
		if ((m_Register[Reg::f] & FLAG_C) > 0 || (regA > 0x9F)) regA += 0x60;
	}
	else
	{
		if ((m_Register[Reg::f] & FLAG_H) > 0) regA = (regA - 6) & 0xFF;
		if ((m_Register[Reg::f] & FLAG_C) > 0) regA -= 0x60;
	}

	m_Register[Reg::f] &= FLAG_N;
	if ((regA & 0x100) == 0x100) m_Register[Reg::f] |= FLAG_C;

	regA &= 0xFF;

	if (regA == 0) m_Register[Reg::f] |= FLAG_Z;
	m_Register[Reg::a] = byte(regA);
}

void GameBoyZ80::RLCr(Reg r)
{
	m_Register[f] = (m_Register[r] & 0x80) ? 0x10 : 0;
	m_Register[r] = (unsigned char)(m_Register[r] << 1);
	if (m_Register[f] & 0x10) ++m_Register[r];
	if (m_Register[r] == 0) m_Register[f] |= 0x80;
}

void GameBoyZ80::RLr(Reg r)
{
	unsigned char oldCarry = (m_Register[f] & 0x10) ? 1 : 0;
	m_Register[f] = (m_Register[r] & 0x80) ? 0x10 : 0;
	m_Register[r] = (unsigned char)(m_Register[r] << 1) + oldCarry;
	if (m_Register[r] == 0) m_Register[f] |= 0x80;
}

void GameBoyZ80::RRCr(Reg r)
{
	m_Register[f] = (m_Register[r] & 0x1) ? 0x10 : 0;
	m_Register[r] = (unsigned char)(m_Register[r] >> 1);
	if (m_Register[f] & 0x10) m_Register[r] |= 0x80;
	if (m_Register[r] == 0) m_Register[f] |= 0x80;
}

void GameBoyZ80::RRr(Reg r)
{
	unsigned char oldCarry = (m_Register[f] & 0x10) ? 1 : 0;
	m_Register[f] = (m_Register[r] & 0x1) ? 0x10 : 0;
	m_Register[r] = (unsigned char)(m_Register[r] >> 1) + (oldCarry << 7);
	if (m_Register[r] == 0) m_Register[f] |= 0x80;
}

void GameBoyZ80::RLCHL()
{
	unsigned char hl = m_MMU.ReadByte((m_Register[h] << 8) + m_Register[l]);
	m_Register[f] = (hl & 0x80) ? 0x10 : 0;
	hl = (unsigned char)(hl << 1);
	if (m_Register[f] & 0x10) ++hl;
	if (hl == 0) m_Register[f] |= 0x80;
	m_MMU.WriteByte((m_Register[h] << 8) + m_Register[l], hl);

	m_ClockM = 16;
}

void GameBoyZ80::RLHL()
{
	unsigned char hl = m_MMU.ReadByte((m_Register[h] << 8) + m_Register[l]);
	unsigned char oldCarry = (m_Register[f] & 0x10) ? 1 : 0;
	m_Register[f] = (hl & 0x80) ? 0x10 : 0;
	hl = (unsigned char)(hl << 1) + oldCarry;
	if (hl == 0) m_Register[f] |= 0x80;
	m_MMU.WriteByte((m_Register[h] << 8) + m_Register[l], hl);

	m_ClockM = 16;
}

void GameBoyZ80::RRCHL()
{
	unsigned char hl = m_MMU.ReadByte((m_Register[h] << 8) + m_Register[l]);
	m_Register[f] = (hl & 0x1) ? 0x10 : 0;
	hl = (unsigned char)(hl >> 1);
	if (m_Register[f] & 0x10) hl |= 0x80;
	if (hl == 0) m_Register[f] |= 0x80;
	m_MMU.WriteByte((m_Register[h] << 8) + m_Register[l], hl);

	m_ClockM = 16;
}

void GameBoyZ80::RRHL()
{
	unsigned char hl = m_MMU.ReadByte((m_Register[h] << 8) + m_Register[l]);
	unsigned char oldCarry = (m_Register[f] & 0x10) ? 1 : 0;
	m_Register[f] = (hl & 0x1) ? 0x10 : 0;
	hl = (unsigned char)(hl >> 1) + (oldCarry << 7);
	if (hl == 0) m_Register[f] |= 0x80;
	m_MMU.WriteByte((m_Register[h] << 8) + m_Register[l], hl);

	m_ClockM = 16;
}

void GameBoyZ80::SLAr_r(Reg r)
{
	m_Register[f] = (m_Register[r] & 0x80) ? 0x10 : 0;
	m_Register[r] = m_Register[r] << 1;
	if (!m_Register[r]) m_Register[f] |= 0x80;
}

void GameBoyZ80::SLAHL()
{
	unsigned char hl = m_MMU.ReadByte((m_Register[h] << 8) + m_Register[l]);

	m_Register[f] = (hl & 0x80) ? 0x10 : 0;
	hl = hl << 1;
	if (!hl) m_Register[f] |= 0x80;

	m_MMU.WriteByte((m_Register[h] << 8) + m_Register[l], hl);
	m_ClockM = 4;
}

void GameBoyZ80::SRAr_r(Reg r)
{
	m_Register[f] = (m_Register[r] & 0x1) ? 0x10 : 0;
	m_Register[r] = (m_Register[r] & 0x80) + (m_Register[r] >> 1);
	//if (m_Register[r] & 0x40) m_Register[r] |= 0x80;
	if (m_Register[r] == 0) m_Register[f] |= 0x80;
}

void GameBoyZ80::SRAHL()
{
	unsigned char hl = m_MMU.ReadByte((m_Register[h] << 8) + m_Register[l]);

	m_Register[f] = (hl & 0x1) ? 0x10 : 0;
	hl = (hl & 0x80) + (hl >> 1);
	//if (hl & 0x40) hl |= 0x80;
	if (hl == 0) m_Register[f] |= 0x80;

	m_MMU.WriteByte((m_Register[h] << 8) + m_Register[l], hl);
	m_ClockM = 4;
}

void GameBoyZ80::SRLr_r(Reg r)
{
	m_Register[f] = (m_Register[r] & 0x1) ? 0x10 : 0;
	m_Register[r] = m_Register[r] >> 1;
	if (!m_Register[r]) m_Register[f] |= 0x80;
}

void GameBoyZ80::SRLHL()
{
	unsigned char hl = m_MMU.ReadByte((m_Register[h] << 8) + m_Register[l]);

	m_Register[f] = (hl & 0x1) ? 0x10 : 0;
	hl = hl >> 1;
	if (!hl) m_Register[f] |= 0x80;

	m_MMU.WriteByte((m_Register[h] << 8) + m_Register[l], hl);
	m_ClockM = 4;
}

void GameBoyZ80::SWAPnibble(unsigned char * var)
{
	unsigned char newValue = ((*var & 0x0F) << 4) | ((*var & 0xF0) >> 4);
	m_Register[f] = (newValue == 0) ? 0x80 : 0;
	*var = newValue;
}

void GameBoyZ80::POPAF()
{
	m_Register[f] = m_MMU.ReadByte(m_SP++);
	m_Register[f] &= 0xF0;
	m_Register[a] = m_MMU.ReadByte(m_SP++);
	m_ClockM = 12;
}

void GameBoyZ80::InstrCB()
{
	m_Opcode = m_MMU.ReadByte(m_PC++);

	switch (m_Opcode & 0xF0)
	{
	case 0x00:
		switch (m_Opcode & 0xF)
		{
		case 0x00: RLCr_b();	break;
		case 0x01: RLCr_c();	break;
		case 0x02: RLCr_d();	break;
		case 0x03: RLCr_e();	break;
		case 0x04: RLCr_h();	break;
		case 0x05: RLCr_l();	break;
		case 0x06: RLCHL();		break;
		case 0x07: RLCr_a();	break;
		case 0x08: RRCr_b();	break;
		case 0x09: RRCr_c();	break;
		case 0x0A: RRCr_d();	break;
		case 0x0B: RRCr_e();	break;
		case 0x0C: RRCr_h();	break;
		case 0x0D: RRCr_l();	break;
		case 0x0E: RRCHL();		break;
		case 0x0F: RRCr_a();	break;
		}
		break;
	case 0x10:
		switch (m_Opcode & 0xF)
		{
		case 0x00: RLr_b();		break;
		case 0x01: RLr_c();		break;
		case 0x02: RLr_d();		break;
		case 0x03: RLr_e();		break;
		case 0x04: RLr_h();		break;
		case 0x05: RLr_l();		break;
		case 0x06: RLHL();		break;
		case 0x07: RLr_a();		break;
		case 0x08: RRr_b();		break;
		case 0x09: RRr_c();		break;
		case 0x0A: RRr_d();		break;
		case 0x0B: RRr_e();		break;
		case 0x0C: RRr_h();		break;
		case 0x0D: RRr_l();		break;
		case 0x0E: RRHL();		break;
		case 0x0F: RRr_a();		break;
		}
		break;
	case 0x20:
		switch (m_Opcode & 0xF)
		{
		case 0x00: SLAr_b();	break;
		case 0x01: SLAr_c();	break;
		case 0x02: SLAr_d();	break;
		case 0x03: SLAr_e();	break;
		case 0x04: SLAr_h();	break;
		case 0x05: SLAr_l();	break;
		case 0x06: SLAHL();		break;
		case 0x07: SLAr_a();	break;
		case 0x08: SRAr_b();	break;
		case 0x09: SRAr_c();	break;
		case 0x0A: SRAr_d();	break;
		case 0x0B: SRAr_e();	break;
		case 0x0C: SRAr_h();	break;
		case 0x0D: SRAr_l();	break;
		case 0x0E: SRAHL();		break;
		case 0x0F: SRAr_a();	break;
		}
		break;
	case 0x30:
		switch (m_Opcode & 0xF)
		{
		case 0x00: SWAPr_b();	break;
		case 0x01: SWAPr_c();	break;
		case 0x02: SWAPr_d();	break;
		case 0x03: SWAPr_e();	break;
		case 0x04: SWAPr_h();	break;
		case 0x05: SWAPr_l();	break;
		case 0x06: SWAPHLm();	break;
		case 0x07: SWAPr_a();	break;
		case 0x08: SRLr_b();	break;
		case 0x09: SRLr_c();	break;
		case 0x0A: SRLr_d();	break;
		case 0x0B: SRLr_e();	break;
		case 0x0C: SRLr_h();	break;
		case 0x0D: SRLr_l();	break;
		case 0x0E: SRLHL();		break;
		case 0x0F: SRLr_a();	break;
		}
		break;
	case 0x40:
		switch (m_Opcode & 0xF)
		{
		case 0x00: BIT0_b();	break;
		case 0x01: BIT0_c();	break;
		case 0x02: BIT0_d();	break;
		case 0x03: BIT0_e();	break;
		case 0x04: BIT0_h();	break;
		case 0x05: BIT0_l();	break;
		case 0x06: BIT0_HL();	break;
		case 0x07: BIT0_a();	break;
		case 0x08: BIT1_b();	break;
		case 0x09: BIT1_c();	break;
		case 0x0A: BIT1_d();	break;
		case 0x0B: BIT1_e();	break;
		case 0x0C: BIT1_h();	break;
		case 0x0D: BIT1_l();	break;
		case 0x0E: BIT1_HL();	break;
		case 0x0F: BIT1_a();	break;
		}
		break;
	case 0x50:
		switch (m_Opcode & 0xF)
		{
		case 0x00: BIT2_b();	break;
		case 0x01: BIT2_c();	break;
		case 0x02: BIT2_d();	break;
		case 0x03: BIT2_e();	break;
		case 0x04: BIT2_h();	break;
		case 0x05: BIT2_l();	break;
		case 0x06: BIT2_HL();	break;
		case 0x07: BIT2_a();	break;
		case 0x08: BIT3_b();	break;
		case 0x09: BIT3_c();	break;
		case 0x0A: BIT3_d();	break;
		case 0x0B: BIT3_e();	break;
		case 0x0C: BIT3_h();	break;
		case 0x0D: BIT3_l();	break;
		case 0x0E: BIT3_HL();	break;
		case 0x0F: BIT3_a();	break;
		}
		break;
	case 0x60:
		switch (m_Opcode & 0xF)
		{
		case 0x00: BIT4_b();	break;
		case 0x01: BIT4_c();	break;
		case 0x02: BIT4_d();	break;
		case 0x03: BIT4_e();	break;
		case 0x04: BIT4_h();	break;
		case 0x05: BIT4_l();	break;
		case 0x06: BIT4_HL();	break;
		case 0x07: BIT4_a();	break;
		case 0x08: BIT5_b();	break;
		case 0x09: BIT5_c();	break;
		case 0x0A: BIT5_d();	break;
		case 0x0B: BIT5_e();	break;
		case 0x0C: BIT5_h();	break;
		case 0x0D: BIT5_l();	break;
		case 0x0E: BIT5_HL();	break;
		case 0x0F: BIT5_a();	break;
		}
		break;
	case 0x70:
		switch (m_Opcode & 0xF)
		{
		case 0x00: BIT6_b();	break;
		case 0x01: BIT6_c();	break;
		case 0x02: BIT6_d();	break;
		case 0x03: BIT6_e();	break;
		case 0x04: BIT6_h();	break;
		case 0x05: BIT6_l();	break;
		case 0x06: BIT6_HL();	break;
		case 0x07: BIT6_a();	break;
		case 0x08: BIT7_b();	break;
		case 0x09: BIT7_c();	break;
		case 0x0A: BIT7_d();	break;
		case 0x0B: BIT7_e();	break;
		case 0x0C: BIT7_h();	break;
		case 0x0D: BIT7_l();	break;
		case 0x0E: BIT7_HL();	break;
		case 0x0F: BIT7_a();	break;
		}
		break;
	case 0x80:
		switch (m_Opcode & 0xF)
		{
		case 0x00: RES0_b();	break;
		case 0x01: RES0_c();	break;
		case 0x02: RES0_d();	break;
		case 0x03: RES0_e();	break;
		case 0x04: RES0_h();	break;
		case 0x05: RES0_l();	break;
		case 0x06: RES0_HL();	break;
		case 0x07: RES0_a();	break;
		case 0x08: RES1_b();	break;
		case 0x09: RES1_c();	break;
		case 0x0A: RES1_d();	break;
		case 0x0B: RES1_e();	break;
		case 0x0C: RES1_h();	break;
		case 0x0D: RES1_l();	break;
		case 0x0E: RES1_HL();	break;
		case 0x0F: RES1_a();	break;
		}
		break;
	case 0x90:
		switch (m_Opcode & 0xF)
		{
		case 0x00: RES2_b();	break;
		case 0x01: RES2_c();	break;
		case 0x02: RES2_d();	break;
		case 0x03: RES2_e();	break;
		case 0x04: RES2_h();	break;
		case 0x05: RES2_l();	break;
		case 0x06: RES2_HL();	break;
		case 0x07: RES2_a();	break;
		case 0x08: RES3_b();	break;
		case 0x09: RES3_c();	break;
		case 0x0A: RES3_d();	break;
		case 0x0B: RES3_e();	break;
		case 0x0C: RES3_h();	break;
		case 0x0D: RES3_l();	break;
		case 0x0E: RES3_HL();	break;
		case 0x0F: RES3_a();	break;
		}
		break;
	case 0xA0:
		switch (m_Opcode & 0xF)
		{
		case 0x00: RES4_b();	break;
		case 0x01: RES4_c();	break;
		case 0x02: RES4_d();	break;
		case 0x03: RES4_e();	break;
		case 0x04: RES4_h();	break;
		case 0x05: RES4_l();	break;
		case 0x06: RES4_HL();	break;
		case 0x07: RES4_a();	break;
		case 0x08: RES5_b();	break;
		case 0x09: RES5_c();	break;
		case 0x0A: RES5_d();	break;
		case 0x0B: RES5_e();	break;
		case 0x0C: RES5_h();	break;
		case 0x0D: RES5_l();	break;
		case 0x0E: RES5_HL();	break;
		case 0x0F: RES5_a();	break;
		}
		break;
	case 0xB0:
		switch (m_Opcode & 0xF)
		{
		case 0x00: RES6_b();	break;
		case 0x01: RES6_c();	break;
		case 0x02: RES6_d();	break;
		case 0x03: RES6_e();	break;
		case 0x04: RES6_h();	break;
		case 0x05: RES6_l();	break;
		case 0x06: RES6_HL();	break;
		case 0x07: RES6_a();	break;
		case 0x08: RES7_b();	break;
		case 0x09: RES7_c();	break;
		case 0x0A: RES7_d();	break;
		case 0x0B: RES7_e();	break;
		case 0x0C: RES7_h();	break;
		case 0x0D: RES7_l();	break;
		case 0x0E: RES7_HL();	break;
		case 0x0F: RES7_a();	break;
		}
		break;
	case 0xC0:
		switch (m_Opcode & 0xF)
		{
		case 0x00: SET0_b();	break;
		case 0x01: SET0_c();	break;
		case 0x02: SET0_d();	break;
		case 0x03: SET0_e();	break;
		case 0x04: SET0_h();	break;
		case 0x05: SET0_l();	break;
		case 0x06: SET0_HL();	break;
		case 0x07: SET0_a();	break;
		case 0x08: SET1_b();	break;
		case 0x09: SET1_c();	break;
		case 0x0A: SET1_d();	break;
		case 0x0B: SET1_e();	break;
		case 0x0C: SET1_h();	break;
		case 0x0D: SET1_l();	break;
		case 0x0E: SET1_HL();	break;
		case 0x0F: SET1_a();	break;
		}
		break;
	case 0xD0:
		switch (m_Opcode & 0xF)
		{
		case 0x00: SET2_b();	break;
		case 0x01: SET2_c();	break;
		case 0x02: SET2_d();	break;
		case 0x03: SET2_e();	break;
		case 0x04: SET2_h();	break;
		case 0x05: SET2_l();	break;
		case 0x06: SET2_HL();	break;
		case 0x07: SET2_a();	break;
		case 0x08: SET3_b();	break;
		case 0x09: SET3_c();	break;
		case 0x0A: SET3_d();	break;
		case 0x0B: SET3_e();	break;
		case 0x0C: SET3_h();	break;
		case 0x0D: SET3_l();	break;
		case 0x0E: SET3_HL();	break;
		case 0x0F: SET3_a();	break;
		}
		break;
	case 0xE0:
		switch (m_Opcode & 0xF)
		{
		case 0x00: SET4_b();	break;
		case 0x01: SET4_c();	break;
		case 0x02: SET4_d();	break;
		case 0x03: SET4_e();	break;
		case 0x04: SET4_h();	break;
		case 0x05: SET4_l();	break;
		case 0x06: SET4_HL();	break;
		case 0x07: SET4_a();	break;
		case 0x08: SET5_b();	break;
		case 0x09: SET5_c();	break;
		case 0x0A: SET5_d();	break;
		case 0x0B: SET5_e();	break;
		case 0x0C: SET5_h();	break;
		case 0x0D: SET5_l();	break;
		case 0x0E: SET5_HL();	break;
		case 0x0F: SET5_a();	break;
		}
		break;
	case 0xF0:
		switch (m_Opcode & 0xF)
		{
		case 0x00: SET6_b();	break;
		case 0x01: SET6_c();	break;
		case 0x02: SET6_d();	break;
		case 0x03: SET6_e();	break;
		case 0x04: SET6_h();	break;
		case 0x05: SET6_l();	break;
		case 0x06: SET6_HL();	break;
		case 0x07: SET6_a();	break;
		case 0x08: SET7_b();	break;
		case 0x09: SET7_c();	break;
		case 0x0A: SET7_d();	break;
		case 0x0B: SET7_e();	break;
		case 0x0C: SET7_h();	break;
		case 0x0D: SET7_l();	break;
		case 0x0E: SET7_HL();	break;
		case 0x0F: SET7_a();	break;
		}
		break;
	}
}

#pragma region Load_Store_8b

// Load register from register
void GameBoyZ80::LDrr_bb() { m_Register[b] = m_Register[b]; m_ClockM = 4; }
void GameBoyZ80::LDrr_bc() { m_Register[b] = m_Register[c]; m_ClockM = 4; }
void GameBoyZ80::LDrr_bd() { m_Register[b] = m_Register[d]; m_ClockM = 4; }
void GameBoyZ80::LDrr_be() { m_Register[b] = m_Register[e]; m_ClockM = 4; }
void GameBoyZ80::LDrr_bh() { m_Register[b] = m_Register[h]; m_ClockM = 4; }
void GameBoyZ80::LDrr_bl() { m_Register[b] = m_Register[l]; m_ClockM = 4; }
void GameBoyZ80::LDrr_ba() { m_Register[b] = m_Register[a]; m_ClockM = 4; }
void GameBoyZ80::LDrr_cb() { m_Register[c] = m_Register[b]; m_ClockM = 4; }
void GameBoyZ80::LDrr_cc() { m_Register[c] = m_Register[c]; m_ClockM = 4; }
void GameBoyZ80::LDrr_cd() { m_Register[c] = m_Register[d]; m_ClockM = 4; }
void GameBoyZ80::LDrr_ce() { m_Register[c] = m_Register[e]; m_ClockM = 4; }
void GameBoyZ80::LDrr_ch() { m_Register[c] = m_Register[h]; m_ClockM = 4; }
void GameBoyZ80::LDrr_cl() { m_Register[c] = m_Register[l]; m_ClockM = 4; }
void GameBoyZ80::LDrr_ca() { m_Register[c] = m_Register[a]; m_ClockM = 4; }
void GameBoyZ80::LDrr_db() { m_Register[d] = m_Register[b]; m_ClockM = 4; }
void GameBoyZ80::LDrr_dc() { m_Register[d] = m_Register[c]; m_ClockM = 4; }
void GameBoyZ80::LDrr_dd() { m_Register[d] = m_Register[d]; m_ClockM = 4; }
void GameBoyZ80::LDrr_de() { m_Register[d] = m_Register[e]; m_ClockM = 4; }
void GameBoyZ80::LDrr_dh() { m_Register[d] = m_Register[h]; m_ClockM = 4; }
void GameBoyZ80::LDrr_dl() { m_Register[d] = m_Register[l]; m_ClockM = 4; }
void GameBoyZ80::LDrr_da() { m_Register[d] = m_Register[a]; m_ClockM = 4; }
void GameBoyZ80::LDrr_eb() { m_Register[e] = m_Register[b]; m_ClockM = 4; }
void GameBoyZ80::LDrr_ec() { m_Register[e] = m_Register[c]; m_ClockM = 4; }
void GameBoyZ80::LDrr_ed() { m_Register[e] = m_Register[d]; m_ClockM = 4; }
void GameBoyZ80::LDrr_ee() { m_Register[e] = m_Register[e]; m_ClockM = 4; }
void GameBoyZ80::LDrr_eh() { m_Register[e] = m_Register[h]; m_ClockM = 4; }
void GameBoyZ80::LDrr_el() { m_Register[e] = m_Register[l]; m_ClockM = 4; }
void GameBoyZ80::LDrr_ea() { m_Register[e] = m_Register[a]; m_ClockM = 4; }
void GameBoyZ80::LDrr_hb() { m_Register[h] = m_Register[b]; m_ClockM = 4; }
void GameBoyZ80::LDrr_hc() { m_Register[h] = m_Register[c]; m_ClockM = 4; }
void GameBoyZ80::LDrr_hd() { m_Register[h] = m_Register[d]; m_ClockM = 4; }
void GameBoyZ80::LDrr_he() { m_Register[h] = m_Register[e]; m_ClockM = 4; }
void GameBoyZ80::LDrr_hh() { m_Register[h] = m_Register[h]; m_ClockM = 4; }
void GameBoyZ80::LDrr_hl() { m_Register[h] = m_Register[l]; m_ClockM = 4; }
void GameBoyZ80::LDrr_ha() { m_Register[h] = m_Register[a]; m_ClockM = 4; }
void GameBoyZ80::LDrr_lb() { m_Register[l] = m_Register[b]; m_ClockM = 4; }
void GameBoyZ80::LDrr_lc() { m_Register[l] = m_Register[c]; m_ClockM = 4; }
void GameBoyZ80::LDrr_ld() { m_Register[l] = m_Register[d]; m_ClockM = 4; }
void GameBoyZ80::LDrr_le() { m_Register[l] = m_Register[e]; m_ClockM = 4; }
void GameBoyZ80::LDrr_lh() { m_Register[l] = m_Register[h]; m_ClockM = 4; }
void GameBoyZ80::LDrr_ll() { m_Register[l] = m_Register[l]; m_ClockM = 4; }
void GameBoyZ80::LDrr_la() { m_Register[l] = m_Register[a]; m_ClockM = 4; }
void GameBoyZ80::LDrr_ab() { m_Register[a] = m_Register[b]; m_ClockM = 4; }
void GameBoyZ80::LDrr_ac() { m_Register[a] = m_Register[c]; m_ClockM = 4; }
void GameBoyZ80::LDrr_ad() { m_Register[a] = m_Register[d]; m_ClockM = 4; }
void GameBoyZ80::LDrr_ae() { m_Register[a] = m_Register[e]; m_ClockM = 4; }
void GameBoyZ80::LDrr_ah() { m_Register[a] = m_Register[h]; m_ClockM = 4; }
void GameBoyZ80::LDrr_al() { m_Register[a] = m_Register[l]; m_ClockM = 4; }
void GameBoyZ80::LDrr_aa() { m_Register[a] = m_Register[a]; m_ClockM = 4; }

// Read byte from memory at HL and store in register
void GameBoyZ80::LDrHLm_a() { m_Register[a] = m_MMU.ReadByte((m_Register[h] << 8) | m_Register[l]); m_ClockM = 8; }
void GameBoyZ80::LDrHLm_b() { m_Register[b] = m_MMU.ReadByte((m_Register[h] << 8) | m_Register[l]); m_ClockM = 8; }
void GameBoyZ80::LDrHLm_c() { m_Register[c] = m_MMU.ReadByte((m_Register[h] << 8) | m_Register[l]); m_ClockM = 8; }
void GameBoyZ80::LDrHLm_d() { m_Register[d] = m_MMU.ReadByte((m_Register[h] << 8) | m_Register[l]); m_ClockM = 8; }
void GameBoyZ80::LDrHLm_e() { m_Register[e] = m_MMU.ReadByte((m_Register[h] << 8) | m_Register[l]); m_ClockM = 8; }
void GameBoyZ80::LDrHLm_h() { m_Register[h] = m_MMU.ReadByte((m_Register[h] << 8) | m_Register[l]); m_ClockM = 8; }
void GameBoyZ80::LDrHLm_l() { m_Register[l] = m_MMU.ReadByte((m_Register[h] << 8) | m_Register[l]); m_ClockM = 8; }

// Store number(byte) in register
void GameBoyZ80::LDrn_a() { m_Register[a] = m_MMU.ReadByte(m_PC); ++m_PC; m_ClockM = 8; }
void GameBoyZ80::LDrn_b() { m_Register[b] = m_MMU.ReadByte(m_PC); ++m_PC; m_ClockM = 8; }
void GameBoyZ80::LDrn_c() { m_Register[c] = m_MMU.ReadByte(m_PC); ++m_PC; m_ClockM = 8; }
void GameBoyZ80::LDrn_d() { m_Register[d] = m_MMU.ReadByte(m_PC); ++m_PC; m_ClockM = 8; }
void GameBoyZ80::LDrn_e() { m_Register[e] = m_MMU.ReadByte(m_PC); ++m_PC; m_ClockM = 8; }
void GameBoyZ80::LDrn_h() { m_Register[h] = m_MMU.ReadByte(m_PC); ++m_PC; m_ClockM = 8; }
void GameBoyZ80::LDrn_l() { m_Register[l] = m_MMU.ReadByte(m_PC); ++m_PC; m_ClockM = 8; }

// Write byte to memory at HL from register
void GameBoyZ80::LDHLmr_a() { m_MMU.WriteByte((m_Register[h] << 8) | m_Register[l], m_Register[a]); m_ClockM = 2; }
void GameBoyZ80::LDHLmr_b() { m_MMU.WriteByte((m_Register[h] << 8) | m_Register[l], m_Register[b]); m_ClockM = 2; }
void GameBoyZ80::LDHLmr_c() { m_MMU.WriteByte((m_Register[h] << 8) | m_Register[l], m_Register[c]); m_ClockM = 2; }
void GameBoyZ80::LDHLmr_d() { m_MMU.WriteByte((m_Register[h] << 8) | m_Register[l], m_Register[d]); m_ClockM = 2; }
void GameBoyZ80::LDHLmr_e() { m_MMU.WriteByte((m_Register[h] << 8) | m_Register[l], m_Register[e]); m_ClockM = 2; }
void GameBoyZ80::LDHLmr_h() { m_MMU.WriteByte((m_Register[h] << 8) | m_Register[l], m_Register[h]); m_ClockM = 2; }
void GameBoyZ80::LDHLmr_l() { m_MMU.WriteByte((m_Register[h] << 8) | m_Register[l], m_Register[l]); m_ClockM = 2; }

// Store value at number to memory at HL
void GameBoyZ80::LDHLmn() { m_MMU.WriteByte((m_Register[h] << 8) + m_Register[l], m_MMU.ReadByte(m_PC)); m_PC++; m_ClockM = 12; }

// Load register 'A' from memory at register-pair
void GameBoyZ80::LDABCm() { m_Register[a] = m_MMU.ReadByte((m_Register[b] << 8) + m_Register[c]); m_ClockM = 8; }
void GameBoyZ80::LDADEm() { m_Register[a] = m_MMU.ReadByte((m_Register[d] << 8) + m_Register[e]); m_ClockM = 8; }

// Store register 'A' in memory at register-pair
void GameBoyZ80::LDBCmA() { m_MMU.WriteByte((m_Register[b] << 8) | m_Register[c], m_Register[a]); m_ClockM = 8; }
void GameBoyZ80::LDDEmA() { m_MMU.WriteByte((m_Register[d] << 8) | m_Register[e], m_Register[a]); m_ClockM = 8; }

// Load register 'A' from memory at position
void GameBoyZ80::LDAmm() { m_Register[a] = m_MMU.ReadByte(m_MMU.ReadWord(m_PC)); m_PC += 2; m_ClockM = 16; }

// Store register 'A' in memory at position
void GameBoyZ80::LDmmA() { m_MMU.WriteByte(m_MMU.ReadWord(m_PC), m_Register[a]); m_PC += 2; m_ClockM = 16; }

// Load register 'A' from 0xFF00 + register 'C'
void GameBoyZ80::LDCmA() { m_Register[a] = m_MMU.ReadByte(0xFF00 + m_Register[c]); m_ClockM = 8; }

// Store register 'A' in memory at 0xFF00 + register 'C'
void GameBoyZ80::LDACm() { m_MMU.WriteByte(0xFF00 + m_Register[c], m_Register[a]); m_ClockM = 8; }

// Load register 'A' from memory at 0xFF00 + number 
void GameBoyZ80::LDHmA() { m_Register[a] = m_MMU.ReadByte(0xFF00 + m_MMU.ReadByte(m_PC)); ++m_PC; m_ClockM = 12; }

// Write register 'A' to memory at 0xFF00 + number 
void GameBoyZ80::LDHAm() { m_MMU.WriteByte(0xFF00 | m_MMU.ReadByte(m_PC), m_Register[a]); ++m_PC; m_ClockM = 12; }

#pragma endregion

#pragma region Load_Store_16b

// Store register-pair from memory at number
void GameBoyZ80::LDBCnn() { m_Register[c] = m_MMU.ReadByte(m_PC++); m_Register[b] = m_MMU.ReadByte(m_PC++), m_ClockM = 12; }
void GameBoyZ80::LDDEnn() { m_Register[e] = m_MMU.ReadByte(m_PC++); m_Register[d] = m_MMU.ReadByte(m_PC++), m_ClockM = 12; }
void GameBoyZ80::LDHLnn() { m_Register[l] = m_MMU.ReadByte(m_PC++); m_Register[h] = m_MMU.ReadByte(m_PC++), m_ClockM = 12; }
void GameBoyZ80::LDSPnn() { m_SP = m_MMU.ReadWord(m_PC); m_PC += 2; m_ClockM = 12; }

// Write stack pointer to memory
void GameBoyZ80::LDnnSP() { m_MMU.WriteWord(m_MMU.ReadWord(m_PC), m_SP); m_PC += 2; m_ClockM = 20; }

// Copy HL to SP 
void GameBoyZ80::LDSPHL() { m_SP = (m_Register[h] << 8) | m_Register[l]; m_ClockM = 8; }

// Push register-pair to memory at stack-pointer -2 
void GameBoyZ80::PUSHBC() { --m_SP; m_MMU.WriteByte(m_SP, m_Register[b]); --m_SP; m_MMU.WriteByte(m_SP, m_Register[c]); m_ClockM = 16; }
void GameBoyZ80::PUSHDE() { --m_SP; m_MMU.WriteByte(m_SP, m_Register[d]); --m_SP; m_MMU.WriteByte(m_SP, m_Register[e]); m_ClockM = 16; }
void GameBoyZ80::PUSHHL() { --m_SP; m_MMU.WriteByte(m_SP, m_Register[h]); --m_SP; m_MMU.WriteByte(m_SP, m_Register[l]); m_ClockM = 16; }
void GameBoyZ80::PUSHAF() { --m_SP; m_MMU.WriteByte(m_SP, m_Register[a]); --m_SP; m_MMU.WriteByte(m_SP, m_Register[f]); m_ClockM = 16; }

// Pop stack-pointer to register-pair
void GameBoyZ80::POPBC() { m_Register[c] = m_MMU.ReadByte(m_SP); m_Register[b] = m_MMU.ReadByte(++m_SP); ++m_SP; m_ClockM = 12; }
void GameBoyZ80::POPDE() { m_Register[e] = m_MMU.ReadByte(m_SP); m_Register[d] = m_MMU.ReadByte(++m_SP); ++m_SP; m_ClockM = 12; }
void GameBoyZ80::POPHL() { m_Register[l] = m_MMU.ReadByte(m_SP); m_Register[h] = m_MMU.ReadByte(++m_SP); ++m_SP; m_ClockM = 12; }

#pragma endregion

#pragma region Arthmetic_8b
// ADD register or value from memory to register 'A'
void GameBoyZ80::ADDa() { ADDvalToA(m_Register[a]); m_ClockM = 4; }
void GameBoyZ80::ADDb() { ADDvalToA(m_Register[b]); m_ClockM = 4; }
void GameBoyZ80::ADDc() { ADDvalToA(m_Register[c]); m_ClockM = 4; }
void GameBoyZ80::ADDd() { ADDvalToA(m_Register[d]); m_ClockM = 4; }
void GameBoyZ80::ADDe() { ADDvalToA(m_Register[e]); m_ClockM = 4; }
void GameBoyZ80::ADDh() { ADDvalToA(m_Register[h]); m_ClockM = 4; }
void GameBoyZ80::ADDl() { ADDvalToA(m_Register[l]); m_ClockM = 4; }
void GameBoyZ80::ADDHL() { ADDvalToA(m_MMU.ReadByte((m_Register[h] << 8) | m_Register[l])); m_ClockM = 8; }
void GameBoyZ80::ADDn() { ADDvalToA(m_MMU.ReadByte(m_PC++)); m_ClockM = 8; }

// ADD register or value from memory to register 'A' and add carry
void GameBoyZ80::ADCa() { ADCvalToA(m_Register[a]); m_ClockM = 4; }
void GameBoyZ80::ADCb() { ADCvalToA(m_Register[b]); m_ClockM = 4; }
void GameBoyZ80::ADCc() { ADCvalToA(m_Register[c]); m_ClockM = 4; }
void GameBoyZ80::ADCd() { ADCvalToA(m_Register[d]); m_ClockM = 4; }
void GameBoyZ80::ADCe() { ADCvalToA(m_Register[e]); m_ClockM = 4; }
void GameBoyZ80::ADCh() { ADCvalToA(m_Register[h]); m_ClockM = 4; }
void GameBoyZ80::ADCl() { ADCvalToA(m_Register[l]); m_ClockM = 4; }
void GameBoyZ80::ADCHL() { ADCvalToA(m_MMU.ReadByte((m_Register[h] << 8) | m_Register[l])); m_ClockM = 8; }
void GameBoyZ80::ADCn()
{
	ADCvalToA(m_MMU.ReadByte(m_PC));
	m_PC++;
	m_ClockM = 8;
}

// SUB register or value from memory from register 'A'
void GameBoyZ80::SUBa() { SUBvalFromA(m_Register[a]); m_ClockM = 4; }
void GameBoyZ80::SUBb() { SUBvalFromA(m_Register[b]); m_ClockM = 4; }
void GameBoyZ80::SUBc() { SUBvalFromA(m_Register[c]); m_ClockM = 4; }
void GameBoyZ80::SUBd() { SUBvalFromA(m_Register[d]); m_ClockM = 4; }
void GameBoyZ80::SUBe() { SUBvalFromA(m_Register[e]); m_ClockM = 4; }
void GameBoyZ80::SUBh() { SUBvalFromA(m_Register[h]); m_ClockM = 4; }
void GameBoyZ80::SUBl() { SUBvalFromA(m_Register[l]); m_ClockM = 4; }
void GameBoyZ80::SUBHL() { SUBvalFromA(m_MMU.ReadByte((m_Register[h] << 8) | m_Register[l])); m_ClockM = 8; }
void GameBoyZ80::SUBn() { SUBvalFromA(m_MMU.ReadByte(m_PC++)); m_ClockM = 8; }

// SUB register or value from memory from register 'A' and sub carry
void GameBoyZ80::SBCa() { SBCvalFromA(m_Register[a]); m_ClockM = 4; }
void GameBoyZ80::SBCb() { SBCvalFromA(m_Register[b]); m_ClockM = 4; }
void GameBoyZ80::SBCc() { SBCvalFromA(m_Register[c]); m_ClockM = 4; }
void GameBoyZ80::SBCd() { SBCvalFromA(m_Register[d]); m_ClockM = 4; }
void GameBoyZ80::SBCe() { SBCvalFromA(m_Register[e]); m_ClockM = 4; }
void GameBoyZ80::SBCh() { SBCvalFromA(m_Register[h]); m_ClockM = 4; }
void GameBoyZ80::SBCl() { SBCvalFromA(m_Register[l]); m_ClockM = 4; }
void GameBoyZ80::SBCHL() { SBCvalFromA(m_MMU.ReadByte((m_Register[h] << 8) | m_Register[l])); m_ClockM = 8; }
void GameBoyZ80::SBCn() 
{
	SBCvalFromA(m_MMU.ReadByte(m_PC)); 
	++m_PC;
	m_ClockM = 8;
}

// AND register or value from memory with register 'A'
void GameBoyZ80::ANDa() { m_Register[a] &= m_Register[a]; m_Register[f] = (m_Register[a] ? 0x20 : 0xA0); m_ClockM = 4; }
void GameBoyZ80::ANDb() { m_Register[a] &= m_Register[b]; m_Register[f] = (m_Register[a] ? 0x20 : 0xA0); m_ClockM = 4; }
void GameBoyZ80::ANDc() { m_Register[a] &= m_Register[c]; m_Register[f] = (m_Register[a] ? 0x20 : 0xA0); m_ClockM = 4; }
void GameBoyZ80::ANDd() { m_Register[a] &= m_Register[d]; m_Register[f] = (m_Register[a] ? 0x20 : 0xA0); m_ClockM = 4; }
void GameBoyZ80::ANDe() { m_Register[a] &= m_Register[e]; m_Register[f] = (m_Register[a] ? 0x20 : 0xA0); m_ClockM = 4; }
void GameBoyZ80::ANDh() { m_Register[a] &= m_Register[h]; m_Register[f] = (m_Register[a] ? 0x20 : 0xA0); m_ClockM = 4; }
void GameBoyZ80::ANDl() { m_Register[a] &= m_Register[l]; m_Register[f] = (m_Register[a] ? 0x20 : 0xA0); m_ClockM = 4; }
void GameBoyZ80::ANDHL() { m_Register[a] &= m_MMU.ReadByte((m_Register[h] << 8) + m_Register[l]); m_Register[f] = (m_Register[a] ? 0x20 : 0xA0); m_ClockM = 8; }
void GameBoyZ80::ANDn() { m_Register[a] &= m_MMU.ReadByte(m_PC++); m_Register[f] = (m_Register[a] ? 0x20 : 0xA0); m_ClockM = 8; }

// OR register or value from memory with register 'A'
void GameBoyZ80::ORa() { m_Register[a] |= m_Register[a]; m_Register[f] = (m_Register[a] ? 0 : 0x80); m_ClockM = 4; }
void GameBoyZ80::ORb() { m_Register[a] |= m_Register[b]; m_Register[f] = (m_Register[a] ? 0 : 0x80); m_ClockM = 4; }
void GameBoyZ80::ORc() { m_Register[a] |= m_Register[c]; m_Register[f] = (m_Register[a] ? 0 : 0x80); m_ClockM = 4; }
void GameBoyZ80::ORd() { m_Register[a] |= m_Register[d]; m_Register[f] = (m_Register[a] ? 0 : 0x80); m_ClockM = 4; }
void GameBoyZ80::ORe() { m_Register[a] |= m_Register[e]; m_Register[f] = (m_Register[a] ? 0 : 0x80); m_ClockM = 4; }
void GameBoyZ80::ORh() { m_Register[a] |= m_Register[h]; m_Register[f] = (m_Register[a] ? 0 : 0x80); m_ClockM = 4; }
void GameBoyZ80::ORl() { m_Register[a] |= m_Register[l]; m_Register[f] = (m_Register[a] ? 0 : 0x80); m_ClockM = 4; }
void GameBoyZ80::ORHL() { m_Register[a] |= m_MMU.ReadByte((m_Register[h] << 8) | m_Register[l]); m_Register[f] = (m_Register[a] ? 0 : 0x80); m_ClockM = 8; }
void GameBoyZ80::ORn() { m_Register[a] |= m_MMU.ReadByte(m_PC++); m_Register[f] = (m_Register[a] ? 0 : 0x80); m_ClockM = 8; }

// XOR register or value from memory with register 'A'
void GameBoyZ80::XORa() { m_Register[a] ^= m_Register[a]; m_Register[f] = (m_Register[a] ? 0 : 0x80); m_ClockM = 4; }
void GameBoyZ80::XORb() { m_Register[a] ^= m_Register[b]; m_Register[f] = (m_Register[a] ? 0 : 0x80); m_ClockM = 4; }
void GameBoyZ80::XORc() { m_Register[a] ^= m_Register[c]; m_Register[f] = (m_Register[a] ? 0 : 0x80); m_ClockM = 4; }
void GameBoyZ80::XORd() { m_Register[a] ^= m_Register[d]; m_Register[f] = (m_Register[a] ? 0 : 0x80); m_ClockM = 4; }
void GameBoyZ80::XORe() { m_Register[a] ^= m_Register[e]; m_Register[f] = (m_Register[a] ? 0 : 0x80); m_ClockM = 4; }
void GameBoyZ80::XORh() { m_Register[a] ^= m_Register[h]; m_Register[f] = (m_Register[a] ? 0 : 0x80); m_ClockM = 4; }
void GameBoyZ80::XORl() { m_Register[a] ^= m_Register[l]; m_Register[f] = (m_Register[a] ? 0 : 0x80); m_ClockM = 4; }
void GameBoyZ80::XORHL() { m_Register[a] ^= m_MMU.ReadByte((m_Register[h] << 8) | m_Register[l]); m_Register[f] = (m_Register[a] ? 0 : 0x80); m_ClockM = 8; }
void GameBoyZ80::XORn() { m_Register[a] ^= m_MMU.ReadByte(m_PC++); m_Register[f] = (m_Register[a] ? 0 : 0x80); m_ClockM = 8; }

// SUB register or value from memory from register 'A' and sub carry
void GameBoyZ80::CPa() { CPval(m_Register[a]); m_ClockM = 4; }
void GameBoyZ80::CPb() { CPval(m_Register[b]); m_ClockM = 4; }
void GameBoyZ80::CPc() { CPval(m_Register[c]); m_ClockM = 4; }
void GameBoyZ80::CPd() { CPval(m_Register[d]); m_ClockM = 4; }
void GameBoyZ80::CPe() { CPval(m_Register[e]); m_ClockM = 4; }
void GameBoyZ80::CPh() { CPval(m_Register[h]); m_ClockM = 4; }
void GameBoyZ80::CPl() { CPval(m_Register[l]); m_ClockM = 4; }
void GameBoyZ80::CPHL() { CPval(m_MMU.ReadByte((m_Register[h] << 8) | m_Register[l])); m_ClockM = 8; }
void GameBoyZ80::CPn() { CPval(m_MMU.ReadByte(m_PC++)); m_ClockM = 8; }

// INC register or value from HL from register 'A' and sub carry
void GameBoyZ80::INCa() { INCreg(a); m_ClockM = 4; }
void GameBoyZ80::INCb() { INCreg(b); m_ClockM = 4; }
void GameBoyZ80::INCc() { INCreg(c); m_ClockM = 4; }
void GameBoyZ80::INCd() { INCreg(d); m_ClockM = 4; }
void GameBoyZ80::INCe() { INCreg(e); m_ClockM = 4; }
void GameBoyZ80::INCh() { INCreg(h); m_ClockM = 4; }
void GameBoyZ80::INCl() { INCreg(l); m_ClockM = 4; }

// DEC register or value from HL from register 'A' and sub carry
void GameBoyZ80::DECa() { DECreg(a); m_ClockM = 4; }
void GameBoyZ80::DECb() { DECreg(b); m_ClockM = 4; }
void GameBoyZ80::DECc() { DECreg(c); m_ClockM = 4; }
void GameBoyZ80::DECd() { DECreg(d); m_ClockM = 4; }
void GameBoyZ80::DECe() { DECreg(e); m_ClockM = 4; }
void GameBoyZ80::DECh() { DECreg(h); m_ClockM = 4; }
void GameBoyZ80::DECl() { DECreg(l); m_ClockM = 4; }

#pragma endregion

#pragma region Arthmetic_16b
// ADD register pair or value16b from memory to HL
void GameBoyZ80::ADDHLBC() { ADDHLval(m_Register[b], m_Register[c]); m_ClockM = 8; }
void GameBoyZ80::ADDHLDE() { ADDHLval(m_Register[d], m_Register[e]); m_ClockM = 8; }
void GameBoyZ80::ADDHLHL() { ADDHLval(m_Register[h], m_Register[l]); m_ClockM = 8; }
void GameBoyZ80::ADDHLsp() { ADDHLval((m_SP & 0xFF00) >> 8, m_SP & 0xFF); m_ClockM = 8; }

// INC word
void GameBoyZ80::INCsp() { m_SP += 1; m_ClockM = 8; }

//DEC word
void GameBoyZ80::DECsp() { m_SP -= 1; m_ClockM = 8; }
#pragma endregion

#pragma region Miscellaneous
// SWAP nibbles
void GameBoyZ80::SWAPr_a() { SWAPnibble(&m_Register[a]); m_ClockM = 8; };
void GameBoyZ80::SWAPr_b() { SWAPnibble(&m_Register[b]); m_ClockM = 8; };
void GameBoyZ80::SWAPr_c() { SWAPnibble(&m_Register[c]); m_ClockM = 8; };
void GameBoyZ80::SWAPr_d() { SWAPnibble(&m_Register[d]); m_ClockM = 8; };
void GameBoyZ80::SWAPr_e() { SWAPnibble(&m_Register[e]); m_ClockM = 8; };
void GameBoyZ80::SWAPr_h() { SWAPnibble(&m_Register[h]); m_ClockM = 8; };
void GameBoyZ80::SWAPr_l() { SWAPnibble(&m_Register[l]); m_ClockM = 8; };
void GameBoyZ80::SWAPHLm() { unsigned char v = m_MMU.ReadByte((m_Register[h] << 8) | m_Register[l]); SWAPnibble(&v); m_MMU.WriteByte((m_Register[h] << 8) | m_Register[l], v); m_ClockM = 16; }

// Invert a
void GameBoyZ80::CPL() { m_Register[a] ^= 0xFF; m_Register[f] |= 0x60; m_ClockM = 4; }

// Invert carry flag
void GameBoyZ80::CCF() { unsigned char cFlag = (m_Register[f] & 0x10) ? 0 : 0x10; m_Register[f] = (m_Register[f] & 0x80) | cFlag; m_ClockM = 4; }

// Set carry flag
void GameBoyZ80::SCF() { m_Register[f] &= 0x80; m_Register[f] |= 0x10; m_ClockM = 4; }

// No operation
void GameBoyZ80::NOP() { m_ClockM = 4; }

// Halt CPU until an interrupt occurs
void GameBoyZ80::HALT() { m_Halt = true; m_ClockM = 4; }

// Halt CPU
void GameBoyZ80::STOP() { m_Stop = true; m_ClockM = 4; }

// Disable interrupts
void GameBoyZ80::DI() { m_IntMasterEnable = false; m_ClockT = 4; m_ClockM = 4; }

// Enable interrupts
void GameBoyZ80::EI() { m_IntMasterEnable = true; m_ClockT = 4; m_ClockM = 4; }

#pragma endregion

#pragma region Rotates_Shifts

void GameBoyZ80::RLCA()		{ RLCr(a);	m_ClockM = 8; m_Register[f] &= 0x10; }
void GameBoyZ80::RLA()		{ RLr(a);	m_ClockM = 8; m_Register[f] &= 0x10; }
void GameBoyZ80::RRCA()		{ RRCr(a);	m_ClockM = 8; m_Register[f] &= 0x10; }
void GameBoyZ80::RRA()		{ RRr(a);	m_ClockM = 8; m_Register[f] &= 0x10; }

void GameBoyZ80::RLCr_a() { RLCr(a); m_ClockM = 8; }
void GameBoyZ80::RLCr_b() { RLCr(b); m_ClockM = 8; }
void GameBoyZ80::RLCr_c() { RLCr(c); m_ClockM = 8; }
void GameBoyZ80::RLCr_d() { RLCr(d); m_ClockM = 8; }
void GameBoyZ80::RLCr_e() { RLCr(e); m_ClockM = 8; }
void GameBoyZ80::RLCr_h() { RLCr(h); m_ClockM = 8; }
void GameBoyZ80::RLCr_l() { RLCr(l); m_ClockM = 8; }

void GameBoyZ80::RLr_a() { RLr(a); m_ClockM = 8; }
void GameBoyZ80::RLr_b() { RLr(b); m_ClockM = 8; }
void GameBoyZ80::RLr_c() { RLr(c); m_ClockM = 8; }
void GameBoyZ80::RLr_d() { RLr(d); m_ClockM = 8; }
void GameBoyZ80::RLr_e() { RLr(e); m_ClockM = 8; }
void GameBoyZ80::RLr_h() { RLr(h); m_ClockM = 8; }
void GameBoyZ80::RLr_l() { RLr(l); m_ClockM = 8; }

void GameBoyZ80::RRCr_a() { RRCr(a); m_ClockM = 8; }
void GameBoyZ80::RRCr_b() { RRCr(b); m_ClockM = 8; }
void GameBoyZ80::RRCr_c() { RRCr(c); m_ClockM = 8; }
void GameBoyZ80::RRCr_d() { RRCr(d); m_ClockM = 8; }
void GameBoyZ80::RRCr_e() { RRCr(e); m_ClockM = 8; }
void GameBoyZ80::RRCr_h() { RRCr(h); m_ClockM = 8; }
void GameBoyZ80::RRCr_l() { RRCr(l); m_ClockM = 8; }

void GameBoyZ80::RRr_a() { RRr(a); m_ClockM = 8; }
void GameBoyZ80::RRr_b() { RRr(b); m_ClockM = 8; }
void GameBoyZ80::RRr_c() { RRr(c); m_ClockM = 8; }
void GameBoyZ80::RRr_d() { RRr(d); m_ClockM = 8; }
void GameBoyZ80::RRr_e() { RRr(e); m_ClockM = 8; }
void GameBoyZ80::RRr_h() { RRr(h); m_ClockM = 8; }
void GameBoyZ80::RRr_l() { RRr(l); m_ClockM = 8; }

// Shift n left into carry. LSB set to 0
void GameBoyZ80::SLAr_a() { SLAr_r(a); m_ClockM = 8; }
void GameBoyZ80::SLAr_b() { SLAr_r(b); m_ClockM = 8; }
void GameBoyZ80::SLAr_c() { SLAr_r(c); m_ClockM = 8; }
void GameBoyZ80::SLAr_d() { SLAr_r(d); m_ClockM = 8; }
void GameBoyZ80::SLAr_e() { SLAr_r(e); m_ClockM = 8; }
void GameBoyZ80::SLAr_h() { SLAr_r(h); m_ClockM = 8; }
void GameBoyZ80::SLAr_l() { SLAr_r(l); m_ClockM = 8; }

void GameBoyZ80::SRAr_a() { SRAr_r(a); m_ClockM = 8; }
void GameBoyZ80::SRAr_b() { SRAr_r(b); m_ClockM = 8; }
void GameBoyZ80::SRAr_c() { SRAr_r(c); m_ClockM = 8; }
void GameBoyZ80::SRAr_d() { SRAr_r(d); m_ClockM = 8; }
void GameBoyZ80::SRAr_e() { SRAr_r(e); m_ClockM = 8; }
void GameBoyZ80::SRAr_h() { SRAr_r(h); m_ClockM = 8; }
void GameBoyZ80::SRAr_l() { SRAr_r(l); m_ClockM = 8; }

void GameBoyZ80::SRLr_a() { SRLr_r(a); m_ClockM = 8; }
void GameBoyZ80::SRLr_b() { SRLr_r(b); m_ClockM = 8; }
void GameBoyZ80::SRLr_c() { SRLr_r(c); m_ClockM = 8; }
void GameBoyZ80::SRLr_d() { SRLr_r(d); m_ClockM = 8; }
void GameBoyZ80::SRLr_e() { SRLr_r(e); m_ClockM = 8; }
void GameBoyZ80::SRLr_h() { SRLr_r(h); m_ClockM = 8; }
void GameBoyZ80::SRLr_l() { SRLr_r(l); m_ClockM = 8; }

#pragma endregion

#pragma region Bit_Opcodes

// Check bit in register or HL
void GameBoyZ80::BIT0_a() { BITbr(0x1, m_Register[a]); m_ClockM = 2; }
void GameBoyZ80::BIT1_a() { BITbr(0x2, m_Register[a]); m_ClockM = 2; }
void GameBoyZ80::BIT2_a() { BITbr(0x4, m_Register[a]); m_ClockM = 2; }
void GameBoyZ80::BIT3_a() { BITbr(0x8, m_Register[a]); m_ClockM = 2; }
void GameBoyZ80::BIT4_a() { BITbr(0x10, m_Register[a]); m_ClockM = 2; }
void GameBoyZ80::BIT5_a() { BITbr(0x20, m_Register[a]); m_ClockM = 2; }
void GameBoyZ80::BIT6_a() { BITbr(0x40, m_Register[a]); m_ClockM = 2; }
void GameBoyZ80::BIT7_a() { BITbr(0x80, m_Register[a]); m_ClockM = 2; }

void GameBoyZ80::BIT0_b() { BITbr(0x1, m_Register[b]); m_ClockM = 8; }
void GameBoyZ80::BIT1_b() { BITbr(0x2, m_Register[b]); m_ClockM = 8; }
void GameBoyZ80::BIT2_b() { BITbr(0x4, m_Register[b]); m_ClockM = 8; }
void GameBoyZ80::BIT3_b() { BITbr(0x8, m_Register[b]); m_ClockM = 8; }
void GameBoyZ80::BIT4_b() { BITbr(0x10, m_Register[b]); m_ClockM = 8; }
void GameBoyZ80::BIT5_b() { BITbr(0x20, m_Register[b]); m_ClockM = 8; }
void GameBoyZ80::BIT6_b() { BITbr(0x40, m_Register[b]); m_ClockM = 8; }
void GameBoyZ80::BIT7_b() { BITbr(0x80, m_Register[b]); m_ClockM = 8; }

void GameBoyZ80::BIT0_c() { BITbr(0x1, m_Register[c]); m_ClockM = 8; }
void GameBoyZ80::BIT1_c() { BITbr(0x2, m_Register[c]); m_ClockM = 8; }
void GameBoyZ80::BIT2_c() { BITbr(0x4, m_Register[c]); m_ClockM = 8; }
void GameBoyZ80::BIT3_c() { BITbr(0x8, m_Register[c]); m_ClockM = 8; }
void GameBoyZ80::BIT4_c() { BITbr(0x10, m_Register[c]); m_ClockM = 8; }
void GameBoyZ80::BIT5_c() { BITbr(0x20, m_Register[c]); m_ClockM = 8; }
void GameBoyZ80::BIT6_c() { BITbr(0x40, m_Register[c]); m_ClockM = 8; }
void GameBoyZ80::BIT7_c() { BITbr(0x80, m_Register[c]); m_ClockM = 8; }

void GameBoyZ80::BIT0_d() { BITbr(0x1, m_Register[d]); m_ClockM = 8; }
void GameBoyZ80::BIT1_d() { BITbr(0x2, m_Register[d]); m_ClockM = 8; }
void GameBoyZ80::BIT2_d() { BITbr(0x4, m_Register[d]); m_ClockM = 8; }
void GameBoyZ80::BIT3_d() { BITbr(0x8, m_Register[d]); m_ClockM = 8; }
void GameBoyZ80::BIT4_d() { BITbr(0x10, m_Register[d]); m_ClockM = 8; }
void GameBoyZ80::BIT5_d() { BITbr(0x20, m_Register[d]); m_ClockM = 8; }
void GameBoyZ80::BIT6_d() { BITbr(0x40, m_Register[d]); m_ClockM = 8; }
void GameBoyZ80::BIT7_d() { BITbr(0x80, m_Register[d]); m_ClockM = 8; }

void GameBoyZ80::BIT0_e() { BITbr(0x1, m_Register[e]); m_ClockM = 8; }
void GameBoyZ80::BIT1_e() { BITbr(0x2, m_Register[e]); m_ClockM = 8; }
void GameBoyZ80::BIT2_e() { BITbr(0x4, m_Register[e]); m_ClockM = 8; }
void GameBoyZ80::BIT3_e() { BITbr(0x8, m_Register[e]); m_ClockM = 8; }
void GameBoyZ80::BIT4_e() { BITbr(0x10, m_Register[e]); m_ClockM = 8; }
void GameBoyZ80::BIT5_e() { BITbr(0x20, m_Register[e]); m_ClockM = 8; }
void GameBoyZ80::BIT6_e() { BITbr(0x40, m_Register[e]); m_ClockM = 8; }
void GameBoyZ80::BIT7_e() { BITbr(0x80, m_Register[e]); m_ClockM = 8; }

void GameBoyZ80::BIT0_h() { BITbr(0x1, m_Register[h]); m_ClockM = 8; }
void GameBoyZ80::BIT1_h() { BITbr(0x2, m_Register[h]); m_ClockM = 8; }
void GameBoyZ80::BIT2_h() { BITbr(0x4, m_Register[h]); m_ClockM = 8; }
void GameBoyZ80::BIT3_h() { BITbr(0x8, m_Register[h]); m_ClockM = 8; }
void GameBoyZ80::BIT4_h() { BITbr(0x10, m_Register[h]); m_ClockM = 8; }
void GameBoyZ80::BIT5_h() { BITbr(0x20, m_Register[h]); m_ClockM = 8; }
void GameBoyZ80::BIT6_h() { BITbr(0x40, m_Register[h]); m_ClockM = 8; }
void GameBoyZ80::BIT7_h() { BITbr(0x80, m_Register[h]); m_ClockM = 8; }

void GameBoyZ80::BIT0_l() { BITbr(0x1, m_Register[l]); m_ClockM = 8; }
void GameBoyZ80::BIT1_l() { BITbr(0x2, m_Register[l]); m_ClockM = 8; }
void GameBoyZ80::BIT2_l() { BITbr(0x4, m_Register[l]); m_ClockM = 8; }
void GameBoyZ80::BIT3_l() { BITbr(0x8, m_Register[l]); m_ClockM = 8; }
void GameBoyZ80::BIT4_l() { BITbr(0x10, m_Register[l]); m_ClockM = 8; }
void GameBoyZ80::BIT5_l() { BITbr(0x20, m_Register[l]); m_ClockM = 8; }
void GameBoyZ80::BIT6_l() { BITbr(0x40, m_Register[l]); m_ClockM = 8; }
void GameBoyZ80::BIT7_l() { BITbr(0x80, m_Register[l]); m_ClockM = 8; }

void GameBoyZ80::BIT0_HL() { BITbr(0x1, m_MMU.ReadByte((m_Register[h] << 8) + m_Register[l])); m_ClockM = 16; }
void GameBoyZ80::BIT1_HL() { BITbr(0x2, m_MMU.ReadByte((m_Register[h] << 8) + m_Register[l])); m_ClockM = 16; }
void GameBoyZ80::BIT2_HL() { BITbr(0x4, m_MMU.ReadByte((m_Register[h] << 8) + m_Register[l])); m_ClockM = 16; }
void GameBoyZ80::BIT3_HL() { BITbr(0x8, m_MMU.ReadByte((m_Register[h] << 8) + m_Register[l])); m_ClockM = 16; }
void GameBoyZ80::BIT4_HL() { BITbr(0x10, m_MMU.ReadByte((m_Register[h] << 8) + m_Register[l])); m_ClockM = 16; }
void GameBoyZ80::BIT5_HL() { BITbr(0x20, m_MMU.ReadByte((m_Register[h] << 8) + m_Register[l])); m_ClockM = 16; }
void GameBoyZ80::BIT6_HL() { BITbr(0x40, m_MMU.ReadByte((m_Register[h] << 8) + m_Register[l])); m_ClockM = 16; }
void GameBoyZ80::BIT7_HL() { BITbr(0x80, m_MMU.ReadByte((m_Register[h] << 8) + m_Register[l])); m_ClockM = 16; }

// Set bit in register or HL
void GameBoyZ80::SET0_a() { m_Register[a] |= 0x1; m_ClockM = 8; }
void GameBoyZ80::SET1_a() { m_Register[a] |= 0x2; m_ClockM = 8; }
void GameBoyZ80::SET2_a() { m_Register[a] |= 0x4; m_ClockM = 8; }
void GameBoyZ80::SET3_a() { m_Register[a] |= 0x8; m_ClockM = 8; }
void GameBoyZ80::SET4_a() { m_Register[a] |= 0x10; m_ClockM = 8; }
void GameBoyZ80::SET5_a() { m_Register[a] |= 0x20; m_ClockM = 8; }
void GameBoyZ80::SET6_a() { m_Register[a] |= 0x40; m_ClockM = 8; }
void GameBoyZ80::SET7_a() { m_Register[a] |= 0x80; m_ClockM = 8; }

void GameBoyZ80::SET0_b() { m_Register[b] |= 0x1; m_ClockM = 8; }
void GameBoyZ80::SET1_b() { m_Register[b] |= 0x2; m_ClockM = 8; }
void GameBoyZ80::SET2_b() { m_Register[b] |= 0x4; m_ClockM = 8; }
void GameBoyZ80::SET3_b() { m_Register[b] |= 0x8; m_ClockM = 8; }
void GameBoyZ80::SET4_b() { m_Register[b] |= 0x10; m_ClockM = 8; }
void GameBoyZ80::SET5_b() { m_Register[b] |= 0x20; m_ClockM = 8; }
void GameBoyZ80::SET6_b() { m_Register[b] |= 0x40; m_ClockM = 8; }
void GameBoyZ80::SET7_b() { m_Register[b] |= 0x80; m_ClockM = 8; }

void GameBoyZ80::SET0_c() { m_Register[c] |= 0x1; m_ClockM = 8; }
void GameBoyZ80::SET1_c() { m_Register[c] |= 0x2; m_ClockM = 8; }
void GameBoyZ80::SET2_c() { m_Register[c] |= 0x4; m_ClockM = 8; }
void GameBoyZ80::SET3_c() { m_Register[c] |= 0x8; m_ClockM = 8; }
void GameBoyZ80::SET4_c() { m_Register[c] |= 0x10; m_ClockM = 8; }
void GameBoyZ80::SET5_c() { m_Register[c] |= 0x20; m_ClockM = 8; }
void GameBoyZ80::SET6_c() { m_Register[c] |= 0x40; m_ClockM = 8; }
void GameBoyZ80::SET7_c() { m_Register[c] |= 0x80; m_ClockM = 8; }

void GameBoyZ80::SET0_d() { m_Register[d] |= 0x1; m_ClockM = 8; }
void GameBoyZ80::SET1_d() { m_Register[d] |= 0x2; m_ClockM = 8; }
void GameBoyZ80::SET2_d() { m_Register[d] |= 0x4; m_ClockM = 8; }
void GameBoyZ80::SET3_d() { m_Register[d] |= 0x8; m_ClockM = 8; }
void GameBoyZ80::SET4_d() { m_Register[d] |= 0x10; m_ClockM = 8; }
void GameBoyZ80::SET5_d() { m_Register[d] |= 0x20; m_ClockM = 8; }
void GameBoyZ80::SET6_d() { m_Register[d] |= 0x40; m_ClockM = 8; }
void GameBoyZ80::SET7_d() { m_Register[d] |= 0x80; m_ClockM = 8; }

void GameBoyZ80::SET0_e() { m_Register[e] |= 0x1; m_ClockM = 8; }
void GameBoyZ80::SET1_e() { m_Register[e] |= 0x2; m_ClockM = 8; }
void GameBoyZ80::SET2_e() { m_Register[e] |= 0x4; m_ClockM = 8; }
void GameBoyZ80::SET3_e() { m_Register[e] |= 0x8; m_ClockM = 8; }
void GameBoyZ80::SET4_e() { m_Register[e] |= 0x10; m_ClockM = 8; }
void GameBoyZ80::SET5_e() { m_Register[e] |= 0x20; m_ClockM = 8; }
void GameBoyZ80::SET6_e() { m_Register[e] |= 0x40; m_ClockM = 8; }
void GameBoyZ80::SET7_e() { m_Register[e] |= 0x80; m_ClockM = 8; }

void GameBoyZ80::SET0_h() { m_Register[h] |= 0x1; m_ClockM = 8; }
void GameBoyZ80::SET1_h() { m_Register[h] |= 0x2; m_ClockM = 8; }
void GameBoyZ80::SET2_h() { m_Register[h] |= 0x4; m_ClockM = 8; }
void GameBoyZ80::SET3_h() { m_Register[h] |= 0x8; m_ClockM = 8; }
void GameBoyZ80::SET4_h() { m_Register[h] |= 0x10; m_ClockM = 8; }
void GameBoyZ80::SET5_h() { m_Register[h] |= 0x20; m_ClockM = 8; }
void GameBoyZ80::SET6_h() { m_Register[h] |= 0x40; m_ClockM = 8; }
void GameBoyZ80::SET7_h() { m_Register[h] |= 0x80; m_ClockM = 8; }

void GameBoyZ80::SET0_l() { m_Register[l] |= 0x1; m_ClockM = 8; }
void GameBoyZ80::SET1_l() { m_Register[l] |= 0x2; m_ClockM = 8; }
void GameBoyZ80::SET2_l() { m_Register[l] |= 0x4; m_ClockM = 8; }
void GameBoyZ80::SET3_l() { m_Register[l] |= 0x8; m_ClockM = 8; }
void GameBoyZ80::SET4_l() { m_Register[l] |= 0x10; m_ClockM = 8; }
void GameBoyZ80::SET5_l() { m_Register[l] |= 0x20; m_ClockM = 8; }
void GameBoyZ80::SET6_l() { m_Register[l] |= 0x40; m_ClockM = 8; }
void GameBoyZ80::SET7_l() { m_Register[l] |= 0x80; m_ClockM = 8; }

void GameBoyZ80::SET0_HL() { m_MMU.WriteByte((m_Register[h] << 8) + m_Register[l], m_MMU.ReadByte((m_Register[h] << 8) + m_Register[l]) | 0x1); m_ClockM = 16; }
void GameBoyZ80::SET1_HL() { m_MMU.WriteByte((m_Register[h] << 8) + m_Register[l], m_MMU.ReadByte((m_Register[h] << 8) + m_Register[l]) | 0x2); m_ClockM = 16; }
void GameBoyZ80::SET2_HL() { m_MMU.WriteByte((m_Register[h] << 8) + m_Register[l], m_MMU.ReadByte((m_Register[h] << 8) + m_Register[l]) | 0x4); m_ClockM = 16; }
void GameBoyZ80::SET3_HL() { m_MMU.WriteByte((m_Register[h] << 8) + m_Register[l], m_MMU.ReadByte((m_Register[h] << 8) + m_Register[l]) | 0x8); m_ClockM = 16; }
void GameBoyZ80::SET4_HL() { m_MMU.WriteByte((m_Register[h] << 8) + m_Register[l], m_MMU.ReadByte((m_Register[h] << 8) + m_Register[l]) | 0x10); m_ClockM = 16; }
void GameBoyZ80::SET5_HL() { m_MMU.WriteByte((m_Register[h] << 8) + m_Register[l], m_MMU.ReadByte((m_Register[h] << 8) + m_Register[l]) | 0x20); m_ClockM = 16; }
void GameBoyZ80::SET6_HL() { m_MMU.WriteByte((m_Register[h] << 8) + m_Register[l], m_MMU.ReadByte((m_Register[h] << 8) + m_Register[l]) | 0x40); m_ClockM = 16; }
void GameBoyZ80::SET7_HL() { m_MMU.WriteByte((m_Register[h] << 8) + m_Register[l], m_MMU.ReadByte((m_Register[h] << 8) + m_Register[l]) | 0x80); m_ClockM = 16; }

// Reset bit in regsiter or HL
void GameBoyZ80::RES0_a() { m_Register[a] &= 0xFE; m_ClockM = 8; }
void GameBoyZ80::RES1_a() { m_Register[a] &= 0xFD; m_ClockM = 8; }
void GameBoyZ80::RES2_a() { m_Register[a] &= 0xFB; m_ClockM = 8; }
void GameBoyZ80::RES3_a() { m_Register[a] &= 0xF7; m_ClockM = 8; }
void GameBoyZ80::RES4_a() { m_Register[a] &= 0xEF; m_ClockM = 8; }
void GameBoyZ80::RES5_a() { m_Register[a] &= 0xDF; m_ClockM = 8; }
void GameBoyZ80::RES6_a() { m_Register[a] &= 0xBF; m_ClockM = 8; }
void GameBoyZ80::RES7_a() { m_Register[a] &= 0x7F; m_ClockM = 8; }

void GameBoyZ80::RES0_b() { m_Register[b] &= 0xFE; m_ClockM = 8; }
void GameBoyZ80::RES1_b() { m_Register[b] &= 0xFD; m_ClockM = 8; }
void GameBoyZ80::RES2_b() { m_Register[b] &= 0xFB; m_ClockM = 8; }
void GameBoyZ80::RES3_b() { m_Register[b] &= 0xF7; m_ClockM = 8; }
void GameBoyZ80::RES4_b() { m_Register[b] &= 0xEF; m_ClockM = 8; }
void GameBoyZ80::RES5_b() { m_Register[b] &= 0xDF; m_ClockM = 8; }
void GameBoyZ80::RES6_b() { m_Register[b] &= 0xBF; m_ClockM = 8; }
void GameBoyZ80::RES7_b() { m_Register[b] &= 0x7F; m_ClockM = 8; }

void GameBoyZ80::RES0_c() { m_Register[c] &= 0xFE; m_ClockM = 8; }
void GameBoyZ80::RES1_c() { m_Register[c] &= 0xFD; m_ClockM = 8; }
void GameBoyZ80::RES2_c() { m_Register[c] &= 0xFB; m_ClockM = 8; }
void GameBoyZ80::RES3_c() { m_Register[c] &= 0xF7; m_ClockM = 8; }
void GameBoyZ80::RES4_c() { m_Register[c] &= 0xEF; m_ClockM = 8; }
void GameBoyZ80::RES5_c() { m_Register[c] &= 0xDF; m_ClockM = 8; }
void GameBoyZ80::RES6_c() { m_Register[c] &= 0xBF; m_ClockM = 8; }
void GameBoyZ80::RES7_c() { m_Register[c] &= 0x7F; m_ClockM = 8; }

void GameBoyZ80::RES0_d() { m_Register[d] &= 0xFE; m_ClockM = 8; }
void GameBoyZ80::RES1_d() { m_Register[d] &= 0xFD; m_ClockM = 8; }
void GameBoyZ80::RES2_d() { m_Register[d] &= 0xFB; m_ClockM = 8; }
void GameBoyZ80::RES3_d() { m_Register[d] &= 0xF7; m_ClockM = 8; }
void GameBoyZ80::RES4_d() { m_Register[d] &= 0xEF; m_ClockM = 8; }
void GameBoyZ80::RES5_d() { m_Register[d] &= 0xDF; m_ClockM = 8; }
void GameBoyZ80::RES6_d() { m_Register[d] &= 0xBF; m_ClockM = 8; }
void GameBoyZ80::RES7_d() { m_Register[d] &= 0x7F; m_ClockM = 8; }

void GameBoyZ80::RES0_e() { m_Register[e] &= 0xFE; m_ClockM = 8; }
void GameBoyZ80::RES1_e() { m_Register[e] &= 0xFD; m_ClockM = 8; }
void GameBoyZ80::RES2_e() { m_Register[e] &= 0xFB; m_ClockM = 8; }
void GameBoyZ80::RES3_e() { m_Register[e] &= 0xF7; m_ClockM = 8; }
void GameBoyZ80::RES4_e() { m_Register[e] &= 0xEF; m_ClockM = 8; }
void GameBoyZ80::RES5_e() { m_Register[e] &= 0xDF; m_ClockM = 8; }
void GameBoyZ80::RES6_e() { m_Register[e] &= 0xBF; m_ClockM = 8; }
void GameBoyZ80::RES7_e() { m_Register[e] &= 0x7F; m_ClockM = 8; }

void GameBoyZ80::RES0_h() { m_Register[h] &= 0xFE; m_ClockM = 8; }
void GameBoyZ80::RES1_h() { m_Register[h] &= 0xFD; m_ClockM = 8; }
void GameBoyZ80::RES2_h() { m_Register[h] &= 0xFB; m_ClockM = 8; }
void GameBoyZ80::RES3_h() { m_Register[h] &= 0xF7; m_ClockM = 8; }
void GameBoyZ80::RES4_h() { m_Register[h] &= 0xEF; m_ClockM = 8; }
void GameBoyZ80::RES5_h() { m_Register[h] &= 0xDF; m_ClockM = 8; }
void GameBoyZ80::RES6_h() { m_Register[h] &= 0xBF; m_ClockM = 8; }
void GameBoyZ80::RES7_h() { m_Register[h] &= 0x7F; m_ClockM = 8; }

void GameBoyZ80::RES0_l() { m_Register[l] &= 0xFE; m_ClockM = 8; }
void GameBoyZ80::RES1_l() { m_Register[l] &= 0xFD; m_ClockM = 8; }
void GameBoyZ80::RES2_l() { m_Register[l] &= 0xFB; m_ClockM = 8; }
void GameBoyZ80::RES3_l() { m_Register[l] &= 0xF7; m_ClockM = 8; }
void GameBoyZ80::RES4_l() { m_Register[l] &= 0xEF; m_ClockM = 8; }
void GameBoyZ80::RES5_l() { m_Register[l] &= 0xDF; m_ClockM = 8; }
void GameBoyZ80::RES6_l() { m_Register[l] &= 0xBF; m_ClockM = 8; }
void GameBoyZ80::RES7_l() { m_Register[l] &= 0x7F; m_ClockM = 8; }

void GameBoyZ80::RES0_HL() { m_MMU.WriteByte((m_Register[h] << 8) + m_Register[l], m_MMU.ReadByte((m_Register[h] << 8) + m_Register[l]) & 0xFE); m_ClockM = 16; }
void GameBoyZ80::RES1_HL() { m_MMU.WriteByte((m_Register[h] << 8) + m_Register[l], m_MMU.ReadByte((m_Register[h] << 8) + m_Register[l]) & 0xFD); m_ClockM = 16; }
void GameBoyZ80::RES2_HL() { m_MMU.WriteByte((m_Register[h] << 8) + m_Register[l], m_MMU.ReadByte((m_Register[h] << 8) + m_Register[l]) & 0xFB); m_ClockM = 16; }
void GameBoyZ80::RES3_HL() { m_MMU.WriteByte((m_Register[h] << 8) + m_Register[l], m_MMU.ReadByte((m_Register[h] << 8) + m_Register[l]) & 0xF7); m_ClockM = 16; }
void GameBoyZ80::RES4_HL() { m_MMU.WriteByte((m_Register[h] << 8) + m_Register[l], m_MMU.ReadByte((m_Register[h] << 8) + m_Register[l]) & 0xEF); m_ClockM = 16; }
void GameBoyZ80::RES5_HL() { m_MMU.WriteByte((m_Register[h] << 8) + m_Register[l], m_MMU.ReadByte((m_Register[h] << 8) + m_Register[l]) & 0xDF); m_ClockM = 16; }
void GameBoyZ80::RES6_HL() { m_MMU.WriteByte((m_Register[h] << 8) + m_Register[l], m_MMU.ReadByte((m_Register[h] << 8) + m_Register[l]) & 0xBF); m_ClockM = 16; }
void GameBoyZ80::RES7_HL() { m_MMU.WriteByte((m_Register[h] << 8) + m_Register[l], m_MMU.ReadByte((m_Register[h] << 8) + m_Register[l]) & 0x7F); m_ClockM = 16; }

#pragma endregion

#pragma region Jumps

// Jump to addr
void GameBoyZ80::JP() { m_PC = m_MMU.ReadWord(m_PC); m_ClockM = 16; /*printf("Jump %d\n", m_PC);*/ }

// Jump to addr if condition is true
void GameBoyZ80::JPNZ()	{ m_ClockM = 12; if ((m_Register[f] & 0x80) == 0x00) JP(); else m_PC += 2; }
void GameBoyZ80::JPZ()	{ m_ClockM = 12; if ((m_Register[f] & 0x80) == 0x80) JP(); else m_PC += 2; }
void GameBoyZ80::JPNC() { m_ClockM = 12; if ((m_Register[f] & 0x10) == 0x00) JP(); else m_PC += 2; }
void GameBoyZ80::JPC()	{ m_ClockM = 12; if ((m_Register[f] & 0x10) == 0x10) JP(); else m_PC += 2; }

// Jump to addr in HL
void GameBoyZ80::JPHL() { m_PC = (m_Register[h] << 8) | m_Register[l]; m_ClockM = 4; }

// Add n to addr, then jump
void GameBoyZ80::JR() { unsigned short e = m_MMU.ReadByte(m_PC++); if (e > 127) e = -((~e + 1) & 0xFF); m_PC += e; m_ClockM = 12; }

// If condition true, add n to addr, then jump
void GameBoyZ80::JRNZ() { if ((m_Register[f] & 0x80) == 0x00)	JR(); else { ++m_PC; m_ClockM = 8; } }
void GameBoyZ80::JRZ()	{ if ((m_Register[f] & 0x80) == 0x80)	JR(); else { ++m_PC; m_ClockM = 8; } }
void GameBoyZ80::JRNC() { if ((m_Register[f] & 0x10) == 0x00)	JR(); else { ++m_PC; m_ClockM = 8; } }
void GameBoyZ80::JRC()	{ if ((m_Register[f] & 0x10) == 0x10)	JR(); else { ++m_PC; m_ClockM = 8; } }

#pragma endregion

#pragma region Calls

// Push next instr onto stack, then jump
void GameBoyZ80::CALLnn() { m_SP -= 2; m_MMU.WriteWord(m_SP, m_PC + 2); m_PC = m_MMU.ReadWord(m_PC); m_ClockM = 24; }

// If condition true, push next instr onto stack, then jump
void GameBoyZ80::CALLNZ()	{ if ((m_Register[f] & 0x80) == 0x00)	CALLnn(); else { m_PC += 2; m_ClockM = 12; } }
void GameBoyZ80::CALLZ()	{ if ((m_Register[f] & 0x80) == 0x80)	CALLnn(); else { m_PC += 2; m_ClockM = 12; } }
void GameBoyZ80::CALLNC()	{ if ((m_Register[f] & 0x10) == 0x00)	CALLnn(); else { m_PC += 2; m_ClockM = 12; } }
void GameBoyZ80::CALLC()	{ if ((m_Register[f] & 0x10) == 0x10)	CALLnn(); else { m_PC += 2; m_ClockM = 12; } }

#pragma endregion

#pragma region Restarts

// Push addr to stack, jump to addr $0000 + n
void GameBoyZ80::RST00() { m_SP -= 2; m_MMU.WriteWord(m_SP, m_PC); m_PC = 0x00; m_ClockM = 16; }
void GameBoyZ80::RST08() { m_SP -= 2; m_MMU.WriteWord(m_SP, m_PC); m_PC = 0x08; m_ClockM = 16; }
void GameBoyZ80::RST10() { m_SP -= 2; m_MMU.WriteWord(m_SP, m_PC); m_PC = 0x10; m_ClockM = 16; }
void GameBoyZ80::RST18() { m_SP -= 2; m_MMU.WriteWord(m_SP, m_PC); m_PC = 0x18; m_ClockM = 16; }
void GameBoyZ80::RST20() { m_SP -= 2; m_MMU.WriteWord(m_SP, m_PC); m_PC = 0x20; m_ClockM = 16; }
void GameBoyZ80::RST28() { m_SP -= 2; m_MMU.WriteWord(m_SP, m_PC); m_PC = 0x28; m_ClockM = 16; }
void GameBoyZ80::RST30() { m_SP -= 2; m_MMU.WriteWord(m_SP, m_PC); m_PC = 0x30; m_ClockM = 16; }
void GameBoyZ80::RST38() { m_SP -= 2; m_MMU.WriteWord(m_SP, m_PC); m_PC = 0x38; m_ClockM = 16; }

#pragma endregion

#pragma region Returns

// Pop two bytes from stack, jump to that addrs
void GameBoyZ80::RET() { m_PC = m_MMU.ReadWord(m_SP); m_SP += 2; m_ClockM = 16; }

// RET condition
void GameBoyZ80::RETCon() { m_PC = m_MMU.ReadWord(m_SP); m_SP += 2; m_ClockM = 20; }

// Return if conditio is met
void GameBoyZ80::RETNZ() { if (!(m_Register[f] & 0x80)) RETCon(); else { m_ClockM = 8; } }
void GameBoyZ80::RETZ() { if (m_Register[f] & 0x80) RETCon(); else { m_ClockM = 8; } }
void GameBoyZ80::RETNC() { if (!(m_Register[f] & 0x10)) RETCon(); else { m_ClockM = 8; } }
void GameBoyZ80::RETC() { if (m_Register[f] & 0x10) RETCon(); else { m_ClockM = 8; } }

// Retun and enable interrupts
void GameBoyZ80::RETI()
{
	m_PC = m_MMU.ReadWord(m_SP); m_SP += 2;
	m_ClockM = 16;
	m_ClockT = 48;
	m_IntMasterEnable = true;
}

#pragma endregion

//#include <iostream>
void GameBoyZ80::OutputDebug(bool b)
{
	system("CLS");
	printf("Current opcode: 0x%X\n", m_Opcode);
	printf("Program counter: 0x%X\n", m_PC);
	printf("Stack pointer: 0x%X : 0x%X\n", m_SP, m_MMU.ReadWord(m_SP));

	printf("AF: %02X%02X\n", m_Register[a], m_Register[f]);
	printf("BC: %02X%02X\n", m_Register[b], m_Register[c]);
	printf("DE: %02X%02X\n", m_Register[d], m_Register[e]);
	printf("HL: %02X%02X\n", m_Register[h], m_Register[l]);
	if (b) system("pause");
}
