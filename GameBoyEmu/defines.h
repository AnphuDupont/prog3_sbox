#pragma once

typedef unsigned short UI16;

const UI16 cINTflag		= 0xFFFF;
const UI16 cINTenable	= 0xFF0F;
const UI16 cZeroPage	= 0xFF80;
const UI16 cIOReg		= 0xFF00;
const UI16 cOAM			= 0xFE00;
const UI16 cEchoRAM		= 0xE000;
const UI16 cIRAM1		= 0xD000;
const UI16 cIRAM0		= 0xC000;
const UI16 cCartRAM		= 0xA000;
const UI16 cBGData2		= 0x9C00;
const UI16 cBGData1		= 0x9800;
const UI16 cCharRam		= 0x8000;
const UI16 cCartROM1	= 0x4000;
const UI16 cCartROM0	= 0x0150;
const UI16 cCartHead	= 0x0100;

#define BIT0(value)	((value) & 0x01)
#define BIT1(value)	((value) & 0x02)
#define BIT2(value)	((value) & 0x04)
#define BIT3(value)	((value) & 0x08)
#define BIT4(value)	((value) & 0x10)
#define BIT5(value)	((value) & 0x20)
#define BIT6(value)	((value) & 0x40)
#define BIT7(value)	((value) & 0x80)

#define FLAG_Z 0x80
#define FLAG_N 0x40
#define FLAG_H 0x20
#define FLAG_C 0x10

#define B0 0x01
#define B1 0x02
#define B2 0x04
#define B3 0x08
#define B4 0x10
#define B5 0x20
#define B6 0x40
#define B7 0x80