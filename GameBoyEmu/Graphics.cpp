#include "Graphics.h"
#include "Memory.h"
#include "defines.h"

//Screen dimension constants 
const int SCREEN_WIDTH = 160;
const int SCREEN_HEIGHT = 144;

//Window dimension constants 
const int WINDOW_WIDTH = 320;
const int WINDOW_HEIGHT = int(WINDOW_WIDTH * (1 / (SCREEN_WIDTH / (double)SCREEN_HEIGHT)));

int Graphics::m_FPS = 0;

const Color Graphics::m_Palette[4]
{
	Color(255, 255, 255),
		Color(192, 192, 192),
		Color(96, 96, 96),
		Color(0, 0, 0)
};

//const Color Graphics::m_Palette[4] 
//{
//	Color(0, 0, 0, 255),
//	Color(255, 255, 255, 255),
//	Color(192, 192, 192, 255),
//	Color(96, 96, 96, 255)
//};

//const Color Graphics::m_Palette[4]
//{
//	Color(224, 248, 208, 255),
//	Color(136, 192, 112, 255),
//	Color(52, 104, 86, 255),
//	Color(8, 24, 32, 255)
//};

Graphics::Graphics(Memory* memoryPtr) :
m_ModeClock(0),
m_Mode(0),
m_Scanline(0),
m_pMMU(memoryPtr),
m_ScrollY(0),
m_ScrollX(0)
{
	pixelRect.w = WINDOW_WIDTH / SCREEN_WIDTH;
	pixelRect.h = WINDOW_HEIGHT / SCREEN_HEIGHT;
}


Graphics::~Graphics()
{
}

void Graphics::Reset()
{
	//for (int i = 0; i < 512; ++i)
	//	for (int j = 0; j < 8; j++)
	//		for (int k = 0; k < 8; k++)
	//			m_TileSet[i][j][k] = 0;

	for (int i = 0; i < 8; i++)
		m_LCDcontrol[i] = false;
	m_LCDcontrol[7] = m_LCDcontrol[4] = m_LCDcontrol[0] = true;

	for (int i = 0, n = cOAM; i < 40; ++i, n += 4)
	{
		m_pMMU->m_Memory[n] = 0;
		m_pMMU->m_Memory[n + 1] = 0;
		m_pMMU->m_Memory[n + 2] = 0;
		m_pMMU->m_Memory[n + 3] = 0;
		m_Sprites[i] = Sprite();
	}
}

void Graphics::Tick(const byte clockM)
{
	m_ModeClock += clockM;

	switch (m_Mode)
	{
	case 2: // OAM read mode
		if (m_ModeClock >= 80)
		{
			m_ModeClock = 0;
			m_Mode = 3;
		}
		break;
	case 3: // VRAM read mode
		if (m_ModeClock >= 172)
		{
			m_ModeClock = m_Mode = 0;
			if (m_LCDcontrol[7])
			{
				if (m_LCDcontrol[0]) {
					if (m_Debug) RenderTileMapDebug();
					else RenderScanline();
					//Paint();
				}
			}
		}
		break;
	case 0: // Horizntal blank
		if (m_ModeClock >= 204)
		{
			m_ModeClock = 0;
			++m_Scanline;

			if (m_Scanline >= SCREEN_HEIGHT)
			{
				m_Mode = 1;
				Paint();
			}
			else m_Mode = 2;
		}
		break;
	case 1: // Vertical blank
		if (m_ModeClock >= 456)
		{
			m_ModeClock = 0;
			++m_Scanline;

			if (m_Scanline >= SCREEN_WIDTH) //153 ?
			{
				m_pMMU->WriteByte(0xFF41, 0x04);
				m_Mode = 2;
				m_Scanline = 0;
			}
		}
		break;
	}
}

void Graphics::UpdateTile(unsigned short /*addr*/, byte /*val*/)
{
	//addr &= 0x1FFE;
	//auto tile = (addr >> 4) & 511;
	//auto y = (addr >> 1) & 7;

	////if (!m_LCDcontrol[4]) { addr += 0x800; tile -= 128; }

	//int sx;
	//for (int i = 0; i < 8; ++i)
	//{
	//	sx = 1 << (7 - i);
	//	m_TileSet[tile][y][i] = ((m_pMMU->m_VRAM[addr] & sx) ? 1 : 0) | ((m_pMMU->m_VRAM[addr + 1] & sx) ? 2 : 0);
	//}

	if (m_Debug) RenderTileMapDebug();
}

void Graphics::RenderScanline()
{
	if (!m_LCDcontrol[7]) return;

#pragma region Background tiles render
	if (m_LCDcontrol[0])
	{
		byte windowY = ReadByte(0xFF4A);
		byte windowX = ReadByte(0xFF4B) - 7;

		unsigned short tileDataMemory = 0;
		unsigned short backgroundMemory = 0;
		bool useWindow = false;
		bool useSignedValues = false;

		if (m_LCDcontrol[5] && windowY < m_Scanline) useWindow = true;

		if (m_LCDcontrol[4])
			tileDataMemory = 0x8000;
		else
		{
			tileDataMemory = 0x8800;
			useSignedValues = true;
		}

		if (!useWindow)
		{
			if (m_LCDcontrol[3]) backgroundMemory = cBGData2;
			else backgroundMemory = cBGData1;
		}
		else
		{
			if (m_LCDcontrol[6]) backgroundMemory = cBGData2;
			else backgroundMemory = cBGData1;
		}

		byte yPos = 0;

		if (!useWindow) yPos = m_ScrollY + m_Scanline;
		else yPos = m_Scanline - windowY;

		unsigned short tileRow = (byte(yPos / 8)) * 32;

		for (byte pixel = 0; pixel < SCREEN_WIDTH; pixel++)
		{
			byte xPos = pixel + m_ScrollX;
			if (useWindow && pixel >= windowX) xPos = pixel - windowX;

			unsigned short tileColumn = xPos / 8;
			signed short tileNumber = 0;
			unsigned short tileAddress = backgroundMemory + tileRow + tileColumn;
			if (useSignedValues) tileNumber = (signed char)m_pMMU->ReadByte(tileAddress);
			else tileNumber = m_pMMU->ReadByte(tileAddress);

			unsigned short tileLocation = tileDataMemory;
			if (useSignedValues) tileLocation += (tileNumber + 128) * 16;
			else tileLocation += tileNumber * 16;

			byte lineNr = yPos % 8;
			lineNr *= 2;
			byte data1 = m_pMMU->ReadByte(tileLocation + lineNr);
			byte data2 = m_pMMU->ReadByte(tileLocation + lineNr + 1);

			//int colorBit = ((xPos % 8) - 7) * -1;
			int colorBit = 1 << (7 - (xPos % 8));
			m_Screen[pixel + m_Scanline * SCREEN_WIDTH] = ((data1 & colorBit) ? 1 : 0) | ((data2 & colorBit) ? 2 : 0);
		}
	}
#pragma endregion

	// TODO -> fix color palette select on sprites ------------------------------------------------------------------------------ <- TODO (0xFF49 / 0xFF48)
#pragma region Sprite render
	if (m_LCDcontrol[1])
	{
		bool doubleSizeSprite = m_LCDcontrol[2] ? true : false;

		for (byte sprite = 0; sprite < 40; sprite++)
		{
			byte index = sprite * 4;
			byte yPos = m_pMMU->ReadByte(cOAM + index) - 16;
			byte xPos = m_pMMU->ReadByte(cOAM + index + 1) - 8;
			byte tileLocation = m_pMMU->ReadByte(cOAM + index + 2);
			byte attributes = m_pMMU->ReadByte(cOAM + index + 3);

			bool yFlip = (attributes & 6) ? true : false;
			bool xFlip = (attributes & 5) ? true : false;

			byte ySize = doubleSizeSprite ? 16 : 8;

			if ((m_Scanline >= yPos) && (m_Scanline < (yPos + ySize)))
			{
				int line = m_Scanline - yPos;

				if (yFlip)
				{
					line -= ySize;
					line *= -1;
				}

				line *= 2;
				byte data1 = m_pMMU->ReadByte((cCharRam + (tileLocation * 16)) + (UI16)line);
				byte data2 = m_pMMU->ReadByte((cCharRam + (tileLocation * 16)) + (UI16)line + 1);

				for (byte tilePixel = 7; tilePixel >= 0; tilePixel--)
				{
					byte colorBit = tilePixel;
					if (xFlip)
					{
						colorBit -= 7;
						colorBit = (byte)((int)colorBit * -1);
					}
					byte color = ((data1 & colorBit) ? 1 : 0) | ((data2 & colorBit) ? 2 : 0);

					// If color white don't draw. (alpha = 0)
					//if (color == 0) continue;

					int xPix = 0 - tilePixel;
					xPix += 7;

					int pixel = xPos + xPix;

					// check if pixel is hidden behind background
					//if (m_LCDcontrol[7] && m_Screen[pixel + m_Scanline * SCREEN_WIDTH] != 0) continue;

					m_Screen[pixel + m_Scanline * SCREEN_WIDTH] = color;
				}
			}
		}
	}
#pragma endregion
}

byte Graphics::ReadByte(unsigned short addr)
{
	switch (addr)
	{
	case 0xFF40:
		return
			(m_LCDcontrol[0] ? 0x01 : 0x00) |
			(m_LCDcontrol[1] ? 0x02 : 0x00) |
			(m_LCDcontrol[3] ? 0x08 : 0x00) |
			(m_LCDcontrol[4] ? 0x10 : 0x00) |
			(m_LCDcontrol[5] ? 0x20 : 0x00) |
			(m_LCDcontrol[6] ? 0x40 : 0x00) |
			(m_LCDcontrol[7] ? 0x80 : 0x00);
	case 0xFF42:
		return m_ScrollY;
	case 0xFF43:
		return m_ScrollX;
	case 0xFF44:
		return m_Scanline;
	case 0xFF4A:
	case 0xFF4B:
		return m_pMMU->m_Memory[addr];
	default: // Unhandled -> crash !
		return 0;
	}
}

void Graphics::WriteByte(unsigned short addr, byte val)
{
	switch (addr)
	{
	case 0xFF40:
		m_LCDcontrol[0] = (val & 0x01) ? true : false;
		m_LCDcontrol[1] = (val & 0x02) ? true : false;
		m_LCDcontrol[3] = (val & 0x08) ? true : false;
		m_LCDcontrol[4] = (val & 0x10) ? true : false;
		m_LCDcontrol[5] = (val & 0x20) ? true : false;
		m_LCDcontrol[6] = (val & 0x40) ? true : false;
		m_LCDcontrol[7] = (val & 0x80) ? true : false;
		break;
	case 0xFF42:
		m_ScrollY = val;
		break;
	case 0xFF43:
		m_ScrollX = val;
		break;
	case 0xFF47:
		for (int i = 0; i < 4; i++)
		{
			int pall = (val >> (i * 2)) & 3;
			m_Palettes[0][i] = m_Palette[pall];
		}
		break;
	case 0xFF48:
		for (int i = 0; i < 4; i++)
		{
			int pall = (val >> (i * 2)) & 3;
			m_Palettes[1][i] = m_Palette[pall];
		}
		break;
	case 0xFF49:
		for (int i = 0; i < 4; i++)
		{
			int pall = (val >> (i * 2)) & 3;
			m_Palettes[2][i] = m_Palette[pall];
		}
		break;
	case 0xFF4A:
	case 0xFF4B:
		m_pMMU->m_Memory[addr] = val;
		break;
	default: // Unhandled -> crash !
		break;
	}
}

void Graphics::Paint()
{
	//if (m_FPS > 60) return;
	int prevColor = -1;

	for (int i = 0; i < SCREEN_HEIGHT; i++)
		for (int j = 0; j < SCREEN_WIDTH; j++)
		{
		if (m_Screen[i * SCREEN_WIDTH + j] != prevColor)
		{
			prevColor = m_Screen[i * SCREEN_WIDTH + j];
			SDL_SetRenderDrawColor(m_pRenderer,
				m_Palette[prevColor].R,
				m_Palette[prevColor].G,
				m_Palette[prevColor].B,
				255);
		}
		SDL_RenderDrawPoint(m_pRenderer, j, i);

		}

	SDL_RenderPresent(m_pRenderer);
	++m_FPS;
}

void Graphics::Updateoam(unsigned short addr, byte val)
{
	unsigned short obj = addr >> 2;
	if (obj < 40)
	{
		switch (addr & 3)
		{
		case 0: m_Sprites[obj].y = val - 16; break;
		case 1: m_Sprites[obj].x = val - 8; break;
		case 2: m_Sprites[obj].tile = val; break;
		case 3:
			m_Sprites[obj].palette = (val & 0x10) ? 1 : 0;
			m_Sprites[obj].xflip = (val & 0x20) ? 1 : 0;
			m_Sprites[obj].yflip = (val & 0x40) ? 1 : 0;
			m_Sprites[obj].priority = (val & 0x80) ? 1 : 0;
			break;
		}
	}
}

void Graphics::RenderTileMapDebug()
{
	byte y = (m_Scanline + m_ScrollY) & 7;
	byte x = m_ScrollX & 7;
	unsigned short tile = (m_Scanline / 8) * (SCREEN_WIDTH / 8);
	int screenOffset = m_Scanline * SCREEN_WIDTH;
	for (int i = 0; i < SCREEN_WIDTH; i++)
	{
		m_Screen[screenOffset + i] = m_TileSet[tile][y][x];
		++x;
		if (x == 8)
		{
			x = 0;
			++tile;
		}
	}
}