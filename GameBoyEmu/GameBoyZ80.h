// Dupont An Phu (c)2014

// GameBoy opcodes :
// http://gameboy.mongenel.com/dmg/opcodes.html

// GameBoy CPU Manual
// http://marc.rawer.de/Gameboy/Docs/GBCPUman.pdf

#include <string>
#include <vector>
#include <functional>

//#include "Sound.h"
#include "Graphics.h"
#include "Memory.h"
#include "Timer.h"

enum Reg // Registers
{
	a, f, b, c, d, e, h, l
};

#pragma once
class GameBoyZ80
{
public:
	GameBoyZ80();
	~GameBoyZ80();

	void Reset();
	bool LoadGame(std::string * programPtr);

	//class SDL_Renderer;
	void SetRenderer(SDL_Renderer* renderer) { m_pGPU->SetRenderer(renderer); }
	void EmulateCycle();

	bool IsStopped() { return m_Stop; }

	static unsigned short GetPC() { return m_PC; }
	unsigned char GetClockM() { return m_ClockM; }

	void OutputDebug(bool = true);
	void SetKey(ControllerKeys k) { m_MMU.SetKey(k); }
	void ResetKey(ControllerKeys k) { m_MMU.ResetKey(k); }

	Graphics* GetGraphics() { return m_pGPU; }

private:

	void Interrupts();

	static unsigned char	m_Register[8],			// a, b, c, d, e, h, l, f
		m_TimerT,
		m_TimerM,
		m_ClockM,				// Clock for last instruction
		m_ClockT;				// Clock for last instruction

	static unsigned short	m_PC,					// Program counter
		m_SP,					// Stack pointer
		m_Opcode;				// Current opcode
	unsigned short oldOp;

	bool m_Halt, m_Stop, m_IntMasterEnable, m_CanDebug;

	Memory m_MMU;
	Graphics *m_pGPU;
	//Sound *m_pSound;
	Timer *m_pTimer;

	void InstrCB();

	void PushPC();

	void XX()
	{
		// Crash
		//m_Stop = true;
	}

	// CPU functions
#pragma region Load_Store_8b

	// Load register from register
	inline void LDrr_bb();
	inline void LDrr_bc();
	inline void LDrr_bd();
	inline void LDrr_be();
	inline void LDrr_bh();
	inline void LDrr_bl();
	inline void LDrr_ba();
	inline void LDrr_cb();
	inline void LDrr_cc();
	inline void LDrr_cd();
	inline void LDrr_ce();
	inline void LDrr_ch();
	inline void LDrr_cl();
	inline void LDrr_ca();
	inline void LDrr_db();
	inline void LDrr_dc();
	inline void LDrr_dd();
	inline void LDrr_de();
	inline void LDrr_dh();
	inline void LDrr_dl();
	inline void LDrr_da();
	inline void LDrr_eb();
	inline void LDrr_ec();
	inline void LDrr_ed();
	inline void LDrr_ee();
	inline void LDrr_eh();
	inline void LDrr_el();
	inline void LDrr_ea();
	inline void LDrr_hb();
	inline void LDrr_hc();
	inline void LDrr_hd();
	inline void LDrr_he();
	inline void LDrr_hh();
	inline void LDrr_hl();
	inline void LDrr_ha();
	inline void LDrr_lb();
	inline void LDrr_lc();
	inline void LDrr_ld();
	inline void LDrr_le();
	inline void LDrr_lh();
	inline void LDrr_ll();
	inline void LDrr_la();
	inline void LDrr_ab();
	inline void LDrr_ac();
	inline void LDrr_ad();
	inline void LDrr_ae();
	inline void LDrr_ah();
	inline void LDrr_al();
	inline void LDrr_aa();

	// Read byte from memory at HL and store in register
	inline void LDrHLm_a();
	inline void LDrHLm_b();
	inline void LDrHLm_c();
	inline void LDrHLm_d();
	inline void LDrHLm_e();
	inline void LDrHLm_h();
	inline void LDrHLm_l();

	// Store number(byte) in register
	inline void LDrn_a();
	inline void LDrn_b();
	inline void LDrn_c();
	inline void LDrn_d();
	inline void LDrn_e();
	inline void LDrn_h();
	inline void LDrn_l();

	// Write byte to memory at HL from register
	inline void LDHLmr_a();
	inline void LDHLmr_b();
	inline void LDHLmr_c();
	inline void LDHLmr_d();
	inline void LDHLmr_e();
	inline void LDHLmr_h();
	inline void LDHLmr_l();

	// Store value at number to memory at HL
	void LDHLmn();

	// Load register 'A' from memory at register-pair
	void LDABCm();
	void LDADEm();

	// Store register 'A' in memory at register-pair
	void LDBCmA();
	void LDDEmA();

	// Load register 'A' from memory at position
	void LDAmm();

	// Store register 'A' in memory at position
	void LDmmA();

	// Load register 'A' from 0xFF00 + register 'C'
	void LDCmA();

	// Store register 'A' in memory at 0xFF00 + register 'C'
	void LDACm();

	// Load register 'A' from HL in memory,
	void LDDHLmA();						// decrement HL
	void LDIHLmA();						// increment HL

	// Store register 'A' to memory at HL,
	void LDDAmHL();						// decrement HL
	void LDIAmHL();						// increment HL

	// Load register 'A' from memory at 0xFF00 + number 
	void LDHmA();

	// Write register 'A' to memory at 0xFF00 + number 
	void LDHAm();

#pragma endregion

#pragma region Load_Store_16b

	// Store register-pair from memory at number
	void LDBCnn();
	void LDDEnn();
	void LDHLnn();
	void LDSPnn();

	// Write stack pointer to memory
	void LDnnSP();

	// Copy HL to SP 
	void LDSPHL();

	// Copy SP to HL, with adding value using two's complement displacement from memory
	void LDHLSPn();

	// Push register-pair to memory at stack-pointer -2 
	void PUSHBC();
	void PUSHDE();
	void PUSHHL();
	void PUSHAF();

	// Pop stack-pointer to register-pair
	void POPBC();
	void POPDE();
	void POPHL();
	void POPAF();

#pragma endregion

#pragma region Arthmetic_8b
	// ADD register or value from memory to register 'A'
	void ADDvalToA(const unsigned char value);
	void ADDa();
	void ADDb();
	void ADDc();
	void ADDd();
	void ADDe();
	void ADDh();
	void ADDl();
	void ADDHL();
	void ADDn();

	// ADD register or value from memory to register 'A' and add carry
	void ADCvalToA(const unsigned char value);
	void ADCa();
	void ADCb();
	void ADCc();
	void ADCd();
	void ADCe();
	void ADCh();
	void ADCl();
	void ADCHL();
	void ADCn();

	// SUB register or value from memory from register 'A'
	void SUBvalFromA(const unsigned char value);
	void SUBa();
	void SUBb();
	void SUBc();
	void SUBd();
	void SUBe();
	void SUBh();
	void SUBl();
	void SUBHL();
	void SUBn();

	// SUB register or value from memory from register 'A' and sub carry
	void SBCvalFromA(const unsigned char value);
	void SBCa();
	void SBCb();
	void SBCc();
	void SBCd();
	void SBCe();
	void SBCh();
	void SBCl();
	void SBCHL();
	void SBCn();

	// AND register or value from memory with register 'A'
	void ANDa();
	void ANDb();
	void ANDc();
	void ANDd();
	void ANDe();
	void ANDh();
	void ANDl();
	void ANDHL();
	void ANDn();

	// OR register or value from memory with register 'A'
	void ORa();
	void ORb();
	void ORc();
	void ORd();
	void ORe();
	void ORh();
	void ORl();
	void ORHL();
	void ORn();

	// XOR register or value from memory with register 'A'
	void XORa();
	void XORb();
	void XORc();
	void XORd();
	void XORe();
	void XORh();
	void XORl();
	void XORHL();
	void XORn();

	// SUB register or value from memory from register 'A' and sub carry
	void CPval(const unsigned char value);
	void CPa();
	void CPb();
	void CPc();
	void CPd();
	void CPe();
	void CPh();
	void CPl();
	void CPHL();
	void CPn();

	// INC register or value from HL from register 'A' and sub carry
	void INCreg(Reg r);
	void INCa();
	void INCb();
	void INCc();
	void INCd();
	void INCe();
	void INCh();
	void INCl();
	void INCHLm();

	// DEC register or value from HL from register 'A' and sub carry
	void DECreg(Reg r);
	void DECa();
	void DECb();
	void DECc();
	void DECd();
	void DECe();
	void DECh();
	void DECl();
	void DECHLm();

#pragma endregion

#pragma region Arthmetic_16b
	// ADD register pair or value16b from memory to HL
	void ADDHLval(const unsigned char bHigh, const unsigned char bLow);
	void ADDHLBC();
	void ADDHLDE();
	void ADDHLHL();
	void ADDHLsp();

	// ADD two's complement value from memory to SP
	void ADDspn();

	// INC word
	void INCBC();
	void INCDE();
	void INCHL();
	void INCsp();

	//DEC word
	void DECBC();
	void DECDE();
	void DECHL();
	void DECsp();
#pragma endregion

#pragma region Miscellaneous
	// SWAP nibbles
	void SWAPnibble(unsigned char * var);
	void SWAPr_a();
	void SWAPr_b();
	void SWAPr_c();
	void SWAPr_d();
	void SWAPr_e();
	void SWAPr_h();
	void SWAPr_l();
	void SWAPHLm();

	// Converts a into packed BCD
	void DAA();

	// Invert a
	void CPL();

	// Invert carry flag
	void CCF();

	// Set carry flag
	void SCF();

	// No operation
	void NOP();

	// Halt CPU until an interrupt occurs
	void HALT();

	// Halt CPU
	void STOP();

	// Disable interrupts
	void DI();

	// Enable interrupts
	void EI();

#pragma endregion

#pragma region Rotates_Shifts

	// Info check URLs at top. 
	void RLCr(Reg r);
	void RLr(Reg r);
	void RRCr(Reg r);
	void RRr(Reg r);

	void RLCA();
	void RLA();
	void RRCA();
	void RRA();

	void RLCHL();
	void RLHL();
	void RRCHL();
	void RRHL();

	void RLCr_a();
	void RLCr_b();
	void RLCr_c();
	void RLCr_d();
	void RLCr_e();
	void RLCr_h();
	void RLCr_l();

	void RLr_a();
	void RLr_b();
	void RLr_c();
	void RLr_d();
	void RLr_e();
	void RLr_h();
	void RLr_l();

	void RRCr_a();
	void RRCr_b();
	void RRCr_c();
	void RRCr_d();
	void RRCr_e();
	void RRCr_h();
	void RRCr_l();

	void RRr_a();
	void RRr_b();
	void RRr_c();
	void RRr_d();
	void RRr_e();
	void RRr_h();
	void RRr_l();

	// Shift n left into carry. LSB set to 0
	void SLAr_r(Reg r);
	void SLAr_a();
	void SLAr_b();
	void SLAr_c();
	void SLAr_d();
	void SLAr_e();
	void SLAr_h();
	void SLAr_l();
	void SLAHL();

	void SRAr_r(Reg r);
	void SRAr_a();
	void SRAr_b();
	void SRAr_c();
	void SRAr_d();
	void SRAr_e();
	void SRAr_h();
	void SRAr_l();
	void SRAHL();

	void SRLr_r(Reg r);
	void SRLr_a();
	void SRLr_b();
	void SRLr_c();
	void SRLr_d();
	void SRLr_e();
	void SRLr_h();
	void SRLr_l();
	void SRLHL();

#pragma endregion

#pragma region Bit_Opcodes

	// Check bit in register or HL
	void BITbr(unsigned char bit, unsigned char val) { m_Register[f] &= 0x10; m_Register[f] |= (val & bit) ? 0x20 : 0xA0; }
	void BIT0_a();
	void BIT1_a();
	void BIT2_a();
	void BIT3_a();
	void BIT4_a();
	void BIT5_a();
	void BIT6_a();
	void BIT7_a();

	void BIT0_b();
	void BIT1_b();
	void BIT2_b();
	void BIT3_b();
	void BIT4_b();
	void BIT5_b();
	void BIT6_b();
	void BIT7_b();

	void BIT0_c();
	void BIT1_c();
	void BIT2_c();
	void BIT3_c();
	void BIT4_c();
	void BIT5_c();
	void BIT6_c();
	void BIT7_c();

	void BIT0_d();
	void BIT1_d();
	void BIT2_d();
	void BIT3_d();
	void BIT4_d();
	void BIT5_d();
	void BIT6_d();
	void BIT7_d();

	void BIT0_e();
	void BIT1_e();
	void BIT2_e();
	void BIT3_e();
	void BIT4_e();
	void BIT5_e();
	void BIT6_e();
	void BIT7_e();

	void BIT0_h();
	void BIT1_h();
	void BIT2_h();
	void BIT3_h();
	void BIT4_h();
	void BIT5_h();
	void BIT6_h();
	void BIT7_h();

	void BIT0_l();
	void BIT1_l();
	void BIT2_l();
	void BIT3_l();
	void BIT4_l();
	void BIT5_l();
	void BIT6_l();
	void BIT7_l();

	void BIT0_HL();
	void BIT1_HL();
	void BIT2_HL();
	void BIT3_HL();
	void BIT4_HL();
	void BIT5_HL();
	void BIT6_HL();
	void BIT7_HL();

	// Set bit in register or HL
	void SET0_a();
	void SET1_a();
	void SET2_a();
	void SET3_a();
	void SET4_a();
	void SET5_a();
	void SET6_a();
	void SET7_a();

	void SET0_b();
	void SET1_b();
	void SET2_b();
	void SET3_b();
	void SET4_b();
	void SET5_b();
	void SET6_b();
	void SET7_b();

	void SET0_c();
	void SET1_c();
	void SET2_c();
	void SET3_c();
	void SET4_c();
	void SET5_c();
	void SET6_c();
	void SET7_c();

	void SET0_d();
	void SET1_d();
	void SET2_d();
	void SET3_d();
	void SET4_d();
	void SET5_d();
	void SET6_d();
	void SET7_d();

	void SET0_e();
	void SET1_e();
	void SET2_e();
	void SET3_e();
	void SET4_e();
	void SET5_e();
	void SET6_e();
	void SET7_e();

	void SET0_h();
	void SET1_h();
	void SET2_h();
	void SET3_h();
	void SET4_h();
	void SET5_h();
	void SET6_h();
	void SET7_h();

	void SET0_l();
	void SET1_l();
	void SET2_l();
	void SET3_l();
	void SET4_l();
	void SET5_l();
	void SET6_l();
	void SET7_l();

	void SET0_HL();
	void SET1_HL();
	void SET2_HL();
	void SET3_HL();
	void SET4_HL();
	void SET5_HL();
	void SET6_HL();
	void SET7_HL();

	// Reset bit in regsiter or HL
	void RES0_a();
	void RES1_a();
	void RES2_a();
	void RES3_a();
	void RES4_a();
	void RES5_a();
	void RES6_a();
	void RES7_a();

	void RES0_b();
	void RES1_b();
	void RES2_b();
	void RES3_b();
	void RES4_b();
	void RES5_b();
	void RES6_b();
	void RES7_b();

	void RES0_c();
	void RES1_c();
	void RES2_c();
	void RES3_c();
	void RES4_c();
	void RES5_c();
	void RES6_c();
	void RES7_c();

	void RES0_d();
	void RES1_d();
	void RES2_d();
	void RES3_d();
	void RES4_d();
	void RES5_d();
	void RES6_d();
	void RES7_d();

	void RES0_e();
	void RES1_e();
	void RES2_e();
	void RES3_e();
	void RES4_e();
	void RES5_e();
	void RES6_e();
	void RES7_e();

	void RES0_h();
	void RES1_h();
	void RES2_h();
	void RES3_h();
	void RES4_h();
	void RES5_h();
	void RES6_h();
	void RES7_h();

	void RES0_l();
	void RES1_l();
	void RES2_l();
	void RES3_l();
	void RES4_l();
	void RES5_l();
	void RES6_l();
	void RES7_l();

	void RES0_HL();
	void RES1_HL();
	void RES2_HL();
	void RES3_HL();
	void RES4_HL();
	void RES5_HL();
	void RES6_HL();
	void RES7_HL();

#pragma endregion

#pragma region Jumps

	// Jump to addr
	void JP();

	// Jump to addr if condition is true
	void JPNZ();
	void JPZ();
	void JPNC();
	void JPC();

	// Jump to addr in HL
	void JPHL();

	// Add n to addr, then jump
	void JR();

	// If condition true, add n to addr, then jump
	void JRNZ();
	void JRZ();
	void JRNC();
	void JRC();

#pragma endregion

#pragma region Calls

	// Push next instr onto stack, then jump
	void CALLnn();

	// If condition true, push next instr onto stack, then jump
	void CALLNZ();
	void CALLZ();
	void CALLNC();
	void CALLC();

#pragma endregion

#pragma region Restarts

	// Push addr to stack, jump to addr $0000 + n
	void RST00();
	void RST08();
	void RST10();
	void RST18();
	void RST20();
	void RST28();
	void RST30();
	void RST38();
	
#pragma endregion

#pragma region Returns

	// Pop two bytes from stack, jump to that addrs
	void RET();

	// RET condition
	void RETCon();

	// Return if conditio is met
	void RETNZ();
	void RETZ();
	void RETNC();
	void RETC();

	// Retun and enable interrupts
	void RETI();

	void CheckInterrupts();

#pragma endregion

};

