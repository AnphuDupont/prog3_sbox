#include "Timer.h"
#include "Memory.h"
#include "GameBoyZ80.h"

void Timer::Reset()
{
	m_Interrupt = false;
	m_Div = m_Tac = m_Tima = m_Tma = 0;
	for (int i = 0; i < 3; ++i)
		m_Clock[i] = 0;
}

unsigned int g_TimerValues[] = { 4096, 16384, 65536, 262144 };

void Timer::Step()
{
	// 4096, 16384, 65536, or 262144
	++m_Tima;
	m_Clock[clockmain] = 0;
	if (m_Tima > g_TimerValues[m_pMMU->m_Memory[0xFF07]])
	{
		m_Tima = m_Tma;
		m_pMMU->m_IF |= 4;
	}
}

void Timer::Inc()
{
	//byte oldClock = m_Clock[clockmain];

	m_Clock[sub] += m_pCPU->GetClockM();
	if (m_Clock[sub] > 3)
	{
		++m_Clock[clockmain];
		m_Clock[sub] -= 4;
		++m_Clock[div];
		if (m_Clock[div] == 16)
		{
			m_Clock[div] = 0;
			++m_Div;
		}
	}

	if (m_Tac & 4)
	{
		switch (m_Tac & 3)
		{
		case 0: if (m_Clock[clockmain] >= 64) Step(); break;
		case 1: if (m_Clock[clockmain] >= 1) Step(); break;
		case 2: if (m_Clock[clockmain] >= 4) Step(); break;
		case 3: if (m_Clock[clockmain] >= 16) Step(); break;
		}
	}
	else if (m_Tac == 0)
	{
		if (m_Div > g_TimerValues[m_Tac])
		{
			m_Interrupt = true;
			m_pMMU->WriteByte(0xFF0F, m_pMMU->ReadByte(0xFF0F) | (1 << 2));
			m_Div = 0;
		}
	}
}

byte Timer::ReadByte(unsigned short addr)
{
	switch (addr)
	{
	case 0xFF04: return m_Div;
	case 0xFF05: return m_Tima;
	case 0xFF06: return m_Tma;
	case 0xFF07: return m_Tac;
	}
	return 0;
}
void Timer::WriteByte(unsigned short addr, byte val)
{
	switch (addr)
	{
	case 0xFF04: m_Div = 0; break;
	case 0xFF05: m_Tima = val; break;
	case 0xFF06: m_Tma = val; break;
	case 0xFF07: m_Tac = val & 7; break;
	}
}