// Dupont An Phu (c)2014

// GameBoy Emulation (in JavaScript)
// http://imrannazar.com/GameBoy-Emulation-in-JavaScript:-The-CPU

// SDL tutorial :
// http://lazyfoo.net/tutorials/SDL/

//Using SDL and standard IO 
#include <SDL.h> 
#include <stdio.h> 
#include <string>
#include <fstream>
#include <iostream>

#include <crtdbg.h>

#include "GameBoyZ80.h"
GameBoyZ80 GBZ80;

#pragma region SDLwindow

//Screen dimension constants 
const int SCREEN_WIDTH = 160;
const int SCREEN_HEIGHT = 144;

//Window dimension constants 
const int WINDOW_WIDTH = 160;
const int WINDOW_HEIGHT = int(WINDOW_WIDTH * (1 / (SCREEN_WIDTH / (double)SCREEN_HEIGHT)));

//The window we'll be rendering to 
SDL_Window* gWindow = NULL;

// Setup renderer
SDL_Renderer* gRenderer = NULL;

// Event struct
SDL_Event event;

bool initSDL()
{
	//Initialize SDL 
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		printf("SDL could not initialize! SDL_Error: %s\n", SDL_GetError());
	}
	else
	{
		//Create window 
		gWindow = SDL_CreateWindow(Memory::m_GameTitle, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, WINDOW_WIDTH, WINDOW_HEIGHT, SDL_WINDOW_SHOWN);
		if (gWindow == NULL)
		{
			printf("Window could not be created! SDL_Error: %s\n", SDL_GetError());
			return 0;
		}
		else
		{
			//Get window surface 
			gRenderer = SDL_CreateRenderer(gWindow, 0, SDL_RENDERER_ACCELERATED);
			return 1;
		}
	}

	if (SDL_Init(SDL_INIT_AUDIO) < 0)
		printf("SDL Audio could not initialize! SDL_Error: %s\n", SDL_GetError());

	return 0;
}

void closeSDL()
{
	//Destroy window 
	SDL_DestroyWindow(gWindow);

	//Quit SDL subsystems 
	SDL_Quit();
}

#pragma endregion

#pragma region Inputs
void CheckInput()
{
	if (event.type == SDL_KEYDOWN)
		switch (event.key.keysym.sym)
	{
		case SDLK_a: GBZ80.SetKey(ControllerKeys::A); break;
		case SDLK_z: GBZ80.SetKey(ControllerKeys::B); break;
		case SDLK_UP: GBZ80.SetKey(ControllerKeys::Up); break;
		case SDLK_DOWN: GBZ80.SetKey(ControllerKeys::Down); break;
		case SDLK_LEFT: GBZ80.SetKey(ControllerKeys::Left); break;
		case SDLK_RIGHT: GBZ80.SetKey(ControllerKeys::Right); break;
		case SDLK_KP_ENTER: GBZ80.SetKey(ControllerKeys::Start); break;
		case SDLK_RSHIFT: GBZ80.SetKey(ControllerKeys::Select); break;
		case SDLK_KP_MULTIPLY: GBZ80.GetGraphics()->SetDebug(true);
	}
	else if (event.type == SDL_KEYUP)
		switch (event.key.keysym.sym)
	{
		case SDLK_a: GBZ80.ResetKey(ControllerKeys::A); break;
		case SDLK_z: GBZ80.ResetKey(ControllerKeys::B); break;
		case SDLK_UP: GBZ80.ResetKey(ControllerKeys::Up); break;
		case SDLK_DOWN: GBZ80.ResetKey(ControllerKeys::Down); break;
		case SDLK_LEFT: GBZ80.ResetKey(ControllerKeys::Left); break;
		case SDLK_RIGHT: GBZ80.ResetKey(ControllerKeys::Right); break;
		case SDLK_KP_ENTER: GBZ80.ResetKey(ControllerKeys::Start); break;
		case SDLK_RSHIFT: GBZ80.ResetKey(ControllerKeys::Select); break;
		case SDLK_KP_MULTIPLY: GBZ80.GetGraphics()->SetDebug(false);
	}
	//GBZ80.OutputDebug();
}
#pragma endregion

void RunGame(std::string * programPtr)
{
	// Initalize cpu
	//chip8.Initialize();
	GBZ80.Reset();
	if (!GBZ80.LoadGame(programPtr)) { printf("Not a valid ROM !\n"); system("pause"); return; }
	if (!initSDL()) { system("pause"); return; }
	GBZ80.SetRenderer(gRenderer);

	Uint32 oldTime = 0, second = 0;
	int frames = 0;

	for (;;)
	{
		Uint32 newTime = SDL_GetTicks();
		if (newTime > oldTime)
		{
			second += newTime - oldTime;
			oldTime = newTime;
		}

		if (second > 50)
		{
			//printf("FPS: %d\tInstrs: %d\n", Graphics::m_FPS, frames);
			frames = 0;
			second = 0;
			Graphics::m_FPS = 0;
			GBZ80.OutputDebug(false);
		}

		//if (Graphics::m_FPS > 59) SDL_Delay(1);

		++frames;

		while (SDL_PollEvent(&event)) {
			if (event.type == SDL_QUIT) return;
			else if (event.type == SDL_KEYDOWN || event.type == SDL_KEYUP) CheckInput();
		}

		for (size_t i = 0; i < 3600; i++)
			GBZ80.EmulateCycle();
		//GBZ80.OutputDebug();
		//SDL_Delay(1);
	}
}

#define UNREFERENCED_PARAMETER(p) p

int main(int argc, char* argv[])
{
	UNREFERENCED_PARAMETER(argc);
	UNREFERENCED_PARAMETER(argv);

	std::string file;

#if defined(DEBUG) | defined(_DEBUG)
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);

	//_CrtSetBreakAlloc(172);
	//std::getline(std::cin, file);
#else
	if (argc <= 1) 
	{
		printf("Please specify a program to run.\n");
		std::getline(std::cin, file);
		//system("PAUSE");
		//return -1;
	}
	else file = argv[1];
#endif

	// Open program file
	// cpu_instrs/individual/01-special.gb
	std::ifstream ifs("roms/Final Fantasy Legend II (USA).gb", std::ios::binary | std::ios::in); //argv[1]
	std::string program((std::istreambuf_iterator<char>(ifs)),
		std::istreambuf_iterator<char>());

	printf("GameBoy Emu, programmed by Dupont An Phu\n\n");

	RunGame(&program);
	closeSDL();

	return 0;
}