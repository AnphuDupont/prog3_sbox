#pragma once

#include "SDL.h"
class Memory;

typedef unsigned char byte;

//struct Color
//{
//	Color() {}
//	Color(byte r, byte g, byte b) : R(r), B(b), G(g) {}
//	unsigned char R, G, B;
//	bool operator!=(const Color & c)
//	{
//		return (R != c.R || G != c.G || B != c.B);
//	}
//};

// Black/white color
struct Color
{
	Color() {}
	Color(byte r, byte g, byte b) : R(r), B(g), G(b) { }
	unsigned char R, G, B;
	bool operator!=(const Color & c)
	{
		return (R != c.R);
	}
};

struct Sprite
{
	Sprite() : y(-16), x(-8), tile(0), palette(0), xflip(0), yflip(0), priority(0) {}
	char y, x, tile, palette, xflip, yflip, priority;
};

class Graphics
{
public:
	Graphics(Memory* memoryPtr);
	~Graphics();

	void SetRenderer(SDL_Renderer *ren) { m_pRenderer = ren; }
	void Reset();
	void Tick(const unsigned char);

	void UpdateTile(unsigned short addr, byte val);
	void Updateoam(unsigned short addr, byte val);
	void UpdateWindow(unsigned short addr, byte val);

	void RenderScanline();
	void Paint();

	byte ReadByte(unsigned short addr);
	void WriteByte(unsigned short addr, byte val);

	void SetDebug(bool t) { m_Debug = t; }
	byte GetScanline() { return m_Scanline; }
	static int m_FPS;
private:
	void RenderTileMapDebug();

	byte m_Screen[160 * 144];
	byte m_TileSet[512][8][8];

	unsigned short m_ModeClock;
	byte m_Mode, m_Scanline, m_ScrollY, m_ScrollX;

	bool m_LCDcontrol[8];
	bool m_Debug = false;

	Memory* m_pMMU;

	Sprite m_Sprites[40];

	static const Color m_Palette[4];
	Color m_Palettes[3][4]; // Background, Object1, Object2

	SDL_Renderer *m_pRenderer;
	SDL_Rect pixelRect;
};

