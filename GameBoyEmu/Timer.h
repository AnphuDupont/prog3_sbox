#pragma once

class Memory;
class GameBoyZ80;

class Timer
{
public:
	Timer(Memory* mmu, GameBoyZ80* z80) : m_pMMU(mmu), m_pCPU(z80) {}
	~Timer() {}

	void Reset();
	void Inc();
	void Step();

	unsigned char ReadByte(unsigned short addr);
	void WriteByte(unsigned short addr, unsigned char val);

	bool m_Interrupt;

private:
	
	unsigned int m_Div = 0, m_Tma = 0, m_Tima = 0, m_Tac = 0;

	enum Clock { clockmain, sub, div };
	unsigned char m_Clock[3];

	Memory* m_pMMU;
	GameBoyZ80* m_pCPU;

};

