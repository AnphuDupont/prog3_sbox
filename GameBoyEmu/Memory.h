#pragma once

#include <string>
#include <vector>
#include "defines.h"
//#include "Graphics.h"

class Graphics;
class Timer;
//class Sound;

typedef unsigned char byte;

enum ControllerKeys
{
	A, B, Up, Down, Left, Right, Start, Select
};

class Memory
{
public:
	Memory();
	~Memory();

	void SetTimer(Timer * t) { m_pTimer = t; }
	void SetGraphicsPtr(Graphics* gPtr) { m_pGPU = gPtr; }
	//void SetSoundPtr(Sound * sPtr) { m_pSound = sPtr; }
	void Reset();

	unsigned char ReadByte(unsigned short addr);
	unsigned short ReadWord(unsigned short addr);

	void WriteByte(unsigned short addr, unsigned char val);
	void WriteWord(unsigned short addr, unsigned short val);

	void WriteToROM(unsigned char val) { m_ROM.push_back(val); }

	byte * GetROMPtr() { return m_ROM.data(); }
/*
private:*/

	//unsigned char m_Memory[8192];			// Main RAM

	static	byte	m_BIOS[256];			// BIOS 
	byte	m_Memory[0xFFFF];		// Graphics RAM

	std::vector<byte> m_ROM;				// ROM0 data

	static bool	m_InBios;
	int m_CartridgeType, m_RomSize, m_RamSize;
	static char m_GameTitle[16];

	void SetKey(ControllerKeys key)
	{
		switch (key)
		{
		case A:		 m_KEYS[0] &= 0xE; break;
		case B:		 m_KEYS[0] &= 0xD; break;
		case Up:	 m_KEYS[1] &= 0xB; break;
		case Down:	 m_KEYS[1] &= 0x7; break;
		case Right:  m_KEYS[1] &= 0xE; break;
		case Left:	 m_KEYS[1] &= 0xD; break;
		case Select: m_KEYS[0] &= 0xB; break;
		case Start:  m_KEYS[0] &= 0x7; break;
		}
		//printf("%h/n", m_KEYS[0]);
		//printf(std::to_string(m_KEYS[1]).c_str() + '/n');

		m_Memory[cINTflag] |= B4;
	}

	void ResetKey(ControllerKeys key)
	{
		switch (key)
		{
		case A:		 m_KEYS[0] |= 0x1; break;
		case B:		 m_KEYS[0] |= 0x2; break;
		case Up:	 m_KEYS[1] |= 0x4; break;
		case Down:	 m_KEYS[1] |= 0x8; break;
		case Right:  m_KEYS[1] |= 0x1; break;
		case Left:	 m_KEYS[1] |= 0x2; break;
		case Select: m_KEYS[0] |= 0x4; break;
		case Start:  m_KEYS[0] |= 0x8; break;
		}
		//printf(std::to_string(m_KEYS[0]).c_str() + '/n');
		//printf(std::to_string(m_KEYS[1]).c_str() + '/n');
	}

	byte m_IE, m_IF;

private:

	byte m_KEYS[2], m_KeyCol;

	enum MBCSet
	{
		ROMbank, RAMbank, RAMon, Mode
	};

	unsigned short m_MBC1[4];		// MBC settings

	unsigned int m_RomOffset, m_RamOffset;

	Timer* m_pTimer;
	Graphics * m_pGPU;
	//Sound * m_pSound;
};

