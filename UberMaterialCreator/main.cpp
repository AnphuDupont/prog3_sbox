#pragma once
#include <string>
#include <vector>
#include <iostream>
#include <sstream>
#include <fstream>
#include <algorithm>
#include <map>
#include <time.h>

#include "inline.h"

using namespace std;

string g_UberShaderHeader;
string g_UberShaderFX;

string g_Includes("#include \"Base/stdafx.h\"\n#include \"Base/GeneralStructs.h\"\n#include \"Content/ContentManager.h\"\n#include \"./Graphics/TextureData.h\"\n#include \"Diagnostics/Logger.h\"\n");

vector<string> g_Keywords;

map<string, string> g_InitMap;
void CreateMap()
{
	g_InitMap.insert(pair<string, string>("XMFLOAT4", "XMFLOAT4(0, 0, 0, 0)"));
	g_InitMap.insert(pair<string, string>("XMFLOAT3", "XMFLOAT3(0, 0, 0)"));
	g_InitMap.insert(pair<string, string>("XMFLOAT2", "XMFLOAT2(0, 0)"));
	g_InitMap.insert(pair<string, string>("bool", "false"));
	g_InitMap.insert(pair<string, string>("float", "0"));
	g_InitMap.insert(pair<string, string>("int", "0"));
}

struct Function
{
	string body;
	string define;
	vector<string> paramName;
	vector<string> paramType;
	string type;
};

struct Variable
{
	bool isStatic = false;
	string name, type;
	vector<string> tags;
};

//Function vector
vector<Function *> g_Functions; // function define, function body
vector<Variable *> g_Variables;
vector<Variable *> g_ShaderVars;
string g_FileStart;

map<string, string> g_MaterialShaderVars;



void DeleteGlobals()
{
	g_Functions.clear();
	g_Variables.clear();
	g_ShaderVars.clear();
}

bool starts_with(const string& s1, const string& s2) {
	return s2.size() <= s1.size() && s1.compare(0, s2.size(), s2) == 0;
}

void LoadFile(const string & src, string & dest)
{
	std::ifstream ifs(src, std::ios::in);
	dest = std::string((std::istreambuf_iterator<char>(ifs)),
		std::istreambuf_iterator<char>());
}

void SplitString(const string & str, vector<string> & vec)
{
	string word;
	if (str.find("SRVvariable") != string::npos)
	{
		string newStr = str;
		newStr.replace(newStr.begin() + newStr.find("SRVvariable"), newStr.end(), "ShaderResourceVariable");
		for (size_t i = 0; i < newStr.size(); i++)
		{
			if (newStr.c_str()[i] < 'a')
			{
				if (word.size() > 0)
				{
					vec.push_back(word);
					word.clear();
				}
				word += tolower(newStr.c_str()[i]);
			}
			else word += newStr.c_str()[i];
		}
		vec.push_back(word);
		return;
	}

	for (size_t i = 0; i < str.size(); i++)
	{
		if (str.c_str()[i] < 'a')
		{
			if (word.size() > 0)
			{
				vec.push_back(word);
				word.clear();
			}
			word += tolower(str.c_str()[i]);
		}
		else word += str.c_str()[i];
	}
	vec.push_back(word);
}

bool IsFxVarEqual(const string & a, const string & b)
{
	if (a == "Texture2D" && b == "TextureData*") return true;
	if (a == "bool" && b == "bool") return true;
	if (a == "float3" && b == "XMFLOAT3") return true;
	if (a == "float4" && b == "XMFLOAT4") return true;
	if (a == "int" && b == "int") return true;
	if (a == "float" && b == "float") return true;
	if ((a == "float3" || a == "float4") && b == "ID3DX11EffectVectorVariable*") return true;
	if ((a == "int" || a == "float" || a == "bool") && b == "ID3DX11EffectScalarVariable*") return true;
	if ((a == "Texture2D") && b == "ID3DX11EffectShaderResourceVariable*") return true;

	return false;
}

void LookForVariables()
{
	string fxVar;
	for (size_t i = 0; i < g_Variables.size(); i++)
	{
		int bestSimilarity = 0;
		fxVar = ltrim_copy(g_Variables[i]->name, "m_pb");
		if (fxVar == "ColorAmbient")
			cout << " " << endl;
		SplitString(fxVar, g_Variables[i]->tags);
		if (!starts_with(g_Variables[i]->type, "ID3D")) continue;
		// Compare
		for (size_t v = 0; v < g_ShaderVars.size(); v++)
		{
			if (g_ShaderVars[v]->tags.size() == 0) SplitString(ltrim_copy(g_ShaderVars[v]->name, " m_b"), g_ShaderVars[v]->tags);

			int similar = 0;
			auto result = g_ShaderVars[v]->name.find(fxVar);
			if (result != string::npos)
				similar = 1000;
			else
			{
				for (size_t j = 0; j < g_Variables[i]->tags.size(); j++)
					for (size_t x = 0; x < g_ShaderVars[v]->tags.size(); x++)
						if (g_ShaderVars[v]->tags[x] == g_Variables[i]->tags[j])
							++similar;
			}

			if (similar == 0) continue;
			if (similar >= bestSimilarity && IsFxVarEqual(g_ShaderVars[v]->type, g_Variables[i]->type))
			{
				//g_MaterialShaderVars.insert(pair<string, string>(g_Variables[v]->name, g_ShaderVars[i]->name));
				g_MaterialShaderVars[g_Variables[i]->name] = g_ShaderVars[v]->name;
				if (similar >= 1000)
					break;
				bestSimilarity = similar;
			}
		}
	}
}

void CreateConstructor(string & dest, const string & className)
{
	dest += "\n";
	for (size_t i = 0; i < g_Variables.size(); i++)
	{
		if (!g_Variables[i]->isStatic)
		{
			if (g_Variables[i]->type.find("*") != string::npos) 
				g_Functions[0]->define += g_Variables[i]->name + "(nullptr),\n";
			else g_Functions[0]->define += g_Variables[i]->name + "(" + g_InitMap[g_Variables[i]->type] + "),\n";
		}
		else
		{
			dest += g_Variables[i]->type + " " + className + "::" + g_Variables[i]->name;
			if (g_Variables[i]->type.find("*") != string::npos) dest += " = nullptr;\n";
			else dest += g_InitMap[g_Variables[i]->type] + ";\n";
		}
	}
	g_Functions[0]->define = rtrim(g_Functions[0]->define, " ,\n") + "\n";
	dest += "\n";
}

void CreateFunctionBody()
{	
	string addBody;
	string funcNameLower;
	for (size_t i = 2; i < g_Functions.size(); i++)
	{
		if (g_Functions[i]->define.find("LoadEffectVariables") != string::npos) continue;
		if (g_Functions[i]->define.find("UpdateEffectVariables") != string::npos) continue;

		int bestSimilarity = 0;
		for (size_t j = 0; j < g_Functions.at(i)->paramType.size(); j++)
			g_Functions.at(i)->body += "\tUNREFERENCED_PARAMETER(" + g_Functions.at(i)->paramName.at(j) + ");\n";

		funcNameLower = g_Functions[i]->define;
		ToLower(funcNameLower);
		for (size_t x = 0; x < g_Variables.size(); x++)
		{
			int similar = 0;
			if (starts_with(g_Variables[x]->type, "ID3D")) continue;
			for (const string & tag : g_Variables[x]->tags)
			{
				if (funcNameLower.find(tag) != string::npos)
					++similar;
			}

			if (similar >= (int)g_Variables[x]->tags.size() - 1 && similar > bestSimilarity)
			{
				bestSimilarity = similar;
				addBody = g_Variables[x]->name + " = ";
				if (g_Functions[i]->paramType[0] == "const wstring&") addBody += "ContentManager::Load<TextureData>(" + g_Functions[i]->paramName[0] + ");";
				else addBody += g_Functions[i]->paramName[0] + ";";
			}
		}

		g_Functions.at(i)->body += addBody;
	}
}

void AddFunctions(string & dest)
{  
	CreateFunctionBody();
	for (size_t i = 0; i < g_Functions.size(); i++)
	{
		dest += g_Functions[i]->type + " " + g_Functions.at(i)->define;
		for (size_t j = 0; j < g_Functions.at(i)->paramType.size(); j++)
		{
			dest += g_Functions.at(i)->paramType.at(j) + " " + g_Functions.at(i)->paramName.at(j);
			if (j < g_Functions.at(i)->paramType.size() - 1) dest += ", ";
		}

		if (i > 1) dest += ")\n";
		dest += "{\n" + g_Functions.at(i)->body + "\n}\n\n";
	}
}

Variable * StringToVar(string & getStr)
{
	Variable * var = new Variable();

	if (starts_with(getStr, "static")) 
	{ 
		ltrim(getStr, "static "); var->isStatic = true; 
	}

	getStr = ltrim(getStr, " ");
	string paramT, paramN;
	if (starts_with(getStr, "const "))
	{
		var->type += "const "; getStr.erase(0, getStr.find(" ") + 1);
	}
	auto space = getStr.find(" ");
	paramT = getStr.substr(0, space);
	paramN = ltrim(getStr, paramT.c_str());
	paramN = ltrim(paramN, " ");
	var->name = paramN;
	var->type += paramT;

	cout << "Created variable " << var->type + " " + var->name << endl;
	return var;
}

void CreateLoadEffectVariables(Function * func)
{
	for (size_t i = 0; i < g_Variables.size(); i++)
	{
		if (g_Variables[i]->type.find("ID3D") != string::npos)
		{
			string type = g_Variables[i]->type;
			func->body += "if (!" + g_Variables[i]->name + ")\n{\n" + g_Variables[i]->name + "= m_pEffect->GetVariableByName(\"";;
			func->body += g_MaterialShaderVars[g_Variables[i]->name] + "\")->";

			if (type.find("Scalar") != string::npos) func->body += "AsScalar();\n";
			else if (type.find("ShaderResource") != string::npos) func->body += "AsShaderResource();\n";
			else if (type.find("SR") != string::npos) func->body += "AsShaderResource();\n";
			else if (type.find("Vector") != string::npos) func->body += "AsVector();\n";

			func->body += "if (!" + g_Variables[i]->name + "->IsValid())\n{\nLogger::LogWarning(L\"" + g_Variables[i]->name + " variable not found!\");\n" +
				g_Variables[i]->name + " = nullptr;\n}\n}\n";
		}
	}
}

bool IsSameType(const string & a, const string & b)
{
	if (a.find("Scalar") != string::npos && (b == "bool" || b == "float" || b == "int")) return true;
	if (a.find("Vector") != string::npos && (b == "XMFLOAT2" || b == "XMFLOAT3" || b == "XMFLOAT4")) return true;
	if (a.find("ShaderResource") != string::npos && (b == "TextureData*")) return true;
	return false;
}

void CreateUpdateEffectVariables(Function * func)
{
	cout << endl << "Linking engine variables to shader variables." << endl << endl;
	vector<Variable *> EffectVar, EngineVar;
	for (size_t i = 0; i < g_Variables.size(); i++)
	{
		if (g_Variables[i]->type.find("ID3D") != string::npos) EffectVar.push_back(g_Variables[i]);
		else EngineVar.push_back(g_Variables[i]);
	}

	for (size_t i = 0; i < EffectVar.size(); i++)
	{
		int bestIndex = 0;
		int bestSimilarity = 0;
		for (size_t x = 0; x < EngineVar.size(); x++)
		{
			int similar = 0;
			for (size_t ef = 0; ef < EffectVar[i]->tags.size(); ef++)
				for (size_t en = 0; en < EngineVar[x]->tags.size(); en++)
					if (EffectVar[i]->tags[ef] == EngineVar[x]->tags[en])
						++similar;
			if (similar > bestSimilarity && IsSameType(EffectVar[i]->type, EngineVar[x]->type))
			{
				bestIndex = x;
				bestSimilarity = similar;
			}
		}
		cout << EffectVar[i]->name << " " << EngineVar[bestIndex]->name << endl;
		//func->body += "\nif (" + EngineVar[bestIndex]->name + " && " + EffectVar[i]->name + ")\n" + EffectVar[i]->name + "->Set";
		func->body += "\nif (" + EffectVar[i]->name;
		if (EngineVar[bestIndex]->type.find("*") != string::npos) func->body += " && " + EngineVar[bestIndex]->name;
		func->body += ")\n" + EffectVar[i]->name + "->Set";
		if (EffectVar[i]->type.find("ShaderResource") != string::npos) func->body += "Resource(" + EngineVar[bestIndex]->name + "->GetShaderResourceView());\n";
		else func->body += "RawValue(&" + EngineVar[bestIndex]->name + ", 0, sizeof(" + EngineVar[bestIndex]->name + "));\n";
	}
}

string g_SourceFileArr[2] = { "./Resources/UberMaterial.h", "./Resources/Lab2_0_UberShader_StartOfDemo.fx" };

void CreateClass(string & destFile, const string & className)
{
	destFile += "#include \"" + className + ".h\"\n";

	Function * constr = new Function();
	constr->define = className + "::" + className + "() : \n" + "Material(L\"./Resources/Effects/" + g_SourceFileArr[1].substr(g_SourceFileArr[1].find_last_of("/") + 1) + "\"),\n";
	g_Functions.push_back(constr);

	Function * destr = new Function();
	destr->define = className + "::~" + className + "()\n";
	g_Functions.push_back(destr);

	cout << "Creating class " + className << endl;
}

int main(int argc, char* argv[])
{
#ifndef _DEBUG
	if (argc != 3) 
	{
		cout << "Please input format : 'sourceHeader.h' 'sourceShader.fx'" << endl;
		cout << "Input : " + argc << endl;
		for (int i = 0; i < argc; i++)
			cout << argv[i] << endl;
		return 0;
	}

	g_SourceFileArr[0] = string(argv[1]);
	g_SourceFileArr[1] = string(argv[2]);
#endif

	clock_t t1, t2;
	t1 = clock();

	LoadFile(g_SourceFileArr[0], g_UberShaderHeader);
	LoadFile(g_SourceFileArr[1], g_UberShaderFX);

	CreateMap();

	string getStr;
	bool classCeated = false;

	// Variables
	string className;
	string destFile;
	destFile += g_Includes;
	cout << "Parsing " + g_SourceFileArr[0] << endl;
	stringstream sstream(g_UberShaderHeader);
	// Convert name links to vector
	while (getline(sstream, getStr, '\n'))
	{
		//cout << getStr << endl;
		getStr = ltrim(getStr);
		getStr = rtrim(getStr, " ;");
		if (!starts_with(getStr, "//") && !starts_with(getStr, "#") && getStr != "")
		{
			// Do stuff
			if ((starts_with(getStr, "virtual void") || starts_with(getStr, "void")) && getStr.find('(') != std::string::npos)
			{
				if (!classCeated) { CreateClass(destFile, className); classCeated = true; }
				Function * func = new Function();
				func->type = "void";
				if (starts_with(getStr, "virtual void")) ltrim(getStr, "virtual void");
					else ltrim(getStr, "void ");
				string name = getStr.substr(0, getStr.find("("));
				func->define = className + "::" + name + "(";
				getStr = getStr.erase(0, getStr.find("(", 0) + 1);
				getStr = rtrim(getStr, " )");

				while (getStr.size() > 0)
				{
					getStr = ltrim(getStr, " ");
					string paramT, paramN;
					if (starts_with(getStr, "const ")) 
					{ paramT += "const "; getStr.erase(0, getStr.find(" ") + 1); }
					auto space = getStr.find(" ");
					auto comma = getStr.find(",");
					paramT += getStr.substr(0, space);
					paramN = getStr.substr(space, comma - space);
					paramN = ltrim(paramN, " ");
					func->paramType.push_back(paramT);
					func->paramName.push_back(paramN);
					if (comma > getStr.size()) break;
					getStr = getStr.erase(0, comma + 1);
				}
				cout << "Created function " << func->define << ")" << endl;
				g_Functions.push_back(func);
				continue;
			}

			if (starts_with(getStr, "class"))
			{
				ltrim(getStr, "class ");
				std::stringstream linestream(getStr);
				std::getline(linestream, className, ' ');

				continue;
			}

			if (starts_with(getStr, "static") || starts_with(getStr, "bool") || starts_with(getStr, "float") || starts_with(getStr, "XMFLOAT") || starts_with(getStr, "Texture"))
			{
				g_Variables.push_back(StringToVar(getStr));
			}
		}
	}

	// Parse shader
	sstream = stringstream(g_UberShaderFX);
	// Convert name links to vector
	while (getline(sstream, getStr, '\n'))
	{
		if (getStr.find("VS_Input") != string::npos) break;

		//cout << getStr << endl;
		if (starts_with(getStr, "float4x4") || starts_with(getStr, "float") || starts_with(getStr, "bool") || starts_with(getStr, "int") || starts_with(getStr, "float4") || starts_with(getStr, "Texture2D"))
		{
			Variable * var = StringToVar(getStr);
			auto colon = var->name.find(":");
			if (colon != string::npos) 
			{
				var->name.erase(colon);
			}
			var->name = var->name.substr(0, var->name.find(" "));
			var->name = rtrim(var->name, "<");
			g_ShaderVars.push_back(var);
		}
	}

	LookForVariables();

	CreateConstructor(destFile, className);
	for (size_t i = 0; i < g_Functions.size(); i++)
	{
		if (g_Functions[i]->define.find("LoadEffectVariables") != string::npos)
			CreateLoadEffectVariables(g_Functions[i]);
		if (g_Functions[i]->define.find("UpdateEffectVariables") != string::npos)
			CreateUpdateEffectVariables(g_Functions[i]);
	}
	AddFunctions(destFile);

	std::ofstream out(className + ".cpp");
	out << destFile;
	out.close();

	DeleteGlobals();

	t2 = clock();
	float diff((float)t2 - (float)t1);
	float seconds = diff / CLOCKS_PER_SEC;
	cout << "\nCreated \"" + className + ".cpp\" in " << seconds << " seconds."  << endl;
	cout << "Using :\n\t" << g_SourceFileArr[0] << "\n\t" << g_SourceFileArr[1] << endl << endl;
	system("pause");
	return 0;
}