#include "Memory.h"
#include "MMC1.h"

Memory * Memory::m_pMemory = nullptr;

Memory::Memory()
{
	m_MMC1 = new MMC1();
}

Memory::~Memory()
{
	delete m_MMC1;
}

Memory * Memory::GetMemory()
{
	if (m_pMemory == 0)
		m_pMemory = new Memory();
	return m_pMemory;
}

void Memory::Reset()
{
	for (int i = 0; i < 0xFFFF; ++i) m_Memory[i] = 0;
	m_MMC1->Reset();
}

UI8 Memory::ReadByte(UI16 addr)
{
	if (addr < 0x2000) return m_Memory[addr & 0x07FF];
	else if (addr < 0x4000) return m_Memory[(addr & 7) | 0x2000];
	else if (addr < 0x4020)
	{
		// APU and IO
		return m_Memory[addr];
	}
	else
	{
		return m_MMC1->ReadByte(addr);
		if (m_pHeaderData->Mapper == 1) return m_MMC1->ReadByte(addr);
		if (addr < 0x8000) return m_Memory[addr];
		return m_ROM[addr - 0x8000];
	}
	return 0;
}

UI16 Memory::ReadWord(UI16 addr)
{
	return (ReadByte(addr) + (ReadByte(addr + 1) << 8));
}

void Memory::WriteByte(UI16 addr, UI8 val)
{
	//printf("Wrote: %04X %02X\n", addr, val);
	if (addr < 0x2000) m_Memory[addr & 0x07FF] = val;
	else if (addr < 0x4000) m_Memory[(addr & 7) | 0x2000] = val;
	else if (addr < 0x4020)
	{
		// APU and IO
		m_Memory[addr] = val;
	}
	else
	{
		m_MMC1->WriteByte(addr, val);
		return;
		if (m_pHeaderData->Mapper == 1) m_MMC1->WriteByte(addr, val);
		if (addr < 0x8000) m_Memory[addr] = val;
	}
	return;
}

void Memory::WriteWord(UI16 addr, UI16 val)
{
	WriteByte(addr, (val & 0x00FF));
	WriteByte(addr + 1, (val & 0xFF00) >> 8);
}

void Memory::SetBit(UI16 addr, UI8 bit)
{
	m_Memory[addr] |= bit;
}

void Memory::UnsetBit(UI16 addr, UI8 bit)
{
	m_Memory[addr] &= ~bit;
}

byte * Memory::pRead(UI16 addr)
{
	if (addr < 0x2000) return &m_Memory[addr & 0x07FF];
	else if (addr < 0x4000) return &m_Memory[(addr & 7) | 0x2000];
	else if (addr < 0x4020)
	{
		// APU and IO
		return m_Memory;
	}
	else
	{
		return m_MMC1->pRead(addr);
		if (m_pHeaderData->Mapper == 1) return m_MMC1->pRead(addr);
		if (addr < 0x8000) return &m_Memory[addr];
	}
	return m_Memory;
}
