#include "MMC1.h"
#include "Memory.h"

namespace MMC_REG
{
	enum MMC_REG
	{
		Control, CHR_0, CHR_1, PRG
	};
}

MMC1::MMC1()
{

}

void MMC1::Reset()
{
	m_pMemory = Memory::GetMemory();

	m_BufferRegister = m_BufferShiftIndex = 0;
	for (int i = 0; i < 4; i++) m_ShiftRegister[i] = 0;

	m_CHR_BankingMode = m_RAM_Enable = false;
	m_CHR0_addr = m_CHR1_addr = m_ROM_addr = m_ROM_BankingMode = 0;
}

UI8 * MMC1::pRead(UI16 addr)
{
	if (addr < 0x1000) return &m_CHR_RAM[m_CHR0_addr];
	if (addr < 0x2000) return &m_CHR_RAM[m_CHR_BankingMode ? m_CHR1_addr : m_CHR0_addr];
	if (addr < 0x8000) return &m_pMemory->m_Memory[addr];

	// PRG ROM
	if (m_ROM_BankingMode == 0 || m_ROM_BankingMode == 1) return &m_pMemory->m_ROM[m_ROM_addr];
	if (addr < 0xC000)
	{
		if (m_ROM_BankingMode == 2) return &m_pMemory->m_ROM[addr - 0x8000];
		return &m_pMemory->m_ROM[m_ROM_addr];
	}

	if (m_ROM_BankingMode == 3) return &m_pMemory->m_ROM[addr - 0xC000];
	return &m_pMemory->m_ROM[m_ROM_addr];
}

UI8 MMC1::ReadByte(UI16 addr)
{
	if (addr < 0x1000) return m_CHR_RAM[m_CHR0_addr];
	if (addr < 0x2000) return m_CHR_RAM[m_CHR_BankingMode ? m_CHR1_addr : m_CHR0_addr];
	if (addr < 0x8000) return m_pMemory->m_Memory[addr];
	
	// PRG ROM
	if (m_ROM_BankingMode == 0 || m_ROM_BankingMode == 1) return m_pMemory->m_ROM[m_ROM_addr + addr - 0x8000];
	if (addr < 0xC000)
	{
		if (m_ROM_BankingMode == 2) return m_pMemory->m_ROM[addr - 0x8000];
		return m_pMemory->m_ROM[m_ROM_addr + addr - 0x8000];
	}

	if (m_ROM_BankingMode == 3) return m_pMemory->m_ROM[m_pMemory->m_pHeaderData->PRG_ROM_SIZE - 0xC000 + addr];
	return m_pMemory->m_ROM[m_ROM_addr + addr - 0x8000];
}

void MMC1::WriteByte(UI16 addr, UI8 val)
{
	///
	/// PPU
	///
	if (addr < 0x1000)
	{
		m_CHR_RAM[m_CHR0_addr] = val;
		return;
	}

	if (addr < 0x2000)
	{
		m_CHR_RAM[m_CHR_BankingMode ? m_CHR1_addr : m_CHR0_addr] = val;
		return;
	}

	///
	/// CPU
	///
	if (addr < 0x8000) // PRG RAM
	{
		m_pMemory->m_Memory[addr] = val;
		return;
	}
	else // PRG ROM Shift Register
	{
		if (BIT7(val)) // If 7th bit is set -> reset serial buffer
		{
			m_BufferShiftIndex = 0;
			m_BufferRegister = 0;
			return;
		}

		if (m_BufferShiftIndex < 4)
		{
			++m_BufferShiftIndex;
			m_BufferRegister >>= 1;
			if (BIT0(val)) m_BufferRegister |= B4;
		}
		else
		{
			m_BufferShiftIndex = 0;
			m_BufferRegister >>= 1;
			if (BIT0(val)) m_BufferRegister |= B4;

			// Select register
			UI16 destAddr = (addr & (BC | BE));
			if (destAddr < 0xA000) // Control
			{
				m_ShiftRegister[MMC_REG::Control] = m_BufferRegister & 0x1F;

				m_CHR_BankingMode = BIT4(m_ShiftRegister[MMC_REG::Control]) ? true : false;
				m_ROM_BankingMode = (m_ShiftRegister[MMC_REG::Control] & 0x0C) >> 2;
				// TODO mirrorring
			}
			else if (destAddr < 0xC000) // CHR 0
			{
				m_ShiftRegister[MMC_REG::CHR_0] = m_BufferRegister & 0x1F;

				if (m_CHR_BankingMode) // 4K
				{
					m_CHR0_addr = 0x1000 * m_ShiftRegister[MMC_REG::CHR_0];
				}
				else // 8K
				{
					RESET_BIT(m_ShiftRegister[MMC_REG::CHR_0], B0);
					m_CHR0_addr = 0x2000 * m_ShiftRegister[MMC_REG::CHR_0];
				}
			}
			else if (destAddr < 0xE000) // CHR 1
			{
				m_ShiftRegister[MMC_REG::CHR_1] = m_BufferRegister & 0x1F;

				if (m_CHR_BankingMode) // 4K
				{
					m_CHR1_addr = 0x1000 * m_ShiftRegister[MMC_REG::CHR_1];
				}
				else // 8K
				{
					//m_CHR1_addr = m_CHR0_addr;
				}
			}
			else // PRG
			{
				m_ShiftRegister[MMC_REG::PRG] = m_BufferRegister & 0x1F;

				m_RAM_Enable = BIT4(m_ShiftRegister[MMC_REG::PRG]) ? true : false;
				if (m_ROM_BankingMode == 0 || m_ROM_BankingMode == 1) // 32K
				{
					RESET_BIT(m_ShiftRegister[MMC_REG::PRG], B0);
					m_CHR0_addr = 0x8000 * m_ShiftRegister[MMC_REG::PRG];
				}
				else
				{
					m_CHR0_addr = 0x4000 * m_ShiftRegister[MMC_REG::PRG];
				}
			}
		}
	}
}