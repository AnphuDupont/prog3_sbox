#pragma once
#include "defines.h"

class Memory;
class MMC1
{
public:
	MMC1();
	~MMC1() {}

	void Reset();
	UI8 * pRead(UI16 addr);
	UI8 ReadByte(UI16 addr);
	void WriteByte(UI16 addr, UI8 val);

private:
	Memory * m_pMemory;
	byte m_BufferRegister, m_BufferShiftIndex;
	byte m_ShiftRegister[4]; // MMC1 has 4x 5-bit shift registers.

	bool m_CHR_BankingMode; // false = 8K; true = 2x 4K
	byte m_CHR_RAM[128 * 1024]; // CHR up to 128K
	UI32 m_CHR0_addr, m_CHR1_addr;

	bool m_RAM_Enable;
	int m_ROM_BankingMode;
	UI32 m_ROM_addr;
};