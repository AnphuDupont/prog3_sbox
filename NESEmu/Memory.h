#pragma once

#include "MMC1.h"
#include "defines.h"
#include <vector>

class Memory
{
public:
	~Memory();

	static Memory * GetMemory();
	void Reset();
	
	void SetBit(UI16 addr, UI8 bit);
	void UnsetBit(UI16 addr, UI8 bit);

	UI8 * pRead(UI16 addr);
	UI8 ReadByte(UI16 addr);
	UI16 ReadWord(UI16 addr);

	void WriteByte(UI16 addr, UI8 val);
	void WriteWord(UI16 addr, UI16 val);

	std::vector<byte> * GetROM() { return &m_ROM; }
	byte m_Header[INES_HEADER_SIZE];
	INES_HEADER * m_pHeaderData;

private:
	friend MMC1;

	Memory();
	static Memory * m_pMemory;

	UI8 m_Memory[0xFFFF];
	std::vector<byte> m_ROM;

	MMC1 * m_MMC1;
};

