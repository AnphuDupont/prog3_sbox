#pragma once

#include "defines.h"
#include <string>

class Memory;

class MOS_6502
{
public:
	~MOS_6502();

	static MOS_6502 * GetInstance();

	void Run();
	bool LoadGame(const std::string &);
	void Reset();

	void OutputDebug();

private:
	MOS_6502();
	static MOS_6502 * m_pCPU;

	INES_HEADER m_Header;

	bool m_flags[8];
	byte m_regs[4];
	UI16 m_PC;

	Memory* m_pMem;

	void ADC(const byte val);
	void AND(const byte val);
	void ASL(byte* pVal);
	void BIT(const byte val);
	void BRK(void);
	void CPr(const CPU_REG::CPU_REG dest, const byte val);
	void DEC(byte* pVal);
	void DEr(const CPU_REG::CPU_REG dest);
	void EOR(const byte val);
	void INC(byte* pVal);
	void INr(const CPU_REG::CPU_REG dest);
	void JMP(const UI16 destAddr);
	void JSR(void);
	void LDr(const CPU_REG::CPU_REG dest, const byte val);
	void LSR(byte* pVal);
	void NOP(void) {}
	void ORA(const byte val);
	void PHA(void);
	void PHP(void);
	void PLA(void);
	void PLP(void);
	void ROL(byte* pVal);
	void ROR(byte* pVal);
	void RTI(void);
	void RTS(void);
	void SBC(const byte val);
	void STr(const CPU_REG::CPU_REG dest, const UI16 addr);
	void TXS(void);
	void Trr(const CPU_REG::CPU_REG src, const CPU_REG::CPU_REG dest);
	void Branch(bool param);
	void SetFlag(const FLAGS::FLAGS f, bool v);

	void IRQ();
};