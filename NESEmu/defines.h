#pragma once

typedef unsigned char UI8;
typedef signed char I8;
typedef unsigned char byte;
typedef signed char s_byte;

typedef unsigned short UI16;
typedef unsigned int UI32;

namespace FLAGS
{
	enum FLAGS
	{
		Negative = 7,
		Overflow = 6,
		//IGNORED = 5,
		Break = 4,
		Decimal = 3,
		Interrupt = 2,
		Zero = 1,
		Carry = 0
	};

}
namespace CPU_REG
{
	enum CPU_REG
	{
		A,
		X,
		Y,
		SP
	};
}

// Color for drawing
#ifdef _WIN32
struct Color
{
	Color() : R(0), B(0), G(0) {}
	Color(float r, float g, float b) : R(r), B(b), G(g) { }
	Color(int r, int g, int b) : R(r / 255.0f), B(b / 255.0f), G(g / 255.0f) { }
	float R, G, B;
};
#else
struct Color
{
	Color() : R(0), B(0), G(0), A(255) {}
	Color(float r, float g, float b) : R(byte(r * 255)), G(byte(g * 255)), B(byte(b * 255)), A(255) { }
	Color(int r, int g, int b) : R(byte(r)), G(byte(g)), B(byte(b)), A(255) { }
	Color(byte r, byte g, byte b) : R(r), G(g), B(b), A(255) { }
	byte R, G, B, A;
};
#endif

#define cMemorySize 0xFFFF
#define cStack 0x0100

//#define UNREFERENCED_PARAMETER(p) p

#define NTSC_CLOCK	1.789773f;
#define PAL_CLOCK	1.662607f;

#define NES_SCREEN_WIDTH 256
#define NES_SCREEN_HEIGHT 240

// Window dimension constants 
const int WINDOW_WIDTH = NES_SCREEN_WIDTH;
const int WINDOW_HEIGHT = NES_SCREEN_HEIGHT;

#define BIT0(value)	((value) & 0x01)
#define BIT1(value)	((value) & 0x02)
#define BIT2(value)	((value) & 0x04)
#define BIT3(value)	((value) & 0x08)
#define BIT4(value)	((value) & 0x10)
#define BIT5(value)	((value) & 0x20)
#define BIT6(value)	((value) & 0x40)
#define BIT7(value)	((value) & 0x80)

#define B0 0x01
#define B1 0x02
#define B2 0x04
#define B3 0x08
#define B4 0x10
#define B5 0x20
#define B6 0x40
#define B7 0x80
#define B8 0x0100
#define B9 0x0200
#define BA 0x0400
#define BB 0x0800
#define BC 0x1000
#define BD 0x2000
#define BE 0x4000
#define BF 0x8000

#define SET_BIT(value, bit) (value |= bit)
#define RESET_BIT(value, bit) (value &= ~bit)
#define MASK(value, bits) (value & bits)

#define PPUCTRL 	0x2000
#define PPUMASK 	0x2001
#define PPUSTATUS 	0x2002
#define OAMADDR 	0x2003
#define OAMDATA 	0x2004
#define PPUSCROLL 	0x2005
#define PPUADDR 	0x2006
#define PPUDATA 	0x2007
#define OAMDMA 		0x4014

#define INES_HEADER_SIZE 0x10

namespace INES_HEADER_DATA
{
	enum INES_HEADER_DATA
	{
		HEADER = 0,
		PRG_ROM_SIZE = 4,
		CHR_ROM_SIZE = 5,
		FLAGS_A = 6,
		FLAGS_B = 7,
		PRG_RAM_SIZE = 8,
		FLAGS_C = 9,
		// INES 1.0
	};
}

struct INES_HEADER
{
	byte * Header;
	UI32 PRG_ROM_SIZE;
	UI32 CHR_ROM_SIZE;
	UI32 PRG_RAM_SIZE;
	byte FLAG_6, FLAG_7, FLAG_9;
	byte Mapper;

	void Parse(byte * pHeader)
	{
		Header = &pHeader[INES_HEADER_DATA::HEADER];
		PRG_ROM_SIZE = pHeader[INES_HEADER_DATA::PRG_ROM_SIZE] * 0x4000;
		CHR_ROM_SIZE = pHeader[INES_HEADER_DATA::CHR_ROM_SIZE] * 0x2000;
		PRG_RAM_SIZE = pHeader[INES_HEADER_DATA::PRG_RAM_SIZE] * 0x2000;
		FLAG_6 = pHeader[INES_HEADER_DATA::FLAGS_A];
		FLAG_7 = pHeader[INES_HEADER_DATA::FLAGS_B];
		FLAG_9 = pHeader[INES_HEADER_DATA::FLAGS_C];

		Mapper = 1;
	}

	UI32 GetPRG_ROMOffset()
	{
		UI32 offset = INES_HEADER_SIZE;
		if (BIT2(FLAG_6)) offset += 512;
		return offset;
	}
};