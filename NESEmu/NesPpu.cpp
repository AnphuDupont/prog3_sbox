#include "NesPpu.h"

NesPpu * NesPpu::m_pPPU = nullptr;

NesPpu::NesPpu()
{
}

NesPpu::~NesPpu()
{
}

NesPpu * NesPpu::GetInstance()
{
	if (m_pPPU == 0)
		m_pPPU = new NesPpu();
	return m_pPPU;
}