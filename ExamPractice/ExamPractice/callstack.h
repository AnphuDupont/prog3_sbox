#pragma once
#include <iostream>
#include <vector>

struct CallStack
{
	CallStack(const char* fName, const unsigned int lNum) : funcName(fName), lineNum(lNum) {}
	const char * funcName;
	const unsigned int lineNum;
};

std::vector<CallStack> gCallStack;

#define TOSTR2(a) #a
#define TOSTR(a) TOSTR2(a)
#define HERE(LINE, FUNC) (__FILE__ "(" TOSTR2(LINE) ") : " TOSTR2(FUNC))
#define PRINT_STACK

#define CALLSTACK \
{ \
	gCallStack.push_back(CallStack(__FUNCTION__, __LINE__)); \
	std::cout << "== CALL STACK ==\n" << std::endl; \
	for (CallStack c : gCallStack) std::cout << HERE(TOSTR(c.lineNum), TOSTR(c.funcName)) << std::endl; \
	gCallStack.pop_back(); \
}