#include "Solutions10.h"
#include <vector>
#include <algorithm>

void SolutionFourteen::Run()
{
	vector<INT64> numbers;
	numbers.resize(1000000);
	for (unsigned int i = 2; i < (INT64)numbers.size(); i++)
	{
		INT64 seqLen = 0;
		for (INT64 n = i; n > 1; ++seqLen)
		{
			if (n < (INT64)numbers.size() && numbers[unsigned int(n)])
			{
				seqLen += numbers[unsigned int(n)]; 
				break; 
			}

			if (n % 2) n = 3 * n + 1; // Odd
			else n /= 2; // Even
		}
		numbers[i] = seqLen;
	}
	result = distance(numbers.begin(), max_element(numbers.begin(), numbers.end()));
}