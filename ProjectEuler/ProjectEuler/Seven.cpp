#include "Solutions.h"

void SolutionSeven::Run()
{
	INT64 primes[10001] = { 2, 3, 5, 7, 11, 13 };
	int found = 6;
	result = 13;
	while (found < 10001)
	{
		result += 2;
		bool isPrime = true;
		for (int i = 1; i < found; i++)
		{
			if (!(result % primes[i])) 
			{
				isPrime = false;
				break;
			}
		}
		if (isPrime)
		{
			primes[found] = result;
			++found;
		}
	}
}