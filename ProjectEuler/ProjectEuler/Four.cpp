#include "Solutions.h"
#include <string>

bool IsPalindrome(const string & num)
{
	for (size_t i = 0; i < num.length() / 2; i++)
		if (num[i] != num[num.length() - (i + 1)]) return false;
	return true;
}

void SolutionFour::Run()
{
	for (int a = 999; a > 0; a--)
		for (int b = 999; b > 0; b--)
			if (a*b > result && IsPalindrome(to_string(a*b))) result = a*b;
}