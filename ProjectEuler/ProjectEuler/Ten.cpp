#include "Solutions10.h"
#include <vector>

// https://en.wikipedia.org/wiki/Sieve_of_Eratosthenes

void SolutionTen::Run()
{
	vector<int> numbers(2000000);
	for (int i = 2; i < (int)numbers.size(); ++i) numbers[i] = i;
	for (int i = 0; i < (int)numbers.size(); ++i)
	{
		if (!numbers[i]) continue;
		for (int x = i + i; x < (int)numbers.size(); x += i) numbers[x] = 0;
	}
	for each (int var in numbers) result += var;
}