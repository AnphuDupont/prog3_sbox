#include "Solutions10.h"
#include <string>

void SolutionSixteen::Run()
{
	const int exponent = 1000;

	int qWords = int(exponent / 64);
	int rest = exponent % 64;

	for each (char var in to_string(0xffffffffffffffff)) result += var - '0';
	result *= qWords;

	UINT64 restSum = 1;
	restSum <<= rest;
	for each (char var in to_string(restSum)) result += var - '0';
}