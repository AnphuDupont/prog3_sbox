#pragma once
#include "Base.h"

struct SolutionTen : public Solution { void Run(); };
struct SolutionEleven : public Solution { void Run(); };
struct SolutionTwelve : public Solution { void Run(); };
struct SolutionThirteen : public Solution { void Run(); };
struct SolutionFourteen : public Solution { void Run(); };
struct SolutionFifteen : public Solution { void Run(); };
struct SolutionSixteen : public Solution { void Run(); };
struct SolutionSeventeen : public Solution { void Run(); };
struct SolutionEighteen : public Solution { void Run(); };
struct SolutionNineteen : public Solution { void Run(); };