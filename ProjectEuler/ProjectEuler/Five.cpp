#include "Solutions.h"

void SolutionFive::Run()
{
	for (result = 20;; result += 20)
	{
		bool ok = true;
		for (int i = 2; i <= 20; i++)
			if (result % i)
			{
				ok = false;
				break;
			}
		if (ok) return;
	}
}