#pragma once
using namespace std;

typedef long long INT64;
typedef unsigned long long UINT64;

struct Solution
{
	virtual void Run() {}
	INT64 result = 0;
};