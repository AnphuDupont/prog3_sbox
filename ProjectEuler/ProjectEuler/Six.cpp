#include "Solutions.h"

void SolutionSix::Run()
{
	INT64 sumSq = 0, sqSum = 0;
	for (INT64 i = 1; i <= 100; i++)
	{
		sumSq += i * i;
		sqSum += i;
	}
	result = int((sqSum * sqSum) - sumSq);
}