#pragma once
#include <iostream>
#include <chrono>
using namespace std;

struct ChronoTimer
{
	std::chrono::steady_clock::time_point t1, t2;

	void Start() 
	{ 
		t1 = std::chrono::steady_clock::now(); 
	};

	void Stop() 
	{
		t2 = std::chrono::steady_clock::now();
		std::cout << std::endl << "Run time : " << std::endl
			<< chrono::duration_cast<chrono::milliseconds>(t2 - t1).count() << "ms" << std::endl
			<< chrono::duration_cast<chrono::microseconds>(t2 - t1).count() << "us" << std::endl
			<< chrono::duration_cast<chrono::nanoseconds>(t2 - t1).count() << "ns" << std::endl
			<< std::endl;
	};
};