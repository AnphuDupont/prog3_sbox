#include "Solutions.h"
#include <vector>
#include <iostream>

void SolutionTwo::Run()
{
	int fib = 1, prev = 0, temp = 0;
	while (fib < 4000000) {
		temp = fib;
		fib += prev;
		if (!(fib % 2)) result += fib;
		prev = temp;
	}
}