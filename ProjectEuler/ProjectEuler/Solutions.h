#pragma once
#include "Base.h"

struct SolutionOne : public Solution { void Run(); };
struct SolutionTwo : public Solution { void Run(); };
struct SolutionThree : public Solution { void Run(); };
struct SolutionFour : public Solution { void Run(); };
struct SolutionFive : public Solution { void Run(); };
struct SolutionSix : public Solution { void Run(); };
struct SolutionSeven : public Solution { void Run(); };
struct SolutionEight : public Solution { void Run(); };
struct SolutionNine : public Solution { void Run(); };