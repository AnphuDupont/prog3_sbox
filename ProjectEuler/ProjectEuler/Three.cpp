#include "Solutions.h"

INT64 FindDivider(INT64 n)
{
	INT64 i = 2;
	while (n % ++i);
	if (i == n) return n;
	INT64 left = FindDivider(i), other = FindDivider(n / i);
	return (left > other) ? left : other;
}

void SolutionThree::Run()
{
	result = (int)FindDivider(600851475143);
}