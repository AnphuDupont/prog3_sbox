#include "Solutions.h"
#include <set>

void SolutionOne::Run()
{
	std::set<size_t> s;
	for (size_t i = 0; i < 1000; i++) if (!(i % 3) || !(i % 5)) s.insert(i);
	for each (int var in s) result += var;
}