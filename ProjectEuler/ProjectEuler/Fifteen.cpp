#include "Solutions10.h"

void SolutionFifteen::Run()
{
	const int gridSize = 20;
	INT64 grid[gridSize + 1][gridSize + 1] = { {1} };
	for (int i = 0; i <= gridSize; i++) grid[0][i] = grid[i][0] = 1;

	for (int i = 1; i <= gridSize; i++)
		for (int j = 1; j <= gridSize; j++)
			grid[i][j] = grid[i - 1][j] + grid[i][j - 1];

	result = grid[gridSize][gridSize];
}