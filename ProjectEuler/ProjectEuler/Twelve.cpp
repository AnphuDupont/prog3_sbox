#include "Solutions10.h"

int Divisors(INT64 num)
{
	INT64 divs = 0, smallestOther = num, i = 1;
	for (; i < smallestOther; ++i)
	{
		if (!(num % i)) 
		{
			smallestOther = num / i;
			++divs;
		}
	}

	divs *= 2;
	return (int)divs;
}

void SolutionTwelve::Run()
{
	for (int inc = 1; Divisors(result) < 500; ++inc)
		result += inc;
}