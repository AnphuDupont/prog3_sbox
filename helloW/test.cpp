#pragma region
//#include <iostream>
//#include <sstream>
//using namespace std;
//int main() {
//	/* Enter your code here. Read input from STDIN. Print output to STDOUT */
//
//	string input, output;
//	getline(cin, input);
//
//	for (char c : input)
//	{
//		output += c;
//		if (c >= 'A')
//			output += c;
//	}
//
//	cout << output;
//
//	return 0;
//}
#pragma endregion 1

#pragma region
//#include <iostream>
//#include <sstream>
//#include <algorithm>
//using namespace std;
//int main() {
//	/* Enter your code here. Read input from STDIN. Print output to STDOUT */
//
//	string input;
//	getline(cin, input);
//
//	int uppercaseCharsCount = count_if(input.begin(), input.end(), isupper);
//	cout << uppercaseCharsCount;
//
//	return 0;
//}
#pragma endregion 2

#pragma region
//#include <iostream>
//#include <vector>
//using namespace std;
//
//int countOnes(int number)
//{
//	int ones = 0;
//	for (int i = 0; i < 32; i++)
//	{
//		//0x00000000000000000000000000000001
//		if (number & 1) ++ones;
//		number = number >> 1;
//	}
//	return ones;
//}
//
//int main() {
//	/* Enter your code here. Read input from STDIN. Print output to STDOUT */
//
//	int cases;
//	cin >> cases;
//
//	vector<int> numbers;
//	for (int i = 0; i < cases; i++)
//	{
//		int number;
//		cin >> number;
//		numbers.push_back(number);
//	}
//
//	for (int num : numbers)
//		cout << countOnes(num) << endl;
//
//	return 0;
//}
#pragma endregion 3

//#include <iostream>
//#include <vector>
//#include <sstream>
//using namespace std;
//
//int unsetLeastTwoSetBits(int number)
//{
//	int bitsUnset = 0;
//	for (int i = 0; i < 32; i++)
//	{
//		if (number >> i & 1)
//		{
//			++bitsUnset;
//			number = number & (2147483647 - 1 << i); // 2147483647 = INT_MAX
//		}
//
//		if (bitsUnset >= 2) break;
//	}
//	return number;
//}
//
//int main() {
//	/* Enter your code here. Read input from STDIN. Print output to STDOUT */
//
//	string input;
//	getline(cin, input);
//
//	std::stringstream stream(input);
//	string temp;
//	vector <int> numbers;
//	while (getline(stream, temp, ' '))
//		numbers.push_back(stoi(temp));
//
//	for (int num : numbers)
//		cout << unsetLeastTwoSetBits(num) << endl;
//
//	return 0;
//}

//#include <iostream>
//#include <vector>
//using namespace std;
//int main() {
//	/* Enter your code here. Read input from STDIN. Print output to STDOUT */
//
//	unsigned int num1, num2;
//	cin >> num1;
//	cin >> num2;
//
//	vector<int> result;
//	bool carry = false;
//
//	for (int i = 0; i < 32; i++)
//	{
//		int unit1 = num1 % (10 * (i));
//		int unit2 = num2 % (10 * (i));
//
//		int temp = unit1 + unit2 + carry;
//		if (carry)
//		{
//			temp -= 10;
//			carry = true;
//		}
//		else carry = false;
//
//		num1 -= unit1;
//		num2 -= unit2;
//
//		result.push_back(temp);
//	}
//
//	//for (vector<int>::reverse_iterator it = result.crbegin(); it != result.crend(); ++it)
//	//	cout << *it;
//
//	return 0;
//}

//#include <iostream>
//#include <map>
//#include <sstream>
//#include <vector>
//using namespace std;
//int main() {
//	/* Enter your code here. Read input from STDIN. Print output to STDOUT */
//
//	string inputNum, inputChars;
//	getline(cin, inputNum);
//	getline(cin, inputChars);
//	
//	std::stringstream streamNum(inputNum);
//	std::stringstream streamChar(inputNum);
//	string temp;
//	vector <int> numbers;
//	vector <char> chars;
//	while (getline(streamNum, temp, ' '))
//		numbers.push_back(stoi(temp));
//	while (getline(streamChar, temp, ' '))
//		numbers.push_back(temp[0]);
//
//	map<int, char> sortedMap;
//	for (unsigned int i = 0; i < numbers.size(); i++)
//	{
//		sortedMap.insert(std::pair<int, char>(numbers.at(0), chars.at(0)));
//	}
//
//	for (unsigned int i = 0; i < numbers.size(); i++)
//	{
//		cout << sortedMap.at(i);
//	}
//
//	return 0;
//}

//#include <iostream>
//using namespace std;
//int main() {
//	/* Enter your code here. Read input from STDIN. Print output to STDOUT */
//
//	int num;
//	cin >> num;
//
//	int count = 0;
//	bool canCount = false;
//	for (int i = 0; i < 32; i++)
//	{
//		if (canCount) ++count;
//		if (num & 1 << (1 + i))
//		{
//			 if (canCount) { cout << --count; break; }
//			 else canCount = true;
//		}
//	}
//
//	return 0;
//}