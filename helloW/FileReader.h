#pragma once
#include <fstream>
#include <vector>
#include <iostream>
using namespace std;

struct FileReader
{
public:
	FileReader(string fname);
	~FileReader() { }
	
	string ReadLine();

	string file;
private:
	int index;
};

FileReader::FileReader(string fname)
{
	std::ifstream ifs(fname, std::ios::binary | std::ios::in);
	file = std::string((std::istreambuf_iterator<char>(ifs)),
		std::istreambuf_iterator<char>());

	index = 0;
}

string FileReader::ReadLine()
{
	if (index >= file.size()) return string();

	string output;
	while (file[index] != '\n')
	{
		if (file[index] == '\r') { ++index; continue; }
		output += file[index];
		++index;
		if (index >= file.size()) return output;
	}
	++index;
	return output;
}

struct StringHelper
{
public:
	static vector<string> Split(string s, const char param)
	{
		vector<string> outputVec;

		int idx = 0;
		while (idx < s.size())
		{
			string output;
			while (s[idx] != param)
			{
				output += s[idx];
				++idx;
				if (idx >= s.size()) break;
			}
			++idx;
			outputVec.push_back(output);
		}

		return outputVec;
	}
};