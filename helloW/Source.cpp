#include <iostream>
#include <string>
using namespace std;

#include "FileReader.h" 

typedef unsigned long long UI64;

int main()
{
	FileReader r("input.txt");

	UI64 total = 0;

	string f = r.ReadLine();
	while (f.size() > 0)
	{
		auto s = StringHelper::Split(f, 'x');

		int l = stoi(s[0]);
		int w = stoi(s[1]);
		int h = stoi(s[2]);

		int sidelw = l * w;
		int sidewh = w * h;
		int sidehl = h * l;

		int smallest = sidelw;
		if (sidewh < smallest) smallest = sidewh;
		if (sidehl < smallest) smallest = sidehl;

		total += 2 * sidelw + 2 * sidewh + 2 * sidehl + smallest;

		f = r.ReadLine();
	}

	cout << total << endl;

	system("pause");
	return 0;
}
