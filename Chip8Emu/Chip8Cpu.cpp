// Dupont An Phu (c)2014

#include "Chip8Cpu.h"
#include "windows.h"
#include "time.h"

unsigned char Chip8Cpu::m_Chip8_fontset[80] = 
{
	0xF0, 0x90, 0x90, 0x90, 0xF0, // 0
	0x20, 0x60, 0x20, 0x20, 0x70, // 1
	0xF0, 0x10, 0xF0, 0x80, 0xF0, // 2
	0xF0, 0x10, 0xF0, 0x10, 0xF0, // 3
	0x90, 0x90, 0xF0, 0x10, 0x10, // 4
	0xF0, 0x80, 0xF0, 0x10, 0xF0, // 5
	0xF0, 0x80, 0xF0, 0x90, 0xF0, // 6
	0xF0, 0x10, 0x20, 0x40, 0x40, // 7
	0xF0, 0x90, 0xF0, 0x90, 0xF0, // 8
	0xF0, 0x90, 0xF0, 0x10, 0xF0, // 9
	0xF0, 0x90, 0xF0, 0x90, 0x90, // A
	0xE0, 0x90, 0xE0, 0x90, 0xE0, // B
	0xF0, 0x80, 0x80, 0x80, 0xF0, // C
	0xE0, 0x90, 0x90, 0x90, 0xE0, // D
	0xF0, 0x80, 0xF0, 0x80, 0xF0, // E
	0xF0, 0x80, 0xF0, 0x80, 0x80  // F
};

Chip8Cpu::Chip8Cpu() :
	m_opcode(0),
	m_I(0),
	m_pc(0),
	m_sp(0),
	m_DrawFlag(false)
{
	for (int i = 0; i < 16; i++)
		m_stack[i] = 0;

	for (int i = 0; i < 4096; i++)
		m_memory[i] = 0;

	for (int i = 0; i < 16; i++)
		m_V[i] = 0;

	for (int i = 0; i < (m_ScrW * m_ScrH); i++)
		m_gfx[i] = 0;

	for (int i = 0; i < 16; i++)
		m_key[i] = 0;
}


Chip8Cpu::~Chip8Cpu()
{
}

void Chip8Cpu::Initialize()
{
	for (int i = 0; i < 16; i++)
		m_stack[i] = 0;

	for (int i = 0; i < 4096; i++)
		m_memory[i] = 0;

	for (int i = 0; i < 16; i++)
		m_V[i] = 0;

	for (int i = 0; i < (m_ScrW * m_ScrH); i++)
		m_gfx[i] = 0;

	for (int i = 0; i < 16; i++)
		m_key[i] = 0;

	m_I = 0;
	m_pc = 0x200;
	m_sp = 0;
	m_opcode = 0;
	m_delay_timer = 0;
	m_sound_timer = 0;

	m_DrawFlag = true;
	srand((unsigned int)time(NULL));

	// Load fontset
	for (int i = 0; i < 80; i++)
		m_memory[i] = m_Chip8_fontset[i]; // +0x50 (80) ?
}

void Chip8Cpu::LoadGame(std::string * programPtr)
{
	for (unsigned int i = 0; i < programPtr->size(); i++)
		m_memory[i + 512] = programPtr->c_str()[i];	// 0x200 = 512 -> program start
}

void Chip8Cpu::EmulateCycle()
{
	// Fetch opcode
	// Decode opcode
	// Execute opcode

	m_opcode = m_memory[m_pc] << 8 | m_memory[m_pc + 1]; // First shift m_memory[m_pc] 8 bits left (<<), then do bitwise OR with m_memory[m_pc + 1]

	ExecuteCurrent();

	// Update timers
	if (m_delay_timer > 0)
		--m_delay_timer;

	if (m_sound_timer > 0)
	{
		if (m_sound_timer == 1)
			Beep(500, 50);
			//printf("BEEP!\n");
		--m_sound_timer;
	}
}

void Chip8Cpu::ExecuteCurrent()
{
	switch (m_opcode & 0xF000) 
	{
	case 0x0000: 
		switch (m_opcode & 0x000F)
		{
		case 0x0000: // Clear the display.
			for (int i = 0; i < (m_ScrW * m_ScrH); i++)
				m_gfx[i] = 0;
			m_DrawFlag = true;
			m_pc += 2;
			break;
		case 0x000E: // Return from a subroutine.
			--m_sp;
			m_pc = m_stack[m_sp];
			m_pc += 2;
			break;
		default:
			printf("Unknown opcode: 0x%X\n", m_opcode);
			//printf("Ignored instruction: 0x%X\n", m_opcode);
			m_pc += 2;
			break;
		}
		break;
		//-----------------------------------------------------------------------------------------------
	case 0x1000: // Jump to location nnn.
		m_pc = m_opcode & 0x0FFF;
		break;
		//-----------------------------------------------------------------------------------------------
	case 0x2000: // Call subroutine at nnn.
		m_stack[m_sp] = m_pc;
		++m_sp;
		m_pc = m_opcode & 0x0FFF;
		break;
		//-----------------------------------------------------------------------------------------------
	case 0x3000: // Skip next instruction if Vx = kk.
		if (m_V[(m_opcode & 0x0F00) >> 8] == (m_opcode & 0x00FF))
			m_pc += 4;
		else m_pc += 2;
		break;
		//-----------------------------------------------------------------------------------------------
	case 0x4000: // Skip next instruction if Vx != kk.
		if (m_V[(m_opcode & 0x0F00) >> 8] != (m_opcode & 0x00FF))
			m_pc += 4;
		else m_pc += 2;
		break;
		//-----------------------------------------------------------------------------------------------
	case 0x5000: // Skip next instruction if Vx = Vy.
		if (m_V[(m_opcode & 0x0F00) >> 8] == m_V[(m_opcode & 0x00F0) >> 4])
			m_pc += 4;
		else m_pc += 2;
		break;
		//-----------------------------------------------------------------------------------------------
	case 0x6000: // Set Vx = kk.
		m_V[(m_opcode & 0x0F00) >> 8] = (m_opcode & 0x00FF);
		m_pc += 2;
		break;
		//-----------------------------------------------------------------------------------------------
	case 0x7000: // Set Vx = Vx + kk.
		m_V[(m_opcode & 0x0F00) >> 8] += (m_opcode & 0x00FF);
		m_pc += 2;
		break;
		//-----------------------------------------------------------------------------------------------
	case 0x8000: 
		switch (m_opcode & 0x000F)
		{
		case 0x0000: // Set Vx = Vy.
			m_V[(m_opcode & 0x0F00) >> 8] = m_V[(m_opcode & 0x00F0) >> 4];
			m_pc += 2;
			break;
		case 0x0001: // Set Vx = Vx OR Vy.
			m_V[(m_opcode & 0x0F00) >> 8] |= m_V[(m_opcode & 0x00F0) >> 4];
			m_pc += 2;
			break;
		case 0x0002: // Set Vx = Vx AND Vy.
			m_V[(m_opcode & 0x0F00) >> 8] &= m_V[(m_opcode & 0x00F0) >> 4];
			m_pc += 2;
			break;
		case 0x0003: // Set Vx = Vx XOR Vy.
			m_V[(m_opcode & 0x0F00) >> 8] ^= m_V[(m_opcode & 0x00F0) >> 4];
			m_pc += 2;
			break;
		case 0x0004: // Set Vx = Vx + Vy, set VF = carry.
			if (m_V[(m_opcode & 0x00F0) >> 4] > (0xFF - m_V[(m_opcode & 0x0F00) >> 8]))
				m_V[0xF] = 1; // carry flag
			else
				m_V[0xF] = 0;
			m_V[(m_opcode & 0x0F00) >> 8] += m_V[(m_opcode & 0x00F0) >> 4];
			m_pc += 2;
			break;
		case 0x0005: // Set Vx = Vx - Vy, set VF = NOT borrow.
			if (m_V[(m_opcode & 0x0F00) >> 8] > (m_V[(m_opcode & 0x00F0) >> 4]))
				m_V[0xF] = 1; // carry flag, not borrow
			else
				m_V[0xF] = 0;
			m_V[(m_opcode & 0x0F00) >> 8] -= m_V[(m_opcode & 0x00F0) >> 4];
			m_pc += 2;
			break;
		case 0x0006: // Set Vx = Vx SHR 1.
			m_V[0xF] = m_V[(m_opcode & 0x0F00) >> 8] & 0x1;
			/*if ((m_V[(m_opcode & 0x0F00) >> 8] & 0x1) == 1)
				m_V[0xF] = 1;
			else
				m_V[0xF] = 0;*/
			//m_V[(m_opcode & 0x0F00) >> 8] = (unsigned char)(m_V[(m_opcode & 0x0F00) >> 8] / 0x2);
			m_V[(m_opcode & 0x0F00) >> 8] >>= 1; // Shift 1 to right == /2, because base 2
			m_pc += 2;
			break;
		case 0x0007: // Set Vx = Vy - Vx, set VF = NOT borrow.
			if (m_V[(m_opcode & 0x00F0) >> 4] > m_V[(m_opcode & 0x0F00) >> 8])
				m_V[0xF] = 1;
			else
				m_V[0xF] = 0;
			m_V[(m_opcode & 0x0F00) >> 8] = m_V[(m_opcode & 0x00F0) >> 4] - m_V[(m_opcode & 0x0F00) >> 8];
			m_pc += 2;
			break;
		case 0x000E: // Set Vx = Vx SHL 1.
			//if (m_V[(m_opcode & 0x0F00) >> 8] == 0x80) // If MSB == 1 (0x80 = 128)
			//	m_V[0xF] = 1;
			//else
			//	m_V[0xF] = 0;
			m_V[0xF] = m_V[(m_opcode & 0x0F00) >> 8] >> 7;
			/*m_V[(m_opcode & 0x0F00) >> 8] *= 2;*/
			m_V[(m_opcode & 0x0F00) >> 8] <<= 1; // Shift 1 to left == *2, because base 2
			m_pc += 2;
			break;
		default:
			printf("Unknown opcode: 0x%X\n", m_opcode);
			break;
		}
		break;
		//-----------------------------------------------------------------------------------------------
	case 0x9000: // Skip next instruction if Vx != Vy.
		if (m_V[(m_opcode & 0x0F00) >> 8] != m_V[(m_opcode & 0x00F0) >> 4])
			m_pc += 4;
		else m_pc += 2;
		break;
		//-----------------------------------------------------------------------------------------------
	case 0xA000: // ANNN: Sets I to the address NNN
		m_I = m_opcode & 0x0FFF;
		m_pc += 2;
		break;
		//-----------------------------------------------------------------------------------------------
	case 0xB000: // Jump to location NNN + V0.
		m_pc = (m_opcode & 0x0FFF) + m_V[0];
		break;
		//-----------------------------------------------------------------------------------------------
	case 0xC000: // Set Vx = random byte AND kk.
		m_V[(m_opcode & 0x0F00) >> 8] = (rand() % 256) & (m_opcode & 0xFF);
		m_pc += 2;
		break;
		//-----------------------------------------------------------------------------------------------
	case 0xD000: // Display n-byte sprite starting at memory location I at (Vx, Vy), set VF = collision.
	{ 
		// Create scope for local variables
		unsigned short	x = m_V[(m_opcode & 0x0F00) >> 8],
						y = m_V[(m_opcode & 0x00F0) >> 4],
						height = m_opcode & 0x000F,
						pixel;

		m_V[0xF] = 0;

		for (int vLine = 0; vLine < height; vLine++)
		{
			pixel = m_memory[m_I + vLine];
			for (int hLine = 0; hLine < 8; hLine++)
				if ((pixel & (0x80 >> hLine)) != 0)
				{
					if (m_gfx[x + hLine + ((y + vLine) * m_ScrW)] == 1)
						m_V[0xF] = 1;
					m_gfx[x + hLine + ((y + vLine) * m_ScrW)] ^= 1;
				}
		}
		m_DrawFlag = true;
		m_pc += 2;
		break;
	}
		//-----------------------------------------------------------------------------------------------
	case 0xE000:
		switch (m_opcode & 0x00FF)
		{
		case 0x009E: // Skip next instruction if key with the value of Vx is pressed.
			if (m_key[m_V[(m_opcode & 0x0F00) >> 8]] != 0)
				m_pc += 4;
			else
				m_pc += 2;
			break;
		case 0x00A1: // Skip next instruction if key with the value of Vx is not pressed.
			if (m_key[m_V[(m_opcode & 0x0F00) >> 8]] == 0)
				m_pc += 4;
			else
				m_pc += 2;
			break;
		default:
			printf("Unknown opcode: 0x%X\n", m_opcode);
			break; 
		}
		break;
		//-----------------------------------------------------------------------------------------------
	case 0xF000:
		switch (m_opcode & 0x00FF)
		{
		case 0x0007: //Set Vx = delay timer value.
			m_V[(m_opcode & 0x0F00) >> 8] = m_delay_timer;
			m_pc += 2;
			break;
		case 0x000A: // Wait for a key press, store the value of the key in Vx.
		{
			bool keyPress = false;

			for (int i = 0; i < 16; ++i)
				if (m_key[i] != 0)
				{
					m_V[(m_opcode & 0x0F00) >> 8] = (unsigned char)i;
					keyPress = true;
				}

			if (!keyPress) // If no keypress exit, auto try again
				return;
			m_pc += 2;
		}
			break;
		case 0x0015: // Set delay timer = Vx.
			m_delay_timer = m_V[(m_opcode & 0x0F00) >> 8];
			m_pc += 2;
			break;
		case 0x0018: // Set sound timer = Vx.
			m_sound_timer = m_V[(m_opcode & 0x0F00) >> 8];
			m_pc += 2;
			break;
		case 0x001E: // Set I = I + Vx.
			if ((m_I + m_V[(m_opcode & 0x0F00) >> 8]) > 0xFFF)	// VF is set to 1 when range overflow (I+VX>0xFFF), and 0 when there isn't.
				m_V[0xF] = 1;
			else
				m_V[0xF] = 0;
			m_I += m_V[(m_opcode & 0x0F00) >> 8];
			m_pc += 2;
			break;
		case 0x0029: // Set I = location of sprite for digit Vx.
			m_I = m_V[(m_opcode & 0x0F00) >> 8] * 0x5;
			m_pc += 2;
			break;
		case 0x0033: // Store BCD representation of Vx in memory locations I, I+1, and I+2.
		{
			unsigned char decimalV = m_V[(m_opcode & 0x0F00) >> 8];
			m_memory[m_I] = decimalV / 100;
			m_memory[m_I + 1] = (decimalV / 10) % 10;
			m_memory[m_I + 2] = (decimalV % 100) % 10;
			m_pc += 2;
			break;
		}
		case 0x0055: // Store registers V0 through Vx in memory starting at location I.
			for (int i = 0; i <= ((m_opcode & 0x0F00) >> 8); ++i)
				m_memory[m_I + i] = m_V[i];
			m_I += ((m_opcode & 0x0F00) >> 8) + 1;
			m_pc += 2;
			break;
		case 0x0065: // Read registers V0 through Vx from memory starting at location I.
			for (int i = 0; i <= ((m_opcode & 0x0F00) >> 8); ++i)
				m_V[i] = m_memory[m_I + i];
			m_I += ((m_opcode & 0x0F00) >> 8) + 1;
			m_pc += 2;
			break;
		default:
			printf("Unknown opcode: 0x%X\n", m_opcode);
			break;
		}
		break;
		//-----------------------------------------------------------------------------------------------
	default:
		printf("Unknown opcode: 0x%X\n", m_opcode);
		//m_pc += 2;
		break;
	}

	//printf("%X\n", m_opcode);
}

void Chip8Cpu::PrintMemoryToConsole()
{
	system("CLS");
	printf("Current opcode: 0x%X\n", m_opcode);
	printf("Address register: 0x%X\n", m_I);
	printf("Program counter: 0x%X\n", m_pc);

	printf("Sound timer: 0x%X\n", m_sound_timer);
	printf("Delay timer: 0x%X\n", m_delay_timer);

	for (int i = 0; i < 16; i++)
		printf("GP reg: 0x%X\n", m_V[i]);
}