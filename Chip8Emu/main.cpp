// Dupont An Phu (c)2014

// Chip 8 Emulator tutorial :
// http://www.multigesture.net/articles/how-to-write-an-emulator-chip-8-interpreter/

// SDL tutorial :
// http://lazyfoo.net/tutorials/SDL/

//Using SDL and standard IO 
#include <SDL.h> 
#include <stdio.h> 
#include <string>
#include <fstream>

#include <crtdbg.h>

#include "Chip8Cpu.h"

#pragma region SDLwindow

//Screen dimension constants 
const int SCREEN_WIDTH = 640; 
const int SCREEN_HEIGHT = 320;

//The window we'll be rendering to 
SDL_Window* gWindow = NULL; 

// Setup renderer
SDL_Renderer* gRenderer = NULL;

// Event struct
SDL_Event event;

bool initSDL()
{
	//Initialize SDL 
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		printf("SDL could not initialize! SDL_Error: %s\n", SDL_GetError());
	}
	else
	{
		//Create window 
		gWindow = SDL_CreateWindow("Chip 8 Emu", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
		if (gWindow == NULL) 
		{ 
			printf("Window could not be created! SDL_Error: %s\n", SDL_GetError()); 
			return 0; 
		}
		else 
		{ 
			//Get window surface 
			gRenderer = SDL_CreateRenderer(gWindow, 0, SDL_RENDERER_ACCELERATED);
			return 1;
		}
	}

	return 0;
}

void closeSDL()
{
	//Destroy window 
	SDL_DestroyWindow(gWindow);

	//Quit SDL subsystems 
	SDL_Quit();
}

#pragma endregion

Chip8Cpu chip8;

void CheckInput()
{
	if (event.type == SDL_KEYDOWN)
		switch (event.key.keysym.sym)
	{
		case SDLK_1: chip8.SetKey(1, 1);
			break;
		case SDLK_2: chip8.SetKey(2, 1);
			break;
		case SDLK_3: chip8.SetKey(3, 1);
			break;
		case SDLK_4: chip8.SetKey(12, 1);
			break;
		case SDLK_q: chip8.SetKey(4, 1);
			break;
		case SDLK_w: chip8.SetKey(5, 1);
			break;
		case SDLK_e: chip8.SetKey(6, 1);
			break;
		case SDLK_r: chip8.SetKey(13, 1);
			break;
		case SDLK_a: chip8.SetKey(7, 1);
			break;
		case SDLK_s: chip8.SetKey(8, 1);
			break;
		case SDLK_d: chip8.SetKey(9, 1);
			break;
		case SDLK_f: chip8.SetKey(14, 1);
			break;
		case SDLK_z: chip8.SetKey(10, 1);
			break;
		case SDLK_x: chip8.SetKey(0, 1);
			break;
		case SDLK_c: chip8.SetKey(11, 1);
			break;
		case SDLK_v: chip8.SetKey(15, 1);
			break;
	}
	else if (event.type == SDL_KEYUP)
		switch (event.key.keysym.sym)
	{
		case SDLK_1: chip8.SetKey(1, 0);
			break;
		case SDLK_2: chip8.SetKey(2, 0);
			break;
		case SDLK_3: chip8.SetKey(3, 0);
			break;
		case SDLK_4: chip8.SetKey(12, 0);
			break;
		case SDLK_q: chip8.SetKey(4, 0);
			break;
		case SDLK_w: chip8.SetKey(5, 0);
			break;
		case SDLK_e: chip8.SetKey(6, 0);
			break;
		case SDLK_r: chip8.SetKey(13, 0);
			break;
		case SDLK_a: chip8.SetKey(7, 0);
			break;
		case SDLK_s: chip8.SetKey(8, 0);
			break;
		case SDLK_d: chip8.SetKey(9, 0);
			break;
		case SDLK_f: chip8.SetKey(14, 0);
			break;
		case SDLK_z: chip8.SetKey(10, 0);
			break;
		case SDLK_x: chip8.SetKey(0, 0);
			break;
		case SDLK_c: chip8.SetKey(11, 0);
			break;
		case SDLK_v: chip8.SetKey(15, 0);
	}
}

void RunGame(std::string * programPtr)
{
	// Initalize cpu
	chip8.Initialize();
	chip8.LoadGame(programPtr);

	SDL_Rect pixelRect;
	pixelRect.w = SCREEN_WIDTH / chip8.m_ScrW;
	pixelRect.h = SCREEN_HEIGHT / chip8.m_ScrH;

	for (;;)
	{
		//chip8.PrintMemoryToConsole(); // Very CPU intensive
		chip8.EmulateCycle();

		if (chip8.DrawFlag())
		{
			SDL_SetRenderDrawColor(gRenderer, 0, 0, 0, 255);
			SDL_RenderClear(gRenderer);
			SDL_SetRenderDrawColor(gRenderer, 255, 255, 255, 255);

			for (int v = 0; v < chip8.m_ScrH; v++)
			{
				for (int h = 0; h < chip8.m_ScrW; h++)
				{
					if (chip8.GetDisplayBuffer()[h + chip8.m_ScrW * v] != 0)
					{
						pixelRect.x = h * pixelRect.w;
						pixelRect.y = v * pixelRect.h;
						SDL_RenderFillRect(gRenderer, &pixelRect); 
					}
				}
			}
			
			SDL_RenderPresent(gRenderer);
			chip8.DrawComplete();
		}

		while (SDL_PollEvent(&event)) {
			if (event.type == SDL_QUIT) return;
			else CheckInput();
		}

		SDL_Delay(2);
	}
}

int main(int argc, char* argv[])
{
	#if defined(DEBUG) | defined(_DEBUG)
		_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);

		//_CrtSetBreakAlloc(172);
	#endif

	if (argc <= 1) 
	{
		printf("Please specify a program to run.");
		return -1;
	}

	if (!initSDL()) 
		return -1;

	// TODO initialize input ?
	
	// Open program file
	std::ifstream ifs(argv[1], std::ios::binary | std::ios::in); //argv[1]
	std::string program((	std::istreambuf_iterator<char>(ifs)),
							std::istreambuf_iterator<char>());

	RunGame(&program);
	closeSDL();

	return 0;
}