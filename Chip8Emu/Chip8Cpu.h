// Dupont An Phu (c)2014

// Chip 8 Technical Reference
// http://devernay.free.fr/hacks/chip8/C8TECH10.HTM#3.1

#include <string>
#include <stdio.h>

#pragma once
class Chip8Cpu
{
public:
	Chip8Cpu();
	~Chip8Cpu();

	void Initialize();
	void LoadGame(std::string * programPtr);

	void EmulateCycle();
	void SetKey(int key, char value) { m_key[key] = value; }

	bool DrawFlag() { return m_DrawFlag; }
	void DrawComplete() { m_DrawFlag = false; }

	unsigned char * GetDisplayBuffer() { return m_gfx; }

	void PrintMemoryToConsole();

	static const unsigned short m_ScrW = 64,	// Screen width
								m_ScrH = 32;	// Screen height

private:
	// Functions
	void ExecuteCurrent();

	// Datamembers
	static unsigned char		m_Chip8_fontset[80];

	unsigned short	m_opcode,					// Current opcode (chip8 opcode = 2 bytes)
					m_I,						// Index / Address register
					m_pc,						// Program counter
					m_stack[16],				// Stack (similar to pc stack, used for subroutines)
					m_sp;						// Stackpointer
					
	unsigned char	m_memory[4096],				// Chip 8 has 4KB memory total
					m_V[16],					// 15 8b GP-registers and carry flag
					m_gfx[m_ScrW * m_ScrH],		// Display buffer
					m_delay_timer,				// Interrupts and 
					m_sound_timer,				// hardware register (counts down to zero) 
					m_key[16];					// HEX-based keypad, store key values

	bool m_DrawFlag;

	/*
	0x000-0x1FF - Chip 8 interpreter (contains font set in emu)
	0x050-0x0A0 - Used for the built in 4x5 pixel font set (0-F)
	0x200-0xFFF - Program ROM and work RAM
	*/

};

