#include "InputManager.h"

std::map<int, KeyData> InputManager::m_KeyMap;
SDL_Event * InputManager::m_Event = nullptr;

void InputManager::Update()
{
	for (std::map<int, KeyData>::iterator it = m_KeyMap.begin(); it != m_KeyMap.end(); ++it)
	{
		if (it->second.Active && it->second.Event == SDL_KEYDOWN)
			it->second.Active = false;
		else if (it->second.Active && it->second.Event == SDL_KEYUP)
			it->second.Active = false;
	}
}

void InputManager::DoEvent()
{
	if (m_Event != nullptr)
	{
		for (std::map<int, KeyData>::iterator it = m_KeyMap.begin(); it != m_KeyMap.end(); ++it)
		{
			if (m_Event->type == SDL_KEYDOWN)
			{
				if (m_Event->key.keysym.scancode == it->second.KeyCode && (m_Event->type == it->second.Event || it->second.Event == SDL_LASTEVENT))
					it->second.Active = true;
			}
			else if (m_Event->type == SDL_KEYUP)
			{
				if (m_Event->key.keysym.scancode == it->second.KeyCode)
					if (m_Event->type == it->second.Event)
						it->second.Active = true;
					else if (it->second.Event == SDL_LASTEVENT)
						it->second.Active = false;
			}
		}
	}
}

bool InputManager::IsKeyDown(int Identifier)
{
	if (m_KeyMap.find(Identifier) == m_KeyMap.end())
		return false;
	return m_KeyMap[Identifier].Active;
}

void InputManager::AddInput(int Id, KeyState state, SDL_Keycode keyCode)
{
	if (m_KeyMap.find(Id) == m_KeyMap.end())
	{
		KeyData d;
		d.Active = false;
		d.KeyCode = keyCode;
		switch (state)
		{
		case Down: d.Event = SDL_KEYDOWN; break;
		case Released: d.Event = SDL_KEYUP;	break;
		case Pressed: d.Event = SDL_EventType::SDL_LASTEVENT; break; // There is no pressed event :(
		}
		m_KeyMap[Id] = d;
	}
}