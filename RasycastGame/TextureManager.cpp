#include "TextureManager.h"

TextureManager::TextureManager(SDL_Renderer * renderer) :
m_LoadingSurface(nullptr),
m_LastTex(nullptr),
m_LastID(-1),
m_Renderer(renderer)
{

}

TextureManager::~TextureManager()
{
	for (std::map<int, SDL_Texture *>::iterator it = m_Textures.begin(); it != m_Textures.end(); it++)
	{
		SDL_DestroyTexture(it->second);
	}
}

SDL_Texture * TextureManager::LoadTexture(int iD, const std::string & src)
{
	if (m_Textures.find(iD) == m_Textures.end())
	{
		m_LoadingSurface = SDL_LoadBMP(src.c_str());
		SDL_Texture * tex = SDL_CreateTextureFromSurface(m_Renderer, m_LoadingSurface);
		m_Textures.insert(std::pair<int, SDL_Texture *>(iD, tex));
		SDL_FreeSurface(m_LoadingSurface);
	}

	return m_Textures[iD];
}

SDL_Texture * TextureManager::GetTexture(int ID)
{
	if (ID == m_LastID) return m_LastTex;

	if (m_Textures.find(ID) != m_Textures.end()) 
	{
		m_LastID = ID;
		m_LastTex = m_Textures[ID];
		return m_LastTex;
	}
	return nullptr;
}