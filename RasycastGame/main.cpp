// Dupont An Phu (c)2014

// SDL tutorial :
// http://lazyfoo.net/tutorials/SDL/

//Using SDL and standard IO 
//#include <SDL.h> 
#include <stdio.h> 
#include <crtdbg.h>
#include "RaycastGame.h"
#include "InputManager.h"
#include <iostream>

#pragma region SDLwindow

////Screen dimension constants 
//const int SCREEN_WIDTH = 640; 
//const int SCREEN_HEIGHT = 480;

//The window we'll be rendering to 
SDL_Window* gWindow = NULL; 

// Setup renderer
SDL_Renderer* gRenderer = NULL;

// Event struct
SDL_Event sdl_event;

bool initSDL()
{
	//Initialize SDL 
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
		printf("SDL could not initialize! SDL_Error: %s\n", SDL_GetError());
	else
	{
		//Create window 
		gWindow = SDL_CreateWindow("Raycast Game", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
		if (gWindow == NULL) 
		{ 
			printf("Window could not be created! SDL_Error: %s\n", SDL_GetError()); 
			return 0; 
		}
		else 
		{ 
			//Get window surface 
			gRenderer = SDL_CreateRenderer(gWindow, 0, SDL_RENDERER_ACCELERATED);
			return 1;
		}
	}

	return 0;
}

void closeSDL()
{
	//Destroy window 
	SDL_DestroyWindow(gWindow);

	//Quit SDL subsystems 
	SDL_Quit();
}

#pragma endregion

void DoGame()
{
	Uint32 oldTime = 0, deltaTime = 0;
	RaycastGame * game = new RaycastGame(gWindow, gRenderer, &sdl_event);
	InputManager::SetEventTracker(&sdl_event);

	game->Initialize();

	int frames = 0;
	float swecond = 0;
	int delay = 0;
	bool foundFrameZone = false;

	// Game Loop
	for (;;)
	{
		Uint32 newTime = SDL_GetTicks();
		if (newTime > oldTime)
		{
			deltaTime = newTime - oldTime;
			oldTime = newTime;
		}

		if (swecond > 1000)
		{
			if (!foundFrameZone)
			{	
				if (frames < 65 && frames > 55) foundFrameZone = true;
				else
				{
					if (frames > 60) ++delay;
					else if (delay > 0) --delay;
					else foundFrameZone = true;
				}
			}
			std::cout << frames << std::endl;
			frames = 0;
			swecond = 0;
		}

		++frames;
		swecond += deltaTime;

#ifndef DEBUG_MODE
		game->Update(deltaTime / 1000.f);
#endif // DEBUG_MODE

		SDL_SetRenderDrawColor(gRenderer, 0, 0, 0, 255);
		SDL_RenderClear(gRenderer);
		game->Paint();
		
#ifdef DEBUG_MODE
		game->Update(deltaTime / 1000.f);
#endif // DEBUG_MODE

		SDL_RenderPresent(gRenderer);

		InputManager::Update();
		while (SDL_PollEvent(&sdl_event)) {
			if (sdl_event.type == SDL_QUIT) { delete game; return; }
			else InputManager::DoEvent();
		}

		SDL_Delay(delay); 
	}
}

int main(int argc, char* argv[])
{
	#if defined(DEBUG) | defined(_DEBUG)
		_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);

		//_CrtSetBreakAlloc(172);
	#endif

	if (!initSDL()) 
		return -1;

	UNREFERENCED_PARAMETER(argc);
	UNREFERENCED_PARAMETER(argv);

	DoGame();
	closeSDL();

	return 0;
}