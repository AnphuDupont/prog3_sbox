// "RaycastGame.cpp" by
// Dupont An Phu (c) 2015

#include "RaycastGame.h"
#include <iostream>

#define DEBUG_MODE

const float RaycastGame::Xscale = SCREEN_WIDTH / Map::Width;
const float RaycastGame::Yscale = SCREEN_HEIGHT / Map::Height;

RaycastGame::RaycastGame(SDL_Window * window, SDL_Renderer * render, SDL_Event * ev) :
	m_TexManager(nullptr),
	m_MoveSpeed(2.f),
	m_TurnSpeed(1.f),
	m_Disp(0),
	m_Rot(0),
	m_RayCount(400),
	m_ColumnWidth(2),
	m_FarClip(400),
	m_Height(20),
	m_ViewDist(0.02f)
{
	SDL_Controller::m_Event = ev;
	SDL_Controller::m_Renderer = render;
	SDL_Controller::m_Window = window;
	m_TexManager = new TextureManager(render);
}

RaycastGame::~RaycastGame()
{
	delete m_TexManager;
}

void RaycastGame::Initialize()
{
	m_TexManager->LoadTexture(0, "./Resources/test.bmp");
	m_TexManager->LoadTexture(1, "./Resources/bricks2.bmp");
	m_TexManager->LoadTexture(2, "./Resources/door.bmp");
	m_TexManager->LoadTexture(3, "./Resources/bricks3.bmp");

	m_Map.TileColor[0] = Color::Basic::White;
	m_Map.TileColor[1] = Color::Basic::Red;	
	m_Map.TileColor[2] = Color::Basic::Yellow;
	m_Map.TileColor[3] = Color::Basic::Blue;
	m_Map.TileColor[4] = Color::Basic::Green;

	m_Player.Position = Vector2(4, 5);
	m_Player.Rotation = 0;

	gameScale = Vector2(SCREEN_WIDTH / m_Map.Width, SCREEN_HEIGHT / m_Map.Height);

	m_Columns.resize(m_RayCount);
	for (size_t i = 0; i < m_Columns.size(); i++)
	{
		//m_Columns.at(i).pos.Y = SCREEN_HEIGHT / 2.f;
		m_Columns.at(i).src.h = 256; // HARDCODED
		m_Columns.at(i).src.w = (int)m_ColumnWidth;
		m_Columns.at(i).src.y = 0;
	}
	std::cout << "TODO set texture height dynamic" << std::endl;

	InputManager::AddInput(InputID::UP, KeyState::Pressed, SDL_SCANCODE_UP);
	InputManager::AddInput(InputID::DOWN, KeyState::Pressed, SDL_SCANCODE_DOWN);
	InputManager::AddInput(InputID::LEFT, KeyState::Pressed, SDL_SCANCODE_LEFT);
	InputManager::AddInput(InputID::RIGHT, KeyState::Pressed, SDL_SCANCODE_RIGHT);
	InputManager::AddInput(InputID::RESET, KeyState::Pressed, SDL_SCANCODE_KP_ENTER);
}

void RaycastGame::Update(float deltaTime)
{
	UNREFERENCED_PARAMETER(deltaTime);
	
	bool upPressed = InputManager::IsKeyDown(InputID::UP);
	if (upPressed || InputManager::IsKeyDown(InputID::DOWN)) m_Disp = upPressed ? 1 : -1;
	else m_Disp = 0;

	bool rightPressed = InputManager::IsKeyDown(InputID::RIGHT);
	if ( rightPressed || InputManager::IsKeyDown(InputID::LEFT)) m_Rot = rightPressed ? 1 : -1;
	else m_Rot = 0;

	if (InputManager::IsKeyDown(InputID::RESET)) m_Player.Position = Vector2(4, 5);

	if (!IsBlocking())
	{
		if (m_Rot != 0)
		{
			m_Player.Rotation += m_Rot * m_TurnSpeed * deltaTime;
			//if (m_Player.Rotation > (float)PI_2) m_Player.Rotation -= (float)PI_2;
			//else if (m_Player.Rotation < 0) m_Player.Rotation += (float)PI_2;
			m_Rot = 0;
		}

		if (m_Disp != 0)
		{
			Vector2 disp(SDL_cosf(m_Player.Rotation), SDL_sinf(m_Player.Rotation));
			m_Player.Position += disp * m_MoveSpeed * deltaTime * (float)m_Disp;
			m_Disp = 0;
		}
	}
	else
	{
		Vector2 disp(SDL_sinf(m_Player.Rotation), SDL_cosf(m_Player.Rotation));
		m_Player.Position -= disp * m_MoveSpeed * deltaTime;
	}

	DoRaycast();
}

void RaycastGame::Paint()
{
#ifdef DEBUG_MODE
	int lastId = -1;
	SDL_Rect r;
	r.w = (int)gameScale.X;
	r.h = (int)gameScale.Y;
	for (int i = 0; i < m_Map.Height; i++)
		for (int j = 0; j < m_Map.Width; j++)
		{
		r.x = j * r.w;
		r.y = i * r.h;

		if (lastId != m_Map.Data[i][j])
		{
			lastId = m_Map.Data[i][j];
			SDL_Controller::SetDrawColor(m_Map.TileColor[lastId]);
		}

		SDL_RenderFillRect(SDL_Controller::m_Renderer, &r);
		}

	SDL_Controller::SetDrawColor(Color::Basic::Red);
	SDL_Rect playerRect;
	playerRect.x = (int)(m_Player.Position.X * r.w);
	playerRect.y = (int)(m_Player.Position.Y * r.h);
	playerRect.w = playerRect.h = 10;
	SDL_RenderFillRect(SDL_Controller::m_Renderer, &playerRect);
#endif
#ifndef DEBUG_MODE
	SDL_Controller::SetDrawColor(Color::Basic::Gray);
	SDL_Controller::FillRect(Vector2(0, 0), (float)SCREEN_WIDTH, (float)SCREEN_HEIGHT);

	//Color::Basic lastColor = Color::Basic::Black;

	//for (size_t i = 0; i < m_Columns.size(); i++)
	//{
	//	SDL_Controller::SetAlpha(m_Columns.at(i).Alpha);
	//	if (lastColor != m_Columns.at(i).col)
	//	{
	//		lastColor = m_Columns.at(i).col;
	//		//SDL_Controller::SetDrawColorShaded(lastColor, m_Columns.at(i).Alpha / 255.f);
	//		SDL_Controller::SetDrawColor(lastColor);
	//	}
	//	SDL_Controller::FillRectCentered(m_Columns.at(i).pos, m_Columns.at(i).Width, m_Columns.at(i).Height);
	//}

	for (size_t i = 0; i < m_Columns.size(); i++)
	{
		//SDL_Controller::FillRectCentered(m_Columns.at(i).pos, m_Columns.at(i).Width, m_Columns.at(i).Height);
		RenderTextureColumn(m_Columns.at(i));
	}

#endif // !DEBUG_MODE
}

// Functions
bool RaycastGame::IsBlocking() const
{
	// Change to AABB
	if (m_Player.Position.Y < 0 || m_Player.Position.Y >= m_Map.Height || 
		m_Player.Position.X < 0 || m_Player.Position.X >= m_Map.Width) {
		return true;
	}
	return (m_Map.Data[(int)floorf(m_Player.Position.Y)][(int)floorf(m_Player.Position.X)] != 0);
}

void RaycastGame::DoRaycast()
{
	for (int i = 0; i < m_RayCount; i++)
	{
		float screenPos = (-m_RayCount / 2.f + i) * m_ColumnWidth;
		float rayViewDist = sqrtf(screenPos * screenPos + m_FarClip * m_FarClip);
		float angle = asinf(screenPos / rayViewDist);
		CastRay(m_Player.Rotation + angle, i);
	}
}

void RaycastGame::CastRay(float angle, int index)
{
	while (angle > (float)PI_2) angle -= (float)PI_2;
	while (angle < 0) angle += (float)PI_2;

	// Determine quadrant direction ray
	bool right = (angle >(PI_2 * 0.75) || angle < (PI_2 * .25));
	bool up = (angle < 0 || angle > M_PI);
	
	float angleSin = sinf(angle);
	float angleCos = cosf(angle);

	float dist = 0;
	Vector2 hitPos, wallPos;
	
#pragma region Vertical Collion
	// Ray direction
	float slope = angleSin / angleCos;
	Vector2 delta(right ? 1.f : -1.f, 0);
	delta.Y = delta.X * slope;

	// Ray start position
	Vector2 rayPos(right ? ceilf(m_Player.Position.X) : floorf(m_Player.Position.X), 0);
	rayPos.Y = m_Player.Position.Y + (rayPos.X - m_Player.Position.X) * slope;

	// Do ray casting
	// First check vertical collision
	// Loop if ray within bounds of map
	while (rayPos.X > 0 && rayPos.X < m_Map.Width && rayPos.Y > 0 && rayPos.Y < m_Map.Height)
	{
		int wallX = (int)floorf(rayPos.X + (right ? 0 : -1.f));
		int wallY = (int)floorf(rayPos.Y);

		// Check if ray is not inside wall
		if (m_Map.Data[wallY][wallX] > 0)
		{
			// If it is inside wall
			// Calculate distance to hitpoint
			Vector2 distVec = rayPos - m_Player.Position;
			distVec *= distVec;
			dist = distVec.X + distVec.Y;

			m_Columns.at(index).col = m_Map.Data[wallY][wallX];

			// Store hit location
			hitPos = rayPos;

			m_Columns.at(index).src.x = (int)((hitPos.Y - floorf(hitPos.Y)) * m_Columns.at(index).texWidth);

			// Exit while loop
			break;
		}

		rayPos += delta;
	}
#pragma endregion
#pragma region Horizontal Collision
	// Ray direction
	slope = angleCos / angleSin;
	delta.Y = up ? -1.f : 1.f;
	delta.X = delta.Y * slope;

	// Ray start position
	rayPos.Y = up ? floorf(m_Player.Position.Y) : ceilf(m_Player.Position.Y);
	rayPos.X = m_Player.Position.X + (rayPos.Y - m_Player.Position.Y) * slope;

	// Do ray casting
	// Loop if ray within bounds of map
	while (rayPos.X > 0 && rayPos.X < m_Map.Width && rayPos.Y > 0 && rayPos.Y < m_Map.Height)
	{
		int wallX = (int)floorf(rayPos.X);
		int wallY = (int)floorf(rayPos.Y + (up ? -1.f : 0));

		// Check if ray is not inside wall
		if (m_Map.Data[wallY][wallX] > 0)
		{
			// If it is inside wall
			// Calculate distance to hitpoint
			Vector2 distVec = rayPos - m_Player.Position;
			distVec *= distVec;
			float tempDist = distVec.X + distVec.Y;

			if (!dist || tempDist < dist)
			{
				dist = tempDist;

				m_Columns.at(index).col = m_Map.Data[wallY][wallX];

				// Store hit location
				hitPos = rayPos; 

				// X
				m_Columns.at(index).src.x = (int)((hitPos.X - floorf(hitPos.X)) * m_Columns.at(index).texWidth);
			}

			// Exit while loop
			break;
		}

		rayPos += delta;
	}
#pragma endregion

#ifdef DEBUG_MODE
	// Ray drawing (! test only !)
	SDL_Controller::SetDrawColor(Color::Basic::Gray);
	if (dist)
		SDL_Controller::DrawLine(m_Player.Position * gameScale, hitPos * gameScale);
#endif // DEBUG_MODE
#ifndef DEBUG_MODE 
	if (dist)
	{
		dist = sqrtf(dist);

		// Fisheye remove
		dist = dist * cosf(m_Player.Rotation - angle);
		
		//m_Columns.at(index).Width = m_ColumnWidth;
		//m_Columns.at(index).Height = m_Height * m_Height / dist;
		//m_Columns.at(index).pos.X = (float)(SCREEN_WIDTH / m_RayCount * index);
		m_Columns.at(index).dest.w = (int)m_ColumnWidth;
		m_Columns.at(index).dest.h = (int)(m_Height * m_Height / dist);
		m_Columns.at(index).dest.x = (int)(SCREEN_WIDTH / m_RayCount * index);
		m_Columns.at(index).dest.y = (int)(SCREEN_WIDTH / 2 - m_Columns.at(index).dest.h / 2) - 64;
	}
#endif
}


void RaycastGame::RenderTextureColumn(TexturedColumn & tc)
{
	//SDL_Rect src, dst;
	//src.x = src.y = 0;
	//src.w = srcWidth;
	//src.h = 256;			// HARDCODED
	//dst.x = (int)dstPos.X;
	//dst.y = (int)dstPos.Y;
	//dst.w = dstW; dst.h = dstH;
	SDL_RenderCopy(SDL_Controller::m_Renderer, m_TexManager->GetTexture(tc.col), &tc.src, &tc.dest);
}