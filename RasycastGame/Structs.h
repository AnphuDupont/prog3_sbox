// "Structs.h" by
// Dupont An Phu (c) 2015

#include "SDL.h"
#pragma once

//#define DEBUG_MODE
#define PI_2 (M_PI * 2)
#define byte Uint8
#define UNREFERENCED_PARAMETER(p) p

struct Vector2
{
	Vector2() : X(0), Y(0) { }
	Vector2(const float x, const float y) : X(x), Y(y) { }
	Vector2(const Vector2 & vec) { X = vec.X; Y = vec.Y; }

	float X, Y;

	void operator+=(const Vector2 & other) { X += other.X; Y += other.Y; }
	void operator-=(const Vector2 & other) { X -= other.X; Y -= other.Y; }
	void operator*=(const Vector2 & other) { X *= other.X; Y *= other.Y; }
	void operator*=(const float mul) { X *= mul; Y *= mul; }
	void operator*=(const int mul) { X *= mul; Y *= mul; }
	void operator/=(const float mul) { X /= mul; Y /= mul; }
	void operator/=(const int mul) { X /= mul; Y /= mul; }
	void operator=(const Vector2 & other) { X = other.X; Y = other.Y; }

	bool operator==(const Vector2 & other) const { return (other.X == X && other.Y == Y); }
	bool operator!=(const Vector2 & other) const { return (other.X != X || other.Y != Y); }

	Vector2 operator-(const Vector2 & other) const { return Vector2(X - other.X, Y - other.Y); }
	Vector2 operator*(const Vector2 & other) const { return Vector2(X * other.X, Y * other.Y); }

	Vector2 operator*(const float mul) const { return Vector2(X * mul, Y * mul); }
	Vector2 operator*(const int mul) const { return Vector2(X * mul, Y * mul); }	
	Vector2 operator/(const float mul) const { return Vector2(X / mul, Y / mul); }
	Vector2 operator/(const int mul) const { return Vector2(X / mul, Y / mul); }
};

struct Player
{
	Player(Vector2 pos, float rot) : Rotation(rot), Position(pos) {}
	Player() : Rotation(0), Position(0, 0) {}

	Vector2 Position;
	float Rotation;
};

struct Color
{
	Color() : R(0), G(0), B(0) {}
	Color(byte r, byte g, byte b) : R(r), G(g), B(b) {}
	byte R, G, B;
	enum Basic
	{
		Black, White, Red, Green, Blue, Yellow, Cyan, Magenta, Silver, Gray
	};
};

struct Map
{
	Map() { }
	static const int Width = 8, Height = 8;
	static const int Data[Width][Height];
	Color::Basic TileColor[5];
};

struct SDL_Controller
{
public:
	static void SetDrawColor(Color c) { SDL_SetRenderDrawColor(m_Renderer, c.R, c.G, c.B, m_Alpha); }
	static void SetDrawColor(Color::Basic b);
	static void SetDrawColorShaded(Color::Basic b, float shading);
	static void DrawLine(Vector2 & start, Vector2 & end);
	static void FillRectCentered(Vector2 pos, float width, float height);
	static void FillRect(Vector2 pos, float width, float height);
	static void SetAlpha(byte a) { m_Alpha = a; }

	static SDL_Window * m_Window;
	static SDL_Renderer * m_Renderer;
	static SDL_Event * m_Event;

private:
	static Color Standard[10];
	static byte m_Alpha;
};

struct Column
{
	Color::Basic col;
	Vector2 pos;
	float Width = 0, Height = 0;
	byte Alpha;
};

struct TexturedColumn
{
	SDL_Rect src;
	SDL_Rect dest;
	//SDL_Texture * texture;
	int col; // Texture ID
	int texWidth = 256;
};