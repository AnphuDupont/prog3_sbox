#pragma once
#include <map>
#include <sdl.h>

enum KeyState
{ Down, Released, Pressed };

struct KeyData
{
	SDL_Keycode KeyCode = 0;
	SDL_EventType Event;
	bool Active;
};

class InputManager
{
public:
	InputManager() {}
	~InputManager() {}

	static void AddInput(int Id, KeyState state, SDL_Keycode keyCode);
	static void SetEventTracker(SDL_Event * e) { m_Event = e; }
	static void Update();
	static void DoEvent();
	static bool IsKeyDown(int Identifier);
private:
	static std::map<int, KeyData> m_KeyMap;
	static SDL_Event * m_Event;
};

