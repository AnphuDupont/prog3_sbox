// "RaycastGame.h" by
// Dupont An Phu (c) 2015

#pragma once
#include <SDL.h>
#include <vector>
#include "Structs.h"
#include "InputManager.h"
#include "TextureManager.h"

//Screen dimension constants 
const int SCREEN_WIDTH = 800;
const int SCREEN_HEIGHT = 600;

class RaycastGame
{
public:
	RaycastGame(SDL_Window * window, SDL_Renderer * render, SDL_Event * ev);
	~RaycastGame();

	void Initialize();
	void Update(float deltaTime);
	void Paint();

private:
	// Functions
	bool IsBlocking() const;
	void DoRaycast();
	void CastRay(float angle, int index);
	void RenderTextureColumn(TexturedColumn & tc);

	// Variables
	TextureManager * m_TexManager;

	Player m_Player;
	Map m_Map;

	Vector2 gameScale;

	static const float Xscale, Yscale;

	float m_MoveSpeed;
	float m_TurnSpeed;
	float m_ColumnWidth;

	int m_Disp;
	int m_Rot;
	int m_RayCount;
	int m_FarClip;
	int m_Height;
	float m_ViewDist;

	//std::vector<Column> m_Columns;
	std::vector<TexturedColumn> m_Columns;

	enum InputID
	{
		UP, DOWN, LEFT, RIGHT, RESET
	};
};