// "Structs.cpp" by
// Dupont An Phu (c) 2015

#include "Structs.h"

byte SDL_Controller::m_Alpha = 255;
SDL_Window * SDL_Controller::m_Window = nullptr;
SDL_Renderer * SDL_Controller::m_Renderer = nullptr;
SDL_Event * SDL_Controller::m_Event = nullptr;
Color SDL_Controller::Standard[10] =
{
	Color(0, 0, 0),
	Color(255, 255, 255),
	Color(255, 0, 0),
	Color(0, 255, 0),
	Color(0, 0, 255),
	Color(255, 255, 0),
	Color(0, 255, 255),
	Color(255, 0, 255),
	Color(192, 192, 192),
	Color(128, 128, 128)
};

void SDL_Controller::SetDrawColor(Color::Basic b)
{
	SDL_SetRenderDrawColor
		(
		m_Renderer,
		Standard[(int)b].R,
		Standard[(int)b].G,
		Standard[(int)b].B,
		m_Alpha
		);
}

void SDL_Controller::SetDrawColorShaded(Color::Basic b, float shading)
{
	SDL_SetRenderDrawColor
		(
		m_Renderer,
		(byte)(Standard[(int)b].R * shading),
		(byte)(Standard[(int)b].G * shading),
		(byte)(Standard[(int)b].B * shading),
		m_Alpha
		);
}

void SDL_Controller::DrawLine(Vector2 & start, Vector2 & end)
{
	SDL_RenderDrawLine(m_Renderer, (int)start.X, (int)start.Y, (int)end.X, (int)end.Y);
}
void SDL_Controller::FillRectCentered(Vector2 pos, float width, float height)
{
	SDL_Rect r;
	r.x = (int)(pos.X - width / 2);
	r.y = (int)(pos.Y - height / 2);
	r.w = (int)width; r.h = (int)height;
	SDL_RenderFillRect(m_Renderer, &r);
}

void SDL_Controller::FillRect(Vector2 pos, float width, float height)
{
	SDL_Rect r;
	r.x = (int)pos.X; r.y = (int)pos.Y;
	r.w = (int)width; r.h = (int)height;
	SDL_RenderFillRect(m_Renderer, &r);
}

const int Map::Data[Width][Height] =
{
	{ 1, 1, 3, 3, 2, 1, 1, 1 },
	{ 3, 0, 0, 0, 0, 0, 0, 1 },
	{ 1, 0, 0, 0, 0, 0, 0, 1 },
	{ 1, 0, 1, 1, 3, 1, 0, 3 },
	{ 1, 0, 3, 0, 0, 3, 0, 1 },
	{ 3, 0, 3, 0, 0, 1, 0, 1 },
	{ 1, 0, 0, 0, 0, 0, 0, 1 },
	{ 1, 1, 1, 3, 1, 1, 1, 1 },
};