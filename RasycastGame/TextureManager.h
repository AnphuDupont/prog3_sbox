#pragma once
#include <map>
#include <SDL.h>
#include <string>

class TextureManager
{
public:
	TextureManager(SDL_Renderer * renderer);
	~TextureManager();

	SDL_Texture * LoadTexture(int iD, const std::string & src);
	SDL_Texture * GetTexture(int texID);

private:
	std::map<int, SDL_Texture *> m_Textures;
	SDL_Surface * m_LoadingSurface;
	SDL_Renderer * m_Renderer; // Deleted in main

	// Texture search optimization
	int m_LastID;
	SDL_Texture * m_LastTex;
};

